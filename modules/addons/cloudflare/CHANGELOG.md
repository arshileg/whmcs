# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.3.5](#1.3.5) - 2016-01-06
### Fixed
- Fixed an issue where Default On did not remain activated upon page refresh
