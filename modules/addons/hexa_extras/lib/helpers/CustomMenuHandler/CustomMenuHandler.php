<?php

include_once "MenuDbHandler.php";
use Illuminate\Database\Capsule\Manager as Capsule;

if (!class_exists('CustomMenuHandler')) {
	class CustomMenuHandler
	{
		public $db;
		private $themeprefix;
		private $subaction;
		private $useraffiliate;

		function __construct($options){
			$this->themeprefix = $options['themeprefix'];
			$this->db = new MenuDbHandler(['themeprefix'=>$this->themeprefix]);
			$this->dir = __DIR__;
			$this->web_url = '../modules/addons/' .$this->themeprefix. '/lib/helpers/CustomMenuHandler';

			$this->useraffiliate = null;
		}

		public function route(){
			$subaction = 'list';
			if (isset($_REQUEST['subaction'])){
				$subaction = $_REQUEST['subaction'];
			}

			$this->subaction = $subaction;

			switch ($subaction) {
				case 'create':
					$this->subactionCreate();
					break;
				case 'update':
					$this->subactionUpdate();
					break;
				case 'delete':
					$this->subactionDelete();
					break;					
				case 'disable':
					$this->subactionDisable();
					break;
				case 'enable':
					$this->subactionEnable();
					break;	
				case 'updatemenu':
					$this->subactionUpdateMenu();
					break;					
				default:
					$this->subactionList();
					break;
			}
		}


		public function subactionUpdateMenu(){
			if (isset($_POST['updatemenu'])){
				$this->db->setting_update('show_side_bar_munu',$_POST['updatemenu']);
			}
			$this->redirect('list');
		}

		public function subactionDelete(){
			$menu_item_id = '-1';
			if (isset($_REQUEST['id'])){
				$menu_item_id = $_REQUEST['id'];
			}
			$this->db->delete($menu_item_id);	
			$this->redirect('list');
		}

		public function subactionDisable(){
			$menu_item_id = '0';
			if (isset($_REQUEST['id'])){
				$menu_item_id = $_REQUEST['id'];
			}
			$this->db->disable($menu_item_id);	
			$this->redirect('list');
		}
		public function subactionEnable(){
			$menu_item_id = '0';
			if (isset($_REQUEST['id'])){
				$menu_item_id = $_REQUEST['id'];
			}
			$this->db->enable($menu_item_id);				
			$this->redirect('list');
		}

		public function subactionList(){
			$menu_items = $this->db->getall();

			$menu_items = $this->db->sort_items($menu_items);

			include $this->dir."/views/list.php";
		}

		public function subactionCreate(){
			$item = null;
			if (isset($_REQUEST['save_data'])){
				$item = $this->load_item();	
				$this->db->create($item);
				$this->subactionList();
				$this->redirect('list');

			}

			$item = new stdClass();
			foreach ($this->db->default as $key => $value){
			    $object->$key = $value;
			}

			$grouplist = $this->getgrouplist();

			include $this->dir."/views/create.php";
		}

		public function subactionUpdate(){
			$item = null;
			if (isset($_REQUEST['save_data'])){
				$item = $this->load_item();

				$this->db->update($item);
				$this->subactionList();
				$this->redirect('list');	
			}
			

			$menu_item_id = '0';
			if (isset($_REQUEST['id'])){
				$menu_item_id = $_REQUEST['id'];
			}			
			$item = $this->db->get($menu_item_id);

			$grouplist = $this->getgrouplist();

			include $this->dir."/views/update.php";
		}


		function getdropdown($name,$list,$value,$default){
		    $html  = '<select class="form-control" name="'.$name.'">';
		    foreach ($list as $key => $item) {
		        if (isset($value) ){
		            $html  .= '<option  '.(($value==$key)?'selected="selected"':'').' value="'.$key.'" >' .htmlspecialchars($item, ENT_QUOTES) . '</option>';
		        }else{
		            $html  .= '<option  '.(($default==$key)?'selected="selected"':'').' value="'.$key.'" >' .htmlspecialchars($item, ENT_QUOTES) . '</option>';
		        }
		    }
		    $html  .= '</select>';
		    return $html;
		}

		function load_item(){
			$item = new stdClass();
			foreach ($this->db->field_list as $value){
				if (isset($_REQUEST[$value])){
					$item->$value = $_REQUEST[$value];	
				}else{
					if(isset($this->db->default[$value])){
						$item->$value = $this->db->default[$value];
					}
				}
			}

			if (isset($_REQUEST['show_type'])){
				if($_REQUEST['show_type']==0){
					$item->only_for_guest = 0;
					$item->only_for_user = 0;
					$item->show_for_id = -1;
				}elseif ($_REQUEST['show_type']==1){
					$item->only_for_guest = 1;
					$item->only_for_user = 0;
					$item->show_for_id = -1;
				}elseif ($_REQUEST['show_type']==2){
					$item->only_for_guest = 0;
					$item->only_for_user = 1;
					$item->show_for_id = -1;
				}elseif ($_REQUEST['show_type']==3){
					$item->only_for_guest = 0;
					$item->only_for_user = 0;
				}elseif ($_REQUEST['show_type']==4){
					$item->only_for_guest = 0;
					$item->only_for_user = 0;
					$item->show_for_id = -1;
				}
			}

			if ($item->url_type==0){
				$item->url = $_REQUEST['guest_url'];
				$item->new_window = 0;				
			}elseif ($item->url_type==1){
				$item->url = $_REQUEST['client_url'];
				$item->new_window = 0;				
			}elseif ($item->url_type==2){
				$item->url = $_REQUEST['custom_url'];
				$item->new_window = 0;
			}elseif ($item->url_type==3){
				$item->url = $_REQUEST['custom_url'];
				$item->new_window = 1;
			}


			if (isset($_REQUEST['icon_collection']) && isset($_REQUEST['icon_sli'])
				 && isset($_REQUEST['icon_awesom_s'])
				 && isset($_REQUEST['icon_awesom_r'])
				 && isset($_REQUEST['icon_awesom_b'])
				  ){
				if ($_REQUEST['icon_collection']==-1){
					$item->icon = '';
				}				
				if ($_REQUEST['icon_collection']==0){
					$item->icon = 'icon ' . $_REQUEST['icon_sli'];
				}
				if ($_REQUEST['icon_collection']==1){
					$item->icon = 'fas ' . $_REQUEST['icon_awesom_s'];
				}
				if ($_REQUEST['icon_collection']==2){
					$item->icon = 'far ' . $_REQUEST['icon_awesom_r'];
				}
				if ($_REQUEST['icon_collection']==3){
					$item->icon = 'fab ' . $_REQUEST['icon_awesom_b'];
				}
				if ($_REQUEST['icon_collection']==4){
					$item->icon = 'fal ' . $_REQUEST['icon_awesom_l'];
				}							
			}

			return $item;
		}


	public function redirect($page){
		$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
		$REQUEST_URI_v = html_entity_decode(preg_replace("/subaction=.{3,20}&/", "subaction=".$page.'&', $_SERVER[REQUEST_URI]));
		$REQUEST_URI_v = html_entity_decode(preg_replace("/subaction=.{3,20}$/", "subaction=".$page, $REQUEST_URI_v));
		$actual_link = $protocol."$_SERVER[HTTP_HOST]$REQUEST_URI_v";

		ob_clean();
		header('location: '.$actual_link);
		die();
	}

	public function getgrouplist(){
		$r = Capsule::table('tblclientgroups')
		    		->select('id','groupname')
		            ->get();
		$a = array();
		foreach ($r as $key => $group) {
			$a[$group->id] = $group->groupname;
		}

		return $a;
	}				
	public function getuseraffiliate($clientid){
		if ($this->useraffiliate == null){
			$r = Capsule::table('tblaffiliates')
			    		->select('clientid')
			    		->where('clientid',$clientid)
			            ->get();
			if (isset($r[0])){
				$this->useraffiliate = true;
				return true;
			}else{
				$this->useraffiliate = false;
				return false;
			}
		}else{
			return $this->useraffiliate;
		}
	}

	}
}