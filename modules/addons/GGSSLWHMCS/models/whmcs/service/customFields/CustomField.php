<?php

namespace MGModule\GGSSLWHMCS\models\whmcs\service\customFields;
use MGModule\GGSSLWHMCS as main;

class CustomField{
    public $id;
    public $name;
    public $value;
}