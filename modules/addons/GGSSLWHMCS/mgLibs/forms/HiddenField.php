<?php

namespace MGModule\GGSSLWHMCS\mgLibs\forms;
use MGModule\GGSSLWHMCS as main;

/**
 * Button Hidden Input Field
 *
 * @author Michal Czech <michael@modulesgarden.com>
 */
class HiddenField extends AbstractField{
    public $type    = 'hidden';
}
