<?php

namespace MGModule\GGSSLWHMCS\mgLibs\forms;
use MGModule\GGSSLWHMCS as main;

/**
 * Form Legend
 *
 * @author Michal Czech <michael@modulesgarden.com>
 */
class LegendField extends AbstractField{
    public $type    = 'legend';
}
