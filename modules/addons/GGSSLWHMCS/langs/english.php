<?php

$_LANG['token']        = ', Error Token:';
$_LANG['generalError'] = 'Something has gone wrong. Check logs and contact admin';

//ggssl configuration
$_LANG['addonAA']['pagesLabels']['label']['apiConfiguration']                   = 'Configuration';
$_LANG['addonAA']['apiConfiguration']['cronSynchronization']['header']          = 'Cron Synchronization';
$_LANG['addonAA']['apiConfiguration']['cronSynchronization']['pleaseNote']      = 'Please Note:';
$_LANG['addonAA']['apiConfiguration']['cronSynchronization']['info']            = 'In order to enable automatic synchronization, please set up a following cron command line (every hour suggested):';
$_LANG['addonAA']['apiConfiguration']['cronSynchronization']['commandLine']['cronFrequency']   = '0 */1 * * *';
$_LANG['addonAA']['apiConfiguration']['item']['header']                         = 'API Configuration';
$_LANG['addonAA']['apiConfiguration']['item']['api_login']['label']             = 'Login';
$_LANG['addonAA']['apiConfiguration']['item']['api_password']['label']          = 'Password';
$_LANG['addonAA']['apiConfiguration']['item']['tech_legend']['label']           = 'Technical Contact';
$_LANG['addonAA']['apiConfiguration']['item']['csr_generator_legend']['label']  = 'CSR Generator';
$_LANG['addonAA']['apiConfiguration']['item']['display_csr_generator']['label'] = 'Allow To Use CSR Generator';
$_LANG['addonAA']['apiConfiguration']['item']['tech_firstname']['label']        = 'Firstname';
$_LANG['addonAA']['apiConfiguration']['item']['use_admin_contact']['label']     = 'Use Administrative Contact Details';
$_LANG['addonAA']['apiConfiguration']['item']['tech_lastname']['label']         = 'Lastname';
$_LANG['addonAA']['apiConfiguration']['item']['tech_organization']['label']     = 'Organization Name';
$_LANG['addonAA']['apiConfiguration']['item']['tech_title']['label']            = 'Job Title';
$_LANG['addonAA']['apiConfiguration']['item']['tech_addressline1']['label']     = 'Address';
$_LANG['addonAA']['apiConfiguration']['item']['tech_phone']['label']            = 'Phone Number';
$_LANG['addonAA']['apiConfiguration']['item']['tech_email']['label']            = 'Email Address';
$_LANG['addonAA']['apiConfiguration']['item']['tech_city']['label']             = 'City';
$_LANG['addonAA']['apiConfiguration']['item']['tech_country']['label']          = 'Country';
$_LANG['addonAA']['apiConfiguration']['item']['tech_fax']['label']              = 'Fax Number';
$_LANG['addonAA']['apiConfiguration']['item']['tech_postalcode']['label']       = 'Zip Code';
$_LANG['addonAA']['apiConfiguration']['item']['tech_region']['label']           = 'State/Region';

$_LANG['addonAA']['apiConfiguration']['item']['testConnection']['content'] = 'Test Connection';
$_LANG['addonAA']['apiConfiguration']['item']['saveItem']['label']         = 'Save';
$_LANG['addonAA']['pagesLabels']['label']['productsConfiguration']         = 'Products Configuration';
$_LANG['addonAA']['pagesLabels']['label']['productsCreator']               = 'Products Creator';
$_LANG['addonAA']['pagesLabels']['apiConfiguration']['saveItem']           = 'Save';

$_LANG['addonAA']['apiConfiguration']['messages']['api_connection_success'] = 'Connection established.';


$_LANG['addonAA']['productsConfiguration']['goGetSSLProduct']     = 'GoGetSSL Product:';
$_LANG['addonAA']['productsConfiguration']['productName']         = 'Product Name:';
$_LANG['addonAA']['productsConfiguration']['configurableOptions'] = 'Configurable Options:';
$_LANG['addonAA']['productsConfiguration']['createConfOptions']   = 'Generate';
$_LANG['addonAA']['productsConfiguration']['editPrices']          = 'Edit Prices';
$_LANG['addonAA']['productsConfiguration']['autoSetup']            = 'Auto Setup:';
$_LANG['addonAA']['productsConfiguration']['autoSetupOrder']       = 'Automatically setup the product as soon as an order is placed';
$_LANG['addonAA']['productsConfiguration']['autoSetupPayment']     = 'Automatically setup the product as soon as the first payment is received';
$_LANG['addonAA']['productsConfiguration']['autoSetupOn']          = 'Automatically setup the product when you manually accept a pending order';
$_LANG['addonAA']['productsConfiguration']['autoSetupOff']         = 'Do not automatically setup this product';
$_LANG['addonAA']['productsConfiguration']['months']              = 'Max Months:';
$_LANG['addonAA']['productsConfiguration']['enableSans']          = 'Enable SANs:';
$_LANG['addonAA']['productsConfiguration']['includedSans']        = 'Included SANs:';
$_LANG['addonAA']['productsConfiguration']['status']              = 'Status:';

$_LANG['addonAA']['productsConfiguration']['statusEnable']  = 'Enable';
$_LANG['addonAA']['productsConfiguration']['statusDisable'] = 'Disable';


$_LANG['addonAA']['productsConfiguration']['paymentType']          = 'Payment Type:';
$_LANG['addonAA']['productsConfiguration']['paymentTypeFree']      = 'Free';
$_LANG['addonAA']['productsConfiguration']['paymentTypeRecurring'] = 'Recurring';
$_LANG['addonAA']['productsConfiguration']['paymentTypeOneTime']   = 'One Time';

$_LANG['addonAA']['productsConfiguration']['pricing']             = 'Pricing:';
$_LANG['addonAA']['productsConfiguration']['pricingMonthly']      = 'One Time/Monthly';
$_LANG['addonAA']['productsConfiguration']['pricingQuarterly']    = 'Quarterly';
$_LANG['addonAA']['productsConfiguration']['pricingSemiAnnually'] = 'Semi-Annually';
$_LANG['addonAA']['productsConfiguration']['pricingAnnually']     = 'Annually';
$_LANG['addonAA']['productsConfiguration']['pricingBiennially']   = 'Biennially';
$_LANG['addonAA']['productsConfiguration']['pricingTriennially']  = 'Triennially';

$_LANG['addonAA']['productsConfiguration']['pricingSetupFee'] = 'Setup Fee';
$_LANG['addonAA']['productsConfiguration']['pricingPrice']    = 'Price';
$_LANG['addonAA']['productsConfiguration']['pricingEnable']   = 'Enable';

$_LANG['addonAA']['productsConfiguration']['save']         = 'Save';
$_LANG['addonAA']['productsConfiguration']['messages'][''] = '';


$_LANG['addonAA']['productsCreator']['singleProductCreator'] = 'Single Product Creator';
$_LANG['addonAA']['productsCreator']['goGetSSLProduct']      = 'GoGetSSL Product:';
$_LANG['addonAA']['productsCreator']['productName']          = 'Product Name:';
$_LANG['addonAA']['productsCreator']['productGroup']         = 'Product Group:';
$_LANG['addonAA']['productsCreator']['autoSetup']            = 'Auto Setup:';
$_LANG['addonAA']['productsCreator']['autoSetupOrder']       = 'Automatically setup the product as soon as an order is placed';
$_LANG['addonAA']['productsCreator']['autoSetupPayment']     = 'Automatically setup the product as soon as the first payment is received';
$_LANG['addonAA']['productsCreator']['autoSetupOn']          = 'Automatically setup the product when you manually accept a pending order';
$_LANG['addonAA']['productsCreator']['autoSetupOff']         = 'Do not automatically setup this product';
$_LANG['addonAA']['productsCreator']['months']               = ' Months:';



$_LANG['addonAA']['productsCreator']['enableSans']   = 'Enable SANs:';
$_LANG['addonAA']['productsCreator']['includedSans'] = 'Included SANs:';

$_LANG['addonAA']['productsCreator']['pricing']             = 'Pricing:';
$_LANG['addonAA']['productsCreator']['pricingMonthly']      = 'One Time/Monthly';
$_LANG['addonAA']['productsCreator']['pricingQuarterly']    = 'Quarterly';
$_LANG['addonAA']['productsCreator']['pricingSemiAnnually'] = 'Semi-Annually';
$_LANG['addonAA']['productsCreator']['pricingAnnually']     = 'Annually';
$_LANG['addonAA']['productsCreator']['pricingBiennially']   = 'Biennially';
$_LANG['addonAA']['productsCreator']['pricingTriennially']  = 'Triennially';

$_LANG['addonAA']['productsCreator']['pricingSetupFee'] = 'Setup Fee';
$_LANG['addonAA']['productsCreator']['pricingPrice']    = 'Price';
$_LANG['addonAA']['productsCreator']['pricingEnable']   = 'Enable';
$_LANG['addonAA']['productsCreator']['saveSingle']      = 'Create Single Product';

$_LANG['addonAA']['productsCreator']['multipleProductCreator'] = 'Multiple Product Creator';
$_LANG['addonAA']['productsCreator']['saveMultiple']           = 'Create Multiple Products';

$_LANG['addonAA']['productsCreator']['messages']['mass_product_created']    = 'Products has been added as hidden, go to `Products Configuration` to unhide it, before that verify product configuration and set prices.';
$_LANG['addonAA']['productsCreator']['messages']['single_product_created']  = 'Product has been added as hidden, go to `Products Configuration` to unhide it, before that verify product configuration.';
$_LANG['addonAA']['productsCreator']['messages']['no_product_group_found']  = 'No product group found.';
$_LANG['addonAA']['productsCreator']['messages']['api_product_not_chosen']  = 'GoGetSSL product not chosen.';
$_LANG['addonAA']['productsCreator']['messages']['api_configuration_empty'] = 'API configuration are empty';

$_LANG['addonAA']['productsConfiguration']['messages']['product_saved']          = 'Product saved.';
$_LANG['addonAA']['productsConfiguration']['messages']['configurable_generated'] = 'Configurable options for product was successfully generated.';

$_LANG['addonAA']['productsConfiguration']['messages']['api_configuration_empty'] = 'API configuration are empty';

$_LANG['anErrorOccurred'] = 'An error occurred';
