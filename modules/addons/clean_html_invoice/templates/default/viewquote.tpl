<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="{$charset}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{$companyname} - {$LANG.quotenumber}{$id}</title>
  <!-- Bootstrap -->
  <link href="{$BASE_PATH_CSS}/bootstrap.min.css" rel="stylesheet">
  <link href="{$BASE_PATH_CSS}/font-awesome.min.css" rel="stylesheet">

  <script src="{$BASE_PATH_JS}/jquery.min.js"></script>
  <script src="{$BASE_PATH_JS}/bootstrap.min.js"></script>
  <script src="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/js/JsBarcode.js"></script>
  <script src="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/js/CODE128.js"></script>

  {if $barcode_output!="Disabled"}
   {literal}
    <script>
    jQuery(document).ready(function(){
      jQuery("#barcode").JsBarcode("{/literal}{$id}{literal}",{
        {/literal}
        {if $barcode_output=="Code 128 Accent Color"}
          lineColor: "{$primary_color}",
        {/if}
        {literal}
        width:3,
        height:20,
        format:"CODE128",
      });
    });
    </script>
  {/literal}
  {/if}

  {if $_LANG.locale == 'ar_AR' || $_LANG.locale == 'he_IL'}
  <link href="{$WEB_ROOT}/modules/addons/clean_html_invoice/assets/css/bootstrap-rtl.min.css" rel="stylesheet">
  {/if}
  {if isset($fonts_html)}
  {$fonts_html}
  {/if}
  <style>
  {literal}
  body { background-color: #f5f5f7; }
  h1, h2 { font-weight: 100; }
  h5 { text-transform: uppercase; letter-spacing: 1.5px; }
  .table>thead:first-child>tr:first-child>td { text-transform: uppercase; letter-spacing: 1.5px; font-weight: 400; padding: 15px 45px; }
  .table>tbody>tr>td { padding: 5px 45px; border-top: 0; }
  .table>tbody>tr.active>td { background-color: #fafafa; }
  input[type=submit] { display: inline-block; margin-bottom: 0; font-size: 14px; font-weight: 400; line-height: 1.42857143; text-align: center; white-space: nowrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; background-color: #fff; color: #333; border-color: #ccc; padding: 6px 12px; }
  .draft { color: #999; border: 1px solid #999; }
  .draft, .unpaid { padding: 5px 15px; }
  .unpaid { color: #c00; border: 1px solid #c00; }
  .paid { color: #779500; border: 2px solid #779500; }
  .paid, .refunded { padding: 5px 15px; }
  .refunded { color: #248; border: 1px solid #248; }
  .cancelled { color: #888; border: 1px solid #888; padding: 5px 15px; }
  .collections { color: #fc0; border: 1px solid #fc0; padding: 5px 15px; }
  .invoice-container .payment-btn-container { padding-top: 15px; }
  .invoice-container .payment-btn-container table { float: right; }
  .invoice-status { text-transform: uppercase; letter-spacing: 1.5px; font-size: 18px; font-weight: 600; margin-top: 24px; }
  .table-invoice { width: 100%; max-width: 100%; margin-bottom: 60px; }
  .table-invoice-header>tbody>tr.primary>td { font-size: 30px; }
  .table-invoice-items>thead>tr>td, .table-transactions>thead>tr>td { text-transform: uppercase; letter-spacing: 1.5px; padding: 5px 30px; }
  .table__cell { padding: 15px 30px; }
  .barcode__cell>td { padding-right: 20px; }
  .table-balance .table__cell, .table-invoice-totals .table__cell { padding: 7px 30px 7px 15px; }
  .table-transactions { margin-bottom: 0; }
  .table-balance { margin-bottom: 90px; }
  .table-transactions .table__cell { padding: 7px 30px; }
  .logo img { max-height: 100px; max-width: 150px; }
  .vertical-view-line { margin: -20px 0 0 40px; border: 2px solid #d0dbe1; height: 20px; width: 0; }
  .logo { margin-top: 18px; margin-bottom: 18px; }
  .invoice-container { background: #fff; overflow: hidden; border: 1px solid #e3e3e3; padding: 20px 0; }
  .well-notes { background-color: #fff; }
  .btn-link { color: #333; }
  @media (min-width:768px) {
    .container { width: 770px !important; }
    .invoice-container, .invoice-container>div { width: 740px; }
  }
  @media (min-width:992px) {
    .container { width: 870px !important; }
    .invoice-container, .invoice-container>div { width: 840px; min-height:1030px; }
  }
  @media (min-width:1200px) {
    .container { width: 870px !important; }
    .invoice-container>div { width: 840px; min-height:1130px;}
  }
  @media only screen and (max-width:480px) {
    .table-invoice-totals { margin-left: -15px; }
  }
  {/literal}
  body { font-family: {$body_font}; }
  h1,h2,h3,h4,h5,h6 { font-family: {$header_font}; }
  .btn-credit { color: #fff!important; background-color: {$primary_color}!important; border-color: {$primary_color}!important; }
  .table-invoice>tbody>tr.primary>td { background-color: {$primary_color}; color: #fff; text-transform: uppercase; letter-spacing: 1.5px; font-weight: 100; }
  .table-invoice>tbody>tr.secondary>td { background-color: {$secondary_color}; }
  {if $page_decorations=="1"}
    .invoice-container > div { box-shadow: 0 0 0 10px {$secondary_color}, 0 0 0 20px {$primary_color}; }
  {/if}
  .well-credit { background-color:#fff; border-top: 4px solid {$primary_color} }
  .table-invoice-items>tbody>tr:nth-child(even) { background-color: {$table_color_even}; }
  .table-invoice-items>tbody>tr:nth-child(odd) { background-color: {$table_color}; }
  </style>
</head>
<body>
  {$clean_html_invoice_settmpvar}

  <div class="contrainer-fluid hidden-print" style="background-color:#fff">
    <div class="container">
      <div class="row">
        <div class="col-xs-8">
          <h2>{$LANG.quotenumber}{$id}</h2>
        </div>
        <div class="col-xs-4">
          <div class="pull-right">
            <div class="invoice-status">
              {if $stage eq "Delivered"}
              <span class="unpaid">{$LANG.quotestagedelivered}</span>
              {elseif $stage eq "Accepted"}
              <span class="paid">{$LANG.quotestageaccepted}</span>
              {elseif $stage eq "On Hold"}
              <span class="refunded">{$LANG.quotestageonhold}</span>
              {elseif $stage eq "Lost"}
              <span class="cancelled">{$LANG.quotestagelost}</span>
              {elseif $stage eq "Dead"}
              <span class="collections">{$LANG.quotestagedead}</span>
              {/if}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row" style="margin-top:15px;">
      <div class="col-sm-12">
        {if $agreetosrequired}
        {include file="$template/includes/panel.tpl" type="danger" headerTitle=$LANG.error bodyContent=$LANG.ordererroraccepttos bodyTextCenter=true}
        {/if}
      </div>
    </div>
  </div>

  <div class="container hidden-print">
    <div class="row" style="margin-top:15px;">
      {if $invalidQuoteIdRequested}
      {include file="$template/includes/panel.tpl" type="danger" headerTitle=$LANG.error bodyContent=$LANG.invoiceserror bodyTextCenter=true}
      {else}
      <div class="col-md-12">
        {if $stage eq "Delivered" || $stage eq "On Hold"}
        <div class="well clearfix" style="background-color:#fff;">
          <div class="pull-left">
            <h5>{$LANG.quotevaliduntil} {$validuntil}</h5>
          </div>
          <div class="pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#acceptQuoteModal"><i class="fa fa-check-circle text-success"></i> {$LANG.quoteacceptbtn}</button>
          </div>
        </div>
        {/if}
      </div>
    </div>
  </div>

  {if $proposal}
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        {if $stage eq "Delivered" || $stage eq "On Hold"}
        <div class="vertical-view-line"></div>
        {/if}
        <div class="well well-notes clearfix">
          <h5>{$LANG.quoteproposal}</h5>
          <hr>
          <p>{$proposal}</p>
        </div>
      </div>
    </div>
  </div>
  {/if}

  {if $notes}

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="vertical-view-line"></div>
        <div class="well well-notes clearfix">
          <h5>{$LANG.invoicesnotes}</h5>
          <hr>
          <p>{$notes}</p>
        </div>
      </div>
    </div>
  </div>
  {/if}

  <div class="container hidden-print">
    <div class="row">
      <div class="col-sm-6">
        <a href="javascript:window.print()" class="btn btn-link"><i class="fa fa-print"></i> {$LANG.print}</a>
        <a href="dl.php?type=q&amp;id={$quoteid}" class="btn btn-link"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="invoice-container">
      <div>
        <div class="row">
          <div class="col-md-12">
            <div class="row" style="margin-top:60px;margin-left:30px;margin-bottom:30px;">
              <div class="col-xs-12 col-sm-5">
                {if $logo}
                <div class="logo"><img src="{$logo}" alt="{$companyname}" title="{$companyname}" /></div>
                {else}
                <h2>{$companyname}</h2>
                {/if}
                <address>
                  {$payto}
                </address>
              </div>
              <div class="col-sm-7 hidden-xs">
                <table class="table-invoice table-invoice-header">
                  <tbody>
                    <tr class="primary">
                      <td class="table__cell">{$LANG.quotenumber}</td>
                      <td class="table__cell">{$id}</td>
                    </tr>
                    <tr class="secondary">
                      <td class="table__cell">
                        <strong><i class="fa fa-calendar-o" aria-hidden="true"></i> {$LANG.quotedatecreated}</strong>
                      </td>
                      <td class="table__cell">{$datecreated}</td>
                    </tr>
                    <tr class="secondary">
                      <td class="table__cell">
                        <strong><i class="fa fa-calendar-o" aria-hidden="true"></i> {$LANG.quotevaliduntil}</strong>
                      </td>
                      <td class="table__cell">{$validuntil}</td>
                    </tr>
                    <tr class="barcode__cell">
                      <td class="text-right" colspan="2">
                        {if $barcode_output!="Disabled"}
                        <img id="barcode"/>
                        {/if}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="container visible-xs">
              <div class="row">
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.quotenumber}</strong></dt>
                    <dd>{$id}</dd>
                  </dl>
                </div>
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.quotedatecreated}</strong></dt>
                    <dd>{$datecreated}</dd>
                  </dl>
                </div>
                <div class="col-xs-4">
                  <dl class="dl-horizontal">
                    <dt><strong>{$LANG.quotevaliduntil}</strong></dt>
                    <dd>{$validuntil}</dd>
                  </dl>
                </div>
              </div>
            </div>

            <table class="table-invoice table-invoice-items">
              <thead>
                <tr>
                  <td><strong>{$LANG.invoicesdescription}</strong></td>
                  <td class="hidden-xs"><strong>{$LANG.quotediscountheading}</strong></td>
                  <td class="text-right"><strong>{$LANG.invoicesamount}</strong></td>
                </tr>
              </thead>
              <tbody>
                {foreach from=$quoteitems item=item}
                <tr>
                  <td class="table__cell">{$item.description}{if $item.taxed eq "true"} <small><i class="fa fa-asterisk" style="color:#777;" aria-hidden="true"></i></small>{/if}
                    <dl class="visible-xs">
                      <dt>{$LANG.quotediscountheading}</dt>
                      <dd>{if $item.discountpc > 0}{$item.discount} ({$item.discountpc}%){else} - {/if}</dd>
                    </dl>
                  </td>
                  <td class="table__cell hidden-xs">{if $item.discountpc > 0}{$item.discount} ({$item.discountpc}%){else} - {/if}</td>
                  <td class="text-right table__cell">{$item.amount}</td>
                </tr>
                {/foreach}
              </tbody>
            </table>
            <div class="row" style="margin-left:15px;">
              <div class="col-md-7">
                <h5><strong>{$LANG.quoterecipient}</strong></h5>
                <address>
                  {if $clientsdetails.companyname}{$clientsdetails.companyname}<br>{/if}
                  {$clientsdetails.firstname} {$clientsdetails.lastname}<br>
                  {$clientsdetails.address1}, {$clientsdetails.address2}<br>
                  {$clientsdetails.city}, {$clientsdetails.state}, {$clientsdetails.postcode}<br>
                  {$clientsdetails.country}
                  {if $customfields}
                  <br><br>
                  {foreach from=$customfields item=customfield}
                  {$customfield.fieldname}: {$customfield.value}<br>
                  {/foreach}
                  {/if}
                </address>

              </div>
              <div class="col-md-5">
                <table class="table-invoice table-invoice-totals">
                  <tbody>
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$LANG.invoicessubtotal}</strong></td>
                      <td class="text-right table__cell"><strong>{$subtotal}</strong></td>
                    </tr>
                    {if $taxrate}
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$taxrate}% {$taxname}</strong></td>
                      <td class="text-right table__cell"><strong>{$tax}</strong></td>
                    </tr>
                    {/if}
                    {if $taxrate2}
                    <tr class="secondary">
                      <td class="table__cell"><strong>{$taxrate2}% {$taxname2}</strong></td>
                      <td class="text-right table__cell"><strong>{$tax2}</strong></td>
                    </tr>
                    {/if}
                    <tr class="primary">
                      <td class="table__cell"><h4>{$LANG.quotelinetotal}</h4></td>
                      <td class="text-right table__cell"><h4>{$total}</h4></td>
                    </tr>
                    {if $taxrate}
                    <tr class="secondary">
                      <td colspan="2" style="padding:6px 12px;"><small><i class="fa fa-fw fa-asterisk" style="color:#777;" aria-hidden="true"></i> {$LANG.invoicestaxindicator}</small></td>
                    </tr>
                    {/if}
                  </tbody>
                </table>
              </div>
            </div>
            {/if}
          </div>
        </div>
      </div>
    </div>

    <div class="row" style="margin-top:30px;">
      <div class="col-sm-12 text-center hidden-print">
        <a class="btn btn-link" href="clientarea.php">{$LANG.invoicesbacktoclientarea}</a>
      </div>
    </div>

  </div>

  <form method="post" action="viewquote.php?id={$quoteid}&amp;action=accept">
    <div class="modal fade" id="acceptQuoteModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{$LANG.quoteacceptbtn}</h4>
          </div>
          <div class="modal-body">
            <p><strong>{$LANG.quoteacceptagreetos}</strong></p>
            <p>{$LANG.quoteacceptcontractwarning}</p>
            <p>
              <label class="checkbox-inline">
                <input type="checkbox" name="agreetos" />
                {$LANG.ordertosagreement} <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a>
              </label>
            </p>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{$LANG.cancel}</button>
            <button type="submit" class="btn btn-primary">{$LANG.quoteacceptbtn}</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
