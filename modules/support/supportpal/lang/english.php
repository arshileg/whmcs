<?php
// Language File - SupportPal WHMCS Integration
// Language: EN-UK
// Version: WHMCS Integration 3.1.0

// Tickets
$_ADDONLANG['tickets_submitted'] = "Ticket Submitted";
$_ADDONLANG['ticket_number'] = "Number";
$_ADDONLANG['tickets_user'] = "User";
$_ADDONLANG['noticketsfound'] = "No Tickets Found";
$_ADDONLANG['tickets_captchaplease'] = 'Please enter the text from the image in the box provided, this helps us to prevent spam.';
$_ADDONLANG['tickets_captchaError'] = 'The code you entered was incorrect, please try again.';
$_ADDONLANG['tickets_uploads_error'] = 'The attachment(s) was too large or not one of the accepted file formats.';
$_ADDONLANG['tickets_locked'] = 'This ticket cannot be re-opened now. Please open a new ticket instead.';
$_ADDONLANG['tickets_fields'] = 'Custom Fields';
$_ADDONLANG['only_to_change'] = 'Enter only to change current value';
$_ADDONLANG['user_closed'] = 'The ticket has now been marked as resolved.';
$_ADDONLANG['user_closed_error'] = 'There was an error while trying to mark the ticket as resolved.';
$_ADDONLANG['fields_saved'] = 'The custom fields have been updated.';
$_ADDONLANG['fields_saved_error'] = 'There was an error while trying to update the custom fields.';
$_ADDONLANG['reply_posted'] = 'Your reply has been posted.';
$_ADDONLANG['reply_posted_error'] = 'There was an error while trying to post your reply.';
$_ADDONLANG['no_departments'] = 'There are no departments.';

// Announcements
$_ADDONLANG['announcement_announcements'] = "Announcements";
$_ADDONLANG['announcement_print'] = "Print this Announcement";
$_ADDONLANG['announcement_all'] = "All Announcements";
$_ADDONLANG['announcement_read_full_announcements'] = "Read Full Announcement";
$_ADDONLANG['announcement_none'] = "There are no announcements to display.";
$_ADDONLANG['announcement_not_allowed'] = "This announcement does not exist.";
$_ADDONLANG['announcement_cat_not_allowed'] = "This category does not exist";

// Downloads
$_ADDONLANG['download_none'] = "There are no downloads to display.";
$_ADDONLANG['download_popular_downloads'] = "Most Popular Downloads";
$_ADDONLANG['download_downloads'] = "Downloads";
$_ADDONLANG['download_now'] = "Download Now";
$_ADDONLANG['download_not_allowed'] = "This download does not exist.";
$_ADDONLANG['download_cat_not_allowed'] = "This category does not exist";

// Knowledgebase
$_ADDONLANG['knowledgebase_viewall'] = "View all articles";
$_ADDONLANG['knowledgebase_none'] = "There are no items to display.";
$_ADDONLANG['knowledgebase_views'] = "Views";
$_ADDONLANG['knowledgebase_articles'] = "Articles";
$_ADDONLANG['knowledgebase_related_articles'] = "Related Articles";
$_ADDONLANG['knowledgebase_popular_articles'] = "Most Popular Articles";
$_ADDONLANG['knowledgebase_popular_subcategories'] = "Subcategories";
$_ADDONLANG['knowledgebase_not_allowed'] = "This article does not exist.";
$_ADDONLANG['knowledgebase_cat_not_allowed'] = "This category does not exist";

// Comment System
$_ADDONLANG['comments'] = 'Comments';
$_ADDONLANG['comments_login_to_comment'] = 'Please login to comment';
$_ADDONLANG['comments_add'] = 'Add Comment';
$_ADDONLANG['comments_comment'] = 'Comment';
$_ADDONLANG['comments_send_email'] = 'Send Email on Replies';
$_ADDONLANG['comments_confirm'] = 'Confirm Submission';
$_ADDONLANG['comments_captchaError'] = 'The code you entered was incorrect, please try again.';
$_ADDONLANG['comments_success'] = 'Success!';
$_ADDONLANG['comments_accepted_for_moderation'] = 'Your comment has been sent to the administrators for approval.';
$_ADDONLANG['comments_reply'] = 'Reply';
$_ADDONLANG['comments_replying_to'] = 'Replying to';
$_ADDONLANG['general_ago'] = 'ago';

// Self-Service
$_ADDONLANG['selfservice_share'] = 'Share via';
$_ADDONLANG['selfservice_share_email'] = 'Email';
$_ADDONLANG['selfservice_share_fb'] = 'Facebook';
$_ADDONLANG['selfservice_share_gplus'] = 'Google Plus';
$_ADDONLANG['selfservice_share_twitter'] = 'Twitter';

// Users
$_ADDONLANG['users_name'] = 'Full Name';
$_ADDONLANG['users_email'] = 'Email Address';

// Validation
$_ADDONLANG['validation_field_required'] = "This field is required.";
$_ADDONLANG['validation_file_type'] = "File type is not allowed, please convert the file to one of the following types:";

// General
$_ADDONLANG['published_on'] = "Published On";
$_ADDONLANG['modified_on'] = "Modified On";
$_ADDONLANG['category'] = "Category";
$_ADDONLANG['categories'] = "Categories";
$_ADDONLANG['yes'] = "Yes";
$_ADDONLANG['no'] = "No";
$_ADDONLANG['general_choose'] = 'Please choose one';

// Buttons
$_ADDONLANG['button_cancel'] = 'Cancel';
$_ADDONLANG['button_submit'] = 'Submit';
$_ADDONLANG['button_attach_more_files'] = "Attach more files";
$_ADDONLANG['button_save_fields'] = 'Save Fields';
