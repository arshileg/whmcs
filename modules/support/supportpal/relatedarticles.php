<?php
/**
 * SupportPal Integration - Related articles
 *
 * Fetch related articles for a given term.
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 3.2.0
 */

use WHMCS\Database\Capsule;

// Files required.
require("../../../init.php");
require_once("functions.php");

// See if a knowledgebase ID has been set.
$knowledgebaseType = Capsule::table('mod_supportpal')
    ->where('name', 'knowledgebase_id')
    ->first(array('value'));
$knowledgebaseType = !empty($knowledgebaseType) ? $knowledgebaseType->value : null;

// Get type ID.
$typeId = getTypeId('knowledgebase', $knowledgebaseType);

// If we have a valid type.
if (!empty($typeId)) {
    $apiCall = _doAPICall('selfservice/article/related', array(
        'term'      => $_GET['term'],
        'type_id'   => $typeId,
        'protected' => 1
    ));

    // Did we get results back?
    if (is_array($result = json_decode($apiCall, true)) && $result['status'] == 'success'
        && $result['count'] > 0) {
        // Encode the title and excerpt.
        foreach ($result['data'] as &$article) {
            $article = array(
                'id'      => $article['id'],
                'title'   => htmlentities($article['title']),
                'excerpt' => htmlentities($article['excerpt'])
            );
        }

        // Convert to json again and return.
        echo json_encode($result);
    } else {
        // Already in correct format, just return as it is.
        echo $apiCall;
    }
} else {
    // We don't have a knowledgebase type
    echo json_encode(array(
        'status'  => 'error',
        'message' => 'No knowledgebase type available.'
    ));
}