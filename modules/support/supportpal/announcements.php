<?php
/**
 * SupportPal Integration Announcements Module
 *
 * Handles the loading and displaying of the SupportPal announcements and announcement categories in WHMCS
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

define("CLIENTAREA", true);

// Recaptcha keys - don't change these
$recaptcha_public_key = '6LeZrcgSAAAAAAfNCs5DGNubKsbwQjM3QQLaYSPE';
$recaptcha_private_key = '6LeZrcgSAAAAAFj43im5C9uzv3GhDqlnqbs0QpKm';

// Files required
require_once("modules/support/supportpal/functions.php");
include("modules/support/supportpal/lang/english.php");
require("modules/support/supportpal/3rd/recaptcha/recaptchalib.php");

$ca = new WHMCS_ClientArea;
$ca->initPage();

// Setup page title and breadcrumb
$pageTitle = Lang::trans('announcementstitle');
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('announcements.php', Lang::trans('announcementstitle'));

// If we're looking for a specific announcement or category, fetch the IDs
$id = isset($_REQUEST["announcement"]) ? $_REQUEST["announcement"] : null;
$categoryId = isset($_REQUEST["category"]) ? $_REQUEST["category"] : null;
$tagId = isset($_REQUEST["tag"]) ? $_REQUEST["tag"] : null;
$page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;

// 10 articles per page
$perPage = 10;

// Get core settings
$settings = getCoreSettings();

// See if an announcement ID has been set
$announcementsType = Capsule::table('mod_supportpal')
    ->where('name', 'announcements_id')
    ->first(array('value'));
$announcementsType = !empty($announcementsType) ? $announcementsType->value : null;

// Get type ID
$typeId = getTypeId('announcements', $announcementsType, $settings['default_brand']);

// Initialise purifier
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);

if (isset($id)) {
    // Specific announcement
    // Set template file for the page
    $ca->setTemplate('../supportpal/announcement_view');

    // Get article
    $data = array(
        'type_id'         => $typeId,
        'increment_views' => 1
    );
    $response = json_decode(_doAPICall('selfservice/article/' . $id, $data), true);

    // Verify the article is in the same type.
    if ($response['status'] == 'success'
        && array_search($typeId, array_column($response['data']['types'], 'id')) !== false) {
        // Set the article title
        $pageTitle = $response['data']['title'];

        // Add to breadcrumb
        $ca->addToBreadCrumb('', htmlentities($pageTitle));

        // Convert the time
        $response['data']['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $response['data']['created_at']);

        // Purify the article text
        $response['data']['text'] = $purifier->purify($response['data']['text']);

        $articleData['article'] = $response['data'];

        // Get self-service settings
        $response = json_decode(_doAPICall('selfservice/settings'), true);
        if ($response['status'] == 'success') {
            $ca->assign('settings', $selfserviceSettings = $response['data']);

            // Get a captcha in case they decide to post a comment
            if ($selfserviceSettings['comment_captcha'] == '2' || (!$ca->isLoggedIn() && $selfserviceSettings['comment_captcha'] == '1')) {
                $ca->assign('recaptcha', recaptcha_get_html($recaptcha_public_key, null));
            }
        }

        // Check if logged in to WHMCS
        $ca->assign('userLoggedIn', $ca->isLoggedIn());

        // Check if a user account exists at the help desk
        $ca->assign('helpdeskUser', $helpdeskUser = getHelpdeskAccount($ca->getUserID()));

        // Do we have a comment to submit?
        if ($_POST && isset($_POST["comment_message"])) {
            // Variable set up to see if the comment should be submitted, defaulted to true
            $isValid = true;

            // If captcha is set, check to see the answer is correct
            if ($selfserviceSettings['comment_captcha'] == '2' ||
                (!$ca->isLoggedIn() && $selfserviceSettings['comment_captcha'] == '1')) {
                $response = recaptcha_check_answer($recaptcha_private_key, $_SERVER["REMOTE_ADDR"],
                    $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                $isValid = $response->is_valid;
            }

            // Should the comment be submitted?
            if ($isValid) {
                // Add article ID and type ID to data
                $comment = $_POST;
                $comment['comment_item_id'] = $id;
                $comment['comment_type_id'] = $typeId;
                
                // Try to submit new comment
                $commentStatus = postComment($comment, $selfserviceSettings, $helpdeskUser);
            } else {
                // Captcha was wrong, show an error
                $commentStatus = -1;
            }

            if ($commentStatus == -1 || $commentStatus == -2) {
                // Restore the form values
                if (isset($_POST['comment_parent_id']) && is_numeric($_POST['comment_parent_id'])) {
                    $ca->assign('comment_parent_id', $_POST['comment_parent_id']);
                }
                $ca->assign('orig_comment_message', isset($_POST['comment_message']) ? htmlentities($_POST['comment_message']) : null);
                $ca->assign('orig_comment_email_author', isset($_POST['comment_email_author']) ? $_POST['comment_email_author'] : null);
            }

            // Send to view
            $ca->assign('orig_comment_name', isset($_POST['comment_name']) ? $_POST['comment_name'] : '');
            $ca->assign('comment_status', $commentStatus);
        }

        // Get comments
        $data = array(
            'article_id'      => $id,
            'type_id'         => $typeId,
            'status'          => 1,
            'order_direction' => 'asc'
        );
        $articleData['comments'] = fetchComments($data, $settings);
        
        // Purify each comment
        foreach ($articleData['comments'] as &$comment) {
            $comment['text'] = $purifier->purify($comment['text']);
        }
    } else {
        $articleData = array();
    }
} else {
    // All announcements, specific category or tag
    // Set template file for the page
    $ca->setTemplate('../supportpal/announcements');

    $data = array(
        'type_id'       => $typeId,
        'public'        => 1,
        'parent_public' => 1,
        'order_column'  => 'name'
    );

    // Get all categories
    $response = json_decode(_doAPICall('selfservice/category', $data), true);

    $categoryData = array();
    if ($response['status'] == 'success') {
        foreach($response['data'] AS $category) {
            $categoryData[] = $category;

            // Specific category
            if (isset($categoryId) && $category['id'] == $categoryId) {
                // Set the name of the category
                $pageTitle = $category['name'];

                // Add to breadcrumb
                $ca->addToBreadCrumb('', htmlentities($category['name']));

                // Pass category to view
                $ca->assign('category', htmlentities(getSlug($category['name'])));
            }
        }
    }

    $data = array(
        'type_id'         => $typeId,
        'category_id'     => $categoryId,
        'tag_id'          => $tagId,
        'include_subcategories' => 1,
        'published'       => 1,
        'protected'       => $ca->isLoggedIn() ? null : 0,
        'start'           => (($page - 1) * $perPage) + 1,
        'limit'           => $perPage,
        'order_column'    => 'created_at',
        'order_direction' => 'desc'
    );

    // Get all announcements
    $response = json_decode(_doAPICall('selfservice/article', $data), true);

    $articleData = array();
    if ($response['status'] == 'success') {
        foreach ($response['data'] AS $value) {
            // Check if any of the categories are public
            $public = false;
            foreach ($value['categories'] AS $category) {
                if ($category['public'] && $category['parent_public']) {
                    $public = true;
                    break;
                }
            }

            // Add to list if at least one category is public
            if ($public) {
                // Convert the time
                $value['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $value['created_at']);

                $articleData[] = $value;
            }
        }

        // Save number of articles
        $articleCount = $response['count'];
    }

    // Create final data array
    $articleData = array(
        'articles'   => $articleData,
        'categories' => $categoryData
    );

    if (isset($tagId)) {
        // Fetch tag
        $response = json_decode(_doAPICall('selfservice/tag/' . $tagId), true);

        if ($response['status'] == 'success') {
            // Set the name of the tag
            $pageTitle = $response['data']['name'];

            $ca->addToBreadCrumb('', htmlentities($response['data']['name']));

            $ca->assign('tag', htmlentities(getSlug($response['data']['name'])));
        }
    }

    // Pagination
    $pages = ceil($articleCount / $perPage);
    $ca->assign('totalPages', $pages);
    $ca->assign('currentPage', $page);
}

// Set page title
$ca->setPageTitle(htmlentities($pageTitle));

// Load the data in
$ca->assign('data', recursive_htmlentities($articleData, array('text')));

// Sidebar menu - not on announcement page
if (isset($articleData['categories'])) {
    Menu::addContext();

    // Show the sidebar with announcements active
    $secondarySidebar = Menu::secondarySidebar('announcementList');

    // Add a new menu above for categories
    $secondarySidebar->addChild('categories', array(
        'label' => Lang::trans('knowledgebasecategories'),
        'uri'   => '#',
        'icon'  => 'fa-folder-open-o',
    ));

    // Retrieve the panel we just created
    $menuPanel = $secondarySidebar->getChild('categories');

    // Add an all announcements link
    $menuPanel->addChild('category-0', [
        'uri'   => 'announcements.php',
        'label' => $_ADDONLANG['announcement_all'],
        'class' => 'active'
    ]);

    // Add each category link
    foreach ($articleData['categories'] AS $category) {
        $menuPanel->addChild('category-' . htmlentities($category['name']), [
            'uri'   => 'announcements.php?category=' . $category['id'],
            'label' => htmlentities($category['name'])
        ]);
    }
}

// Load the SupportPal URL
$baseUrl = Capsule::table('mod_supportpal')
    ->where('name', 'base_url')
    ->first(array('value'));
$ca->assign('supportpalUrl', !empty($baseUrl) ? $baseUrl->value : '');

// Set the language file
$ca->assign('LANG2', $_ADDONLANG);

$ca->output();
