<?php
/**
 * SupportPal Integration View Ticket Module
 *
 * Handles the loading and displaying of an SupportPal support ticket in WHMCS
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

define("CLIENTAREA", true);

// Files required
require_once("modules/support/supportpal/functions.php");
include("modules/support/supportpal/lang/english.php");

$ca = new WHMCS_ClientArea;
$ca->initPage();

// Require to be logged in
$ca->requireLogin();

// Get some variables
$ticket_number = isset($_GET["number"]) ? $_GET["number"] : null;
$ticket_token = isset($_GET["token"]) ? $_GET["token"] : null;

// Setup page title and breadcrumb
$ca->setPageTitle(Lang::trans('supportticketsviewticket'));
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('clientarea.php', Lang::trans('clientareatitle'));
$ca->addToBreadCrumb('supporttickets.php', Lang::trans('supportticketspagetitle'));
$ca->addToBreadCrumb('viewticket.php?number=' . htmlentities($ticket_number) . '&token=' . md5($ticket_number),
    Lang::trans('supportticketsviewticket'));

// Set template file for the page
$ca->setTemplate('../supportpal/viewticket');

// Initialise error keeper
$ticketError = false;

// Initialise purifier
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);

// Start the work!
if ($ca->isLoggedIn() && !empty($ticket_number) && !empty($ticket_token)) {

    // Get WHMCS user account
    $user = Capsule::table('tblclients')->where('id', $ca->getUserID())->first();

    // Get user account from help desk
    $helpdeskUser = getHelpdeskAccount(null, $user->email);

    // Verify user exists
    if (isset($helpdeskUser)) {
        // Get ticket
        $data = array(
            'number' => $ticket_number,
            'user'   => $helpdeskUser['id']
        );
        $response = json_decode(_doAPICall('ticket/ticket', $data), true);

        // Verify ticket exists
        if ($response['status'] == 'success') {
            $ticketData = $response['data'][0];

            // Verify token
            if ($ticket_token == sha1($ticketData['id'] . '-' . $ticketData['number'])) {
                // Get core and ticket settings
                $coreSettings = getCoreSettings();
                $response = json_decode(_doAPICall('ticket/settings'), true);
                if ($response['status'] == 'success') {
                    $settings = $response['data'];

                    // Add date format
                    $settings['date_format'] = dateformat_PHP_to_jQueryUI($coreSettings['date_format']);

                    $ca->assign('settings', $settings);
                }

                // Handling downloading attachment
                if (isset($_GET['download_file'])) {
                    downloadAttachment($_GET['download_file']);
                }

                // Handling posting reply
                if (isset($_POST['reply']) && $_POST['reply'] == 1) {
                    // Start building the array
                    $data = array(
                        'ticket_id'       => $ticketData['id'],
                        'user_id'         => $helpdeskUser['id'],
                        'user_ip_address' => $_SERVER['REMOTE_ADDR'],
                        'message_type'    => 0,
                        'text'            => nl2br($_POST['replymessage'])
                    );

                    // Add attachments
                    if (isset($_FILES['attachments'])) {
                        $data['attachment'] = array();

                        // Add each attachment to data array
                        foreach ($_FILES['attachments']['tmp_name'] as $key => $value) {
                            if (!empty($value)) {
                                $data['attachment'][] = array(
                                    'filename' => $_FILES['attachments']['name'][$key],
                                    'contents' => base64_encode(file_get_contents($value))
                                );
                            }
                        }
                    }

                    // Do the curl call
                    $response = json_decode(_doAPICall('ticket/message', $data, 'POST'), true);

                    if ($response['status'] == 'success') {
                        header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&reply_posted=1");
                        exit;
                    } else {
                        // Didn't post reply
                        header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&reply_posted=0");
                        exit;
                    }
                }

                // Handle closing ticket
                if (isset($_GET['close']) && $_GET['close'] == 1) {
                    // Post the data to the API
                    $data = array(
                        'status' => $settings['default_resolved_status']
                    );
                    $response = json_decode(_doAPICall('ticket/ticket/' . $ticketData['id'], $data, 'PUT'), true);

                    if ($response['status'] == 'success') {
                        header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&user_closed=1");
                        exit;
                    } else {
                        // Didn't close properly
                        header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&user_closed=0");
                        exit;
                    }
                }

                // Handle updating fields
                if (isset($_POST['update_fields']) && $_POST['update_fields'] == 1) {
                    // Ensure ticket isn't closed or locked
                    if ($ticketData['status_id'] !== $settings['default_resolved_status'] && !$ticketData['locked']) {
                        $data = array(
                            'customfield' => array()
                        );

                        // Save the new custom field data
                        foreach ($_POST as $key => $value) {
                            // Explode it
                            $exploded = explode("_", $key);

                            // Check if it starts with cf_ and it's not encrypted
                            if (isset($exploded[0]) && ($exploded[0] == "cf")
                                && isset($exploded[1]) && is_numeric($exploded[1]) && $value != '** ENCRYPTED **') {
                                $data['customfield'][$exploded[1]] = html_entity_decode($value, ENT_QUOTES | ENT_HTML401);
                            }
                        }

                        // Send to the API
                        $response = json_decode(_doAPICall("ticket/ticket/" . $ticketData['id'], $data, 'PUT'), true);

                        if ($response['status'] == 'success') {
                            header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&saved_fields=1");
                            exit;
                        } else {
                            // Didn't save properly
                            header("LOCATION: viewticket.php?number=" . $ticket_number . "&token=" . $ticket_token . "&saved_fields=0");
                            exit;
                        }

                    }
                }

                // Save token
                $ticketData['token'] = $ticket_token;

                // Convert time
                $ticketData['created_at'] = date($coreSettings['date_format'] . ' ' . $coreSettings['time_format'], $ticketData['created_at']);
                $ticketData['updated_at'] = date($coreSettings['date_format'] . ' ' . $coreSettings['time_format'], $ticketData['updated_at']);

                // Get custom fields
                $customFields = array();
                $data = array(
                    'department_id' => $ticketData['department_id'],
                    'order_column'  => 'order'
                );
                $response = json_decode(_doAPICall('ticket/customfield', $data), true);
                if ($response['status'] == 'success') {
                    foreach ($response['data'] AS $customfield) {
                        // Check if we have a value set for this field
                        foreach ($ticketData['customfields'] AS $value) {
                            if ($customfield['id'] == $value['field_id']) {
                                if ($customfield['encrypted']) {
                                    // Encrypted
                                    $value['value'] = '** ENCRYPTED **';
                                } else if ($customfield['type'] == 2 || $customfield['type'] == 4) {
                                    // Checklist & multiple options come with a json-encoded array
                                    $value['value'] = json_decode($value['value']);
                                } else if ($customfield['type'] == 3) {
                                    // Convert date timestamp to proper date format
                                    $value['value'] = date($coreSettings['date_format'], $value['value']);
                                }
                                // Save value
                                $customfield['value'] = $value['value'];
                            }
                        }

                        // Add field to array to send to view
                        $customFields[$customfield['id']] = $customfield;
                    }

                    // Encode the custom fields name and description
                    foreach ($customFields as &$field) {
                        $field['name'] = htmlentities($field['name']);
                        $field['description'] = htmlentities($field['description']);
                    }

                    // Update custom fields if related service field is set
                    $customFields = updateRelatedServiceField($customFields, $ca->getUserID());
                }

                // Send custom fields to view with keys reset
                $ca->assign('customfields', array_values($customFields));

                // Get messages
                $data = array(
                    'ticket_id'       => $ticketData['id'],
                    'order_column'    => 'created_at',
                    'order_direction' => $settings['ticket_reply_order'] == 0 ? 'asc' : 'desc'
                );
                $response = json_decode(_doAPICall('ticket/message', $data), true);
                if ($response['status'] == 'success') {
                    // Purify text and format date on each message
                    foreach ($response['data'] AS &$message) {
                        $message['text'] = $purifier->purify($message['text']);
                        $message['created_at'] = date($coreSettings['date_format'] . ' ' . $coreSettings['time_format'], $message['created_at']);
                    }

                    $ca->assign('messages', $response['data']);
                }

                // Send all data to view
                $ca->assign('data', $ticketData);
            } else {
                // Invalid token
                $ticketError = true;
            }
        } else {
            // Couldn't find ticket
            $ticketError = true;
        }
    } else {
        // Help desk account doesn't exist
        $ticketError = true;
    }
} else {
    // Not logged in or invalid parameters
    $ticketError = true;
}

$ca->assign('ticketError', $ticketError);

// Load the SupportPal URL
$baseUrl = WHMCS\Database\Capsule::table('mod_supportpal')
    ->where('name', 'base_url')
    ->first(array('value'));
$ca->assign('supportpalUrl', !empty($baseUrl) ? $baseUrl->value : '');

// Set the language file
$ca->assign('LANG2', $_ADDONLANG);

$ca->output();
