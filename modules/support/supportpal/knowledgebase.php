<?php
/**
 * SupportPal Integration Knowledgebase Module
 *
 * Handles the loading and displaying of the SupportPal knowledgebase articles and categories in WHMCS
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

define("CLIENTAREA", true);

// Recaptcha keys - don't change these
$recaptcha_public_key = '6LeZrcgSAAAAAAfNCs5DGNubKsbwQjM3QQLaYSPE';
$recaptcha_private_key = '6LeZrcgSAAAAAFj43im5C9uzv3GhDqlnqbs0QpKm';

// Files required
require_once("modules/support/supportpal/functions.php");
include("modules/support/supportpal/lang/english.php");
require("modules/support/supportpal/3rd/recaptcha/recaptchalib.php");

$ca = new WHMCS_ClientArea;
$ca->initPage();

// Setup page title and breadcrumb
$pageTitle = Lang::trans('knowledgebasetitle');
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('knowledgebase.php', Lang::trans('knowledgebasetitle'));

// If we're looking for a specific announcement or category, fetch the IDs
$id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : null;
$categoryId = isset($_REQUEST["category"]) ? $_REQUEST["category"] : null;
$tagId = isset($_REQUEST["tag"]) ? $_REQUEST["tag"] : null;
$page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 1;

// 10 articles per page
$perPage = 10;

// Get core settings
$settings = getCoreSettings();

// See if a knowledgebase ID has been set
$knowledgebaseType = Capsule::table('mod_supportpal')
    ->where('name', 'knowledgebase_id')
    ->first(array('value'));
$knowledgebaseType = !empty($knowledgebaseType) ? $knowledgebaseType->value : null;

// Get type ID
$typeId = getTypeId('knowledgebase', $knowledgebaseType, $settings['default_brand']);

// Initialise purifier
$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);

if (isset($id)) {
    // Specific article
    // Set template file for the page
    $ca->setTemplate('../supportpal/knowledgebase_view');

    // Get article
    $data = array(
        'type_id'         => $typeId,
        'increment_views' => 1
    );
    $response = json_decode(_doAPICall('selfservice/article/' . $id, $data), true);

    // Verify the article is in the same type.
    if ($response['status'] == 'success'
        && array_search($typeId, array_column($response['data']['types'], 'id')) !== false) {
        // Set the article title
        $pageTitle = $response['data']['title'];

        // Add to breadcrumb
        $ca->addToBreadCrumb('', htmlentities($pageTitle));

        // Convert the time
        $response['data']['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $response['data']['created_at']);

        // Purify the article text
        $response['data']['text'] = $purifier->purify($response['data']['text']);

        $articleData['article'] = $response['data'];

        // Get self-service settings
        $response = json_decode(_doAPICall('selfservice/settings'), true);
        if ($response['status'] == 'success') {
            $ca->assign('settings', $selfserviceSettings = $response['data']);

            // Get a captcha in case they decide to post a comment
            if ($selfserviceSettings['comment_captcha'] == '2' || (!$ca->isLoggedIn() && $selfserviceSettings['comment_captcha'] == '1')) {
                $ca->assign('recaptcha', recaptcha_get_html($recaptcha_public_key, null));
            }
        }

        // Check if logged in to WHMCS
        $ca->assign('userLoggedIn', $ca->isLoggedIn());

        // Check if a user account exists at the help desk
        $ca->assign('helpdeskUser', $helpdeskUser = getHelpdeskAccount($ca->getUserID()));

        // Do we have a comment to submit?
        if ($_POST && isset($_POST["comment_message"])) {
            // Variable set up to see if the comment should be submitted, defaulted to true
            $isValid = true;

            // If captcha is set, check to see the answer is correct
            if ($selfserviceSettings['comment_captcha'] == '2' ||
                (!$ca->isLoggedIn() && $selfserviceSettings['comment_captcha'] == '1')) {
                $response = recaptcha_check_answer($recaptcha_private_key, $_SERVER["REMOTE_ADDR"],
                    $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                $isValid = $response->is_valid;
            }

            // Should the comment be submitted?
            if ($isValid) {
                // Add article ID and type ID to data
                $comment = $_POST;
                $comment['comment_item_id'] = $id;
                $comment['comment_type_id'] = $typeId;

                // Try to submit new comment
                $commentStatus = postComment($comment, $selfserviceSettings, $helpdeskUser);
            } else {
                // Captcha was wrong, show an error
                $commentStatus = -1;
            }

            if ($commentStatus == -1 || $commentStatus == -2) {
                // Restore the form values
                if (isset($_POST['comment_parent_id']) && is_numeric($_POST['comment_parent_id'])) {
                    $ca->assign('comment_parent_id', $_POST['comment_parent_id']);
                }
                $ca->assign('orig_comment_message', isset($_POST['comment_message']) ? $_POST['comment_message'] : null);
                $ca->assign('orig_comment_email_author', isset($_POST['comment_email_author']) ? $_POST['comment_email_author'] : null);
            }

            // Send to view
            $ca->assign('orig_comment_name', isset($_POST['comment_name']) ? $_POST['comment_name'] : '');
            $ca->assign('comment_status', $commentStatus);
        }

        // Get comments
        $data = array(
            'article_id'      => $id,
            'type_id'         => $typeId,
            'status'          => 1,
            'order_direction' => 'asc'
        );
        $articleData['comments'] = fetchComments($data, $settings);

        // Purify each comment
        foreach ($articleData['comments'] as &$comment) {
            $comment['text'] = $purifier->purify($comment['text']);
        }
    } else {
        $articleData = array();
    }
 } else if (isset($categoryId) || isset($tagId)) {
    // Specific category or tag
    // Set template file for the page
    $ca->setTemplate('../supportpal/knowledgebase_category');

    $subcategoryData = array();

    if (isset($categoryId)) {
        // Get category
        $response = json_decode(_doAPICall('selfservice/category/' . $categoryId), true);

        // Only continue if the category has the same self-service type and is public
        if ($response['status'] == 'success' && $response['data']['type']['id'] == $typeId
            && $response['data']['public'] && $response['data']['parent_public']) {
            // Set the name of the category
            $pageTitle = $response['data']['name'];

            // Add to breadcrumb
            $ca->addToBreadCrumb('', htmlentities($response['data']['name']));

            // Get children categories
            $data = array(
                'type_id'      => $typeId,
                'parent_id'    => $categoryId,
                'public'       => 1,
                'order_column' => 'name'
            );
            $response = json_decode(_doAPICall('selfservice/category', $data), true);

            if ($response['status'] == 'success') {
                $subcategoryData = $response['data'];
            }

            foreach($subcategoryData AS $key => $category) {
                // Fetch 3 random articles per category
                $data = array(
                    'type_id'      => $typeId,
                    'category_id'  => $category['id'],
                    'include_subcategories' => 1,
                    'published'    => 1,
                    'protected'    => $ca->isLoggedIn() ? null : 0,
                    'limit'        => 3,
                    'order_column' => 'random'
                );
                $response = json_decode(_doAPICall('selfservice/article', $data), true);

                // Save articles and count under category
                $subcategoryData[$key]['articles'] = $response['data'];
                $subcategoryData[$key]['count'] = $response['count'];
            }
        }
    }

    // Get announcements for category/tag
    $data = array(
        'type_id'         => $typeId,
        'category_id'     => $categoryId,
        'tag_id'          => $tagId,
        'published'       => 1,
        'protected'       => $ca->isLoggedIn() ? null : 0,
        'start'           => (($page - 1) * $perPage) + 1,
        'limit'           => $perPage,
        'order_column'    => 'views',
        'order_direction' => 'desc'
    );

    $response = json_decode(_doAPICall('selfservice/article', $data), true);

    $articleData = array();
    if ($response['status'] == 'success') {
        foreach ($response['data'] AS $value) {
            // Check if any of the categories are public
            $public = false;
            foreach ($value['categories'] AS $category) {
                // If we have a specific category, just check that one
                if (isset($categoryId) && $categoryId != $category['id']) {
                    continue;
                }

                if ($category['public'] && $category['parent_public']) {
                    $public = true;
                    break;
                }
            }

            // Add to list if at least one category is public
            if ($public) {
                // Convert the time
                $value['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $value['created_at']);

                $articleData[] = $value;
            }

            // Save number of articles
            $articleCount = $response['count'];
        }
    }

    // Create final data array
    $articleData = array(
        'categories' => $subcategoryData,
        'articles'   => $articleData
    );

    if (isset($tagId)) {
        // Fetch tag
        $response = json_decode(_doAPICall('selfservice/tag/' . $tagId), true);

        if ($response['status'] == 'success') {
            // Set the name of the tag
            $pageTitle = $response['data']['name'];

            $ca->addToBreadCrumb('', htmlentities($response['data']['name']));

            $ca->assign('tag', htmlentities(getSlug($response['data']['name'])));
        }
    }

    // Pagination
    $pages = ceil($articleCount / $perPage);
    $ca->assign('totalPages', $pages);
    $ca->assign('currentPage', $page);
} else {
    // All announcements, specific category or tag
    // Set template file for the page
    $ca->setTemplate('../supportpal/knowledgebase');

    $data = array(
        'type_id'      => $typeId,
        'parent_id'    => 0,
        'public'       => 1,
        'order_column' => 'name'
    );

    // Get all categories
    $response = json_decode(_doAPICall('selfservice/category', $data), true);

    $categoryData = array();
    if ($response['status'] == 'success') {
        $categoryData = $response['data'];
    }

    foreach($categoryData AS $key => $category) {
        // Fetch 3 random articles per category
        $data = array(
            'type_id'      => $typeId,
            'category_id'  => $category['id'],
            'include_subcategories' => 1,
            'published'    => 1,
            'protected'    => $ca->isLoggedIn() ? null : 0,
            'limit'        => 3,
            'order_column' => 'random'
        );
        $response = json_decode(_doAPICall('selfservice/article', $data), true);

        // Save articles and count under category
        $categoryData[$key]['articles'] = $response['data'];
        $categoryData[$key]['count'] = $response['count'];
    }

    $data = array(
        'type_id'         => $typeId,
        'include_subcategories' => 1,
        'published'       => 1,
        'protected'       => $ca->isLoggedIn() ? null : 0,
        'limit'           => 10,
        'order_column'    => 'views',
        'order_direction' => 'desc'
    );

    // Get all announcements
    $response = json_decode(_doAPICall('selfservice/article', $data), true);

    $articleData = array();
    if ($response['status'] == 'success') {
        foreach ($response['data'] AS $value) {
            // Only want 5
            if (count($articleData) >= 5) {
                break;
            }

            // Check if any of the categories are public
            $public = false;
            foreach ($value['categories'] AS $category) {
                if ($category['public'] && $category['parent_public']) {
                    $public = true;
                }
            }

            // Add to list if at least one category is public
            if ($public) {
                // Convert the time
                $value['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $value['created_at']);

                $articleData[] = $value;
            }
        }
    }

    // Create final data array
    $articleData = array(
        'categories'      => $categoryData,
        'popularArticles' => $articleData
    );

    if (isset($tagId)) {
        // Fetch tag
        $response = json_decode(_doAPICall('selfservice/tag/' . $tagId), true);

        if ($response['status'] == 'success') {
            // Set the name of the tag
            $pageTitle = $response['data']['name'];

            $ca->addToBreadCrumb('', htmlentities($response['data']['name']));

            $ca->assign('tag', htmlentities(getSlug($response['data']['name'])));
        }
    }
}

// Set page title
$ca->setPageTitle(htmlentities($pageTitle));

// Load the data in
$ca->assign('data', recursive_htmlentities($articleData, array('text')));

// Sidebar menu - not on article page
if (isset($articleData['categories'])) {
    Menu::addContext();

    // Show the sidebar with announcements active
    $secondarySidebar = Menu::secondarySidebar('announcementList');

    // So long as we have one category
    if (count($articleData['categories'])) {
        // Add a new menu above for categories
        $secondarySidebar->addChild('categories', array(
            'label' => Lang::trans('knowledgebasecategories'),
            'uri'   => '#',
            'icon'  => 'fa-folder-open-o',
        ));

        // Retrieve the panel we just created
        $menuPanel = $secondarySidebar->getChild('categories');

        // Add each category link
        foreach ($articleData['categories'] AS $category) {
            $menuPanel->addChild('category-' . htmlentities($category['name']), [
                'uri'   => 'knowledgebase.php?category=' . $category['id'],
                'label' => htmlentities($category['name']) . ($category['count'] > 0 ? '<span class="badge">' . $category['count'] . '</span>' : '')
            ]);
        }
    }
}

// Load the SupportPal URL
$baseUrl = Capsule::table('mod_supportpal')
    ->where('name', 'base_url')
    ->first(array('value'));
$ca->assign('supportpalUrl', !empty($baseUrl) ? $baseUrl->value : '');

// Set the language file
$ca->assign('LANG2', $_ADDONLANG);

$ca->output();
