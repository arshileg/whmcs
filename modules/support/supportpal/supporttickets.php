<?php
/**
 * SupportPal Integration Support Tickets Module
 *
 * Handles the loading and displaying of the grid of SupportPal support tickets in WHMCS
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

define("CLIENTAREA", true);

// Files required
require_once("modules/support/supportpal/functions.php");
include("modules/support/supportpal/lang/english.php");

$ca = new WHMCS_ClientArea;
$ca->initPage();

// Require to be logged in
$ca->requireLogin();

// Setup page title and breadcrumb
$ca->setPageTitle(Lang::trans('supportticketspagetitle'));
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('clientarea.php', Lang::trans('clientareatitle'));
$ca->addToBreadCrumb('supporttickets.php', Lang::trans('supportticketspagetitle'));

// Start the work!
if ($ca->isLoggedIn()) {

    // Set template file for the page
    $ca->setTemplate('../supportpal/supporttickets');

    // Possible ordering options
    $orderByOptions = array( 'number', 'subject', 'created_at', 'updated_at' );

	// Get the page and number of rows to show, and order column and direction
    $page = isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 1;
	$perPage = isset($_GET['rows']) && is_numeric($_GET['rows']) ? $_GET['rows'] : 10;
    $orderBy = isset($_GET['order_by']) && in_array($_GET['order_by'], $orderByOptions) ? $_GET['order_by'] : 'updated_at';
    $orderDirection = isset($_GET['direction']) && ($_GET['direction'] == 'asc' || $_GET['direction'] == 'desc')
        ? $_GET['direction'] : 'desc';

	// Get the user in the help desk
    $helpdeskUser = getHelpdeskAccount($ca->getUserID());

    // Initialise
    $ticketData = array();
    $ticketCount = 0;

    // Only bother if the user exists in AD
    if (isset($helpdeskUser)) {
        // Get core settings
        $settings = getCoreSettings();

        // Get departments
        $departments = array();
        $response = json_decode(_doAPICall('ticket/department'), true);
        if ($response['status'] == 'success') {
            foreach ($response['data'] AS $department) {
                $departments[$department['id']] = recursive_htmlentities($department);
            }
        }

        // Get statuses
        $statuses = array();
        $response = json_decode(_doAPICall('ticket/status'), true);
        if ($response['status'] == 'success') {
            foreach ($response['data'] AS $status) {
                $statuses[$status['id']] = recursive_htmlentities($status);
            }
        }

        // Get priorities
        $priorities = array();
        $response = json_decode(_doAPICall('ticket/priority'), true);
        if ($response['status'] == 'success') {
            foreach ($response['data'] AS $priority) {
                $priorities[$priority['id']] = recursive_htmlentities($priority);
            }
        }

        // Get tickets
        $data = array(
            'user'            => $helpdeskUser['id'],
            'internal'        => 0,
            'start'           => (($page - 1) * $perPage) + 1,
            'limit'           => $perPage,
            'order_column'    => $orderBy,
            'order_direction' => $orderDirection
        );
        $response = json_decode(_doAPICall('ticket/ticket', $data), true);
        if ($response['status'] == 'success') {
            $ticketData = $response['data'];
            $ticketCount = $response['count'];
        }

        // Set fields with names
        foreach ($ticketData AS &$ticket) {
            // Encode ticket number and subject
            $ticket['number'] = htmlentities($ticket['number']);
            $ticket['subject'] = htmlentities($ticket['subject']);

            // Set department name
            $ticket['department'] = $departments[$ticket['department_id']]['name'];

            // Set status name and colour
            $ticket['status'] = $statuses[$ticket['status_id']]['name'];
            $ticket['status_colour'] = $statuses[$ticket['status_id']]['colour'];

            // Set priority name and colour
            $ticket['priority'] = $priorities[$ticket['priority_id']]['name'];
            $ticket['priority_colour'] = $priorities[$ticket['priority_id']]['colour'];

            // Convert the time
            $ticket['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $ticket['created_at']);
            $ticket['updated_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $ticket['updated_at']);

            // Get the token for the URL
            $ticket['token'] = sha1($ticket['id'] . '-' . $ticket['number']);
        }
    }

    // Pagination
    $pages = ceil($ticketCount / $perPage);
    $ca->assign('totalRecords', $ticketCount);
    $ca->assign('totalPages', $pages > 0 ? $pages : 1);
    $ca->assign('currentPage', $page);
    $ca->assign('rows', $perPage);

    // Ordering
    $ca->assign('orderBy', $orderBy);
    $ca->assign('orderDirection', $orderDirection);

    // Send data to view
    $ca->assign('data', $ticketData);
}

// Set the language file
$ca->assign('LANG2', $_ADDONLANG);

$ca->output();

