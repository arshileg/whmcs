<?php
/**
 * SupportPal Integration Functions
 *
 * The core functions used in the WHMCS integration.
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

/**
 * Interfaces with the SupportPal API via cURL. Function handles, GET, POST, PUT & DELETE
 *
 * @param   string  $apiCall  The API function that we want to use
 * @param   array   $data     The data for the API function parameters
 * @param   string  $method   If we're using GET, POST, PUT or DELETE, uses GET by default
 * @return  mixed
 */
function _doAPICall($apiCall, $data = array(), $method = 'GET', $details = array())
{
    // Get base URL
    if (isset($details['base_url'])) {
        $baseUrl = $details['base_url'];
    } else {
        $baseUrl = Capsule::table('mod_supportpal')
            ->where('name', 'base_url')
            ->first(array('value'));
        $baseUrl = !empty($baseUrl) ? $baseUrl->value : '';
    }

    // Setup API URL
    $apiUrl = rtrim($baseUrl, '/') . "/api/";

    // Get API key
    if (isset($details['api_token'])) {
        $apiToken = $details['api_token'];
    } else {
        $apiToken = Capsule::table('mod_supportpal')
            ->where('name', 'api_token')
            ->first(array('value'));
        $apiToken = !empty($apiToken) ? html_entity_decode($apiToken->value) : '';
    }

    // Start cURL
    $c = curl_init();
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($c, CURLOPT_USERPWD, $apiToken . ":x");
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

    // Start building the URL
    $apiCall = $apiUrl . $apiCall;

    // Add brand to data by default
    if (isset($details['brand_id'])) {
        $data['brand_id'] = $details['brand_id'];
    } else {
        $brandId = Capsule::table('mod_supportpal')
            ->where('name', 'brand_id')
            ->first(array('value'));
        $data['brand_id'] = !empty($brandId) ? $brandId->value : 1;
    }

    // We need to set this so the WHMCS plugin doesn't do a call back
    $data['whmcs'] = true;

    // Check what type of API call we are making
    if ($method == 'GET') {
        // Add the array of data to the URL
        if (is_array($data)) {
            $apiCall .= "?";
            foreach ($data as $key => $value) {
                if (isset($value)) {
                    if (is_array($value)) {
                        // In case we have an array for a value, add each item of array
                        foreach ($value as $arrayKey => $arrayValue) {
                            $apiCall .= $key . '[' . $arrayKey . ']' . "=" . $arrayValue . "&";
                        }
                    } else {
                        $apiCall .= $key . "=" . $value . "&";
                    }
                }
            }

            // Remove the final &
            $apiCall = rtrim($apiCall, '&');
        }
    } else if ($method == 'PUT' || $method == 'DELETE') {
        // PUT and DELETE require an $id variable to be appended to the URL
        if (isset($data['id'])) {
            $apiCall .= "/" . $data['id'];
        }

        // Setup the remainder of the cURL request
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($c, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: ' . $method));
    } else {
        // Setup the remainder of the cURL request
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
    }

    // Set the URL
    curl_setopt($c, CURLOPT_URL, $apiCall);

    // Execute the API call and return the response
    $result = curl_exec($c);

    if (curl_error($c)) {
        // Check if there was an error
        $result = json_encode(array(
            'status'  => 'error',
            'message' => curl_error($c)
        ));
    } elseif (! is_array(json_decode($result, true))) {
        // Output not correct, show error with curl error message
        $result = json_encode(array(
            'status'  => 'error',
            'message' => $result
        ));
    }
    
    // Close curl connection
    curl_close($c);

    // Return the results of the API call
    return $result;
}

/**
 * Converts a string to a slug
 *
 * @param  string  $s  String to convert
 * @return string
 */
function getSlug($s) {
    // credit to http://www.papersoup.org/post/seo-friendly-url-slugs-php
    $parser = "-";
    $retSlug = preg_replace('/[^A-Za-z0-9 +-]+/', '', $s); // alphanumeric, spaces, dashes, plus sign etc
    $retSlug = preg_replace('/ /', $parser, $retSlug); // convert spaces into our style
    return strtolower($retSlug);
}

/**
 * Get the core settings, mainly for the date and time format
 *
 * @return array
 */
function getCoreSettings()
{
    $settings = array();
    $response = json_decode(_doAPICall('core/settings'), true);
    if ($response['status'] == 'success') {
        $settings = $response['data'];
    } else {
        $settings['date_format'] = 'jS F Y';
        $settings['time_format'] = 'h:i A';
    }

    return $settings;
}

/**
 * Get self-service type ID
 *
 * @param  string $name The slug
 * @param  int|null $typeId Can define a set ID to use
 * @param  int|null $brandId
 * @return int
 */
function getTypeId($name, $typeId = null, $brandId = null)
{
    if (empty($typeId)) {
        $data = array();
        if (isset($brandId)) {
            $data['brand_id'] = $brandId;
        }

        $response = json_decode(_doAPICall('selfservice/type', $data), true);
        if ($response['status'] == 'success') {
            foreach ($response['data'] AS $type) {
                if ($type['slug'] == $name) {
                    $typeId = $type['id'];
                    break;
                }
            }
        }
    }

    return $typeId;
}

/**
 * Fetch comments on a given article
 * @param  array       $data
 * @return array|null
 */
function fetchComments($data, $settings)
{
    $response = json_decode(_doAPICall('selfservice/comment', $data), true);

    if ($response['status'] == 'success') {
        // Store first batch of comments
        $comments = $response['data'];

        // Check if we have more to fetch, if so, fetch all
        $total = $response['count'];
        $start = 1;
        while ($start + 50 < $total) {
            $data['start'] = $start = $start + 50;
            $response = json_decode(_doAPICall('selfservice/comment', $data), true);
            if ($response['status'] == 'success') {
                $comments = array_merge($comments, $response['data']);
            }
        }

        // Build a tree from comments
        return buildCommentTree($comments, $settings);
    }

    return null;
}

/**
 * Builds the tree for comments recursively
 *
 * @param  array $comments
 * @param  int   $commentId
 * @return array
 */
function buildCommentTree($comments, $settings, $commentId = null)
{
    $finalComments = array();

    foreach ($comments AS $key => $value) {
        if ($value['root_parent_id'] == $commentId) {
            // Set a timestamp
            $comments[$key]['created_at'] = date($settings['date_format'] . ' ' . $settings['time_format'], $value['created_at']);

            // Add to the returning array
            $finalComments[] = $comments[$key];

            // Find any children and add them to the array at this point
            $finalComments = array_merge($finalComments, buildCommentTree($comments, $settings, $value['id']));
        }
    }

    return $finalComments;
}

/**
 * Post a comment
 *
 * @param  array       $comment
 * @param  array       $selfserviceSettings
 * @param  array|null  $helpdeskUser
 * @return int
 */
function postComment($comment, $selfserviceSettings, $helpdeskUser = null)
{
    // Setup the POST data array
    $data = array(
        "text"         => $comment['comment_message'],
        "article_id"   => $comment['comment_item_id'],
        "type_id"      => $comment['comment_type_id'],
        "parent_id"    => !empty($comment['comment_parent_id']) ? $comment['comment_parent_id'] : null,
        "status"       =>
            (isset($selfserviceSettings['comment_moderation']) && $selfserviceSettings['comment_moderation'] == '0') ? 1 : 0,
        "notify_reply" =>
            (isset($comment['comment_email_author']) && $comment['comment_email_author'] == 'on') ? 1 : 0
    );

    // If we have an account or need to use the name
    if (isset($helpdeskUser)) {
        $data['author_id'] = $helpdeskUser['id'];
    } else {
        $data['name'] = $comment['comment_name'];
    }

    // Send data to API
    $commentData = json_decode(_doAPICall('selfservice/comment', $data, 'POST'), true);

    // Save the status
    if ($commentData['status'] == 'success') {
        if ($commentData['data']['status'] == 0) {
            // Success, but in moderation
            return 0;
        } else {
            // Success
            return 1;
        }
    } else {
        // Something went wrong
        return -2;
    }
}

/**
 * Get logged in user's help desk account
 *
 * @param   int          $id     The user ID
 * @param   string|null  $email  Alternatively provide the email directly
 * @return  array|null
 */
function getHelpdeskAccount($id = 0, $email = null)
{
    if (!isset($email) && $id > 0) {
        // Get WHMCS user account
        $user = Capsule::table('tblclients')->where('id', $id)->first();
        $email = $user->email;
    }

    if (isset($email)) {
        // Check if user exists
        $data = array('email' => $email);
        $response = json_decode(_doAPICall('user/user', $data), true);

        if ($response['status'] == 'success' && $response['count'] > 0) {
            return $response['data'][0];
        }
    }

    return null;
}

/**
 * Updates the custom field options if the related service field is set and is present
 * in the custom fields array.
 *
 * @param   array  $customFields
 * @param   int    $userId
 * @return  array
 */
function updateRelatedServiceField(array $customFields, $userId = 0)
{
    // Check if we have the related product/service field
    $relatedField = Capsule::table('mod_supportpal')
        ->where('name', 'related_service_field')
        ->first(array('value'));

    // If we do, and the custom field is visible in our array, update the options
    if (!empty($relatedField) && !empty($relatedField->value)
        && isset($customFields[$relatedField->value])) {
        // Fetch client's products, ordered by product name and domain
        $products = Capsule::table('tblhosting')
            ->join('tblproducts', 'tblhosting.packageid', '=', 'tblproducts.id')
            ->where('userid', $userId)
            ->orderBy('tblproducts.name')
            ->orderBy('domain')
            ->get(array('tblhosting.id', 'tblproducts.name', 'domain', 'domainstatus'));

        // Do we have any products?
        if (!empty($products)) {
            // Convert to formatted array for UI
            $productOptions = array();
            foreach ($products as $product) {
                $productOptions[] = [
                    'id' => $product->id,
                    'value' => $product->name
                        . (!empty($product->domain) ? ' - ' . $product->domain : '')
                        . ' (' . $product->domainstatus . ')'
                ];
            }

            // Update options to our product list
            $customFields[$relatedField->value]['options'] = $productOptions;
        } else {
            // No products, so don't show custom field
            unset($customFields[$relatedField->value]);
        }
    }

    return $customFields;
}

/*
 * Matches each symbol of PHP date format standard
 * with jQuery equivalent codeword
 * @author Tristan Jahier
 */
function dateformat_PHP_to_jQueryUI($php_format)
{
    $SYMBOLS_MATCHING = array(
        // Day
        'd' => 'dd',
        'D' => 'D',
        'j' => 'd',
        'l' => 'DD',
        'N' => '',
        'S' => '',
        'w' => '',
        'z' => 'o',
        // Week
        'W' => '',
        // Month
        'F' => 'MM',
        'm' => 'mm',
        'M' => 'M',
        'n' => 'm',
        't' => '',
        // Year
        'L' => '',
        'o' => '',
        'Y' => 'yy',
        'y' => 'y',
        // Time
        'a' => '',
        'A' => '',
        'B' => '',
        'g' => '',
        'G' => '',
        'h' => '',
        'H' => '',
        'i' => '',
        's' => '',
        'u' => ''
    );
    $jqueryui_format = "";
    $escaping = false;
    for($i = 0; $i < strlen($php_format); $i++)
    {
        $char = $php_format[$i];
        if($char === '\\') // PHP date format escaping character
        {
            $i++;
            if($escaping) $jqueryui_format .= $php_format[$i];
            else $jqueryui_format .= '\'' . $php_format[$i];
            $escaping = true;
        }
        else
        {
            if($escaping) { $jqueryui_format .= "'"; $escaping = false; }
            if(isset($SYMBOLS_MATCHING[$char]))
                $jqueryui_format .= $SYMBOLS_MATCHING[$char];
            else
                $jqueryui_format .= $char;
        }
    }
    return $jqueryui_format;
}

/**
 * Download a file from a give URL in chunks
 *
 * @return
 */
function downloadAttachment($id)
{
    // Get attachment details
    $response = json_decode(_doAPICall('ticket/attachment/' . $id), true);

    if ($response['status'] == 'success') {
        // Generate download URL
        $baseUrl = Capsule::table('mod_supportpal')
            ->where('name', 'base_url')
            ->first(array('value'));
        $baseUrl = !empty($baseUrl) ? $baseUrl->value : '';
        $url = rtrim($baseUrl, '/') . "/api/ticket/attachment/" . $id . "/download";

        // Get API token
        $apiToken = Capsule::table('mod_supportpal')
            ->where('name', 'api_token')
            ->first(array('value'));
        $apiToken = !empty($apiToken) ? html_entity_decode($apiToken->value) : '';

        // Get size of file
        $size = getSize($url, $apiToken);

        // Output headers to force download
        header('Content-Description: File Transfer');
        header("Content-Type: application/octet-stream");

        // Change filename so it can't be found on the system
        header('Content-Disposition: attachment; filename=' . $response['data']['original_name']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $size);

        // Fetch file
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $apiToken . ":x");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);

        $isJson = !preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/', preg_replace('/"(\\.|[^"\\\\])*"/', '', $result));
        if ($isJson && ($json = json_decode($result)) !== false && isset($json->status) && $json->status == 'error') {
            die('An error occurred while downloading the file.');
        } else {
            // Output headers to force download
            header('Content-Description: File Transfer');
            header("Content-Type: application/octet-stream");

            // Change filename so it can't be found on the system
            header('Content-Disposition: attachment; filename=' . $response['data']['original_name']);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . $size);

            print $result;
            exit;
        }
    }
}

/**
 * Get total size of file
 *
 * @param  string  $url
 * @param  string  $apiToken
 * @return  int
 */
function getSize($url, $apiToken)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $apiToken . ":x");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_exec($ch);
    $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    curl_close($ch);

    return intval($size);
}

/**
 * Get the current domain with SSL considered
 *
 * @return string
 */
function getDomain()
{
    // If it's HTTP or HTTPS
    $domain = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $domain .= "s";
    }
    $domain .= "://";

    // Build URL
    if ($_SERVER["SERVER_PORT"] != "80") {
        $domain .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
    } else {
        $domain .= $_SERVER["SERVER_NAME"];
    }

    return $domain;
}

/**
 * Apply htmlentities recursively on an array.
 *
 * @param  array  $array
 * @param  array  $exclude
 * @return array
 */
function recursive_htmlentities(array $array, array $exclude = array())
{
    array_walk_recursive($array, function (&$value, $key) use ($exclude) {
        if (! in_array($key, $exclude)) {
            $value = htmlentities($value);
        }
    });

    return $array;
}