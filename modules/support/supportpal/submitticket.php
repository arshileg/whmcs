<?php
/**
 * SupportPal Integration Submit Ticket Module
 *
 * Handles the submission of a SupportPal support ticket from WHMCS
 *
 * @category    SupportPal
 * @package     SupportPal Integration
 * @copyright   Copyright (c) 2016 SupportPal
 * @version     3.2.0
 * @since       File available since version 1.0.0
 */

use WHMCS\Database\Capsule;

define("CLIENTAREA", true);

// Files required
require_once("modules/support/supportpal/functions.php");
include("modules/support/supportpal/lang/english.php");
require("modules/support/supportpal/3rd/recaptcha/recaptchalib.php");

$ca = new WHMCS_ClientArea;
$ca->initPage();

// Require to be logged in
$ca->requireLogin();

// Setup page title and breadcrumb
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('clientarea.php', Lang::trans('clientareatitle'));
$ca->addToBreadCrumb('supporttickets.php', Lang::trans('supportticketspagetitle'));
$ca->addToBreadCrumb('submitticket.php', Lang::trans('supportticketssubmitticket'));

// If we've declared what step we're on then use that, else it's step 1
$step = isset($_GET['step'])? $_GET['step'] : 1;

// Set the page title depending on what page we're on
if ($step == 1 || $step == 2) {
    $pageTitle = $_LANG['supportticketssubmitticket'];
} else {
    $pageTitle = $_ADDONLANG['tickets_submitted'];
}

// Set page title
$ca->setPageTitle($pageTitle);

// Start the work!
if ($ca->isLoggedIn()) {

    // Get WHMCS user account
    $user = Capsule::table('tblclients')->where('id', $ca->getUserID())->first();

    if ($step == 1) {
        // Set template file for the page
        $ca->setTemplate('../supportpal/supportticketsubmit-stepone');

        // Get the departments
        $data = array(
            'parent_id' => 0,
            'public'    => 1
        );
        $response = json_decode(_doAPICall('ticket/department', $data), true);

        $departmentData = array();
        if ($response['status'] == 'success') {
            $departmentData = $response['data'];
        }

        // Load the departments in
        $ca->assign('data', recursive_htmlentities($departmentData));
    } else if ($step == 2) {

        // Get core settings
        $coreSettings = getCoreSettings();

        // Get ticket settings
        $response = json_decode(_doAPICall('ticket/settings'), true);
        if ($response['status'] == 'success') {
            $settings = $response['data'];

            // Add date format
            $settings['date_format'] = dateformat_PHP_to_jQueryUI($coreSettings['date_format']);

            $ca->assign('settings', $settings);
        }

        // Get web channel settings
        $response = json_decode(_doAPICall('ticket/channel/web/settings'), true);
        if ($response['status'] == 'success') {
            $channelSettings = $response['data'];
            if ($channelSettings['show_captcha'] == '2') {
                $ca->assign('recaptcha', recaptcha_get_html($recaptcha_public_key, null));
            }
        }

        // Get custom fields
        $customFields = array();
        $data = array(
            'department_id' => $_GET['deptid'],
            'order_column'  => 'order'
        );
        $response = json_decode(_doAPICall('ticket/customfield', $data), true);
        if ($response['status'] == 'success') {
            foreach ($response['data'] AS $customfield) {
                $customFields[$customfield['id']] = $customfield;
            }
        }

        // Time to work your magic
        if (isset($_GET["submit"]) && ($_GET["submit"] == 1) && isset($_POST["message"])) {

            // Variable set up to see if the ticket should be submitted, defaulted to true
            $isValid = true;

            // If captcha is set, check to see the answer is correct
            if ($channelSettings['show_captcha'] == '2') {
                $response = recaptcha_check_answer($recaptcha_private_key, $_SERVER["REMOTE_ADDR"],
                    $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
                $isValid = $response->is_valid;
            }

            if ($isValid) {
                // Check if user is in help desk
                $helpdeskUser = getHelpdeskAccount(null, $user->email);

                // Get brand ID
                $brandId = Capsule::table('mod_supportpal')
                    ->where('name', 'brand_id')
                    ->first(array('value'));
                $brandId = !empty($brandId) ? $brandId->value : 1;

                // Start building data for ticket
                $data = array(
                    'department'      => $_POST['deptid'],
                    'brand'           => $brandId,
                    'status'          => $settings['default_open_status'],
                    'priority'        => $_POST['urgency'],
                    'subject'         => html_entity_decode($_POST['subject'], ENT_QUOTES | ENT_HTML401),
                    'text'            => nl2br($_POST['message']),
                    'user_ip_address' => $_SERVER['REMOTE_ADDR']
                );

                if (isset($helpdeskUser)) {
                    // User ID
                    $data['user'] = $helpdeskUser['id'];
                } else {
                    // User details
                    $data['user_firstname'] = $user->firstname;
                    $data['user_lastname'] = $user->lastname;
                    $data['user_email'] = $user->email;
                    if (!empty($user->companyname)) {
                        $data['user_organisation'] = $user->companyname;
                    }
                }

                $data['customfield'] = array();

                // Check for custom fields
                foreach ($_POST AS $key => $value) {
                    // Explode key
                    $exploded = explode("_", $key);

                    // Check if it starts with cf_
                    if (isset($exploded[0]) && ($exploded[0] == "cf") && isset($exploded[1]) && is_numeric($exploded[1])) {
                        $data['customfield'][$exploded[1]] = html_entity_decode($value, ENT_QUOTES | ENT_HTML401);
                    }
                }

                // Add attachments
                if (isset($_FILES['attachments'])) {
                    $data['attachment'] = array();

                    // Add each attachment to data array
                    foreach ($_FILES['attachments']['tmp_name'] as $key => $value) {
                        if (!empty($value)) {
                            $data['attachment'][] = array(
                                'filename' => $_FILES['attachments']['name'][$key],
                                'contents' => base64_encode(file_get_contents($value))
                            );
                        }
                    }
                }

                // Create the ticket
                $response = json_decode(_doAPICall('ticket/ticket', $data, 'POST'), true);

                if ($response['status'] == 'success') {
                    header("LOCATION: submitticket.php?step=confirm&number=" . $response['data']['number']
                        . "&token=" . sha1($response['data']['id'] . '-' . $response['data']['number']));
                    exit;
                }

                // Something went wrong
                $submit_status = 0;

            } else {
                // Invalid captcha
                $submit_status = -1;
            }

            $ca->assign('submit_status', $submit_status);

            // Restore form values
            if (isset($_POST['urgency'])) {
                $ca->assign('priority', $_POST['urgency']);
            }
            if (isset($_POST['subject'])) {
                $ca->assign('subject', $_POST['subject']);
            }
            if (isset($_POST['message'])) {
                $ca->assign('message', $_POST['message']);
            }

            // Restore custom fields
            foreach ($data['customfield'] AS $key => $value) {
                if (isset($customFields[$key])) {
                    $customFields[$key]['value'] = $value;
                }
            }

        }

        // Set template file for the page
        $ca->setTemplate('../supportpal/supportticketsubmit-steptwo');

        // Get department
        $response = json_decode(_doAPICall('ticket/department/' . $_GET["deptid"]), true);

        if ($response['status'] == 'success') {
            // Ensure it is public and a top level department
            if ($response['data']['public'] && is_null($response['data']['parent_id'])) {
                $department = $response['data'];
                $ca->assign('department', $department);
            }
        }

        // Make sure we have a department to continue
        if (!isset($department)) {
            // Redirect back to step 1
            header("LOCATION: submitticket.php?step=1");
            exit;
        }

        // Send name and email to view
        $ca->assign('clientname', $user->firstname . ' ' . $user->lastname);
        $ca->assign('email', $user->email);

        // Get priorities
        $data = array(
            'department_id' => $department['id']
        );
        $response = json_decode(_doAPICall('ticket/priority', $data), true);
        if ($response['status'] == 'success') {
            $ca->assign('priorities', $response['data']);
        }

        // Update custom fields if related service field is set
        $customFields = updateRelatedServiceField($customFields, $ca->getUserID());

        // Encode the custom fields name and description
        foreach ($customFields as &$field) {
            $field['name'] = htmlentities($field['name']);
            $field['description'] = htmlentities($field['description']);
        }

        // Send custom fields to view with keys reset
        $ca->assign('customfields', array_values($customFields));

        // Check if we have article suggestions enabled
        $suggestions = Capsule::table('mod_supportpal')
            ->where('name', 'article_suggestions')
            ->first(array('value'));
        $suggestions = !empty($suggestions) ? $suggestions->value : 0;

        $ca->assign('article_suggestions', $suggestions);
    } else if ($step == "confirm") {
        // Set template file for the page
        $ca->setTemplate('../supportpal/supportticketsubmit-confirm');

        // Load the new tickets number and hash
        $ca->assign('ticket_number', htmlentities($_GET['number']));
        $ca->assign('ticket_token', $_GET['token']);
    }
}

// Load the SupportPal URL
$baseUrl = Capsule::table('mod_supportpal')
    ->where('name', 'base_url')
    ->first(array('value'));
$ca->assign('supportpalUrl', !empty($baseUrl) ? $baseUrl->value : '');

// Set the language file
$ca->assign('LANG2', $_ADDONLANG);

$ca->output();
