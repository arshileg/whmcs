<?php
/**
  * Pay.Ge Payment GateWay
  * (C) 2016 HOSTNODES LTD
  * Web: https://hostnodes.ge
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see http://docs.whmcs.com/Gateway_Module_Meta_Data_Parameters
 *
 * @return array
 */
function payge_MetaData()
{
    return array(
        'DisplayName' => 'Pay.Ge',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCredtCardInput' => false,
        'TokenisedStorage' => false,
    );
}

/**
 * Define gateway configuration options.
 *
 * The fields you define here determine the configuration options that are
 * presented to administrator users when activating and configuring your
 * payment gateway module for use.
 *
 * Supported field types include:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each field type and their possible configuration parameters are
 * provided in the sample function below.
 *
 * @return array
 */
function payge_config()
{
    return array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'Pay.Ge',
        ),
        // Username given by bank
        'user' => array(
            'FriendlyName' => 'Username',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter merchant username',
        ),
        // Password given by bank
        'pass' => array(
            'FriendlyName' => 'Password',
            'Type' => 'text',
            'Size' => '50',
            'Default' => '',
            'Description' => 'Enter merchant password',
        ),
		// Testing mode switcher
		 'test' => array(
            'FriendlyName' => 'TestMode',
            'Type' => 'text',
            'Size' => '50',
            'Default' => 'true',
            'Description' => 'Enter "true" or "false" keywords',
        )
    );
}

/**
 * Payment link.
 *
 * Required by third party payment gateway modules only.
 *
 * Defines the HTML output displayed on an invoice. Typically consists of an
 * HTML form that will take the user to the payment gateway endpoint.
 *
 * @param array $params Payment Gateway Module Parameters
 *
 * @see http://docs.whmcs.com/Payment_Gateway_Module_Parameters
 *
 * @return string
 */
function payge_link($params)
{
    // Gateway Configuration Parameters
    $user = $params['user'];
    $pass = $params['pass'];
	$test = $params['test'];

    // Invoice Parameters
    $invoiceId = $params['invoiceid'];
    $description = $params['description'];
    $amount = $params['amount'];
    $currencyCode = $params['currency'];

    // Client Parameters
    $firstname = $params['clientdetails']['firstname'];
    $lastname = $params['clientdetails']['lastname'];
    $email = $params['clientdetails']['email'];
    $address1 = $params['clientdetails']['address1'];
    $address2 = $params['clientdetails']['address2'];
    $city = $params['clientdetails']['city'];
    $state = $params['clientdetails']['state'];
    $postcode = $params['clientdetails']['postcode'];
    $country = $params['clientdetails']['country'];
    $phone = $params['clientdetails']['phonenumber'];

    // System Parameters
    $companyName = $params['companyname'];
    $systemUrl = $params['systemurl'];
    $returnUrl = $params['returnurl'];
    $langPayNow = $params['langpaynow'];
    $moduleDisplayName = $params['name'];
    $moduleName = $params['paymentmethod'];
    $whmcsVersion = $params['whmcsVersion'];
    
    $amount = $amount * 100; // Tetri to Lari
    
	// Setting required variables for pay.ge system
	$payge['clientname'] = $params['clientdetails']['id']; // Setting user name to user ID
	$payge['amount'] = $amount; // Unit = 1 Tetri
	$payge['currency'] = 'GEL'; // Currency name
    $payge['description'] =  $invoiceId; // Setting transaction description to invoice ID
	$payge['language'] = 'KA'; // Setting gateway language

    // Generating usique ID of transaction

    $uniqueid = uniqid($payge['description'].'-', true);
	
	// Setting testing mode
	
	if($test=='true'){
		
		$testmode = 1;
		
	}elseif($test=='false'){
		
		$testmode = 0;
		
	} else {
		
		$testmode = 0;
		
	}
	

	// Setting check code for transaction
	$check = hash('sha256', $pass.$user.$uniqueid.$payge['amount'].$payge['currency'].$payge['description'].$payge['clientname'].$payge['description'].$payge['language'].$testmode);
	
	// Generating HTML output
    $htmlOutput = '<form method="post" action="https://merchant.pay.ge/pay">';
        
    $htmlOutput .= '<input type="hidden" name="merchant" value="'.$user.'" />';
	$htmlOutput .= '<input type="hidden" name="ordercode" value="'.$uniqueid.'" />';
	$htmlOutput .= '<input type="hidden" name="amount" value="'.$amount.'" />';
    $htmlOutput .= '<input type="hidden" name="currency" value="'.$payge['currency'].'" />';
	$htmlOutput .= '<input type="hidden" name="description" value="'.$payge['description'].'" />';
	$htmlOutput .= '<input type="hidden" name="clientname" value="'.$payge['clientname'].'" />';
    $htmlOutput .= '<input type="hidden" name="customdata" value="'.$payge['description'].'" />';
	$htmlOutput .= '<input type="hidden" name="lng" value="'.$payge['language'].'" />';
	$htmlOutput .= '<input type="hidden" name="testmode" value="'.$testmode.'" />';
	$htmlOutput .= '<input type="hidden" name="successurl" value="https://billing.hostnodes.ge/payge.php?" />';
	$htmlOutput .= '<input type="hidden" name="errorurl" value="https://billing.hostnodes.ge/payge.php?" />';
	$htmlOutput .= '<input type="hidden" name="cancelurl" value="https://billing.hostnodes.ge/payge.php?" />';
	$htmlOutput .= '<input type="hidden" name="callbackurl" value="https://billing.hostnodes.ge/payge_callback.php?" />';	
	$htmlOutput .= '<input type="hidden" name="check" value="'.$check.'" />';
		
    $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
    $htmlOutput .= '</form>';

    return $htmlOutput;
}