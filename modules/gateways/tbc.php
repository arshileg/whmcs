<?php
/**
  * TBC Payment GateWay
  * (C) 2016 HOSTNODES LTD
  * Web: https://hostnodes.ge
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see http://docs.whmcs.com/Gateway_Module_Meta_Data_Parameters
 *
 * @return array
 */
function tbc_MetaData()
{
    return array(
        'DisplayName' => 'TBC Bank',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCredtCardInput' => false,
        'TokenisedStorage' => false,
    );
}

/**
 * Define gateway configuration options.
 *
 * The fields you define here determine the configuration options that are
 * presented to administrator users when activating and configuring your
 * payment gateway module for use.
 *
 * Supported field types include:
 * * text
 * * password
 * * yesno
 * * dropdown
 * * radio
 * * textarea
 *
 * Examples of each field type and their possible configuration parameters are
 * provided in the sample function below.
 *
 * @return array
 */
function tbc_config()
{
    return array(
        // the friendly display name for a payment gateway should be
        // defined here for backwards compatibility
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'TBC Bank',
        ),
        // Certificate key given by the bank
        'secret' => array(
            'FriendlyName' => 'Certificate key',
            'Type' => 'text',
            'Size' => '25',
            'Default' => '',
            'Description' => 'Enter certificate key here',
        ),
        // Certificate file path in your home directory.
        'certpath' => array(
            'FriendlyName' => 'Certificate file path',
            'Type' => 'text',
            'Size' => '50',
            'Default' => $_SERVER['DOCUMENT_ROOT'],
            'Description' => 'Enter certificate path here',
        ),
        // Processor file path in your home directory.
        'processor' => array(
            'FriendlyName' => 'Processor file path',
            'Type' => 'text',
            'Size' => '50',
            'Default' => $_SERVER['DOCUMENT_ROOT'],
            'Description' => 'Enter processor path here',
        ),
		// Security key for cron jobs.
        'cron' => array(
            'FriendlyName' => 'Cron job key',
            'Type' => 'text',
            'Size' => '50',
            'Default' => 'a1b2c3d4e5',
            'Description' => 'Enter cron key here',
        ),
    );
}

/**
 * Payment link.
 *
 * Required by third party payment gateway modules only.
 *
 * Defines the HTML output displayed on an invoice. Typically consists of an
 * HTML form that will take the user to the payment gateway endpoint.
 *
 * @param array $params Payment Gateway Module Parameters
 *
 * @see http://docs.whmcs.com/Payment_Gateway_Module_Parameters
 *
 * @return string
 */
function tbc_link($params)
{
    // Gateway Configuration Parameters
    $secret = $params['secret'];
    $path = $params['certpath'];
    $processor = $params['processor'];

    // Invoice Parameters
    $invoiceId = $params['invoiceid'];
    $description = $params["description"];
    $amount = $params['amount'];
    $currencyCode = $params['currency'];

    // Client Parameters
    $firstname = $params['clientdetails']['firstname'];
    $lastname = $params['clientdetails']['lastname'];
    $email = $params['clientdetails']['email'];
    $address1 = $params['clientdetails']['address1'];
    $address2 = $params['clientdetails']['address2'];
    $city = $params['clientdetails']['city'];
    $state = $params['clientdetails']['state'];
    $postcode = $params['clientdetails']['postcode'];
    $country = $params['clientdetails']['country'];
    $phone = $params['clientdetails']['phonenumber'];

    // System Parameters
    $companyName = $params['companyname'];
    $systemUrl = $params['systemurl'];
    $returnUrl = $params['returnurl'];
    $langPayNow = $params['langpaynow'];
    $moduleDisplayName = $params['name'];
    $moduleName = $params['paymentmethod'];
    $whmcsVersion = $params['whmcsVersion'];
    
    require $processor;
    
    $Payment = new TbcPayProcessor( $path, $secret, $_SERVER['REMOTE_ADDR'] );
    
    $amount = $amount * 100; // Lari to Tetri
    
    $Payment->amount      = $amount; // Unit = 1 Tetri
    $Payment->currency    = 981; // 981 = GEL
    $Payment->description = $invoiceId;
    $Payment->language    = 'GE'; // Interface language
    
    $start = $Payment->sms_start_transaction();

    if ( isset($start['TRANSACTION_ID']) AND !isset($start['error']) ) {
	    $trans_id = $start['TRANSACTION_ID'];
    }
    
    
    
    if ( isset($start['error']) ) {
        
        $htmlOutput = $start['error'];
        
    } else {
        
        session_start();
        
        $_SESSION['tbc']['invoiceid'] = $invoiceId;
        $_SESSION['tbc']['amount'] = $amount;
        $_SESSION['tbc']['transaction'] = $trans_id;

        $htmlOutput = '<form method="post" action="https://securepay.ufc.ge/ecomm2/ClientHandler">';
        
        $htmlOutput .= '<input type="hidden" name="trans_id" value="'.$trans_id.'" />';
        
        $htmlOutput .= '<input type="submit" value="' . $langPayNow . '" />';
        $htmlOutput .= '</form>';
    
    }

    return $htmlOutput;
}