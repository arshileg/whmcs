<?php

if( ($_GET['gateway']=='PayGe') && ($_GET['pass']=='pMDyyEGVL2eW692Z') ) {

	// Setting hash secret key and continue
	$hash_pass = 'dkKp2U27hYsACH9e';

} elseif( ($_GET['gateway']=='EMoneyGe') && ($_GET['pass']=='8RuXdn5nKkfgE6rY') ) {

	// Setting hash secret key and continue
	$hash_pass = 'Z66mmfDkyUm2pf83';

} else {

	// Exit if gateway details doesn't match
	echo 'Please enter gateway details.';
	exit();

}


// Including WHCMS file
require_once __DIR__ . '/init.php';


// Calculate hash for information retrieving
// HASH = Gateway + Gateway Password + InvoiceID + Hash Password
$get_hash = md5($_GET['gateway'].$_GET['pass'].$_GET['invoiceid'].$hash_pass);

// Calculate hash for payment command.
// HASH = Gateway + Gateway Password + InvoiceID + Amount + TansactionID + Hash Password
$pay_hash = md5($_GET['gateway'].$_GET['pass'].$_GET['invoiceid'].$_GET['amount'].$_GET['transaction'].$hash_pass);

// Converting all chars to lowecase
$hash_to_lower = strtolower( $_GET['hash'] );



if( (isset($_GET['invoiceid'])) && (ctype_digit($_GET['invoiceid'])) && (isset($_GET['get'])) && ($get_hash == $hash_to_lower) ){

	$values["invoiceid"] = $_GET['invoiceid'];
	$results = localAPI('getinvoice', $values, 'jayer');

	if(empty($results)){

		echo '
		<payment>
			<error>
				<code>1</code>
				<description>ტექნიკური შეცდომა</description>
			</error>
		</payment>
		';

	} elseif($results['status']=='error'){
		
		echo '
		<payment>
			<error>
				<code>2</code>
				<description>ინვოისი ვერ მოიძებნა</description>
			</error>
		</payment>
		';

	} elseif($results['result']=='success') {

		if($results['status']=='Unpaid'){

			$invoicestatus = 1;

		} else {

			$invoicestatus = 0;

		}

		$values["clientid"] = $results["userid"];
		$user = localAPI('getclientsdetails', $values, 'jayer');

		if($user['status']=='Active'){

			$userstatus = 1;

		} else {

			$userstatus = 0;

		}

		$credit = $results["total"] - $results["balance"];
		
		$currencies = localAPI('GetCurrencies', array(), 'jayer');
		
		foreach($currencies['currencies']['currency'] as $currency_rate) {
			
			if($currency_rate['code']==$user['currency_code']){
				
				$curr_code = $currency_rate['code'];
				$curr_rate = $currency_rate['rate'];
				
			}
			
		}
		
		echo '
		<payment>
			<success>
				<code>3</code>
				<description>მონაცემები ნაპოვნია</description>
				<user>
					<id>'.$user["userid"].'</id>
					<status>'.$userstatus.'</status>
					<personalid>'.$user["customfields2"].'</personalid>
					<name>'.$user["fullname"].'</name>
				</user>
				<invoice>
					<id>'.$results["invoiceid"].'</id>
					<status>'.$invoicestatus.'</status>
					<total>'.$results["total"].'</total>
					<credit>'.$credit.'</credit>
					<amount>'.$results["balance"].'</amount>
					<currency>'.$user['currency_code'].'</currency>
					<duedate>'.$results["duedate"].'</duedate>
				</invoice>
				<currency>
					<code>'.$curr_code.'</code>
					<rate>'.$curr_rate.'</rate>
				</currency>
			</success>
		</payment>
		';

	}

} elseif( (isset($_GET['pay'])) && (isset($_GET['gateway'])) && (isset($_GET['amount'])) && (isset($_GET['transaction'])) && (isset($_GET['date'])) && (isset($_GET['invoiceid'])) && (ctype_alpha($_GET['gateway'])) &&  (ctype_digit($_GET['amount'])) && (ctype_alnum($_GET['transaction'])) && (ctype_digit($_GET['invoiceid'])) && (ctype_print($_GET['date'])) && ($pay_hash == $hash_to_lower) ){



	$values["invoiceid"] = $_GET['invoiceid'];
	$results = localAPI('getinvoice', $values, 'jayer');

	if(empty($results)){

		echo '
		<payment>
			<error>
				<code>1</code>
				<description>ტექნიკური შეცდომა</description>
			</error>
		</payment>
		';

	} elseif($results['status']=='error'){
		
		echo '
		<payment>
			<error>
				<code>2</code>
				<description>ინვოისი ვერ მოიძებნა</description>
			</error>
		</payment>
		';

	} elseif($results['result']=='success') {

		if($results['status']=='Unpaid'){

			$invoicestatus = 1;

		} else {

			$invoicestatus = 0;

		}


		$values["invoiceid"] = $_GET['invoiceid'];
 		$values["transid"] = $_GET['gateway'].': '.$_GET['transaction'];
 				$amount = $_GET['amount'] / 100;
 		$values["amountin"] = $amount;
 		$values["paymentmethod"] = "banktransfer";

 		if($invoicestatus==1){

 			$results = localAPI('AddTransaction', $values, 'jayer');
			
 			if($results['result']=='success'){

 				echo '
					<payment>
						<success>
							<code>5</code>
							<description>ტრანზაქცია დასრულდა წარმატებით</description>
						</success>
					</payment>
					';
 			} else {

 				echo '
					<payment>
						<error>
							<code>6</code>
							<description>დუბლირებული ტრანზაქცია</description>
						</error>
					</payment>
					';

 			}

 		} elseif($invoicestatus==0) {

 			echo '
			<payment>
				<error>
					<code>4</code>
					<transaction>'.$results['transactions']['transaction'][0]['transid'].'</transaction>
					<description>ინვოისი უკვე გადახდილია</description>
				</error>
			</payment>
			';

 		}


	}


} else {

	echo '
	<payment>
		<error>
			<code>0</code>
			<description>არასწორი პარამეტრები</description>
		</error>
	</payment>
	';

}

?>