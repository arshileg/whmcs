{if $item->hasChildren()}
  <div class="row">
    <div class="col-md-12">
      <div class="p-1">
      <div menuItemName="{$item->getName()}" {if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
      <ul class="nav nav-material nav-material-horizontal">
        <li class="active"><a href="#panel_{$item->getAttribute('id')}" data-toggle="tab">
                    {if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}
                    {$item->getLabel()}
                    {if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
        </a></li>
        {if $item->getExtra('btn-link') && $item->getExtra('btn-text')}
            <li class="pull-right flip">
                <a href="{$item->getExtra('btn-link')}" >
                    {if $item->getExtra('btn-icon')}<i class="fas {$item->getExtra('btn-icon')}"></i>{/if}
                    {$item->getExtra('btn-text')}
                </a>
           </li>
        {/if}
      </ul>
      {if $item->hasBodyHtml()}
          <div class="panel-body">
              {$item->getBodyHtml()}
          </div>
      {/if}
            {if $item->hasChildren()}
                    <div class="tab-content">
                      <div class="tab-pane active" id="panel_{$item->getAttribute('id')}">
                        <table class="table table-data table-hover">

                        <thead>
                          <tr>
                            <th>{hexa_get_project_management_lang('title')}</th>
                            <th class="hidden-sm hidden-xs">{hexa_get_project_management_lang('created')}</th>
                            <th class="hidden-xs">{hexa_get_project_management_lang('duedate')}</th>
                            <th>{hexa_get_project_management_lang('status')}</th>
                            <th class="hidden-sm hidden-xs">{hexa_get_project_management_lang('lastmodified')}</th>
                            <th class="hidden-xs"></th>
                          </tr>
                        </thead>

                        <tbody>{foreach $item->getChildren() as $childItem}
                        {if $childItem->getUri()}
                        <tr>

                        <td>
                                <a menuItemName="{$childItem->getName()}" href="{$childItem->getUri()}" class="{if $childItem->getClass()} {$childItem->getClass()}{/if}{if $childItem->isCurrent()} active{/if}"{if $childItem->getAttribute('dataToggleTab')} data-toggle="tab"{/if}{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if} id="{$childItem->getId()}">
                                    {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
                          {hexa_get_projects_info_from_db_($childItem->getUri(),'title')}
                                    {if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
                                </a>
                        </td>
                        <td class="hidden-sm hidden-xs">
                          {hexa_get_projects_info_from_db_($childItem->getUri(),'created')}
                        </td>
                        <td class="hidden-sm hidden-xs">
                          {hexa_get_projects_info_from_db_($childItem->getUri(),'duedate')}
                        </td>
                        <td class="hidden-sm hidden-xs">
                          {hexa_get_projects_info_from_db_($childItem->getUri(),'status')}
                        </td>
                        <td class="hidden-sm hidden-xs">
                           {hexa_get_projects_info_from_db_($childItem->getUri(),'lastmodified')}
                        </td>


                        <td class="cell-view">
                          <a href="{$childItem->getUri()}">
                              <span class="glyphicon glyphicon-chevron-right pull-right flip"></span>
                          </a>
                        </td>

                        </tr>
                        {/if}
                        {/foreach}
                    </tbody>
                  </table>
                  </div>
                  </div>
            {/if}
        {if $item->hasFooterHtml()}
          <div class="panel-footer">
              {$item->getFooterHtml()}
          </div>
        {/if}
        </div>
      </div>
    </div>
  </div>
{/if}
