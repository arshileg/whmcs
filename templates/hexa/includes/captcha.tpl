{if $captcha}
        {if $filename == 'index'}
            <div class="domainchecker-homepage-captcha">
        {/if}

        {if $captcha == "recaptcha"}
            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
            <div id="google-recaptcha-domainchecker" class="g-recaptcha center-block" data-sitekey="{$reCaptchaPublicKey}"></div>
        {else}
            <div class="form-group">
                <div id="default-captcha-domainchecker" class="{if $filename == 'domainchecker'}input-group input-group-box{/if}">
                    <p>{lang key="captchaverify"}</p>
                    <div class="captchaimage py-1">
                        <img id="inputCaptchaImage" src="includes/verifyimage.php" align="middle" />
                    </div>

                        <input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control" />
                    </div>
            </div>
        {/if}

        {if $filename == 'index'}
            </div>
        {/if}
{/if}
