
<div class="row">
  <div class="col-md-12">
    <div class="p-1">
    <div menuItemName="{$item->getName()}" {if $item->getAttribute('id')} id="{$item->getAttribute('id')}"{/if}>
    <ul class="nav nav-material nav-material-horizontal">
      <li class="active"><a href="#panel_{$item->getAttribute('id')}" data-toggle="tab">
                  {if $item->hasIcon()}<i class="{$item->getIcon()}"></i>&nbsp;{/if}
                  {$item->getLabel()}
                  {if $item->hasBadge()}&nbsp;<span class="badge">{$item->getBadge()}</span>{/if}
      </a></li>
      {if $item->getExtra('btn-link') && $item->getExtra('btn-text')}
          <li class="pull-right flip">
              <a href="{$item->getExtra('btn-link')}" >
                  {if $item->getExtra('btn-icon')}<i class="fas {$item->getExtra('btn-icon')}"></i>{/if}
                  {$item->getExtra('btn-text')}
              </a>
         </li>
      {/if}
    </ul>
    {if $item->hasBodyHtml()}
        <div class="panel-body">
            {$item->getBodyHtml()}
        </div>
    {/if}
          {if $item->hasChildren()}
                  <div class="tab-content">
                    <div class="tab-pane active" id="panel_{$item->getAttribute('id')}">
                      <table class="table table-data table-hover">

                      <thead>
                        <tr>
                          <th>{$LANG.orderproduct}</th>
                          <th></th>
                          <th class="hidden-sm hidden-xs">{$LANG.clientareahostingdomain}</th>
                          <th class="hidden-sm hidden-xs">{$LANG.orderbillingcycle}</th>
                          <th class="hidden-sm hidden-xs">{$LANG.clientareahostingnextduedate}</th>

                          <th></th>
                        </tr>
                      </thead>

                      <tbody>{foreach $item->getChildren() as $childItem}
                      {if $childItem->getUri()}
                      <tr>
                      <td>
                        {hexa_get_services_info_from_db_($childItem->getUri(),'groupname')}
                      </td>
                      <td>
                              <a menuItemName="{$childItem->getName()}" href="{$childItem->getUri()}" class="{if $childItem->getClass()} {$childItem->getClass()}{/if}{if $childItem->isCurrent()} active{/if}"{if $childItem->getAttribute('dataToggleTab')} data-toggle="tab"{/if}{if $childItem->getAttribute('target')} target="{$childItem->getAttribute('target')}"{/if} id="{$childItem->getId()}">
                                  {if $childItem->hasIcon()}<i class="{$childItem->getIcon()}"></i>&nbsp;{/if}
                        {hexa_get_services_info_from_db_($childItem->getUri(),'productname')}
                                  {if $childItem->hasBadge()}&nbsp;<span class="badge">{$childItem->getBadge()}</span>{/if}
                              </a>
                      </td>
                      <td class="hidden-sm hidden-xs">
                        {hexa_get_services_info_from_db_($childItem->getUri(),'domain')}
                      </td>
                      <td class="hidden-sm hidden-xs">
                        {hexa_get_services_info_from_db_($childItem->getUri(),'billingcycle')}
                      </td>
                      <td class="hidden-sm hidden-xs">
                        {hexa_get_services_info_from_db_($childItem->getUri(),'nextduedate')}
                      </td>
                      <td class="cell-view">
                        <a href="{$childItem->getUri()}">
                            <span class="glyphicon glyphicon-chevron-right pull-right flip"></span>
                        </a>
                      </td>

                      </tr>
                      {/if}
                     {foreachelse}
                     <tr>
                       <td colspan="{if $masspay}7{else}6{/if}" class="norecords">{$LANG.norecordsfound}</td>
                     </tr>{/foreach}
                  </tbody>
                </table>
                </div>
                </div>
          {/if}
      {if $item->hasFooterHtml()}
        <div class="panel-footer">
            {$item->getFooterHtml()}
        </div>
      {/if}
      </div>
    </div>
  </div>
</div>
