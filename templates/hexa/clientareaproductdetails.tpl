{include file="$template/pageheader.tpl" title=$product}
{if $modulecustombuttonresult}
{if $modulecustombuttonresult == "success"}
{include file="$template/includes/alert.tpl" type="success" msg=$LANG.moduleactionsuccess textcenter=true idname="alertModuleCustomButtonSuccess"}
{else}
{include file="$template/includes/alert.tpl" type="error" msg=$LANG.moduleactionfailed|cat:' ':$modulecustombuttonresult textcenter=true idname="alertModuleCustomButtonFailed"}
{/if}
{/if}
{if $pendingcancellation}
{include file="$template/includes/alert.tpl" type="error" msg=$LANG.cancellationrequestedexplanation textcenter=true idname="alertPendingCancellation"}
{/if}
<ul class="nav nav-material nav-material-horizontal px-lg-30 pt-lg-30" id="tabs">
  <li class="active"><a href="#tabOverview" data-toggle="tab">{$LANG.information}</a></li>
  {if $modulechangepassword}<li><a href="#tabChangepw" data-toggle="tab">{$LANG.serverchangepassword}</a></li>{/if}
  {if $downloads}<li><a href="#tabDownloads" data-toggle="tab">{$LANG.downloadstitle}</a></li>{/if}
  {if $addonsavailable}<li><a href="#tabAddons" data-toggle="tab">{$LANG.clientareahostingaddons}</a></li>{/if}
  {if $packagesupgrade || $configoptionsupgrade || $showcancelbutton || $modulecustombuttons}<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{$LANG.productmanagementactions}</a>
    <ul class="dropdown-menu">
      {foreach from=$modulecustombuttons key=label item=command}
      <li><a href="clientarea.php?action=productdetails&amp;id={$id}&amp;modop=custom&amp;a={$command}">{$label}</a></li>
      {/foreach}
      {if $packagesupgrade}<li><a href="upgrade.php?type=package&amp;id={$id}">{$LANG.upgradedowngradepackage}</a></li>{/if}
      {if $configoptionsupgrade}<li><a href="upgrade.php?type=configoptions&amp;id={$id}">{$LANG.upgradedowngradeconfigoptions}</a></li>{/if}
      {if $showcancelbutton}<li><a href="clientarea.php?action=cancel&amp;id={$id}">{$LANG.clientareacancelrequestbutton}</a></li>{/if}
    </ul>
  </li>
  {/if}
</ul>
<div class="tab-content">
  <div class="tab-pane clearfix active" id="tabOverview">
    {if $tplOverviewTabOutput}
    {$tplOverviewTabOutput}
    {if $serverdata.type eq 'cpanel'}
    {if $moduleclientarea}<div class="moduleoutput text-right">{$moduleclientarea|replace:'modulebutton':'btn'}</div>{/if}
    {/if}
  {else}
<div class="alert alert-{$rawstatus|strtolower} alert-product-details alert-block mt-2">
<div class="alert-watermark m-1">
    <i class="fas fa-{if $type eq "hostingaccount" || $type == "reselleraccount"}hdd{elseif $type eq "server"}database{else}archive{/if} fa-4x fa-inverse"></i>
  </div>
  <h4>{$product}</h4>
  <span class="mr-2"><span aria-hidden="true" class="icon icon-layers"></span> {$groupname}</span>
  <span class="mr-2"><span aria-hidden="true" class="icon icon-info"></span> {$status}</span>
  {if $packagesupgrade}
  <span class="mr-2"><span aria-hidden="true" class="icon icon-arrow-up-circle"></span> <a href="upgrade.php?type=package&amp;id={$id}">{$LANG.upgrade}</a></span>
    {/if}
</div>
  {include file="$template/subheader.tpl" title=$LANG.information}
  <div class="well mx-1 mt-1">
    <div class="row">
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt>{$LANG.clientareahostingregdate}</dt>
          <dd>{$regdate}</dd>
          {if $firstpaymentamount neq $recurringamount}
          <dt>{$LANG.firstpaymentamount}</dt>
          <dd>{$firstpaymentamount}</dd>
          {/if}
        </dl>
      </div>
      <div class="col-md-4">
        <dl class="dl-horizontal">
          {if $billingcycle != $LANG.orderpaymenttermonetime && $billingcycle != $LANG.orderfree}
          <dt>{$LANG.recurringamount}</dt>
          <dd>{$recurringamount}</dd>
          {/if}
          <dt>{$LANG.orderbillingcycle}</dt>
          <dd>{$billingcycle}</dd>
        </dl>
      </div>
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt>{$LANG.orderpaymentmethod}</dt>
          <dd>{$paymentmethod}</dd>
          <dt>{$LANG.clientareahostingnextduedate}</dt>
          <dd>{$nextduedate}</dd>
          {if $suspendreason}
          <dt>{$LANG.suspendreason}</dt>
          <dd>{$suspendreason}</dd>
          {/if}
        </dl>
      </div>
    </div>
  </div>
  {foreach $hookOutput as $output}
  <div>
  {$output}
  </div>
  {/foreach}
  {if $domain || $moduleclientarea || $configurableoptions || $customfields || $lastupdate}
  {if $domain}
  <div class="row px-1 pt-1">
  <div class="col-md-12">
  <h4>{if $type eq "server"}{$LANG.sslserverinfo}{elseif ($type eq "hostingaccount" || $type eq "reselleraccount") && $serverdata}{$LANG.hostingInfo}{else}{$LANG.clientareahostingdomain}{/if}</h4>
</div>
</div>
  <div class="well mx-1" id="domain">
    {if $type eq "server"}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.serverhostname}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$domain}
      </div>
    </div>
    {if $dedicatedip}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.primaryIP}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$dedicatedip}
      </div>
    </div>
    {/if}
    {if $assignedips}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.assignedIPs}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$assignedips|nl2br}
      </div>
    </div>
    {/if}
    {if $ns1 || $ns2}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.domainnameservers}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$ns1}<br />{$ns2}
      </div>
    </div>
    {/if}
    {elseif ($type eq "hostingaccount" || $type eq "reselleraccount") && $serverdata}
    {if $domain}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.orderdomain}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$domain}&nbsp;<a href="http://{$domain}" target="_blank" class="btn btn-default btn-xs" >{$LANG.visitwebsite}</a>
      </div>
    </div>
    {/if}
    {if $username}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.serverusername}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$username}
      </div>
    </div>
    {/if}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.servername}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$serverdata.hostname}
      </div>
    </div>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.domainregisternsip}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$serverdata.ipaddress}
      </div>
    </div>
    {if $serverdata.nameserver1 || $serverdata.nameserver2 || $serverdata.nameserver3 || $serverdata.nameserver4 || $serverdata.nameserver5}
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong>{$LANG.domainnameservers}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {if $serverdata.nameserver1}{$serverdata.nameserver1} ({$serverdata.nameserver1ip})<br />{/if}
        {if $serverdata.nameserver2}{$serverdata.nameserver2} ({$serverdata.nameserver2ip})<br />{/if}
        {if $serverdata.nameserver3}{$serverdata.nameserver3} ({$serverdata.nameserver3ip})<br />{/if}
        {if $serverdata.nameserver4}{$serverdata.nameserver4} ({$serverdata.nameserver4ip})<br />{/if}
        {if $serverdata.nameserver5}{$serverdata.nameserver5} ({$serverdata.nameserver5ip})<br />{/if}
      </div>
    </div>
    {/if}
    {else}
    <p>
      {$domain}
    </p>
    <p>
      <a href="http://{$domain}" class="btn btn-default" target="_blank">{$LANG.visitwebsite}</a>
      {if $domainId}
      <a href="clientarea.php?action=domaindetails&id={$domainId}" class="btn btn-default" target="_blank">{$LANG.managedomain}</a>
      {/if}
      <input type="button" onclick="popupWindow('whois.php?domain={$domain}','whois',650,420);return false;" value="{$LANG.whoisinfo}" class="btn btn-default" />
    </p>
    {/if}
    {if $moduleclientarea}
    <div class="text-center module-client-area">
      {$moduleclientarea}
    </div>
    {/if}
  </div>
  {elseif $moduleclientarea}
  {include file="$template/subheader.tpl" title=$LANG.manage}
  <div class="well mx-1">
    {if $moduleclientarea}
    <div class="text-center module-client-area">
      {$moduleclientarea}
    </div>
    {/if}
  </div>
  {/if}
  {if $configurableoptions}
  {include file="$template/subheader.tpl" title=$LANG.orderconfigpackage}
  <div class="well mx-1">
    {foreach from=$configurableoptions item=configoption}
    <div class="row">
      <div class="col-sm-5">
        <strong>{$configoption.optionname}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {if $configoption.optiontype eq 3}{if $configoption.selectedqty}{$LANG.yes}{else}{$LANG.no}{/if}{elseif $configoption.optiontype eq 4}{$configoption.selectedqty} x {$configoption.selectedoption}{else}{$configoption.selectedoption}{/if}
      </div>
    </div>
    {/foreach}
  </div>
  {/if}
  {if $customfields}
  {include file="$template/subheader.tpl" title=$LANG.additionalInfo}
  <div class="well mx-1">
    {foreach from=$customfields item=field}
    <div class="row">
      <div class="col-sm-5">
        <strong>{$field.name}</strong>
      </div>
      <div class="col-sm-7 text-left">
        {$field.value}
      </div>
    </div>
    {/foreach}
  </div>
  {/if}
  {if $lastupdate}
  {include file="$template/subheader.tpl" title=$LANG.resourceUsage}
  <div class="well mx-1">
    <div class="col-sm-10 col-sm-offset-1">
      <div class="col-sm-6">
        <h4>{$LANG.diskSpace}</h4>
        <input type="text" value="{$diskpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
        <p>{$diskusage}MB / {$disklimit}MB</p>
      </div>
      <div class="col-sm-6">
        <h4>{$LANG.bandwidth}</h4>
        <input type="text" value="{$bwpercent|substr:0:-1}" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
        <p>{$bwusage}MB / {$bwlimit}MB</p>
      </div>
    </div>
    <div class="clearfix">
    </div>
    <p><small>{$LANG.clientarealastupdated}: {$lastupdate}</small></p>
    <script src="{$BASE_PATH_JS}/jquery.knob.js"></script>
    <script type="text/javascript">
    jQuery(function() {ldelim}
    jQuery(".dial-usage").knob({ldelim}'format':function (v) {ldelim} alert(v); {rdelim}{rdelim});
    {rdelim});
    </script>
  </div>
  {/if}

{/if}
<script src="{$BASE_PATH_JS}/bootstrap-tabdrop.js"></script>
<script type="text/javascript">
jQuery('.nav-tabs-overflow').tabdrop();
</script>
{/if}
</div>
<div class="tab-pane" id="tabDownloads">
  {include file="$template/subheader.tpl" title=$LANG.downloadstitle}
  {include file="$template/includes/alert.tpl" type="info" msg="{lang key="clientAreaProductDownloadsAvailable"}"}
  <div class="row  px-1 pt-1">
    {foreach from=$downloads item=download}
    <div class="col-md-6">
    <div class="well">
      <h4>{$download.title}</h4>
      <p>{$download.description}</p>
      <div class="form-group">
        <a href="{$download.link}" class="btn btn-default"><i class="fas fa-download"></i> {$LANG.downloadname}</a>
      </div>
    </div>
    </div>
    {/foreach}
  </div>
</div>
<div class="tab-pane" id="tabAddons">
{include file="$template/subheader.tpl" title=$LANG.clientareahostingaddons}
  {if $addonsavailable}
  {include file="$template/includes/alert.tpl" type="info" msg="{lang key="clientAreaProductAddonsAvailable"}"}
  {/if}
  <div class="row  px-1 pt-1">
    {foreach from=$addons item=addon}
    <div class="col-md-12">
    <div class="well">
      <h4>{$addon.name} <small><span class="label status-{$addon.rawstatus|strtolower}">{$addon.status}</span></small></h4>
      <span class="mr-2">{$addon.pricing}</span><span class="mr-2"><strong>{$LANG.registered}:</strong> {$addon.regdate}</span><span class="mr-2"><strong>{$LANG.clientareahostingnextduedate}:</strong> {$addon.nextduedate}</span>
      <div class="form-group">{$addon.managementActions}</div>
    </div>
    </div>
    {/foreach}
  </div>
</div>
<div class="tab-pane" id="tabChangepw">
  {if $modulechangepwresult}
  {if $modulechangepwresult == "success"}
  {include file="$template/includes/alert.tpl" type="success" msg=$modulechangepasswordmessage textcenter=true}
  {elseif $modulechangepwresult == "error"}
  {include file="$template/includes/alert.tpl" type="error" msg=$modulechangepasswordmessage|strip_tags textcenter=true}
  {/if}
  {/if}
<div class="row">
<div class="col-md-6 col-md-offset-3">
<h3>{$LANG.serverchangepassword}</h3>
  <div class="well">
    <form class="using-password-strength" method="post" action="{$smarty.server.PHP_SELF}?action=productdetails#tabChangepw" role="form">
      <input type="hidden" name="id" value="{$id}" />
      <input type="hidden" name="modulechangepassword" value="true" />
      <div id="newPassword1" class="form-group has-feedback">
        <label for="inputNewPassword1" class="control-label">{$LANG.newpassword}</label>
          <input type="password" class="form-control" id="inputNewPassword1" name="newpw"  autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
          {include file="$template/includes/pwstrength-notips.tpl"}
      </div>
      <div id="newPassword2" class="form-group has-feedback">
        <label for="inputNewPassword2" class="control-label">{$LANG.confirmnewpassword}</label>
          <input type="password" class="form-control" id="inputNewPassword2" name="confirmpw"  autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
          <div id="inputNewPassword2Msg">
        </div>
      </div>
      <div class="form-group">
          <input class="btn btn-primary btn-lg btn-block" type="submit" value="{$LANG.clientareasavechanges}" />
        </div>
    </form>
  </div>
  </div>
  </div>
  </div>
</div>
