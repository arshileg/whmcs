{if $inactive}
{include file="$template/pageheader.tpl" title=$LANG.affiliatestitle icon=diamond}
<div class="alert alert-warning">
  <p>{$LANG.affiliatesdisabled}</p>
</div>
{else}
{include file="$template/pageheader.tpl" title=$LANG.affiliatestitle desc=$LANG.affiliatesrealtime icon=diamond}
<div class="row px-1 py-2">
  <div class="col-md-12">
    <h4>{$LANG.affiliatesreferallink}</h4>
    <div class="form-group">
      <input type="text" value="{$referrallink}" class="form-control input-sm" />
    </div>
  </div>
</div>
<div class="row px-1">
  <div class="col-md-4">
    <div class="well well-lg">
      {$LANG.affiliatesvisitorsreferred}
      <span class="label label-default pull-right flip">{$visitors}</span>
    </div>
  </div>
  <div class="col-md-4">
    <div class="well  well-lg">
      {$LANG.affiliatessignups}
      <span class="label label-success pull-right flip">{$signups}</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      {$LANG.affiliatesconversionrate}
      <span class="label label-info pull-right flip">{$conversionrate}%</span>
    </div>
  </div>
</div>

<div class="row px-1">
  <div class="col-md-4">
    <div class="well  well-lg">
      {$LANG.affiliatescommissionspending}
      <span class="label label-default pull-right flip">{$pendingcommissions}</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      {$LANG.affiliatescommissionsavailable}
      <span class="label label-success pull-right flip">{$balance}</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      {$LANG.affiliateswithdrawn}
      <span class="label label-info pull-right flip">{$withdrawn}</span>
    </div>
  </div>
</div>


{if $withdrawrequestsent}
<div class="alert alert-success">
  <p>{$LANG.affiliateswithdrawalrequestsuccessful}</p>
</div>
{else}
{if $withdrawlevel}
<input type="button" class="btn btn-default btn-sm" value="{$LANG.affiliatesrequestwithdrawal}" onclick="window.location='{$smarty.server.PHP_SELF}?action=withdrawrequest'" />
{/if}
{/if}

{include file="$template/subheader.tpl" title=$LANG.affiliatesreferals}
{include file="$template/includes/tablelist.tpl" tableName="AffiliatesList"}
<script type="text/javascript">
jQuery(document).ready( function ()
{
  var table = jQuery('#tableAffiliatesList').removeClass('hidden').DataTable();
  {if $orderby == 'regdate'}
  table.order(0, '{$sort}');
  {elseif $orderby == 'product'}
  table.order(1, '{$sort}');
  {elseif $orderby == 'amount'}
  table.order(2, '{$sort}');
  {elseif $orderby == 'status'}
  table.order(4, '{$sort}');
  {/if}
  table.draw();
  jQuery('#tableLoading').addClass('hidden');
});
</script>
<div class="panel panel-default panel-datatable">
  <div class="panel-heading clearfix">
    <div class="filter_top panel panel-default panel-actions view-filter-btns" >
      <div class="elemetsholder ">
        <a class="btn btn-link btn-xs" href="#" ></a>
      </div>
    </div>
  </div>
  <table class="table table-striped table-framed" id="tableAffiliatesList">
    <thead>
      <tr>
        <th class="hidden-sm hidden-xs">{$LANG.affiliatessignupdate}</th>
        <th>{$LANG.orderproduct}</th>
        <th class="hidden-sm hidden-xs">{$LANG.affiliatesamount}</th>
        <th class="hidden-xs">{$LANG.affiliatescommission}</th>
        <th>{$LANG.affiliatesstatus}</th>
      </tr>
    </thead>
    {foreach from=$referrals item=referral}
    <tr>
      <td class="hidden-sm hidden-xs" data-order="{$referral.datets}" >{$referral.date}</td>
      <td>{$referral.service}

        <ul class="cell-inner-list visible-sm visible-xs small">
          <li><span class="item-title">{$LANG.affiliatessignupdate}: </span>{$referral.date}</li>
          <li><span class="item-title">{$LANG.affiliatesamount}: </span>{$referral.amountdesc}</li>
          <li class="hidden-sm"><span class="item-title">{$LANG.affiliatescommission}: </span>{$referral.commission}</li>
        </ul>

      </td>
      <td data-order="{$referral.amountnum}" class="hidden-sm hidden-xs">{$referral.amountdesc}</td>
      <td data-order="{$referral.commissionnum}" class="hidden-xs">{$referral.commission}</td>
      <td><span class='label status status-{$referral.rawstatus|strtolower}'>{$referral.status}</span></td>
    </tr>
    {/foreach}
  </table>
  <div class="text-center" id="tableLoading">
    <p><i class="fas fa-spinner fa-spin"></i> {$LANG.loading}</p>
  </div>
</div>
<ul class="pagination px-1">
  <li class="prev{if !$prevpage} disabled{/if}"><a href="{if $prevpage}affiliates.php?page={$prevpage}{else}javascript:return false;{/if}">&larr; {$LANG.previouspage}</a></li>
  <li class="next{if !$nextpage} disabled{/if}"><a href="{if $nextpage}affiliates.php?page={$nextpage}{else}javascript:return false;{/if}">{$LANG.nextpage} &rarr;</a></li>
</ul>
{if $affiliatelinkscode}
{include file="$template/subheader.tpl" title=$LANG.affiliateslinktous}
<div class="textcenter">
  {$affiliatelinkscode}
</div>
{/if}
{/if}
