{if $errormessage}

    {include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage textcenter=true}

{else}

    {include file="$template/includes/alert.tpl" type="success" msg=$LANG.sslconfigcomplete textcenter=true}

    <p class="p-1">{$LANG.sslconfigcompletedetails}</p>

    <form method="post" action="clientarea.php?action=productdetails" class="p-2">
        <input type="hidden" name="id" value="{$serviceid}" />
        <div class="form-group">
	<input type="submit" value="{$LANG.invoicesbacktoclientarea}" class="btn btn-default"/>
	</div>
    </form>
{/if}
