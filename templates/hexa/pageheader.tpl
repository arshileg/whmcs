<div class="row">
<div class="col-md-12">
<h3 class="page-header">
{if $icon}<span aria-hidden="true" class="icon icon-{$icon}"></span>{/if} {$title} {if $desc}<i class="fas fa-angle-down show-info {if $expanded_page_information}down{/if}" aria-hidden="true"></i>{/if}
</h3>
{if $desc}
<blockquote class="page-information {if !$expanded_page_information}hidden{/if}">
<p>{$desc}</p>
</blockquote>
{/if}
</div>
</div>
