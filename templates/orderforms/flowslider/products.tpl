<script type="text/javascript" src="{$BASE_PATH_JS}/jquery-ui.min.js"></script>
<script type="text/javascript" src="{$WEB_ROOT}/templates/orderforms/{$carttpl}/assets/js/main.js"></script>
<link rel="stylesheet" type="text/css" href="{$WEB_ROOT}/templates/orderforms/{$carttpl}/assets/css/uistyle.css" />
<link rel="stylesheet" type="text/css" href="{$WEB_ROOT}/templates/orderforms/{$carttpl}/assets/css/style.css" />

<style type="text/css">
{if isset($flowcart_primary_color)}
#flow-menu li.group-first .flow-icon, #flow-menu li.group-second .flow-icon,#flow-menu li.group-third .flow-icon,#flow-menu li.group-fourth .flow-icon { color: {$flowcart_primary_color}; }
#stepbar > li.active > a,#stepbar > li.active > span, #flow-menu > li.active > a { background-color: {$flowcart_primary_color}; color: #fff; }
ul.nav-wizard li.active { color:#fff; background: {$flowcart_primary_color}; }
ul.nav-wizard li.active:after { border-left:16px solid {$flowcart_primary_color}; }
ul.nav-wizard li.active a,ul.nav-wizard li.active a:active,ul.nav-wizard li.active a:visited,ul.nav-wizard li.active a:focus { color: #3a87ad; background: {$flowcart_primary_color};}
.well-primary,.nav-tabs > li.active > a,.nav-tabs > li.active > a:hover,.nav-tabs > li.active > a:focus { border-top:2px {$flowcart_primary_color} solid; }
.fa-primary,.btn-link, .btn-link a:hover, .btn-link:hover, .btn-link:focus { color: {$flowcart_primary_color}; }
.btn-default:hover, .btn-default:focus { color: #fff; background-color: {$flowcart_primary_color}; border-color: {$flowcart_primary_color};}
.order-row h6, .order-row .order-price, .order-row .cart-features a, .order-row h6 a { color: {$flowcart_primary_color}; }
.well-primary, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border-top-color: {$flowcart_primary_color}; }
.loader .fa, .pricing .fa, .sliderlabel.selected, .price { color: {$flowcart_primary_color}; }

.ui-widget-header, .sliderlabel.selected span, .cart-normal .badge, .cart-normal .label  { background-color: {$flowcart_primary_color}; }
.has-ribbon:after{ background-color: {$flowcart_primary_color} ; }

{/if}
{if isset($flowcart_secondary_color)}
.currency-product a { color: {$flowcart_secondary_color}; }
input.cycle-1:checked+label.radio i, input.cycle-curr:checked+label.radio i, input.cycle-1:checked+label.radio i { color: {$flowcart_secondary_color}; }
.dropdown-menu li a:hover,.dropdown-menu li a:focus { background-color: {$flowcart_secondary_color}; }
.fa-money.active { color: {$flowcart_secondary_color}; }
.bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-success, .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-success { background: {$flowcart_secondary_color}; }
.cart-normal .label-secondary { background-color: {$flowcart_secondary_color}; }
{/if}
</style>
<script type="text/javascript">
var productscount = {$productscount};
var productsNums = new Array();
var last_val = 0;
{foreach from=$products key=num item=product}
productsNums[{$num}] = {$product.pid};
{/foreach}
{literal}
jQuery(document).ready(function(){
  var slider = jQuery( "#productslider" ).slider({
    animate: "slow",
    min: 0,
    max: (productscount*100)-1,
    value: 0,
    range: "min",
    slide: function( event, ui ) {
      num = Math.floor(ui.value/100);
      if(num!=last_val){
        last_val = num;
        snum = ((1+parseInt(num))*100)-50;
        jQuery('#productslider').slider("value",snum);
        selproduct_slider(snum);
        return false;
      }else{
        return false;
      }
    }
  });

  //jQuery("#productslider").width(width*(productscount-1)+'px');
  jQuery("#productslider").css('width','auto');
  function resize_label(){
    var width = jQuery("#productslider").width()/productscount;
    jQuery(".sliderlabel").width(width+'px');
  }
  jQuery( window ).resize(function() {
    resize_label();
  });

  resize_label();
  selproduct(productsNums[0],0);
  {/literal}
  {if $pid}
  selproduct(productsNums[{$pid}],null);
  {/if}
  {literal}
});
{/literal}
</script>
<div id="order-slider">
  {include file="orderforms/flowcart7/products-header.tpl"}
  <div class="flowcart-row">
    {if !$loggedin && $currencies}
    <div class="row">
      <div class="col-md-12">
        <div class="pull-right currency-product">
          {foreach from=$currencies item=curr}
          <a href="cart.php?gid={$gid}&currency={$curr.id}"><i class="fal fa-money{if $curr.id eq $currency.id} active{/if}"></i> <span class="{if $curr.id eq $currency.id} active{/if}">{$curr.code}</span></a>
          {/foreach}
        </div>
      </div>
    </div>
    {/if}

{if $flowcart_show_Product_Group_Headline || $flowcart_show_Product_Group_Tagline}
<div class="row">
<div class="col-md-12">
        <div class="header-lined">
        {if $flowcart_show_Product_Group_Headline}
                <h1>
                    {if $productGroup.headline}
                        {$productGroup.headline}
                    {else}
                        {$productGroup.name}
                    {/if}
                </h1>
        {/if}
        {if $flowcart_show_Product_Group_Tagline}
                {if $productGroup.tagline}
                    <p>{$productGroup.tagline}</p>
                {/if}
        {/if}
            </div>
</div>
</div>
{/if}

{if count($productGroup.features) > 0 && $flowcart_show_Group_Features=="Above Pricing"}
    <div class="includes-container">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="head-area">
                    <span>
                        {$LANG.whatIsIncluded}
                    </span>
                </div>
                <ul id="list-contents" class="list-contents productGroupfeatures">
                    {foreach $productGroup.features as $features}
                        <li class="{$flowcart_features_columns}"><i class="flow-icon {$flowcart_feature_icon}"></i>{$features.feature}</li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}

    <div class="well panel-slider">
      <div class="cartslider">
        <div align="center">
          <div id="productslider" class="hidden-xs"></div>
          {foreach from=$products key=num item=product}
          <div class="sliderlabel hidden-xs" id="prodlabel{$product.pid}" data-product-pid="{$product.pid}" data-product-num="{$num}" data-price="{$product.pricing.minprice.price}" onclick="selproduct('{$product.pid}',{$num})"><span></span>{$product.name}</div>
          {/foreach}
          {foreach from=$products key=num item=product}
          <div class="btn btn-default btn-block btn-lg visible-xs" onclick="selproduct('{$product.pid}',{$num})">{$product.name}</div>
          {/foreach}
        </div>
      </div>
      <div class="row">
        <div class="proddesc">
          {foreach from=$products key=num item=product}
          <div class="product clearfix" id="product{$product.pid}">
            <div class="col-md-9">
              {if $flowcart_filter_BR_line_break}
                {$product.featuresdesc|regex_replace:"/(<br>|<br [^>]*>|<\\/>)/":""}
              {else}
                {$product.featuresdesc|nl2br|trim}
              {/if}
            </div>
            <div class="col-md-3 pv-lg">
              {if $product.bid}
              {if $product.displayprice}<span class="pricing">{$product.displayprice}</span><br>{/if}
              {$LANG.bundledeal}
              {else}
              {if $product.pricing.hasconfigoptions}<div class="startingfrom text-center">{$LANG.startingfrom}</div>{/if}
              <div class="price text-center">{$product.pricing.minprice.price}</div>
              {assign var="pricingTerm" value='orderpaymentterm'|cat:$product.pricing.minprice.cycle}
              <div class="pricing-term text-center"><i class="fal fa-calendar-o"></i> {$LANG.$pricingTerm}</div>
              {/if}
            </div>
          </div>
          {/foreach}
        </div>
      </div>
    </div>


    {if count($productGroup.features) > 0 && $flowcart_show_Group_Features=="Below Pricing"}
    <div class="includes-container">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="head-area">
                    <span>
                        {$LANG.whatIsIncluded}
                    </span>
                </div>
                <ul id="list-contents" class="list-contents productGroupfeatures">
                    {foreach $productGroup.features as $features}
                        <li class="{$flowcart_features_columns}"><i class="flow-icon {$flowcart_feature_icon}"></i>{$features.feature}</li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <form id="formaddprod" method="post" action="cart.php?a=add&{if $product.bid}bid={$product.bid}{else}pid={$product.pid}{/if}">
            <input type="submit" value="{$LANG.ordernowbutton}" class="btn btn-default btn-lg pull-right" />
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
