jQuery(document).ready(function(){
	var current_tab; 
	current_tab = jQuery('#custtype').val();


	if(jQuery('#mainfrm .user-details').length){
	    jQuery('a[data-toggle="tab"]').on('click', function (e) {
	        jQuery(this).find('input').prop("checked", true);
	        var name = jQuery(this).find('input[type=radio]').attr('name');
	        var namev = jQuery(this).find('input[type=radio]').val();
	        if(namev=='new'){
	    		jQuery("#btnCompleteOrder").removeAttr('formnovalidate');
				jQuery("#btnCompleteOrder").parents('form').removeAttr('novalidate');   
	        }else{
	        	jQuery("#btnCompleteOrder").attr('formnovalidate', true);
				jQuery("#btnCompleteOrder").parents('form').attr('novalidate', true);        	
	        }
	    });

		if(current_tab){
			if(current_tab!='existing'){
	    		jQuery('.user-details li:first-child a').click();
	    		jQuery("#btnCompleteOrder").removeAttr('formnovalidate');
				jQuery("#btnCompleteOrder").parents('form').removeAttr('novalidate');    		
			}else{
				jQuery('.user-details li:last-child a').click();
				jQuery("#btnCompleteOrder").attr('formnovalidate', true);
				jQuery("#btnCompleteOrder").parents('form').attr('novalidate', true);
			}
		}    
	}
	jQuery('.domain-tabs li:first-child').addClass('active').find('input').prop("checked", true);
});
function emptyCart() {
    if (confirm("Are you sure you want to empty cart?")) {
        window.location.href = 'cart.php?a=empty';
        return false;
    }
}
function selectproduct(pid) {
    jQuery("#checkoutbtn").hide();
    jQuery("#loading1").slideDown();
    jQuery("#configcontainer1").fadeOut();
    jQuery("#configcontainer2").fadeOut();
    jQuery("#configcontainer3").fadeOut();
    jQuery("#signupcontainer").fadeOut();
    jQuery.post("cart.php", 'ajax=1&a=add&pid='+pid,
        function(data){
            if (data=='') {
                signupstep();
            } else {
                jQuery("#configcontainer1").html(data);
                jQuery("#configcontainer1").slideDown();
            }
            jQuery("#loading1").slideUp();
            recalcsummary();
        });
}
function prodconfcomplete() {
    jQuery.post("cart.php", 'a=confproduct&ajax=1&'+jQuery("#orderfrm").serialize(),
        function(data){
            if (data) {
            } else {
                jQuery.post("cart.php", 'a=confdomains&ajax=1',
                    function(data){
                        if (data) {
                            jQuery("#domainconf-update").html(data);
                            jQuery("#domainconf-update").slideDown();
                        } else {
                        }
                    });
            }
        });
}
function showcats() {
    jQuery("#categories").slideToggle();
}
function selproduct(pid,num) {
    var snum;
    if(num){

    }else{
        num  = jQuery("#prodlabel"+pid).attr('data-product-num');
    }

    snum = ((1+parseInt(num))*100)-50;
    jQuery('#productslider').slider("value", snum);

    jQuery(".product").hide();
    jQuery("#product"+pid).show();
    jQuery(".sliderlabel").removeClass("selected");
    jQuery("#prodlabel"+pid).addClass("selected");
    jQuery(".pricing").text(jQuery("#prodlabel"+pid).attr('data-price'));
    last_val = num;
    setprodid(pid);
}

function selproduct_slider(snum) {
    var num,pid;
    num = Math.floor(snum/100);
    pid = productsNums[num];
    jQuery(".product").hide();
    jQuery("#product"+pid).show();
    jQuery(".sliderlabel").removeClass("selected");
    jQuery("#prodlabel"+pid).addClass("selected");
    jQuery(".pricing").text(jQuery("#prodlabel"+pid).attr('data-price'));
    last_val = num;
    setprodid(pid);
}

function recalcsummary(refresh) {
    var postf = 'a=view&cartsummary=1&ajax=1';
    if(refresh)
        postf += '&refresh=1';
    jQuery.post("cart.php", postf,function(data){        
        jQuery(".cart-right").html(data);
    });
}
function prodconfrecalcsummary()
{
    jQuery(".cartloader").fadeIn(100);
    jQuery.post("cart.php", 'ajax=1&a=confproduct&calctotal=true&'+jQuery("#orderfrm").serialize(),function(data)
    {
        jQuery.post("cart.php", 'a=view&cartsummary=1&ajax=1',function(data)
        {
            jQuery(".cart-right").html(data).promise().done(function()
            {
                jQuery(".cartloader:visible").fadeOut(100);
            });
        });
    });
    jQuery("input[name='previousbillingcycle']").val(jQuery("input[name='billingcycle']:checked").val());
}
function changecycle() {
    jQuery.post("cart.php", 'ajax=1&a=confproduct&calctotal=true&'+jQuery("#orderfrm").serialize(),
        function(data){
          var cycle = jQuery('[name="billingcycle"]:checked').val();
          jQuery('[name="previousbillingcycle"]').val(cycle);
          recalcsummary();
      });
}
function domainconfigupdate() {
    jQuery.post("cart.php", 'a=confdomains&update=1&ajax=1&'+jQuery("#domainconfigfrm").serialize(),
        function(data){
            recalcsummary();
        });
}
function recalctotals() {
    jQuery("#producttotal").append('<img src="assets/img/loading.gif" border="0" alt="Loading..." />');
    jQuery.post("cart.php", 'ajax=1&a=confproduct&calctotal=true&'+jQuery("#orderfrm").serialize(),
        function(data){
            jQuery("#producttotal").html(data);
        });
}
function addtocart(gid) {
    jQuery("#loading1").slideDown();
    jQuery.post("cart.php", 'ajax=1&a=confproduct&'+jQuery("#orderfrm").serialize(),
        function(data){
            if (data) {
                jQuery("#modal-error .modal-body").html(data);
                jQuery("#modal-error").modal('show');
            } else {
                if (gid) window.location='cart.php?gid='+gid;
                else window.location='cart.php?a=confdomains';
            }
        });
}
function domaincontactchange() {
    if (jQuery("#domaincontact").val()=="addingnew") {
        jQuery("#domaincontactfields").slideDown();
    } else {
        jQuery("#domaincontactfields").slideUp();
    }
}
function removepromo() 
{
    jQuery.post("cart.php", { a: "removepromo", ajax: 1 },
        function(data){
            recalcsummary();
        });
}
function applypromo(promo, callback) 
{
    if(!promo) promo = jQuery("#promocode").val();
    jQuery.post("cart.php", { a: "applypromo", promocode: promo },
        function(data){
            if (data) alert(data);
            else {
                recalcsummary();
        }
    });
}

function setprodid(pid){
    jQuery("#formaddprod").attr('action','cart.php?a=add&pid='+pid);
}

function removeItem(type,num) {
    var response = confirm("Are you sure you want to remove this item from your cart?");
    if (response) {
        jQuery.post("cart.php", 'a=remove&r='+type+'&i='+num,function() {
            recalcsummary();
        });
    }
}
function adddomain() 
{
  var pid = jQuery('#prouctidf').val();
  var domainoption = jQuery('#domainoption').val();
  var billingcycle = jQuery("#productbilling").val();
  var domainoption = jQuery(".domain-tabs .active input").val();
  jQuery.post("cart.php", 'ajax=1&a=add&pid='+pid+'&domainselect=1&billingcycle='+billingcycle+'&'+jQuery("#"+domainoption+" .domainfrm").serialize(),
    function(data){
        recalcsummary();
    });
}
function suggesteddomaincycle(elem) {
    if(!jQuery(elem).closest('tr').find('input').is(':checked')) 
    {
        jQuery(elem).closest('tr').find('input').prop('checked', true);
        adddomain()
    }
}
function showCCForm() {
    jQuery("#ccinputform").slideDown();
}
function hideCCForm() {
    jQuery("#ccinputform").slideUp();
}
function useExistingCC() {
    jQuery(".newccinfo").hide();
}
function enterNewCC() {
    jQuery(".newccinfo").show();
}
function showcustomns () {
    var nameservers = jQuery('.nameservers');
    if (nameservers.is(':visible')) {
        nameservers.fadeOut(200);
    } else {
        nameservers.fadeIn(200).css({'visibility':'visible'});
    }
}
function backTo(show_, hide_) {
    var show = jQuery(show_);
    var hide = jQuery(hide_);
    
    jQuery(hide).fadeOut(200).promise().done(function() {
        jQuery(show).fadeIn(200);
        jQuery('html, body').animate({
            scrollTop: jQuery(show).offset().top - 50
        }, 200);
    });
    
    return false;       
}
function currencychange() {
    jQuery.post("cart.php", 'a=view&cartsummary=1&ajax=1&currency='+jQuery("input[name=currency]:checked").val(),
    function(data){
        jQuery("#cartsummary").html(data);
    });
}
