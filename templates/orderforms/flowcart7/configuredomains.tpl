{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" title=$LANG.step2 desc=$LANG.step2desc step=2}
<script>
var _localLang = {
  'addToCart': '{$LANG.orderForm.addToCart|escape}',
  'addedToCartRemove': '{$LANG.orderForm.addedToCartRemove|escape}'
}
</script>

{if $errormessage}
<div class="alert alert-danger" role="alert">
  <p>{$LANG.orderForm.correctErrors}:</p>
  <ul>
    {$errormessage}
  </ul>
</div>
{/if}
<div class="row flowcart-row">
  <div class="col-md-12 ">
    <form method="post" action="{$smarty.server.PHP_SELF}?a=confdomains" id="frmConfigureDomains">
      <input type="hidden" name="update" value="true" />
      <p>{$LANG.orderForm.reviewDomainAndAddons}</p>
      {foreach $domains as $num => $domain}
      <hr>
        <h4>{$domain.domain}</h4>
        <p>{$LANG.orderregperiod} {$domain.regperiod} {$LANG.orderyears} {$LANG.hosting}
        {if $domain.hosting}<span class="text-sucess">[{$LANG.cartdomainshashosting}]</span>{else}<a href="cart.php" class="text-danger">[{$LANG.cartdomainsnohosting}]</a>{/if}</p>
      {if $domain.eppenabled}
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-addon"><i class="fas fa-lock"></i></span>
          <input type="text" name="epp[{$num}]" id="inputEppcode{$num}" value="{$domain.eppvalue}" class="form-control" placeholder="{$LANG.domaineppcode}" />
        </div>
<p class="help-block">{$LANG.domaineppcodedesc}</p>
        </div>
      {/if}
  {if $domain.dnsmanagement || $domain.emailforwarding || $domain.idprotection}
  <div class="row addon-products">
    {if $domain.dnsmanagement}
    <div class="col-sm-{math equation="12 / numAddons" numAddons=$domain.addonsCount}">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
         <div class="checkbox">
          <label>
            <input type="checkbox" name="dnsmanagement[{$num}]"{if $domain.dnsmanagementselected} checked{/if} />
            {$LANG.domaindnsmanagement}
          </label>
        </div>
          {$LANG.domainaddonsdnsmanagementinfo}
        </div>
        <div class="panel-price p-1">
          <h6>{$domain.dnsmanagementprice} / {$domain.regperiod} {$LANG.orderyears}</h6>
        </div>
      </div>
    </div>
    {/if}

    {if $domain.idprotection}
    <div class="col-sm-{math equation="12 / numAddons" numAddons=$domain.addonsCount}">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
          <div class="checkbox">
          <label>
            <input type="checkbox" name="idprotection[{$num}]"{if $domain.idprotectionselected} checked{/if} />{$LANG.domainidprotection}
          </label>
        </div>
          {$LANG.domainaddonsidprotectioninfo}
        </div>
        <div class="panel-price p-1">
          <h6>{$domain.idprotectionprice} / {$domain.regperiod} {$LANG.orderyears}</h6>
        </div>
      </div>
    </div>
    {/if}

    {if $domain.emailforwarding}
    <div class="col-sm-{math equation="12 / numAddons" numAddons=$domain.addonsCount}">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
          <div class="checkbox">
          <label>
            <input type="checkbox" name="emailforwarding[{$num}]"{if $domain.emailforwardingselected} checked{/if} />
            {$LANG.domainemailforwarding}
          </label>
        </div>
          {$LANG.domainaddonsemailforwardinginfo}
        </div>
        <div class="panel-price p-1">
        <h6>{$domain.emailforwardingprice} / {$domain.regperiod} {$LANG.orderyears}</h6>
        </div>

      </div>
    </div>
    {/if}

  </div>
  {/if}
  <div class="row">
    <div class="col-md-12">
  {foreach from=$domain.fields key=domainfieldname item=domainfield}
<div class="form-group"
    <label>{$domainfieldname}
    {$domainfield}
  </label>
  </div>
  {/foreach}
</div>
</div>
{/foreach}

{if $atleastonenohosting}
<h4>{$LANG.domainnameservers}</h4>
  <p>{$LANG.cartnameserversdesc}</p>

  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs1">{$LANG.domainnameserver1}</label>
        <input type="text" class="form-control" id="inputNs1" name="domainns1" value="{$domainns1}" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs2">{$LANG.domainnameserver2}</label>
        <input type="text" class="form-control" id="inputNs2" name="domainns2" value="{$domainns2}" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs3">{$LANG.domainnameserver3}</label>
        <input type="text" class="form-control" id="inputNs3" name="domainns3" value="{$domainns3}" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs1">{$LANG.domainnameserver4}</label>
        <input type="text" class="form-control" id="inputNs4" name="domainns4" value="{$domainns4}" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs5">{$LANG.domainnameserver5}</label>
        <input type="text" class="form-control" id="inputNs5" name="domainns5" value="{$domainns5}" />
      </div>
    </div>
  </div>
  {/if}
  <div class="text-right">
    <button type="submit" class="btn btn-primary btn-lg">{$LANG.continue}</button>
  </div>
</form>
</div>
</div>
