
{function name=getFontAwesomeCCIcon}
{if $ccType eq "Visa"}
fa-cc-visa
{elseif $ccType eq "MasterCard"}
fa-cc-mastercard
{elseif $ccType eq "Discover"}
fa-cc-discover
{elseif $ccType eq "American Express"}
fa-cc-amex
{elseif $ccType eq "JCB"}
fa-cc-jcb
{elseif $ccType eq "Diners Club" || $ccType eq "EnRoute"}
fa-cc-diners-club
{else}
fa-credit-card
{/if}
{/function}

<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-material nav-material-horizontal mb-1">
         <li class="pull-right flip"><a href="#" id="btnAlreadyRegistered" class="{if $loggedin || !$loggedin && $custtype eq "existing"} hidden{/if}">{$LANG.orderForm.alreadyRegistered}</a></li>
        <li class="pull-right flip"><a href="#" id="btnNewUserSignup" class="{if $loggedin || $custtype neq "existing"} hidden{/if}"><i class="fas fa-user-plus" aria-hidden="true"></i> {$LANG.orderForm.createAccount}</a></li>
    </ul>

    <form method="post" action="{$smarty.server.PHP_SELF}?a=checkout" name="orderfrm" id="frmCheckout">
      <input type="hidden" name="submit" value="true" />
      <input type="hidden" name="custtype" id="inputCustType" value="{$custtype}" />

      <div id="containerExistingUserSignin"{if $loggedin || $custtype neq "existing"} class="hidden"{/if}>

        <h6>{$LANG.orderForm.existingCustomerLogin}</h6>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="inputLoginEmail">{$LANG.orderForm.emailAddress}</label>
              <input type="text" name="loginemail" id="inputLoginEmail" class="form-control input-lg">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="inputLoginPassword">{$LANG.clientareapassword}</label>
              <input type="password" name="loginpassword" id="inputLoginPassword" class="form-control input-lg">
            </div>
          </div>
        </div>
      {include file="orderforms/$carttpl/linkedaccounts.tpl" linkContext="checkout-existing"}
      </div>

      <div id="containerNewUserSignup">
       <div{if $loggedin || $custtype eq "existing"} class="hidden"{/if}>
      {include file="orderforms/$carttpl/linkedaccounts.tpl" linkContext="checkout-new"}
      </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="inputFirstName">{$LANG.orderForm.firstName}</label>
              <input type="text" name="firstname" id="inputFirstName" class="form-control input-lg" value="{$clientsdetails.firstname}"{if $loggedin} readonly="readonly"{/if} autofocus>
            </div>
          </div>
          <div class="col-sm-4">
            <label for="inputLastName">{$LANG.orderForm.lastName}</label>
            <input type="text" name="lastname" id="inputLastName" class="form-control input-lg" value="{$clientsdetails.lastname}"{if $loggedin} readonly="readonly"{/if}>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="inputCompanyName">{$LANG.orderForm.companyName}</label>
              <input type="text" name="companyname" id="inputCompanyName" class="form-control input-lg" placeholder="({$LANG.orderForm.optional})" value="{$clientsdetails.companyname}"{if $loggedin} readonly="readonly"{/if}>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputEmail">{$LANG.orderForm.emailAddress}</label>
            <input type="email" name="email" id="inputEmail" class="form-control input-lg" value="{$clientsdetails.email}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputPhone">{$LANG.orderForm.phoneNumber}</label>
            <input type="tel" name="phonenumber" id="inputPhone" class="form-control input-lg" value="{$clientsdetails.phonenumber}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
      </div>
<hr class="flow-checkout">
{if !$loggedin}

    <div id="containerNewUserSecurity"{if (!$loggedin && $custtype eq "existing") || ($remote_auth_prelinked && !$securityquestions) } class="hidden"{/if}>
        <div id="containerPassword" class="row{if $remote_auth_prelinked && $securityquestions} hidden{/if}">
            <div id="passwdFeedback" style="display: none;" class="alert alert-info text-center col-sm-12"></div>
            <div class="col-sm-6">
                    <input type="password" name="password" id="inputNewPassword1" data-error-threshold="{$pwStrengthErrorThreshold}" data-warning-threshold="{$pwStrengthWarningThreshold}" class="field form-control input-lg" placeholder="{$LANG.clientareapassword}"{if $remote_auth_prelinked} value="{$password}"{/if}>
            </div>
            <div class="col-sm-6">
                    <input type="password" name="password2" id="inputNewPassword2" class="field form-control input-lg" placeholder="{$LANG.clientareaconfirmpassword}"{if $remote_auth_prelinked} value="{$password}"{/if}>
            </div>
            <div class="col-sm-6 mt-1">
                    <div class="progress-bar progress-bar-success progress-bar-striped" style="height:3px;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="passwordStrengthMeterBar">
                </div>
            </div>
            <div class="col-sm-6" style="margin-top:7px;">
                <p class="small text-muted" id="passwordStrengthTextLabel">{$LANG.pwstrength}: {$LANG.pwstrengthenter}</p>
            </div>
        </div>

{if $securityquestions}
<div class="row">
  <div class="col-sm-6">
    <select name="securityqid" id="inputSecurityQId" class="form-control input-lg">
      <option value="">{$LANG.clientareasecurityquestion}</option>
      {foreach $securityquestions as $question}
      <option value="{$question.id}"{if $question.id eq $securityqid} selected{/if}>
        {$question.question}
      </option>
      {/foreach}
    </select>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <input type="password" name="securityqans" id="inputSecurityQAns" class="form-control input-lg" placeholder="{$LANG.clientareasecurityanswer}">
    </div>
  </div>
</div>
{/if}
</div>
<hr class="flow-checkout">
{/if}
      <div class="row">
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputAddress1">{$LANG.orderForm.streetAddress}</label>
            <input type="text" name="address1" id="inputAddress1" class="form-control input-lg" value="{$clientsdetails.address1}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="inputAddress2">{$LANG.orderForm.streetAddress2}</label>
            <input type="text" name="address2" id="inputAddress2" class="form-control input-lg" value="{$clientsdetails.address2}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
      </div>
        <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="inputCity">{$LANG.orderForm.city}</label>
            <input type="text" name="city" id="inputCity" class="form-control input-lg" value="{$clientsdetails.city}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <label for="inputState">{$LANG.orderForm.state}</label>
            <input type="text" name="state" id="inputState" class="form-control input-lg" value="{$clientsdetails.state}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label for="inputPostcode">{$LANG.orderForm.postcode}</label>
            <input type="text" name="postcode" id="inputPostcode" class="form-control input-lg" value="{$clientsdetails.postcode}"{if $loggedin} readonly="readonly"{/if}>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputCountry">{$LANG.orderForm.country}</label>
            <select name="country" id="inputCountry" class="form-control input-lg"{if $loggedin} disabled="disabled"{/if}>
              {foreach $countries as $countrycode => $countrylabel}
              <option value="{$countrycode}"{if (!$country && $countrycode == $defaultcountry) || $countrycode eq $country} selected{/if}>
                {$countrylabel}
              </option>
              {/foreach}
            </select>
          </div>
        </div>
      </div>
<hr class="flow-checkout">
      {if $customfields}
        <h6>{$LANG.orderadditionalrequiredinfo}</h6>
        <div class="row">
          {foreach $customfields as $customfield}
          <div class="col-sm-6">
            <div class="form-group">
              <label for="customfield{$customfield.id}">{$customfield.name}</label>
              {$customfield.input}
              {if $customfield.description}
             <p class="help-block">
                {$customfield.description}
            </p>
              {/if}
            </div>
          </div>
          {/foreach}
        </div>
      {/if}
    </div>
    {if $domainsinorder}
    <div class="row">
      <div class="col-sm-12">
        <h6>{$LANG.domainregistrantinfo}</h6>
      <p class="small text-muted">{$LANG.orderForm.domainAlternativeContact}</p>
      <div class="form-group">
        <select name="contact" id="inputDomainContact" class="form-control input-lg">
          <option value="">{$LANG.usedefaultcontact}</option>
          {foreach $domaincontacts as $domcontact}
          <option value="{$domcontact.id}"{if $contact == $domcontact.id} selected{/if}>
            {$domcontact.name}
          </option>
          {/foreach}
          <option value="addingnew"{if $contact == "addingnew"} selected{/if}>
            {$LANG.clientareanavaddcontact}...
          </option>
        </select>
      </div>
    </div>
    </div>

    <div class="{if $contact neq "addingnew"} hidden{/if}" id="domainRegistrantInputFields">
      <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCFirstName">
            {$LANG.orderForm.firstName}
          </label>
          <input type="text" name="domaincontactfirstname" id="inputDCFirstName" class="form-control input-lg" value="{$domaincontact.firstname}">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCLastName">
            {$LANG.orderForm.lastName}
          </label>
          <input type="text" name="domaincontactlastname" id="inputDCLastName" class="form-control input-lg"  value="{$domaincontact.lastname}">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCCompanyName">
          {$LANG.orderForm.companyName} ({$LANG.orderForm.optional})
          </label>
          <input type="text" name="domaincontactcompanyname" id="inputDCCompanyName" class="form-control input-lg" value="{$domaincontact.companyname}">
        </div>
      </div>
      </div>
      <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="inputDCEmail">
            {$LANG.orderForm.emailAddress}
          </label>
          <input type="email" name="domaincontactemail" id="inputDCEmail" class="form-control input-lg"  value="{$domaincontact.email}">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label for="inputDCPhone">
            {$LANG.orderForm.phoneNumber}
          </label>
          <input type="tel" name="domaincontactphonenumber" id="inputDCPhone" class="form-control input-lg" value="{$domaincontact.phonenumber}">
        </div>
      </div>
    </div>
      <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label for="inputDCAddress1">
            {$LANG.orderForm.streetAddress}
          </label>
          <input type="text" name="domaincontactaddress1" id="inputDCAddress1" class="form-control input-lg" value="{$domaincontact.address1}">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCAddress2">
          {$LANG.orderForm.streetAddress2}
          </label>
          <input type="text" name="domaincontactaddress2" id="inputDCAddress2" class="form-control input-lg" value="{$domaincontact.address2}">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCCity">
          {$LANG.orderForm.city}
          </label>
          <input type="text" name="domaincontactcity" id="inputDCCity" class="form-control input-lg" value="{$domaincontact.city}">
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label for="inputDCState">
         {$LANG.orderForm.state}
          </label>
          <input type="text" name="domaincontactstate" id="inputDCState" class="form-control input-lg" value="{$domaincontact.state}">
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label for="inputDCPostcode">
          {$LANG.orderForm.postcode}
          </label>
          <input type="text" name="domaincontactpostcode" id="inputDCPostcode" class="form-control input-lg"  value="{$domaincontact.postcode}">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-9">
        <div class="form-group">
          <label for="domaincontactcountry">{$LANG.orderForm.country}</label>
          <select name="domaincontactcountry" id="inputDCCountry" class="form-control input-lg">
            {foreach $countries as $countrycode => $countrylabel}
            <option value="{$countrycode}"{if (!$domaincontact.country && $countrycode == $defaultcountry) || $countrycode eq $domaincontact.country} selected{/if}>
              {$countrylabel}
            </option>
            {/foreach}
          </select>
        </div>
      </div>
    </div>
</div>
{/if}

{foreach $hookOutput as $output}
    <div>
        {$output}
    </div>
{/foreach}

{if $canUseCreditOnCheckout}
    <div id="applyCreditContainer" class="apply-credit-container" data-apply-credit="{$applyCredit}">
        <p>{lang key='cart.availableCreditBalance' amount=$creditBalance}</p>

        {if $creditBalance->toNumeric() >= $total->toNumeric()}
            <label class="radio">
                <input id="useFullCreditOnCheckout" type="radio" name="applycredit" value="1"{if $applyCredit} checked{/if}>
                {lang key='cart.applyCreditAmountNoFurtherPayment' amount=$total}
            </label>
        {else}
            <label class="radio">
                <input id="useCreditOnCheckout" type="radio" name="applycredit" value="1"{if $applyCredit} checked{/if}>
                {lang key='cart.applyCreditAmount' amount=$creditBalance}
            </label>
        {/if}

        <label class="radio">
            <input id="skipCreditOnCheckout" type="radio" name="applycredit" value="0"{if !$applyCredit} checked{/if}>
            {lang key='cart.applyCreditSkip' amount=$creditBalance}
        </label>
    </div>
{/if}
<div id="paymentGatewaysContainer" class="form-group">
        <h6>{$LANG.orderpaymentmethod}</h6>
        <div class="text-left">
            {foreach key=num item=gateway from=$gateways}
                <label class="{if $flowcart_Payment_Method_Display=='Inline Radio'}radio-inline{else}radio{/if}">
                    <input type="radio" name="paymentmethod" value="{$gateway.sysname}" class="payment-methods{if $gateway.type eq "CC"} is-credit-card{/if}"{if $selectedgateway eq $gateway.sysname} checked{/if} />
                    {$gateway.name}
                </label>
            {/foreach}
    </div>
</div>
    <div class="alert alert-danger text-center gateway-errors hidden"></div>
    <div id="flowcart_data_holder" class="hidden"></div>
    <div id="creditCardInputFields"{if $selectedgatewaytype neq "CC"} class="hidden"{/if}>
      {if $clientsdetails.cclastfour}
      <div class="row margin-bottom">
        <div class="col-sm-12">
          <div class="text-left pb-2">
            <label class="radio-inline">
              <input type="radio" name="ccinfo" value="useexisting" id="useexisting" {if $clientsdetails.cclastfour} checked{else} disabled{/if} />
              {$LANG.creditcarduseexisting}
              {if $clientsdetails.cclastfour}
              ({$clientsdetails.cclastfour})
              {/if}
            </label>
            <label class="radio-inline">
              <input type="radio" name="ccinfo" value="new" id="new" {if !$clientsdetails.cclastfour || $ccinfo eq "new"} checked{/if} />
              {$LANG.creditcardenternewcard}
            </label>
          </div>
        </div>
      </div>
      {else}
      <input type="hidden" name="ccinfo" value="new" />
      {/if}
      <div id="newCardInfo" class="row{if $clientsdetails.cclastfour && $ccinfo neq "new"} hidden{/if}">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="cctype">
            {$LANG.creditcardcardtype}
            </label>
            <input type="hidden" id="cctype" name="cctype" value="{$acceptedcctypes.0}" />
            <div class="dropdown" id="cardType">
              <button class="btn btn-default btn-lg btn-block dropdown-toggle field" type="button" id="creditCardType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span id="selectedCardType">
                  <i class="fab {getFontAwesomeCCIcon ccType=$acceptedcctypes.0} fa-fw"></i>
                  {$acceptedcctypes.0}
                </span>
                <span class="fas fa-caret-down fa-fw"></span>
              </button>
              <ul class="dropdown-menu" id="creditCardTypeDropDown" aria-labelledby="creditCardType">
                {foreach $acceptedcctypes as $cardType}
                <li>
                  <a href="#">
                    <i class="fab {getFontAwesomeCCIcon ccType=$cardType} fa-fw"></i>
                    <span class="type">
                      {$cardType}
                    </span>
                  </a>
                </li>
                {/foreach}
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardNumber">
              {$LANG.orderForm.cardNumber}
            </label>
            <input type="tel" name="ccnumber" id="inputCardNumber" class="form-control input-lg" placeholder="•••• •••• •••• ••••" autocomplete="cc-number">
          </div>
        </div>
        {if $showccissuestart}
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardStart">
              <i class="fas fa-calendar-check-o"></i>
            </label>
            <input type="tel" name="ccstartdate" id="inputCardStart" class="form-control input-lg" placeholder="MM / YY ({$LANG.creditcardcardstart})" autocomplete="cc-exp">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardIssue">
              <i class="fas fa-asterisk"></i>
            </label>
            <input type="tel" name="ccissuenum" id="inputCardIssue" class="form-control input-lg" placeholder="{$LANG.creditcardcardissuenum}">
          </div>
        </div>
        {/if}
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardExpiry">
              {$LANG.creditcardcardexpires} MM / YY
            </label>
            <input type="tel" name="ccexpirydate" id="inputCardExpiry" class="form-control input-lg" placeholder="MM / YY{if $showccissuestart} ({$LANG.creditcardcardexpires}){/if}" autocomplete="cc-exp">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardCVV">
              {$LANG.orderForm.cvv}
            </label>
            <input type="tel" name="cccvv" id="inputCardCVV" class="form-control input-lg"  placeholder="CVV" autocomplete="cc-cvc">
          </div>
        </div>
      </div>
      <div id="existingCardInfo" class="row{if !$clientsdetails.cclastfour || $ccinfo eq "new"} hidden{/if}">
        <div class="col-sm-12">
          <div class="form-group">
            <label for="inputCardCvvExisting">
              {$LANG.orderForm.cvv}
            </label>
            <input type="tel" name="cccvvexisting" id="inputCardCvvExisting" class="form-control input-lg" placeholder="{$LANG.orderForm.cvv}" autocomplete="cc-cvc">
          </div>
        </div>
      </div>
    </div>

    {if $shownotesfield}
    <div id="additional_notes">
      <h6>{$LANG.orderForm.additionalNotes}</h6>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <textarea name="notes" class="form-control" rows="4" placeholder="{$LANG.ordernotesdescription}">{$orderNotes}</textarea>
          </div>
        </div>
      </div>
    </div>
    {/if}

  {if $showMarketingEmailOptIn}
  <div class="{if $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment (Wide)' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button'}hidden{/if}">
  <div class="form-group">
      <label class="checkbox-inline">
            <input type="checkbox" name="marketingoptin" id="marketingoptin" value="1"{if $marketingEmailOptIn} checked{/if}> {lang key='emailMarketing.joinOurMailingList'}
        </label>
  </div>
  </div>
  {/if}


          {if $accepttos}
          <p class="{if $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment (Wide)' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button'}hidden{/if}">
            <label class="checkbox-inline">
              <input type="checkbox" name="accepttos" id="accepttos" />
              &nbsp;
              {$LANG.ordertosagreement}
              <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a>
            </label>
          </p>
          {/if}
          <div class="text-right">
          <button type="submit"
          id="btnCompleteOrder"
          class="{if $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button and Payment (Wide)' || $flowcart_final_order_button_tosidebar == 'Sidebar Order Button'}hidden{/if} btn btn-primary btn-lg disable-on-click spinner-on-click{if $recaptcha} btn-recaptcha{/if}{if $recaptchaInvisible} btn-recaptcha-invisible{/if}"
          {if $cartitems==0} disabled="disabled"{/if} onclick="this.value='{$LANG.pleasewait}'">
            {$LANG.completeorder}
          </button>
        </div>
  </form>
</div>
</div>
<script type="text/javascript" src="{$BASE_PATH_JS}/jquery.payment.js"></script>
