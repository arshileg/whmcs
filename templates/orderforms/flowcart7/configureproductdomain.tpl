{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" title=$LANG.step2 desc=$LANG.step2desc step=2}
<script>
jQuery(document).ready(function(){
  jQuery('.domain-tabs li:first-child').addClass('active').find('input').prop("checked", true);
});
</script>
<div class="row flowcart-row">
  <div class="col-md-12">
    <div id="domain-search" class="domain-search-cart">
      <ul class="nav nav-material nav-material-horizontal domain-tabs">
        {if $incartdomains}
        <li><a href="#incart" aria-controls="incart" role="tab" data-toggle="tab"><label class="domainoptions"><i class="fas fa-cart-arrow-down"></i>
          <input type="radio" name="domainoption" value="incart" id="selincart" />{$LANG.cartproductdomainuseincart}</label></a>
        </li>
        {/if}
        {if $registerdomainenabled}
        <li><a href="#register" aria-controls="register" role="tab" data-toggle="tab"><label class="domainoptions"><i class="fas fa-globe"></i>
          <input type="radio" name="domainoption" value="register" id="selregister" />{$LANG.cartregisterdomainchoice|sprintf2:$companyname}</label></a>
        </li>
        {/if}
        {if $transferdomainenabled}
        <li><a href="#transfer" aria-controls="transfer" role="tab" data-toggle="tab"><label class="domainoptions"><i class="fas fa-truck"></i>
          <input type="radio" name="domainoption" value="transfer" id="seltransfer" />{$LANG.domainstransfer}</label></a>
        </li>
        {/if}
        {if $owndomainenabled}
        <li><a href="#owndomain" aria-controls="owndomain" role="tab" data-toggle="tab"><label class="domainoptions"><i class="fas fa-exchange"></i>
          <input type="radio" name="domainoption" value="owndomain" id="selowndomain" />{$LANG.flowcartuseexisting}</label></a>
        </li>
        {/if}
        {if $subdomains}
        <li><a href="#subdomain" aria-controls="subdomain" role="tab" data-toggle="tab"><label class="domainoptions">
          <input type="radio" name="domainoption" value="subdomain" id="selsubdomain" />{$LANG.orderusesubdomain}</label></a>
        </li>
        {/if}
      </ul>
    </div>
    <form id="frmProductDomain">
      <input type="hidden" id="frmProductDomainPid" value="{$pid}" />
      <div class="domain-selection-options tab-content">
        {if $incartdomains}
        <div class="option tab-pane active" id="incart">
          <div class="domain-input-group input-group-lg clearfix" id="domainincart">
            <div class="row">
              <div class="col-sm-10">
                <div class="form-group">
                  <div class="domains-row">
                    <select id="incartsld" name="incartdomain" class="form-control input-lg">
                      {foreach key=num item=incartdomain from=$incartdomains}
                      <option value="{$incartdomain}">{$incartdomain}</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block btn-lg">
                  {$LANG.orderForm.use}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/if}
        {if $registerdomainenabled}
        <div class="option tab-pane {if !$incartdomains}active{/if}" id="register">
          <div class="domain-input-group clearfix" id="domainregister">
            <div class="row">
              <div class="col-md-10">
                <div class="row domains-row">
                  <div class="col-sm-10">
                    <div class="form-group">
                      <div class="input-group input-group-lg">
                        <span class="input-group-addon">{$LANG.orderForm.www}</span>
                        <input type="text" id="registersld" value="{$sld}" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="{lang key='orderForm.enterDomain'}" />
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      <select id="registertld" class="form-control input-lg">
                        {foreach from=$registertlds item=listtld}
                        <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                        {/foreach}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn-lg btn-block">
                  {$LANG.orderForm.check}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/if}
        {if $transferdomainenabled}
        <div class="option tab-pane {if !$registerdomainenabled && !$incartdomains}active{/if}" id="transfer">
          <div class="domain-input-group clearfix" id="domaintransfer">
            <div class="row">
              <div class="col-md-10">
                <div class="row domains-row">
                  <div class="col-sm-10">
                    <div class="form-group">
                      <div class="input-group input-group-lg">
                        <span class="input-group-addon">www.</span>
                        <input type="text" id="transfersld" value="{$sld}" class="form-control" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="{lang key='orderForm.enterDomain'}"/>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      <select id="transfertld" class="form-control input-lg">
                        {foreach from=$transfertlds item=listtld}
                        <option value="{$listtld}"{if $listtld eq $tld} selected="selected"{/if}>{$listtld}</option>
                        {/foreach}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn-block btn-lg">
                  {$LANG.orderForm.transfer}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/if}
        {if $owndomainenabled}
        <div class="option tab-pane {if !$transferdomainenabled && !$registerdomainenabled && !$incartdomains}active{/if}" id="owndomain">
          <div class="domain-input-group clearfix" id="domainowndomain">
            <div class="row">
              <div class="col-sm-8">
                <div class="domains-row">
                    <div class="form-group">
                      <div class="input-group input-group-lg">
                        <span class="input-group-addon">www.</span>
                      <input type="text" id="owndomainsld" value="{$sld}" placeholder="{$LANG.yourdomainplaceholder}" class="form-control input-lg" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="{lang key='orderForm.enterDomain'}" />
                    </div>
                  </div>
                  </div>
                </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      <input type="text" id="owndomaintld" value="{$tld|substr:1}" placeholder="{$LANG.yourtldplaceholder}" class="form-control input-lg" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="{lang key='orderForm.required'}" />
                    </div>
                  </div>
              <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block btn-lg" id="useOwnDomain">
                  {$LANG.orderForm.use}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/if}
        {if $subdomains}
        <div class="option tab-pane" id="subdomain">
          <div class="domain-input-group clearfix" id="domainsubdomain">
            <div class="row">
              <div class="col-md-10">
                <div class="row domains-row">
                  <div class="col-sm-1">
                    <p class="form-control-static input-lg">http://</p>
                  </div>
                  <div class="col-sm-5">
                    <div class="form-group">
                      <input type="text" id="subdomainsld" value="{$sld}" placeholder="yourname" class="form-control input-lg" autocapitalize="none" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="{lang key='orderForm.enterDomain'}" />
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div class="form-group">
                      <select id="subdomaintld" class="form-control input-lg">
                        {foreach $subdomains as $subid => $subdomain}
                        <option value="{$subid}">{$subdomain}</option>
                        {/foreach}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn-block btn-lg">
                  {$LANG.orderForm.check}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/if}
      </div>
      {if $freedomaintlds}
      <p>* <em>{$LANG.orderfreedomainregistration} {$LANG.orderfreedomainappliesto}: {$freedomaintlds}</em></p>
      {/if}

    </form>
  </div>
  <div class="col-md-12">
    <form method="post" action="cart.php?a=add&pid={$pid}&domainselect=1" id="frmProductDomainSelections">
      <div id="DomainSearchResults" class="hidden">
        <div id="searchDomainInfo">
          <p id="primaryLookupSearching" class="domain-lookup-loader domain-lookup-primary-loader domain-searching domain-checker-result-headline">
            <i class="fas fa-circle-o-notch fa-spin"></i>
            <span class="domain-lookup-register-loader">{lang key='orderForm.checkingAvailability'}...</span>
            <span class="domain-lookup-transfer-loader">{lang key='orderForm.verifyingTransferEligibility'}...</span>
            <span class="domain-lookup-other-loader">{lang key='orderForm.verifyingDomain'}...</span>
          </p>
          <div id="primaryLookupResult" class="domain-lookup-result well well-lg well-primary domain-lookup-primary-results hidden">
              <div class="domain-unavailable domain-checker-unavailable headline"><i class="fas fa-times-circle fa-lg text-danger" aria-hidden="true"></i> {lang key='orderForm.domainIsUnavailable'}</div>
              <div class="domain-available domain-checker-available"><i class="fas fa-check-circle fa-3x fa-pull-left text-success" aria-hidden="true"></i> {$LANG.domainavailable1} <strong></strong> {$LANG.domainavailable2}</div>
              <div class="btn btn-primary domain-contact-support headline">{$LANG.domainContactUs}</div>
              <div class="transfer-eligible">
                <p class="domain-checker-available headline">{lang key='orderForm.transferEligible'}</p>
                <p>{lang key='orderForm.transferUnlockBeforeContinuing'}</p>
              </div>
              <div class="transfer-not-eligible">
                <p class="domain-checker-unavailable headline">{lang key='orderForm.transferNotEligible'}</p>
                <p>{lang key='orderForm.transferNotRegistered'}</p>
                <p>{lang key='orderForm.trasnferRecentlyRegistered'}</p>
                <p>{lang key='orderForm.transferAlternativelyRegister'}</p>
              </div>
              <div class="domain-invalid">
                <p class="domain-checker-unavailable headline">{lang key='orderForm.domainInvalid'}</p>
                <p>
                  {lang key='orderForm.domainLetterOrNumber'}<span class="domain-length-restrictions">{lang key='orderForm.domainLengthRequirements'}</span><br />
                  {lang key='orderForm.domainInvalidCheckEntry'}
                </p>
              </div>
              <div class="domain-price">
                <span class="register-price-label">{lang key='orderForm.domainPriceRegisterLabel'}</span>
                <span class="transfer-price-label hidden">{lang key='orderForm.domainPriceTransferLabel'}</span>
                <span class="price"></span>
              </div>
              <input type="hidden" id="resultDomainOption" name="domainoption" />
              <input type="hidden" id="resultDomain" name="domains[]" />
              <input type="hidden" id="resultDomainPricingTerm" />
            </div>
        </div>

        {if $registerdomainenabled}
        {if $spotlightTlds}
        <div id="spotlightTlds" class="spotlight-tlds clearfix hidden">
          <div class="spotlight-tlds-container">
            {foreach $spotlightTlds as $key => $data}
            <div class="spotlight-tld-container spotlight-tld-container-{$spotlightTlds|count}">
              <div id="spotlight{$data.tldNoDots}" class="spotlight-tld">
                {if $data.group}
                <div class="spotlight-tld-{$data.group}">{$data.groupDisplayName}</div>
                {/if}
                <h4>{$data.tld}</h4>
                <span class="domain-lookup-loader domain-lookup-spotlight-loader">
                  <i class="fas fa-circle-o-notch fa-spin"></i>
                </span>
                <div class="domain-lookup-result">
                  <button type="button" class="btn unavailable hidden" disabled="disabled">
                    {lang key='domainunavailable'}
                  </button>
                  <button type="button" class="btn invalid hidden" disabled="disabled">
                    {lang key='domainunavailable'}
                  </button>
                  <span class="available price hidden">{$data.register}</span>
                  <button type="button" class="btn hidden btn-add-to-cart" data-whois="0" data-domain="">
                    <span class="to-add"><i class="fas fa-plus"></i> {lang key='orderForm.add'}</span>
                    <span class="added">{lang key='domaincheckeradded'}</span>
                    <span class="unavailable">{$LANG.domaincheckertaken}</span>
                  </button>
                  <button type="button" class="btn btn-primary domain-contact-support hidden">Contact Support to Purchase</button>
                </div>
              </div>
            </div>
            {/foreach}
          </div>
        </div>
        {/if}

        <div class="suggested-domains hidden">
          <h6>{lang key='orderForm.suggestedDomains'}</h6>
          <div id="suggestionsLoader" class="panel-body domain-lookup-loader domain-lookup-suggestions-loader">
            <i class="fas fa-circle-o-notch fa-spin"></i> {lang key='orderForm.generatingSuggestions'}
          </div>
          <ul id="domainSuggestions" class="domain-lookup-result list-group hidden">
            <li class="domain-suggestion list-group-item hidden">
              <span class="promo hidden"></span>
              <span class="domain"></span><span class="extension"></span>
              <button type="button" class="btn btn-add-to-cart product-domain" data-whois="1" data-domain="">
                <span class="to-add"><i class="fas fa-plus"></i> {$LANG.addtocart}</span>
                <span class="added">{lang key='domaincheckeradded'}</span>
                <span class="unavailable">{$LANG.domaincheckertaken}</span>
              </button>
              <button type="button" class="btn btn-primary domain-contact-support hidden">Contact Support to Purchase</button>
              <span class="price"></span>
            </li>
          </ul>
          <div class="panel-footer more-suggestions hidden">
            <a id="moreSuggestions" href="#" onclick="loadMoreSuggestions();return false;">{lang key='domainsmoresuggestions'}</a>
            <span id="noMoreSuggestions" class="no-more small hidden">{lang key='domaincheckernomoresuggestions'}</span>
          </div>
          <div class="text-left text-muted domain-suggestions-warning">
            <p class="small">{lang key='domainssuggestionswarnings'}</p>
          </div>
        </div>
        {/if}
        <div class="text-right">
          <button id="btnDomainContinue" type="submit" class="btn btn-primary btn-lg hidden" disabled="disabled">{$LANG.continue}</button>
        </div>
      </div>
    </form>
  </div>
</div>
