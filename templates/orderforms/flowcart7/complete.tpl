{include file="orderforms/$carttpl/common.tpl"}
  <div class="alert alert-info order-confirmation">
  <h4>{$LANG.orderconfirmation}</h4>
    {$LANG.ordernumberis} <span>{$ordernumber}</span>
  </div>
  <div class="row flowcart-row">
    <div class="col-md-12">
  <p class="lead">{$LANG.orderreceived}</p>
  <p>{$LANG.orderfinalinstructions}</p>
            {if $invoiceid && !$ispaid}
                <div class="alert alert-warning">
                    {$LANG.ordercompletebutnotpaid}
                    <a href="viewinvoice.php?id={$invoiceid}" target="_blank" class="alert-link">
                        {$LANG.invoicenumber}{$invoiceid}
                    </a>
                </div>
            {/if}

            {foreach $addons_html as $addon_html}
                <div class="order-confirmation-addon-output">
                    {$addon_html}
                </div>
            {/foreach}

            {if $ispaid}
                <!-- Enter any HTML code which should be displayed when a user has completed checkout here -->
                <!-- Common uses of this include conversion and affiliate tracking scripts -->
            {/if}
          <div class="form-group">
                <a href="clientarea.php" class="btn btn-default">{$LANG.orderForm.continueToClientArea}</a>
            </div>
        </div>
    </div>
