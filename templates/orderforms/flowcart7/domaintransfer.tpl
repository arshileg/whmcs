{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" title=$LANG.transferdomain desc=$groupname step=2}
<div class="row flowcart-row">
  <div class="col-md-12">
    <h4>
      {$LANG.transferdomain}
    </h4>
    <p>{lang key='orderForm.transferExtend'}. {lang key='orderForm.extendExclusions'}.</p>
    <form method="post" action="cart.php" id="frmDomainTransfer">
      <input type="hidden" name="a" value="addDomainTransfer">
      <div class="row">
        <div class="col-md-8">
            <div class="form-group">
              <label for="inputTransferDomain">{lang key='domainname'}</label>
              <input type="text" class="form-control" name="domain" id="inputTransferDomain" value="{$lookupTerm}" placeholder="{lang key='yourdomainplaceholder'}.{lang key='yourtldplaceholder'}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.enterDomain'}" />
            </div>
                    </div>
            <div class="col-md-4">
            <div class="form-group">
              <label for="inputAuthCode" style="width:100%;">
                {lang key='orderForm.authCode'}
              </label>
              <input type="text" class="form-control" name="epp" id="inputAuthCode" placeholder="{lang key='orderForm.authCodePlaceholder'}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.required'}" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div id="transferUnavailable" class="alert alert-warning slim-alert text-center hidden"></div>
            {if $captcha}
            <div class="captcha-container" id="captchaContainer">
              {if $captcha eq "recaptcha"}
              <script src="https://www.google.com/recaptcha/api.js" async defer></script>
              <div id="google-recaptcha" class="g-recaptcha recaptcha-transfer center-block" data-sitekey="{$reCaptchaPublicKey}" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="{lang key='orderForm.required'}" ></div>
              {else}
              <div class="default-captcha">
                <p>{lang key="cartSimpleCaptcha"}</p>
                <div>
                  <img id="inputCaptchaImage" src="includes/verifyimage.php" />
                  <input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control input-sm" data-toggle="tooltip" data-placement="right" data-trigger="manual" title="{lang key='orderForm.required'}" />
                </div>
              </div>
              {/if}
            </div>
            {/if}
          <button type="submit" id="btnTransferDomain" class="btn btn-lg btn-primary btn-transfer">
            <span class="loader hidden" id="addTransferLoader">
              <i class="fas fa-fw fa-spinner fa-spin"></i>
            </span>
            <span id="addToCart">{lang key="orderForm.addToCart"}</span>
          </button>
        </div>
      </div>
    </form>
    <p class="t small"></p>
  </div>
</div>
