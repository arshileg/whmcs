{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/products-header.tpl"}

{assign var="itemsprow" value="6"}
{assign var="itemsprow_c" value=0}
{assign var="col" value=flowcart_group_column_width($gid,$group_column_width)}
{if $col == 'col-md-15'}{assign var="itemsprow" value="5"}
{elseif $col == 'col-md-3'}{assign var="itemsprow" value="4"}
{elseif $col == 'col-md-4'}{assign var="itemsprow" value="3"}
{elseif $col == 'col-md-6'}{assign var="itemsprow" value="2"}
{elseif $col == 'col-md-12'}{assign var="itemsprow" value="1"}
{/if}
{if !$loggedin && $currencies && !$flowcart_hide_currency_switch}
<div class="row pt-1">
  <div class="col-md-12">
    <div class="pull-right currency-product">
      {if count($currencies) < 4 }
        {foreach from=$currencies item=curr}
        <a href="cart.php?gid={$gid}&currency={$curr.id}"><i class="fas fa-money{if $curr.id eq $currency.id} active{/if}"></i> <span>{$curr.code}</span></a>
        {/foreach}
      {else}
        <div class="btn-group">
          <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {$LANG.choosecurrency} <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            {foreach from=$currencies item=curr}
             <li><a href="cart.php?gid={$gid}&currency={$curr.id}"><i class="fas fa-money{if $curr.id eq $currency.id} active{/if}"></i> <span>{$curr.code}</span></a></li>
            {/foreach}
          </ul>
        </div>
      {/if}
    </div>
  </div>
</div>
{/if}
<div class="cart-wrapper">
<div id="product-packages">

{if $flowcart_show_Product_Group_Headline || $flowcart_show_Product_Group_Tagline}
<div class="row">
<div class="col-md-12">
        <div class="header-lined">
        {if $flowcart_show_Product_Group_Headline}
                <h1>
                    {if $productGroup.headline}
                        {$productGroup.headline}
                    {else}
                        {$productGroup.name}
                    {/if}
                </h1>
        {/if}
        {if $flowcart_show_Product_Group_Tagline}
                {if $productGroup.tagline}
                    <p>{$productGroup.tagline}</p>
                {/if}
        {/if}
            </div>
</div>
</div>
{/if}
{if count($productGroup.features) > 0 && $flowcart_show_Group_Features=="Above Pricing"}
    <div class="includes-container">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="head-area">
                    <span>
                        {$LANG.whatIsIncluded}
                    </span>
                </div>
                <ul id="list-contents" class="list-contents productGroupfeatures">
                    {foreach $productGroup.features as $features}
                        <li class="{$flowcart_features_columns}"><i class="flow-icon {$flowcart_feature_icon}"></i>{$features.feature}</li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}

{foreach $hookAboveProductsOutput as $output}
  <div>
      {$output}
  </div>
{/foreach}

    {foreach key=num item=product from=$products name=mainProducts}
    {if $itemsprow_c == 0}
    <div class="row pricing">
    {/if}
    {if $num == 0}{assign var="order" value="first"}
    {elseif $num == 1}{assign var="order" value="second"}
    {elseif $num == 2}{assign var="order" value="third"}
    {elseif $num == 3}{assign var="order" value="fourth"}
    {elseif $num == 4}{assign var="order" value="fifth"}
    {elseif $num == 5}{assign var="order" value="sixth"}
    {/if}
    <div class="{$col} fadeInCart animatedProduct pricing-{$order} {if !$product.features}no-product-features{/if} {if empty(trim($product.featuresdesc))}no-product-featuresdesc{/if}">
      <div class="plan"><div class="header">
        <h4>{$product.name}</h4>
      </div>
      {if $product.features}
      <ul>
        {foreach from=$product.features key=feature item=value}
        <li>
          {if $value|strstr:'YES'}{$value|replace:'YES':'<i class="fas fa-lg fa-check"></i>'} <small>{$feature}</small>
          {elseif $value|strstr:'NO'}{$value|replace:'NO':'<i class="fas fa-lg fa-times"></i>'} <small>{$feature}</small>
          {else} <strong>{$value}</strong> <small>{$feature}</small>{/if}
        </li>
        {/foreach}
      </ul>
      {/if}
      {if !empty(trim($product.featuresdesc))}
      <p>
              {if $flowcart_filter_BR_line_break}
                {$product.featuresdesc|regex_replace:"/(<br>|<br [^>]*>|<\\/>)/":""}
              {else}
                {$product.featuresdesc|nl2br|trim}
              {/if}
      </p>
      {/if}
      <div class="product-pricing price" id="product{$product@iteration}-price">
          {if $product.bid}
              {$LANG.bundledeal}<br>
              {if $product.displayprice}
                  <h5>{$product.displayprice}</h5>
              {/if}
          {else}
              {if $product.pricing.hasconfigoptions}
                  <h6>{$LANG.startingfrom}</h6>
              {/if}
              <h5>{$product.pricing.minprice.price}</h5>
              {if $product.pricing.minprice.cycle eq "monthly"}
                  <h6>{$LANG.orderpaymenttermmonthly}</h6>
              {elseif $product.pricing.minprice.cycle eq "quarterly"}
                  <h6>{$LANG.orderpaymenttermquarterly}</h6>
              {elseif $product.pricing.minprice.cycle eq "semiannually"}
                  <h6>{$LANG.orderpaymenttermsemiannually}</h6>
              {elseif $product.pricing.minprice.cycle eq "annually"}
                  <h6>{$LANG.orderpaymenttermannually}</h6>
              {elseif $product.pricing.minprice.cycle eq "biennially"}
                  <h6>{$LANG.orderpaymenttermbiennially}</h6>
              {elseif $product.pricing.minprice.cycle eq "triennially"}
                  <h6>{$LANG.orderpaymenttermtriennially}</h6>
              {/if}
              <br>
              {if $product.pricing.minprice.setupFee}
                  <small>{$product.pricing.minprice.setupFee->toPrefixed()} {$LANG.ordersetupfee}</small>
              {/if}
          {/if}
      </div>
      <a class="btn btn-default btn-sm {if $product.qty eq '0'} disabled{/if}" href="{$smarty.server.PHP_SELF}?a=add&{if $product.bid}bid={$product.bid}{else}pid={$product.pid}{/if}">{$LANG.ordernowbutton}</a></div>
    </div>

    {assign var="itemsprow_c" value=$itemsprow_c+1}
    {if $itemsprow_c == $itemsprow}
      </div>
      {assign var="itemsprow_c" value=0}
    {/if}
    {/foreach}
    {if $itemsprow_c != $itemsprow}
      </div>
      {assign var="itemsprow_c" value=0}
    {/if}

{if count($productGroup.features) > 0 && $flowcart_show_Group_Features=="Below Pricing"}
    <div class="includes-container">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="head-area">
                    <span>
                        {$LANG.whatIsIncluded}
                    </span>
                </div>
                <ul id="list-contents" class="list-contents productGroupfeatures">
                    {foreach $productGroup.features as $features}
                        <li class="{$flowcart_features_columns}"><i class="flow-icon {$flowcart_feature_icon}"></i>{$features.feature}</li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}

{foreach $hookBelowProductsOutput as $output}
 <div>
    {$output}
 </div>
{/foreach}

</div>
</div>
