{include file="orderforms/flowcart7/common.tpl"}
{include file="orderforms/flowcart7/custom-styles.tpl"}
{include file="orderforms/flowcart7/cart-header.tpl" title=$LANG.registerdomain desc=$groupname step=2}

            <div class="row flowcart-row">
                <div class="col-md-8">
                    {if $totalResults < $totalDomainCount}
                            {lang key='domainRenewal.showingDomains' showing=$totalResults totalCount=$totalDomainCount}
                            <a id="linkShowAll" href="{routePath('cart-domain-renewals')}">{lang key='domainRenewal.showAll'}</a>
                    {/if}
                    <div id="domainRenewals" class="mt-1">                                            
                        {foreach $renewalsData as $renewalData}
                            <div class="domain-renewal well" data-domain="{$renewalData.domain}">
                                    {if !$renewalData.eligibleForRenewal}
                                        <span class="label label-info">
                                            {lang key='domainRenewal.unavailable'}
                                        </span>
                                    {elseif ($renewalData.pastGracePeriod && $renewalData.pastRedemptionGracePeriod)}
                                        <span class="label label-info">
                                            {lang key='domainrenewalspastgraceperiod'}
                                        </span>
                                    {elseif !$renewalData.beforeRenewLimit && $renewalData.daysUntilExpiry > 0}
                                        <span class="label label-{if $renewalData.daysUntilExpiry > 30}success{else}warning{/if}">
                                            {lang key='domainRenewal.expiringIn' days=$renewalData.daysUntilExpiry}
                                        </span>
                                    {elseif $renewalData.daysUntilExpiry === 0}
                                        <span class="label label-grey">
                                            {lang key='expiresToday'}
                                        </span>
                                    {elseif $renewalData.beforeRenewLimit}
                                        <span class="label label-info">
                                            {lang key='domainRenewal.maximumAdvanceRenewal' days=$renewalData.beforeRenewLimitDays}
                                        </span>
                                    {else}
                                        <span class="label label-danger">
                                            {lang key='domainRenewal.expiredDaysAgo' days=$renewalData.daysUntilExpiry*-1}
                                        </span>
                                    {/if}
                                <h4>{$renewalData.domain}
                                    <small>{lang key='clientareadomainexpirydate'}: {$renewalData.expiryDate->format('j M Y')} ({$renewalData.expiryDate->diffForHumans()})</small>
                                </h4>

                                {if ($renewalData.pastGracePeriod && $renewalData.pastRedemptionGracePeriod)}
                                {else}
                                <form>
                                    <div class="form-group">
                                        <label for="renewalPricing{$renewalData.id}" class="control-label">
                                            {lang key='domainRenewal.availablePeriods'}
                                            {if $renewalData.inGracePeriod || $renewalData.inRedemptionGracePeriod}
                                                *
                                            {/if}
                                        </label>
                                            <select class="form-control select-renewal-pricing" id="renewalPricing{$renewalData.id}" data-domain-id="{$renewalData.id}">
                                                {foreach $renewalData.renewalOptions as $renewalOption}
                                                    <option value="{$renewalOption.period}">
                                                        {$renewalOption.period} {lang key='orderyears'} @ {$renewalOption.rawRenewalPrice}
                                                        {if $renewalOption.gracePeriodFee && $renewalOption.gracePeriodFee->toNumeric() != 0.00}
                                                            + {$renewalOption.gracePeriodFee} {lang key='domainRenewal.graceFee'}
                                                        {/if}
                                                        {if $renewalOption.redemptionGracePeriodFee && $renewalOption.redemptionGracePeriodFee->toNumeric() != 0.00}
                                                            + {$renewalOption.redemptionGracePeriodFee} {lang key='domainRenewal.redemptionFee'}
                                                        {/if}
                                                    </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                </form>
                                {/if}

                                    {if !$renewalData.eligibleForRenewal || $renewalData.beforeRenewLimit || ($renewalData.pastGracePeriod && $renewalData.pastRedemptionGracePeriod)}
                                    {else}
                                        <button id="renewDomain{$renewalData.id}" class="btn btn-default btn-sm btn-add-renewal-to-cart" data-domain-id="{$renewalData.id}">
                                            <span class="to-add">
                                                <i class="fas fa-fw fa-spinner fa-spin"></i>
                                                {lang key='addtocart'}
                                            </span>
                                            <span class="added">{lang key='domaincheckeradded'}</span>
                                        </button>
                                    {/if}
                            </div>
                        {foreachelse}
                            <div class="no-domains">
                                {lang key='domainRenewal.noDomains'}
                            </div>
                        {/foreach}
                    </div>

                    <div class="text-center">
                        <small>
                            {if $hasDomainsInGracePeriod}
                                * {lang key='domainRenewal.graceRenewalPeriodDescription'}
                            {/if}
                        </small>
                    </div>
                </div>
                <div class="col-md-4" id="scrollingPanelContainer">
                    <div id="orderSummary">
                        <div class="order-summary">
                            <h4>{lang key='ordersummary'} <span class="loader" id="orderSummaryLoader"><i class="fas fa-fw fa-refresh fa-spin"></i></span></h4>
                            <div class="well well-primary">
                            <div class="summary-container" id="producttotal"></div>
                        </div>
                        </div>
                        <div class="form-group">
                            <a id="btnGoToCart" class="btn btn-primary btn-lg btn-block" href="{$WEB_ROOT}/cart.php?a=view">{$LANG.continue}</a>
                          </div>
                    </div>
                </div>
    </div>
    <form id="removeRenewalForm" method="post" action="{$WEB_ROOT}/cart.php">
        <input type="hidden" name="a" value="remove" />
        <input type="hidden" name="r" value="" id="inputRemoveItemType" />
        <input type="hidden" name="i" value="" id="inputRemoveItemRef" />
        <div class="modal fade modal-remove-item" id="modalRemoveItem" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="{lang key='orderForm.close'}">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <i class="fas fa-times fa-3x"></i>
                            <span>{lang key='orderForm.removeItem'}</span>
                        </h4>
                    </div>
                    <div class="modal-body">
                        {lang key='cartremoveitemconfirm'}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{lang key='no'}</button>
                        <button type="submit" class="btn btn-primary">{lang key='yes'}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<script>recalculateRenewalTotals();</script>