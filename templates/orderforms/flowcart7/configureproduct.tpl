{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" desc=$LANG.cartproductconfig step=3}

{assign var="billingcellsize" value='col-sm-12'}

{assign var="billingcyclecount" value=0}
{if $pricing.monthly}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}
{if $pricing.quarterly}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}
{if $pricing.semiannually}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}
{if $pricing.annually}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}
{if $pricing.biennially}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}
{if $pricing.triennially}
  {assign var="billingcyclecount" value=$billingcyclecount+1}
{/if}

{if $billingcyclecount==2}
  {assign var="billingcellsize" value='col-sm-6'}
{/if}
{if $billingcyclecount==3}
  {assign var="billingcellsize" value='col-sm-4'}
{/if}
{if $billingcyclecount==4}
  {assign var="billingcellsize" value='col-sm-3'}
{/if}
{if $billingcyclecount==5}
  {assign var="billingcellsize" value='col-sm-2'}
{/if}
{if $billingcyclecount>5}
  {assign var="billingcellsize" value='col-sm-2'}
{/if}

{assign var="qmonthprice" value=0}
{math assign="qmonthprice" equation="d / b" b=3 d=$pricing.rawpricing.quarterly format="%.2f"}
{assign var="smonthprice" value=0}
{math assign="smonthprice" equation="d / b" b=6 d=$pricing.rawpricing.semiannually format="%.2f"}
{assign var="amonthprice" value=0}
{math assign="amonthprice" equation="d / b" b=12 d=$pricing.rawpricing.annually format="%.2f"}
{assign var="bmonthprice" value=0}
{math assign="bmonthprice" equation="d / b" b=24 d=$pricing.rawpricing.biennially format="%.2f"}
{assign var="tmonthprice" value=0}
{math assign="tmonthprice" equation="d / b" b=36 d=$pricing.rawpricing.triennially format="%.2f"}

{assign var="monthly_bill" value=$pricing.rawpricing.monthly}
{assign var="quarterly_bill" value=$pricing.rawpricing.quarterly}
{assign var="semiannually_bill" value=$pricing.rawpricing.semiannually}
{assign var="annually_bill" value=$pricing.rawpricing.annually}
{assign var="biennially_bill" value=$pricing.rawpricing.biennially}
{assign var="triennially_bill" value=$pricing.rawpricing.triennially}

<script>
var _localLang = {
  'addToCart': '{$LANG.orderForm.addToCart|escape}',
  'addedToCartRemove': '{$LANG.orderForm.addedToCartRemove|escape}'
}
</script>
<div class="row flowcart-row">
  <div class="col-md-12 ">

    <form id="frmConfigureProduct">
      <input type="hidden" name="configure" value="true" />
      <input type="hidden" name="i" value="{$i}" />

      <div class="row">
        <div class="col-md-8">

          <div class="alert alert-danger hidden" role="alert" id="containerProductValidationErrors">
            <p>{$LANG.orderForm.correctErrors}:</p>
            <ul id="containerProductValidationErrorsList"></ul>
          </div>

          {if $pricing.type eq "recurring"}
          <h4>{$LANG.cartchoosecycle} - {$productinfo.name}</h4>

            <!--billingcycle data start-->

            <div class="row" id="billingcycle_data">
            {if $pricing.monthly}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="monthly" id="sel-monthly" class="cycle-1"{if $billingcycle eq "monthly"} checked{/if}>
            <label for="sel-monthly" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class="cal cal-monthly"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$monthly_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermmonthly}</span>
            </label>
            {/if}
            {if $pricing.quarterly}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="quarterly" id="sel-quarterly" class="cycle-1"{if $billingcycle eq "quarterly"} checked{/if}>
            <label for="sel-quarterly" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class=" cal cal-quarterly"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$quarterly_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermquarterly}</span>
              {if $flowcart_ProductMonthlyPricingBreakdown}<span class="monthly-breakdown">({$currency.prefix}{$qmonthprice}{$currency.suffix|replace:'USD':''}/{$LANG.flowcartmonth})</span>{/if}
            </label>
            {/if}
            {if $pricing.semiannually}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="semiannually" id="sel-semiannually" class="cycle-1"{if $billingcycle eq "semiannually"} checked{/if}>
            <label for="sel-semiannually" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class="cal cal-semiannually"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$semiannually_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermsemiannually}</span>
              {if $flowcart_ProductMonthlyPricingBreakdown}<span class="monthly-breakdown">({$currency.prefix}{$smonthprice}{$currency.suffix|replace:'USD':''}/{$LANG.flowcartmonth})</span>{/if}
            </label>
            {/if}
            {if $pricing.annually}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="annually" id="sel-annually" class="cycle-1"{if $billingcycle eq "annually"} checked{/if}>
            <label for="sel-annually" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class="cal cal-annually"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$annually_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermannually}</span>
              {if $flowcart_ProductMonthlyPricingBreakdown}<span class="monthly-breakdown">({$currency.prefix}{$amonthprice}{$currency.suffix|replace:'USD':''}/{$LANG.flowcartmonth})</span>{/if}
            </label>
            {/if}
            {if $pricing.biennially}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="biennially" id="sel-biennially" class="cycle-1"{if $billingcycle eq "biennially"} checked{/if}>
            <label for="sel-biennially" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class="cal cal-biennially"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$biennially_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermbiennially}</span>
              {if $flowcart_ProductMonthlyPricingBreakdown}<span class="monthly-breakdown">({$currency.prefix}{$bmonthprice}{$currency.suffix|replace:'USD':''}/{$LANG.flowcartmonth})</span>{/if}
            </label>
            {/if}
            {if $pricing.triennially}
            <input type="radio" name="billingcycle" onchange="{if $configurableoptions}updateConfigurableOptions({$i}, this.value);{else}recalctotals();{/if}" value="triennially" id="sel-triennially" class="cycle-1"{if $billingcycle eq "triennially"} checked{/if}>
            <label for="sel-triennially" class="cycle-1 {$billingcellsize} col-xs-6 radio"><div class="cal cal-triennially"><i class="fal fa-calendar fa-3x"></i></div><b>{$currency.prefix}{$triennially_bill|number_format:2:".":","} {$currency.suffix}</b><span>{$LANG.orderpaymenttermtriennially}</span>
              {if $flowcart_ProductMonthlyPricingBreakdown}<span class="monthly-breakdown">({$currency.prefix}{$tmonthprice}{$currency.suffix|replace:'USD':''}/{$LANG.flowcartmonth})</span>{/if}
            </label>
            {/if}
            </div>
            {/if}
            <!--billingcycle data end-->




          {if $productinfo.type eq "server"}
          <div class="sub-heading">
            <span>{$LANG.cartconfigserver}</span>
          </div>

          <div class="field-container">

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputHostname">{$LANG.serverhostname}</label>
                  <input type="text" name="hostname" class="form-control" id="inputHostname" value="{$server.hostname}" placeholder="servername.yourdomain.com">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputRootpw">{$LANG.serverrootpw}</label>
                  <input type="password" name="rootpw" class="form-control" id="inputRootpw" value="{$server.rootpw}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputNs1prefix">{$LANG.serverns1prefix}</label>
                  <input type="text" name="ns1prefix" class="form-control" id="inputNs1prefix" value="{$server.ns1prefix}" placeholder="ns1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputNs2prefix">{$LANG.serverns2prefix}</label>
                  <input type="text" name="ns2prefix" class="form-control" id="inputNs2prefix" value="{$server.ns2prefix}" placeholder="ns2">
                </div>
              </div>
            </div>

          </div>
          {/if}

          {if $configurableoptions}
          <div class="sub-heading">
            <span>{$LANG.orderconfigpackage}</span>
          </div>
          <div class="product-configurable-options" id="productConfigurableOptions">
            <div class="row">
              {foreach $configurableoptions as $num => $configoption}
              {if $configoption.optiontype eq 1}
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputConfigOption{$configoption.id}">{$configoption.optionname}</label>
                  <select name="configoption[{$configoption.id}]" id="inputConfigOption{$configoption.id}" class="form-control">
                    {foreach key=num2 item=options from=$configoption.options}
                    <option value="{$options.id}"{if $configoption.selectedvalue eq $options.id} selected="selected"{/if}>
                      {$options.name}
                    </option>
                    {/foreach}
                  </select>
                </div>
              </div>
              {elseif $configoption.optiontype eq 2}
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption{$configoption.id}">{$configoption.optionname}</label>
                  {foreach key=num2 item=options from=$configoption.options}
                  {if $flowcart_display_product_radio == 'Inline'}
                  <div class="radio">
                  <label>
                    <input type="radio"  name="configoption[{$configoption.id}]" value="{$options.id}"{if $configoption.selectedvalue eq $options.id} checked="checked"{/if} />
                    {if $options.name}
                    {$options.name}
                    {else}
                    {$LANG.enable}
                    {/if}
                  </label>
                </div>
                  {else}
                  <label class="radio-inline">
                    <input type="radio" name="configoption[{$configoption.id}]" value="{$options.id}"{if $configoption.selectedvalue eq $options.id} checked="checked"{/if} />
                    {if $options.name}
                    {$options.name}
                    {else}
                    {$LANG.enable}
                    {/if}
                  </label>
                {/if}
                  {/foreach}
                </div>
              </div>
              {elseif $configoption.optiontype eq 3}
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption{$configoption.id}">{$configoption.optionname}</label>
                  <br />
                  <label>
                    <input type="checkbox" name="configoption[{$configoption.id}]" id="inputConfigOption{$configoption.id}" value="1"{if $configoption.selectedqty} checked{/if} />
                    {if $configoption.options.0.name}
                    {$configoption.options.0.name}
                    {else}
                    {$LANG.enable}
                    {/if}
                  </label>
                </div>
              </div>
              {elseif $configoption.optiontype eq 4}
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption{$configoption.id}">{$configoption.optionname}</label>
                  {if $configoption.qtymaximum}
                  {if !$rangesliderincluded}
                  <script type="text/javascript" src="{$BASE_PATH_JS}/ion.rangeSlider.min.js"></script>
                  <link href="{$BASE_PATH_CSS}/ion.rangeSlider.css" rel="stylesheet">
                  <link href="{$BASE_PATH_CSS}/ion.rangeSlider.skinModern.css" rel="stylesheet">
                  {assign var='rangesliderincluded' value=true}
                  {/if}
                  <input type="text" name="configoption[{$configoption.id}]" value="{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}" id="inputConfigOption{$configoption.id}" class="form-control" />
                  <script>
                  var sliderTimeoutId = null;
                  var sliderRangeDifference = {$configoption.qtymaximum} - {$configoption.qtyminimum};
                  // The largest size that looks nice on most screens.
                  var sliderStepThreshold = 25;
                  // Check if there are too many to display individually.
                  var setLargerMarkers = sliderRangeDifference > sliderStepThreshold;

                  jQuery("#inputConfigOption{$configoption.id}").ionRangeSlider({
                    min: {$configoption.qtyminimum},
                    max: {$configoption.qtymaximum},
                    grid: true,
                    grid_snap: setLargerMarkers ? false : true,
                    onChange: function() {
                      if (sliderTimeoutId) {
                        clearTimeout(sliderTimeoutId);
                      }

                      sliderTimeoutId = setTimeout(function() {
                        sliderTimeoutId = null;
                        recalctotals();
                      }, 250);
                    }
                  });
                  </script>
                  {else}
                  <div>
                    <input type="number" name="configoption[{$configoption.id}]" value="{if $configoption.selectedqty}{$configoption.selectedqty}{else}{$configoption.qtyminimum}{/if}" id="inputConfigOption{$configoption.id}" min="{$configoption.qtyminimum}" onchange="recalctotals()" onkeyup="recalctotals()" class="form-control form-control-qty" />
                    <span class="form-control-static form-control-static-inline">
                      x {$configoption.options.0.name}
                    </span>
                  </div>
                  {/if}
                </div>
              </div>
              {/if}
              {if $num % 2 != 0}
            </div>
            <div class="row">
              {/if}
              {/foreach}
            </div>
          </div>

          {/if}

          {if $customfields}

          <div class="sub-heading">
            <span>{$LANG.orderadditionalrequiredinfo}</span>
          </div>

          <div class="field-container">
            {foreach $customfields as $customfield}
            <div class="form-group">
              <label for="customfield{$customfield.id}">{$customfield.name}</label>
              {$customfield.input}
              {if $customfield.description}
              <span class="field-help-text">
                {$customfield.description}
              </span>
              {/if}
            </div>
            {/foreach}
          </div>
          {/if}
          {if $addons || count($addonsPromoOutput) > 0}
            <h4>{$LANG.cartavailableaddons}</h4>

            {foreach $addonsPromoOutput as $output}
                <div>
                    {$output}
                </div>
            {/foreach}

          <div class="row addon-products">
            {foreach $addons as $addon}
            <div class="{$flowcart_addon_columns}">
              <div class="well panel-addons {flowcart_clean_css_identifier($addon.name)}">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="addons[{$addon.id}]"{if $addon.status} checked{/if} />
                    {$addon.name}
                  </label>
                </div>
                  <p>{$addon.description}</p> <h6>{$addon.pricing}</h6>
              </div>
            </div>
            {/foreach}
          </div>
          {/if}
        </div>
        <div class="col-md-4" id="scrollingPanelContainer">
          <div id="orderSummary">
            <div class="order-summary">
              <h4>{$LANG.ordersummary} <span class="loader" id="orderSummaryLoader"><i class="fas fa-fw fa-refresh fa-spin"></i></span></h4>
              <div class="well well-primary">
                <div class="summary-container" id="producttotal"></div>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" id="btnCompleteProductConfig" class="btn btn-primary btn-lg btn-block">{$LANG.continue}</button>
            </div>
          </div>

        </div>

      </div>

    </form>
  </div>
</div>

<script>recalctotals();</script>
