{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" title=$LANG.rs_step4 desc=$LANG.rs_step4_desc step=4 pid=$products[0].pid}
<div class="alert alert-danger error-heading">
    <h4>{$LANG.cartfraudcheck}</h4>
            <i class="fas fa-warning"></i>{$errortitle}
        </div>
        <div class="row flowcart-row">
            <div class="col-md-12">
                <p>{$error}</p>
                <a href="submitticket.php" class="btn btn-default btn-lg">{$LANG.orderForm.submitTicket}</a>
            </div>
        </div>
