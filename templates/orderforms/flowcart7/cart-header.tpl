{if $step}

{if $LANG.locale == 'ar_AR' || $LANG.locale == 'fa_IR' || $LANG.locale == 'he_IL'}
<ul id="stepbar" class="mb-1 nav nav-wizard flo-rtl {if !$flowcart_hide_domain_step}steps4{else}steps3{/if}">

  <li class="step4{if $step == 4} active{/if}">
<a href="cart.php?a=checkout">{if !$flowcart_hide_domain_step}4.{else}3.{/if} <span class="hidden-xs">{$LANG.cartreviewcheckout}</span></a>
</li>

  <li class="step3{if $step == 3} active{elseif $step > 3}done{/if}">
    {if $step > 3}
  <i class="fas fa-fw fa-check"></i>
      {else}
      {/if}
      {if !$flowcart_hide_domain_step}3. {else}2.{/if} <span class="hidden-xs">{$LANG.flowcartsettings}</span>
      {if $step > 3}
    {else}
    {/if}
  </li>

  {if !$flowcart_hide_domain_step}
  <li class="step2 {if $step == 2}active{elseif $step > 2}done{/if}">
    {if $step > 2}
    <i class="fas fa-fw fa-check"></i>
      {else}
      {/if} 2. <span class="hidden-xs">{$LANG.flowcartdomainconfig}</span>
      {if $step > 2}
    {else}
    {/if}
  </li>
  {/if}
  <li class="step1 {if $step == 1}active{elseif $step > 1}done{/if}"><a href="cart.php">{if $step > 1}<i class="fas fa-check"></i>{/if} 1. <span class="hidden-xs">{$LANG.flowcartproduct}</span>{if $smarty.get.action != "confproduct"}</a>{/if}
  </li>

</ul>

{else}
<ul id="stepbar" class="mb-1 nav nav-wizard flo-ltr {if !$flowcart_hide_domain_step}steps4{else}steps3{/if}">
  <li class="step1 {if $step == 1}active{elseif $step > 1}done{/if}"><a href="cart.php">{if $step > 1}<i class="fas fa-check"></i>{/if} 1. <span class="hidden-xs">{$LANG.flowcartproduct}</span>{if $smarty.get.action != "confproduct"}</a>{/if}
  </li>
  {if !$flowcart_hide_domain_step}
  <li class="step2 {if $step == 2}active{elseif $step > 2}done{/if}">
    {if $step > 2}
    <i class="fas fa-fw fa-check"></i>
      {else}
      {/if} 2. <span class="hidden-xs">{$LANG.flowcartdomainconfig}</span>
      {if $step > 2}
    {else}
    {/if}
  </li>
  {/if}
  <li class="step3{if $step == 3} active{elseif $step > 3}done{/if}">
    {if $step > 3}
  <i class="fas fa-fw fa-check"></i>
      {else}
      {/if}
      {if !$flowcart_hide_domain_step}3. {else}2.{/if} <span class="hidden-xs">{$LANG.flowcartsettings}</span>
      {if $step > 3}
    {else}
    {/if}
  </li>
  <li class="step4{if $step == 4} active{/if}">
<a href="cart.php?a=checkout">{if !$flowcart_hide_domain_step}4.{else}3.{/if} <span class="hidden-xs">{$LANG.cartreviewcheckout}</span></a>
</li>
</ul>
{/if}

{/if}
