{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/products-header.tpl"}
<div class="row flowcart-row">
<h4>{$LANG.cartproductaddons}</h4>
    {if count($addons) == 0}
    <div class="alert alert-warning text-center" role="alert">
      {$LANG.cartproductaddonsnone}
    </div>
    <p class="text-center">
      <a href="clientarea.php" class="btn btn-default">
        <i class="fas fa-arrow-circle-left"></i>
        {$LANG.orderForm.returnToClientArea}
      </a>
    </p>
    {/if}
    <div class="products text-center">
      <div class="row">
        {foreach $addons as $num => $addon}
        <div class="col-md-4">
        <div class="well well-lg well-primary clearfix">
          <div class="product clearfix" id="product{$num}">
            <form method="post" action="{$smarty.server.PHP_SELF}?a=add">
              <input type="hidden" name="aid" value="{$addon.id}" />
              <header>
                <h4>{$addon.name}</h4s>
                {if $product.qty}
                <span class="qty">
                  {$product.qty} {$LANG.orderavailable}
                </span>
                {/if}
              </header>
              <div class="product-desc">
                <p>{$addon.description}</p>
                <div class="form-group">
                  <select name="productid" id="inputProductId{$num}" class="form-control">
                    {foreach $addon.productids as $product}
                    <option value="{$product.id}">
                      {$product.product}{if $product.domain} - {$product.domain}{/if}
                    </option>
                    {/foreach}
                  </select>
                </div>
              </div>
              <footer>
                <div class="product-pricing h4 p-1">
                  {if $addon.free}
                  {$LANG.orderfree}
                  {else}
                  <span class="price">{$addon.recurringamount} {$addon.billingcycle}</span>
                  {if $addon.setupfee}<br />+ {$addon.setupfee} {$LANG.ordersetupfee}{/if}
                  {/if}
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success btn-block btn-lg">{$LANG.ordernowbutton}</button>
              </div>
              </footer>
            </form>
          </div>
        </div>
        </div>
     {/foreach}
      </div>
    </div>
  </div>
