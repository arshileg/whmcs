<script>
  if (window.location.href.indexOf('cart.php?a=view')!=-1){
     window.location = 'cart.php?a=checkout';
  }
</script>
<script>
// Define state tab index value
var statesTab = 10;
var stateNotRequired = true;
</script>
{include file="orderforms/$carttpl/common.tpl"}
{include file="orderforms/$carttpl/custom-styles.tpl"}
{include file="orderforms/$carttpl/cart-header.tpl" title=$LANG.rs_step4 desc=$LANG.rs_step4_desc step=4 pid=$products[0].pid}

<script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
<script type="text/javascript" src="{$BASE_PATH_JS}/PasswordStrength.js"></script>
<script>
window.langPasswordStrength = "{$LANG.pwstrength}";
window.langPasswordWeak = "{$LANG.pwstrengthweak}";
window.langPasswordModerate = "{$LANG.pwstrengthmoderate}";
window.langPasswordStrong = "{$LANG.pwstrengthstrong}";
{if $flowcart_final_order_button_tosidebar == "Sidebar Order Button and Payment" || $flowcart_final_order_button_tosidebar == "Sidebar Order Button and Payment (Wide)"}
  {if $flowcart_final_order_button_tosidebar == "Sidebar Order Button and Payment"}
    window.flowcartmovetoleftcard = 3;
  {else}
    window.flowcartmovetoleftcard = 4;
  {/if}
{else}
  window.flowcartmovetoleftcard = 0;
{/if}
</script>

{if $promoerrormessage}
<div class="alert alert-warning text-center" role="alert">
{$promoerrormessage}
</div>
{elseif $errormessage}
<div class="alert alert-danger" role="alert">
<p>{$LANG.orderForm.correctErrors}:</p>
<ul>
  {$errormessage}
</ul>
</div>
{elseif $promotioncode && $rawdiscount eq "0.00"}
<div class="alert alert-info text-center" role="alert">
{$LANG.promoappliedbutnodiscount}
</div>
{elseif $promoaddedsuccess}
<div class="alert alert-success text-center" role="alert">
{$LANG.orderForm.promotionAccepted}
</div>
{/if}
{if $bundlewarnings}
<div class="alert alert-warning" role="alert">
<strong>{$LANG.bundlereqsnotmet}</strong><br />
<ul>
  {foreach from=$bundlewarnings item=warning}
  <li>{$warning}</li>
  {/foreach}
</ul>
</div>
{/if}

{foreach $hookOutput as $output}
    <div>
        {$output}
    </div>
{/foreach}

{if !$loggedin && $currencies && !$flowcart_hide_currency_switch}
<div class="row">
  <div class="col-md-12">
    <div class="pull-right currency-product">
      {foreach from=$currencies item=curr}
      <a href="cart.php?a=view&currency={$curr.id}"><i class="fas fa-money{if $curr.id eq $currency.id} active{/if}"></i> <span>{$curr.code}</span></a>
      {/foreach}
    </div>
  </div>
</div>
{/if}

<div id="order-standard_cart">
<div class="row flowcart-row">
<div class="col-md-12">
  <div class="row">
    <div id="flowcart_leftside" class="col-md-8">
      {foreach $gatewaysoutput as $gatewayoutput}
      <div class="view-cart-gateway-checkout">
        {$gatewayoutput}
      </div>
      {/foreach}
{include file="orderforms/$carttpl/checkout.tpl"}
    </div>
    <div class="col-md-4" id="scrollingPanelContainer">
      <div class="order-summary" id="orderSummary">
      <!--ordersummary-->
        <h4>{$LANG.ordersummary} <span class="loader" id="orderSummaryLoader"><i class="fas fa-fw fa-refresh fa-spin"></i></span> </h4>
        <div class="well well well-primary clearfix">
          <form method="post" action="{$smarty.server.PHP_SELF}?a=view">
            <div class="view-cart-items">
              {foreach $products as $num => $product}
                  <div class="row my-2">
                    <div class="col-sm-9">
                        <h6>{$product.productinfo.name} {if $product.domain} - {$product.domain}{/if}</h6>
                      {if $product.configoptions}
                      <small>
                        {foreach key=confnum item=configoption from=$product.configoptions}
                        &nbsp;&raquo; {$configoption.name}: {if $configoption.type eq 1 || $configoption.type eq 2}{$configoption.option}{elseif $configoption.type eq 3}{if $configoption.qty}{$configoption.option}{else}{$LANG.no}{/if}{elseif $configoption.type eq 4}{$configoption.qty} x {$configoption.option}{/if}<br />
                        {/foreach}
                      </small>
                      {/if}
                      <div class="item-price">
                        <span>{$product.pricing.totalTodayExcludingTaxSetup}</span>
                        <span class="cycle">{$product.billingcyclefriendly}</span>
                        {if $product.pricing.productonlysetup}
                        {$product.pricing.productonlysetup->toPrefixed()} {$LANG.ordersetupfee}
                        {/if}
                        {if $product.proratadate}<br />({$LANG.orderprorata} {$product.proratadate}){/if}
                      </div>
                    </div>
                    <div class="col-sm-3 col-xs-2 text-right">
                      {if $flowcart_edit_btn_in_cart_summary}
                      <a href="{$smarty.server.PHP_SELF}?a=confproduct&i={$num}" class="btn btn-xs btn-link">
                        <i class="fas fa-pencil" aria-hidden="true"></i>
                      </a>
                      {/if}
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('p','{$num}')">
                        <i class="fas fa-times text-danger"></i>
                      </button>
                    </div>
                    {if $showqtyoptions}
                    <div class="col-md-12 item-qty">
                      {if $product.allowqty}
                      <div class="input-group">
                      <input type="number" name="qty[{$num}]" value="{$product.qty}" class="form-control" />
                           <span class="input-group-btn">
                             <button class="btn btn-default"type="submit"><i class="fas fa-refresh" aria-hidden="true"></i></button>
                           </span>
                       </div>
                      {/if}
                    </div>
                    {/if}
                  </div>
              {foreach key=addonnum item=addon from=$product.addons}
                  <div class="row">
                    <div class="col-md-12">
                      <p class="small">
                      <span class="item-title">
                      <i class="fas fa-fw text-muted fa-puzzle-piece" aria-hidden="true"></i> {$addon.name}
                      </span>
                      {if $addon.setup}<span class="item-setup">{$addon.setup} {$LANG.ordersetupfee}</span>{/if}
                      <span>{$addon.totaltoday}</span>
                      <span class="cycle">{$addon.billingcyclefriendly}</span>
                    </p>
                    </div>
                  </div>
              {/foreach}
              {/foreach}
              {foreach $addons as $num => $addon}
                  <div class="row my-2">
                    <div class="col-sm-10">
                      <h6>{$addon.name}</h6>
                      <p>{$addon.productname}{if $addon.domainname} <strong>{$addon.domainname}</strong>{/if}</p>
                      {if $addon.setup}
                      <p>{$addon.setup} {$LANG.ordersetupfee}</p>
                      {/if}
                      <p>{$addon.pricingtext} {$addon.billingcyclefriendly}</p>
                    </div>
                    <div class="col-sm-2 text-right hidden-xs">
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('a','{$num}')">
                        <i class="fas fa-times text-danger"></i>
                      </button>
                    </div>
                  </div>
              {/foreach}
              {foreach $domains as $num => $domain}
                  <div class="row my-2">
                    <div class="col-md-9">
                        <h6>{if $domain.domain} {$domain.domain}{/if} {if $domain.type eq "register"}{$LANG.orderdomainregistration}{else}{$LANG.orderdomaintransfer}{/if}</h6>
                      {if $domain.dnsmanagement}<p class="small"><i class="fas fa-plus fa-fw text-muted" aria-hidden="true"></i> {$LANG.domaindnsmanagement}</p>{/if}
                      {if $domain.emailforwarding}<p class="small"><i class="fas fa-plus fa-fw text-muted" aria-hidden="true"></i> {$LANG.domainemailforwarding}</p>{/if}
                      {if $domain.idprotection}<p class="small"><i class="fas fa-plus fa-fw text-muted" aria-hidden="true"></i> {$LANG.domainidprotection}</p>{/if}
                      <div class="item-price">
                        {if count($domain.pricing) == 1 || $domain.type == 'transfer'}
                        <span name="{$domain.domain}Price">{$domain.price}</span>
                        <p class="cycle">{$domain.regperiod} {$domain.yearsLanguage}</p>
                        <span class="renewal cycle small">
                          {if isset($domain.renewprice)}{lang key='domainrenewalprice'} <span class="renewal-price cycle">
                             {$domain.renewprice->toPrefixed()}{$domain.shortYearsLanguage}</span>{/if}
                        </span>
                        {else}
                        <p><span name="{$domain.domain}Price">{$domain.price}</span></p>
                        <div class="form-group">
                          <div class="dropdown">
                            <button class="btn btn-default btn-xs  dropdown-toggle" type="button" id="{$domain.domain}Pricing" name="{$domain.domain}Pricing" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              {$domain.regperiod} {$domain.yearsLanguage}
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="{$domain.domain}Pricing">
                              {foreach $domain.pricing as $years => $price}
                              <li>
                                <a href="#" onclick="selectDomainPeriodInCart('{$domain.domain}', '{$price.register}', {$years}, '{if $years == 1}{lang key='orderForm.year'}{else}{lang key='orderForm.years'}{/if}');return false;">
                                  {$years} {if $years == 1}{lang key='orderForm.year'}{else}{lang key='orderForm.years'}{/if} @ {$price.register}
                                </a>
                              </li>
                              {/foreach}
                            </ul>
                          </div>
                        </div>
                        <p class="renewal cycle small">
                          {if isset($domain.renewprice)}{lang key='domainrenewalprice'} <span class="renewal-price cycle">{$domain.renewprice->toPrefixed()}{$domain.shortYearsLanguage}</span>{/if}
                        </p>
                        {/if}
                      </div>
                    </div>
                    <div class="col-sm-3 col-xs-2 text-right">
                      {if $flowcart_edit_btn_in_cart_summary}
                      <a href="{$smarty.server.PHP_SELF}?a=confdomains" class="btn btn-link btn-xs">
                        <i class="fas fa-pencil"></i>
                      </a>
                      {/if}
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('d','{$num}')">
                        <i class="fas fa-times text-danger"></i>
                      </button>
                    </div>
                  </div>
              {/foreach}
              {foreach key=num item=domain from=$renewals}
              <div class="item">
                <div class="row my-2">
                  <div class="col-md-9">
                    <h6>
                      {$LANG.domainrenewal}
                      {$domain.domain}
                    </h6>
                    {if $domain.dnsmanagement}&nbsp;&raquo; {$LANG.domaindnsmanagement}<br />{/if}
                    {if $domain.emailforwarding}&nbsp;&raquo; {$LANG.domainemailforwarding}<br />{/if}
                    {if $domain.idprotection}&nbsp;&raquo; {$LANG.domainidprotection}<br />{/if}
                    {$domain.price}<br />
                    {$domain.regperiod} {$LANG.orderyears}                    
                  </div>
                  <div class="col-sm-3 col-xs-2 text-right">
                    <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('r','{$num}')">
                      <i class="fas fa-times text-danger"></i>
                      <span class="visible-xs">{$LANG.orderForm.remove}</span>
                    </button>
                  </div>
                </div>
              </div>
              {/foreach}
              {foreach $upgrades as $num => $upgrade}
                  <div class="item">
                      <div class="row">
                          <div class="col-sm-7">
                              <span class="item-title">
                                  {$LANG.upgrade}
                              </span>
                              <span class="item-group">
                                  {if $upgrade->type == 'service'}
                                      {$upgrade->originalProduct->productGroup->name}<br>{$upgrade->originalProduct->name} => {$upgrade->newProduct->name}
                                  {elseif $upgrade->type == 'addon'}
                                      {$upgrade->originalAddon->name} => {$upgrade->newAddon->name}
                                  {/if}
                              </span>
                              <span class="item-domain">
                                  {if $upgrade->type == 'service'}
                                      {$upgrade->service->domain}
                                  {/if}
                              </span>
                          </div>
                          <div class="col-sm-4 item-price">
                              <span>{$upgrade->newRecurringAmount}</span>
                              <span class="cycle">{$upgrade->localisedNewCycle}</span>
                          </div>
                          <div class="col-sm-1">
                              <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('u','{$num}')">
                                  <i class="fas fa-times"></i>
                                  <span class="visible-xs">{$LANG.orderForm.remove}</span>
                              </button>
                          </div>
                      </div>
                      {if $upgrade->totalDaysInCycle > 0}
                      <div class="row row-upgrade-credit">
                          <div class="col-sm-7">
                              <span class="item-group">
                                  {$LANG.upgradeCredit}
                              </span>
                              <div class="upgrade-calc-msg">
                                  {lang key="upgradeCreditDescription" daysRemaining=$upgrade->daysRemaining totalDays=$upgrade->totalDaysInCycle}
                              </div>
                          </div>
                          <div class="col-sm-4 item-price">
                              <span>-{$upgrade->creditAmount}</span>
                          </div>
                      </div>
                      {/if}
                  </div>
              {/foreach}
              {if $cartitems == 0}
                  <div class="view-cart-empty">
                    <h6><i class="fas fa-fw fa-shopping-cart" aria-hidden="true"></i> {$LANG.cartempty}</h6>
                  </div>
              {/if}
            </div>
          </form>
          <div class="summary-container">
            <div class="subtotal clearfix">
              <span class="pull-left">{$LANG.ordersubtotal}</span>
              <span id="subtotal" class="pull-right">{$subtotal}</span>
            </div>
            {if $promotioncode || $taxrate || $taxrate2}
            <div class="bordered-totals">
              {if $promotioncode}
              <div class="clearfix">
                <span class="pull-left">{$promotiondescription}</span>
                <span id="discount" class="pull-right">{$discount}</span>
              </div>
              {/if}
              {if $taxrate}
              <div class="clearfix">
                <span class="pull-left">{$taxname} @ {$taxrate}%</span>
                <span id="taxTotal1" class="pull-right">{$taxtotal}</span>
              </div>
              {/if}
              {if $taxrate2}
              <div class="clearfix">
              <span class="pull-left">{$taxname2} @ {$taxrate2}%</span>
              <span id="taxTotal2" class="pull-right">{$taxtotal2}</span>
              </div>
              {/if}
            </div>
            {/if}
            <div class="row my-2">
            <div class="col-md-12">
            <div class="recurring-totals">
              <span class="pull-left">{$LANG.orderForm.totals}</span>
              <span id="recurring" class="pull-right text-right recurring-charges">
                <span id="recurringMonthly" {if !$totalrecurringmonthly}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringmonthly}</span> {$LANG.orderpaymenttermmonthly}<br />
                </span>
                <span id="recurringQuarterly" {if !$totalrecurringquarterly}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringquarterly}</span> {$LANG.orderpaymenttermquarterly}<br />
                </span>
                <span id="recurringSemiAnnually" {if !$totalrecurringsemiannually}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringsemiannually}</span> {$LANG.orderpaymenttermsemiannually}<br />
                </span>
                <span id="recurringAnnually" {if !$totalrecurringannually}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringannually}</span> {$LANG.orderpaymenttermannually}<br />
                </span>
                <span id="recurringBiennially" {if !$totalrecurringbiennially}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringbiennially}</span> {$LANG.orderpaymenttermbiennially}<br />
                </span>
                <span id="recurringTriennially" {if !$totalrecurringtriennially}style="display:none;"{/if}>
                  <span class="cost">{$totalrecurringtriennially}</span> {$LANG.orderpaymenttermtriennially}<br />
                </span>
              </span>
            </div>
          </div>
        </div>
              <div class="row order-total my-1 text-uppercase">
                <div class="col-md-12">
              <span id="totalDueToday" class="amt pull-right flip">{$total}</span>
              <span class="pull-left flip">{$LANG.ordertotalduetoday}</span>
            </div>
          </div>
          </div>
{if !$flowcart_hide_promo}
<div class="row mt-2">
<div class="col-md-12">
          <div class="promo" id="applyPromo">
            {if $promotioncode}
            <div class="row">
              <div class="col-md-12">
            <div class="pull-left view-cart-promotion-code">
              {$promotioncode} - {$promotiondescription}
            </div>
            <div class="pull-right">
            <a id="removePromotionCode" href="{$smarty.server.PHP_SELF}?a=removepromo" class="btn btn-link btn-xs"><i class="fas fa-lg fa-times text-danger"></i></a>
          </div>
        </div>
      </div>
            {else}
            <form id="formPromotionCode" method="post" action="cart.php?a=checkout">
              <div class="form-group {if $promoerrormessage} has-error has-feedback {/if} ">
                {if $promoerrormessage}<label class="control-label" for="inputPromotionCode">{$promoerrormessage}</label>{/if}
                <div class="input-group">
                  <input type="text" name="promocode" id="inputPromotionCode" class="form-control" placeholder="{lang key="orderPromoCodePlaceholder"}" required="required" aria-describedby="inputPromotionCodeStatus">

                  <span class="input-group-btn">
                    <button type="submit" name="validatepromo" class="btn btn-block" value="{$LANG.orderpromovalidatebutton}"><i class="fas fa-chevron-right"></i></button>
                  </span>
                  {if $promoerrormessage}
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span id="inputPromotionCodeStatus" class="sr-only">(error)</span>
                  {/if}
                </div>
              </div>
            </form>
            {/if}
          </div>
        </div>
      </div>
  {/if}
        </div>
        {if $cartitems > 0 && $flowcart_display_empty_cart_button}
        <span class="empty-cart">
          <button type="button" class="btn btn-block btn-danger btn-lg" id="btnEmptyCart">
            <i class="fas fa-trash"></i></button>
        </span>
        {/if}
        <!--endordersummary-->
      </div>

      {if $taxenabled && !$loggedin}
<div class="view-cart-tabs">
      <div id="calcTaxes">
        <h4>{$LANG.orderForm.estimateTaxes}</h4>
        <form method="post" action="cart.php?a=setstateandcountry">
            <div class="form-group">
              <label for="inputState" class="control-label">{$LANG.orderForm.state}</label>
                <input type="text" name="state" id="inputState" value="{$clientsdetails.state}" class="form-control"{if $loggedin} disabled="disabled"{/if} />
              </div>
            <div class="form-group">
              <label for="inputCountry" class="control-label">{$LANG.orderForm.country}</label>
                <select name="country" id="inputCountry" class="form-control">
                  {foreach $countries as $countrycode => $countrylabel}
                  <option value="{$countrycode}"{if (!$country && $countrycode == $defaultcountry) || $countrycode eq $country} selected{/if}>
                    {$countrylabel}
                  </option>
                  {/foreach}
                </select>
              </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default btn-block">
                {$LANG.orderForm.updateTotals}
              </button>
            </div>
        </form>
      </div>
  </div>
      {/if}
      <div id="paymentGatewaysHolder"></div>
      {if $flowcart_final_order_button_tosidebar == "Sidebar Order Button and Payment" || $flowcart_final_order_button_tosidebar == "Sidebar Order Button and Payment (Wide)" || $flowcart_final_order_button_tosidebar == "Sidebar Order Button"}
       {if $accepttos}<div class="well well-complete">

  {if $showMarketingEmailOptIn}
  <div class="form-group">
      <label class="checkbox-inline accepttossidebar">
            <input type="checkbox" name="marketingoptin" id="marketingoptinsidebar" value="1"{if $marketingEmailOptIn} checked{/if}> {lang key='emailMarketing.joinOurMailingList'}
        </label>
  </div>
  {/if}

            <div class="form-group">
              <label class="checkbox-inline accepttossidebar">
                <input type="checkbox" name="accepttos" id="accepttossidebar" />
                {$LANG.ordertosagreement}
                <a href="{$tosurl}" target="_blank">{$LANG.ordertos}</a>
              </label>
            </div>
        {/if}
      <div class="well-complete"><button id="btnCompleteOrdersidebar" class="btn btn-warning btn-lg btn-block" {if $cartitems==0} disabled="disabled"{/if} onclick="this.value='{$LANG.pleasewait}'" >{$LANG.completeorder}</button></div>
      {if $accepttos}</div>{/if}
      {/if}
    </div>
  </div>
</div>
</div>
</div>
<form method="post" action="cart.php">
<input type="hidden" name="a" value="remove" />
<input type="hidden" name="r" value="" id="inputRemoveItemType" />
<input type="hidden" name="i" value="" id="inputRemoveItemRef" />
<div class="modal fade modal-remove-item" id="modalRemoveItem" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">
          <span>{$LANG.orderForm.removeItem}</span>
        </h4>
      </div>
      <div class="modal-body">
        {$LANG.cartremoveitemconfirm}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{$LANG.no}</button>
        <button type="submit" class="btn btn-primary">{$LANG.yes}</button>
      </div>
    </div>
  </div>
</div>
</form>
<form method="post" action="cart.php">
<input type="hidden" name="a" value="empty" />
<div class="modal fade modal-remove-item" id="modalEmptyCart" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="{$LANG.orderForm.close}">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">
          <i class="fas fa-trash"></i>
          <span>{$LANG.emptycart}</span>
        </h4>
      </div>
      <div class="modal-body">
        {$LANG.cartemptyconfirm}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{$LANG.no}</button>
        <button type="submit" class="btn btn-primary">{$LANG.yes}</button>
      </div>
    </div>
  </div>
</div>
</form>
