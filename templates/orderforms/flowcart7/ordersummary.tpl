{if $producttotals}
<h6>{if $producttotals.allowqty && $producttotals.qty > 1}{$producttotals.qty} x {/if}{$producttotals.productinfo.name}</h6>

<div class="order-row clearfix">
    <span class="pull-left flip">{$producttotals.productinfo.name}</span>
    <span class="pull-right flip">{$producttotals.pricing.baseprice}</span>
</div>

{foreach $producttotals.configoptions as $configoption}
    {if $configoption}
        <div class="clearfix">
            <span class="pull-left">&nbsp;&raquo; {$configoption.name}: {$configoption.optionname}</span>
            <span class="pull-right">{$configoption.recurring}{if $configoption.setup} + {$configoption.setup} {$LANG.ordersetupfee}{/if}</span>
        </div>
    {/if}
{/foreach}

{foreach $producttotals.addons as $addon}
    <div class="row">
      <div class="col-md-12">
        <span class="pull-left"><i class="fas fa-fw fa-plus text-muted" aria-hidden="true"></i> {$addon.name}</span>
        <span class="pull-right">{$addon.recurring}</span>
    </div>
  </div>
{/foreach}

{if $producttotals.pricing.setup || $producttotals.pricing.recurring || $producttotals.pricing.addons}
    <div class="summary-totals mt-1">
        {if $producttotals.pricing.setup}
            <div class="clearfix">
                <span class="pull-left">{$LANG.cartsetupfees}:</span>
                <span class="pull-right">{$producttotals.pricing.setup}</span>
            </div>
        {/if}
        {foreach from=$producttotals.pricing.recurringexcltax key=cycle item=recurring}
            <div class="clearfix">
                <span class="pull-left">{$cycle}:</span>
                <span class="pull-right">{$recurring}</span>
            </div>
        {/foreach}
        {if $producttotals.pricing.tax1}
            <div class="clearfix">
                <span class="pull-left">{$carttotals.taxname} @ {$carttotals.taxrate}%:</span>
                <span class="pull-right">{$producttotals.pricing.tax1}</span>
            </div>
        {/if}
        {if $producttotals.pricing.tax2}
            <div class="clearfix">
                <span class="pull-left">{$carttotals.taxname2} @ {$carttotals.taxrate2}%:</span>
                <span class="pull-right">{$producttotals.pricing.tax2}</span>
            </div>
        {/if}
    </div>
{/if}

<div class="order-row order-total text-uppercase">
  <span>{$LANG.ordertotalduetoday}</span><span class="totaldue pull-right">{$producttotals.pricing.totaltoday}</span>
 </div>
{elseif $renewals}
    {if $carttotals.renewals}
        <h6>{lang key='domainrenewals'}</h6>
        {foreach $carttotals.renewals as $domainId => $renewal}
            <div class="clearfix order-row" id="cartDomainRenewal{$domainId}">
                <span class="pull-left">
                    {$renewal.domain} - {$renewal.regperiod} {if $renewal.regperiod == 1}{lang key='orderForm.year'}{else}{lang key='orderForm.years'}{/if}
                </span>
                <span class="pull-right">
                    {$renewal.priceBeforeTax}
                    <a onclick="removeItem('r','{$domainId}'); return false;" href="#" id="linkCartRemoveDomainRenewal{$domainId}">
                        <i class="fas fa-fw fa-times text-danger"></i>
                    </a>
                </span>
            </div>
            {if $renewal.dnsmanagement}
                <div class="clearfix">
                    <span class="pull-left">+ {lang key='domaindnsmanagement'}</span>
                </div>
            {/if}
            {if $renewal.emailforwarding}
                <div class="clearfix">
                    <span class="pull-left">+ {lang key='domainemailforwarding'}</span>
                </div>
            {/if}
            {if $renewal.idprotection}
                <div class="clearfix">
                    <span class="pull-left">+ {lang key='domainidprotection'}</span>
                </div>
            {/if}
            {if $renewal.hasGracePeriodFee}
                <div class="clearfix">
                    <span class="pull-left">+ {lang key='domainRenewal.graceFee'}</span>
                </div>
            {/if}
            {if $renewal.hasRedemptionGracePeriodFee}
                <div class="clearfix">
                    <span class="pull-left">+ {lang key='domainRenewal.redemptionFee'}</span>
                </div>
            {/if}

        {/foreach}
    {/if}
    <div class="summary-totals mt-1">
        <div class="clearfix">
            <span class="pull-left">{lang key='ordersubtotal'}:</span>
            <span class="pull-right">{$carttotals.subtotal}</span>
        </div>
        {if ($carttotals.taxrate && $carttotals.taxtotal) || ($carttotals.taxrate2 && $carttotals.taxtotal2)}
            {if $carttotals.taxrate}
                <div class="clearfix">
                    <span class="pull-left">{$carttotals.taxname} @ {$carttotals.taxrate}%:</span>
                    <span class="pull-right">{$carttotals.taxtotal}</span>
                </div>
            {/if}
            {if $carttotals.taxrate2}
                <div class="clearfix">
                    <span class="pull-left">{$carttotals.taxname2} @ {$carttotals.taxrate2}%:</span>
                    <span class="pull-right">{$carttotals.taxtotal2}</span>
                </div>
            {/if}
        {/if}
    </div>
    <div class="order-row order-total text-uppercase">
        <span class="amt">{$carttotals.total}</span>
        <span>{lang key='ordertotalduetoday'}</span>
    </div>
{/if}