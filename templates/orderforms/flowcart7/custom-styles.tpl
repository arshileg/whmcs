<style type="text/css">
{if isset($flowcart_primary_color)}
#flow-menu li.group-first .flow-icon, #flow-menu li.group-second .flow-icon,#flow-menu li.group-third .flow-icon,#flow-menu li.group-fourth .flow-icon { color: {$flowcart_primary_color}; }
#stepbar > li.active > a,#stepbar > li.active > span, #flow-menu > li.active > a { background-color: {$flowcart_primary_color}; color: #fff; }
ul.nav-wizard li.active { color:#fff; background: {$flowcart_primary_color}; }
ul.nav-wizard li.active:after { border-left:16px solid {$flowcart_primary_color}; }
ul.nav-wizard.flo-rtl li.active:after { border: 30px solid transparent; border-left: 0;border-right: 16px solid {$flowcart_primary_color}; left: -16px; right: auto; }
ul.nav-wizard li.active a,ul.nav-wizard li.active a:active,ul.nav-wizard li.active a:visited,ul.nav-wizard li.active a:focus { color: #3a87ad; background: {$flowcart_primary_color};}
.well-primary,.nav-tabs > li.active > a,.nav-tabs > li.active > a:hover,.nav-tabs > li.active > a:focus { border-top:2px {$flowcart_primary_color} solid; }
.fa-primary,.btn-link, .btn-link a:hover, .btn-link:hover, .btn-link:focus { color: {$flowcart_primary_color}; }
.btn-default:hover, .btn-default:focus { color: #fff; background-color: {$flowcart_primary_color}; border-color: {$flowcart_primary_color};}
.order-row h6, .order-row .order-price, .order-row .cart-features a, .order-row h6 a { color: {$flowcart_primary_color}; }
.well-primary, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border-top-color: {$flowcart_primary_color}; }
.loader .fa, .pricing .fa { color: {$flowcart_primary_color}; }

{/if}
{if isset($flowcart_secondary_color)}
.currency-product a { color: {$flowcart_secondary_color}; }
input.cycle-1:checked+label.radio i, input.cycle-curr:checked+label.radio i, input.cycle-1:checked+label.radio i { color: {$flowcart_secondary_color}; }
.fa-money.active { color: {$flowcart_secondary_color}; }
.bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-success, .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-success { background: {$flowcart_secondary_color}; }
{/if}
{if $flowcart_hide_domain_step}
#stepbar > li {
	width: 33.333%;
}
{/if}

{if $flowcart_well_background_color}
#order-standard_cart .well {
    background-color: {$flowcart_well_background_color};
}
{/if}
{if $flowcart_well_text_color}
#order-standard_cart .well {
   color: {$flowcart_well_text_color};
}
{/if}

#flow-menu li.dashboard.flowcart_items_menu, #stepbar li.dashboard.flowcart_items_menu
{
	right: 0 ;
	left: auto;
	z-index: 10000; 
}
#flow-menu > .flowcart_items_menu li .flow-icon 
{
	font-size: 14px;
	color: #333;
	padding-right: 10px;
}
.productGroupfeatures{
	padding-left: 0;
}
.productGroupfeatures li{
	float: left;
	padding-right: 10px;
}
.productGroupfeatures i{
	padding-right: 5px;
}
</style>