{include file="orderforms/$carttpl/common.tpl"}
<div class="alert alert-danger">
  <h4>{$LANG.thereisaproblem}</h4>
  <h6>{$errortitle}</h6>
  <p>{$errormsg}</p>
  <a href="javascript:history.go(-1)" class="btn btn-danger">{$LANG.problemgoback}</a>
</div>
