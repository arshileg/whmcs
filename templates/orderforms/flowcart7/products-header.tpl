<style>
#flow-menu > li, #stepbar > li{
   width: {$width_flowcart_items}%;
}
</style>

<div class="hidden-xs">
<ul id="flow-menu" class="mb-1 {if $LANG.locale == 'ar_AR' || $LANG.locale == 'fa_IR'}flow-menu-flip{/if}">
  <li class="dashboard dropdown">
    <a href="#" data-toggle="dropdown"><i class="fas fa-bars"></i></a>
    <ul class="dropdown-menu">

      {if isset($flowcart_items_menu) && count($flowcart_items_menu)>0 }
          {foreach from=$flowcart_items_menu item=item}
              <li><a href="cart.php?gid={$item.gid}">{$item.name}</a></li>
          {/foreach}
          <li class="divider"></li>
      {/if}

      {if $loggedin}
      <li><a href="{$smarty.server.PHP_SELF}?gid=addons">{$LANG.cartproductaddons}</a></li>
      {if $renewalsenabled}<li><a href="{$smarty.server.PHP_SELF}?gid=renewals">{$LANG.domainrenewals}</a><li>{/if}
      {/if}
      {if $registerdomainenabled}<li><a href="{$smarty.server.PHP_SELF}?a=add&domain=register">{$LANG.registerdomain}</a></li>{/if}
      {if $transferdomainenabled}<li><a href="{$smarty.server.PHP_SELF}?a=add&domain=transfer">{$LANG.transferdomain}</a></li>{/if}
      <li class="divider"></li>
      <li><a href="{$smarty.server.PHP_SELF}?a=view">{$LANG.viewcart}</a></li>
    </ul>
  </li>

  {if isset($flowcart_items)}
    {assign var="items_c" value="0"}
    {foreach from=$flowcart_items item=item}
        {if $items_c == 6}
            {break}
        {/if}
        <li class="group-first{if $gid eq $item.gid} active animatedCart fadeInCart{/if}">
          <a href="cart.php?gid={$item.gid}">
            <span class="hidden-sm flow-icon {if isset($item.icon)} {$item.icon} {/if} "></span><span class="flowtitle">{$item.name}</span>
          </a>
        </li>
        {assign var="items_c" value=$items_c+1}
    {/foreach}

  {/if}
</ul>
</div>

<div class="form-group visible-xs">
<div class="btn-group btn-block">
    <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        {$LANG.cartchooseanothercategory} <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        {foreach key=num item=productgroup from=$flowcart_items}
            <li><a href="cart.php?gid={$productgroup.gid}">{$productgroup.name}</a></li>
        {/foreach}
        {if $loggedin}
            <li><a href="cart.php?gid=addons">{$LANG.cartproductaddons}</a></li>
            {if $renewalsenabled}
                <li><a href="cart.php?gid=renewals">{$LANG.domainrenewals}</a></li>
            {/if}
        {/if}
        {if $registerdomainenabled}
            <li><a href="cart.php?a=add&domain=register">{$LANG.registerdomain}</a></li>
        {/if}
        {if $transferdomainenabled}
            <li><a href="cart.php?a=add&domain=transfer">{$LANG.transferdomain}</a></li>
        {/if}
        <li><a href="cart.php?a=checkout">{$LANG.viewcart}</a></li>
    </ul>
</div>
</div>
