{include file="$template/pageheader.tpl" title=$LANG.invoices desc=$LANG.invoicesintro icon=drawer}
{include file="$template/includes/tablelist.tpl" tableName="InvoicesList" filterColumn="4" noSortColumns="5"}
<div class="alert alert-info mb-0">
{$LANG.invoicesoutstandingbalance}: {$totalbalance}
{if $masspay}
<div class="pull-right flip"><a href="clientarea.php?action=masspay&all=true" class="btn btn-link"><span class="glyphicon glyphicon-ok-circle"></span> {$LANG.masspayall}</a></div>
{/if}
</div>

<script type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableInvoicesList').removeClass('hidden').DataTable();
        {if $orderby == 'default'}
            table.order([4, 'desc'], [2, 'asc']);
        {elseif $orderby == 'invoicenum'}
            table.order(0, '{$sort}');
        {elseif $orderby == 'date'}
            table.order(1, '{$sort}');
        {elseif $orderby == 'duedate'}
            table.order(2, '{$sort}');
        {elseif $orderby == 'total'}
            table.order(3, '{$sort}');
        {elseif $orderby == 'status'}
            table.order(4, '{$sort}');
        {/if}
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
</script>

<div class="table-container clearfix">
    <div class="panel panel-default panel-datatable">
    <div class="panel-heading clearfix"> {include file="$template/includes/sidebar.tpl" sidebar=$primarySidebar}</div>
    <table id="tableInvoicesList" class="table table-list hidden">
        <thead>
            <tr>
                <th>{$LANG.invoicestitle}</th>
                <th class="hidden-sm hidden-xs">{$LANG.invoicesdatecreated}</th>
                <th class="hidden-xs">{$LANG.invoicesdatedue}</th>
                <th class="hidden-sm hidden-xs">{$LANG.invoicestotal}</th>
                <th>{$LANG.invoicesstatus}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach key=num item=invoice from=$invoices}
                <tr>
                    <td><a href="viewinvoice.php?id={$invoice.id}">{$invoice.invoicenum}</a>

                    <ul class="cell-inner-list visible-sm visible-xs small">
                    <li><span class="item-title">{$LANG.invoicestotal}: </span>{$invoice.total}</li>
                    <li><span class="item-title">{$LANG.invoicesdatecreated}: </span>{$invoice.datecreated}</li>
                    <li class="hidden-sm"><span class="item-title">{$LANG.invoicesdatedue}: </span>{$invoice.datedue}</li>
                    </ul>
                    </td>

                    <td class="hidden-sm hidden-xs"><span class="hidden">{$invoice.normalisedDateCreated}</span>{$invoice.datecreated}</td>
                    <td class="hidden-xs"><span class="hidden">{$invoice.normalisedDateDue}</span>{$invoice.datedue}</td>
                    <td data-order="{$invoice.totalnum}" class="hidden-sm hidden-xs">{$invoice.total}</td>
                    <td><span class="label label-{$invoice.statusClass}">{$invoice.status}</span></td>
                    <td class="text-right"><a href="viewinvoice.php?id={$invoice.id}" class="btn btn-link">
                     <i class="fa fa-chevron-right fa-lg pull-right flip"></i>
                        </a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> {$LANG.loading}</p>
    </div>
    </div>
</div>
