<h3 class="page-header"><span aria-hidden="true" class="icon icon-user"></span> {$LANG.clientareanavdetails}</h3>
<script type="text/javascript" src="{$BASE_PATH_JS}/StatesDropdown.js"></script>
{include file="$template/clientareadetailslinks.tpl"}
{if $successful}
{include file="$template/includes/alert.tpl" type="success" msg=$LANG.changessavedsuccessfully textcenter=true}
{/if}
{if $errormessage}
{include file="$template/includes/alert.tpl" type="error" errorshtml=$errormessage}
{/if}
<form method="post" action="{$smarty.server.PHP_SELF}?action=details" role="form" class="form-horizontal p-2">
  <div class="form-group">
    <label for="inputFirstName" class="col-sm-2 control-label">{$LANG.clientareafirstname}</label>
    <div class="col-sm-10">
      <input type="text" name="firstname" id="inputFirstName" value="{$clientfirstname}"{if in_array('firstname', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputLastName" class="col-sm-2 control-label">{$LANG.clientarealastname}</label>
    <div class="col-sm-10">
      <input type="text" name="lastname" id="inputLastName" value="{$clientlastname}"{if in_array('lastname', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputCompanyName" class="col-sm-2 control-label">{$LANG.clientareacompanyname}</label>
    <div class="col-sm-6">
      <input type="text" name="companyname" id="inputCompanyName" value="{$clientcompanyname}"{if in_array('companyname', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-2 control-label">{$LANG.clientareaemail}</label>
    <div class="col-sm-10">
      <input type="email" name="email" id="inputEmail" value="{$clientemail}"{if in_array('email', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputPhone" class="col-sm-2 control-label">{$LANG.clientareaphonenumber}</label>
    <div class="col-sm-10">
      <input type="tel" name="phonenumber" id="inputPhone" value="{$clientphonenumber}"{if in_array('phonenumber',$uneditablefields)} disabled=""{/if} class="form-control" />
    </div>
  </div>
  <hr>
  <div class="form-group">
    <label for="inputAddress1" class="col-sm-2 control-label">{$LANG.clientareaaddress1}</label>
    <div class="col-sm-10">
      <input type="text" name="address1" id="inputAddress1" value="{$clientaddress1}"{if in_array('address1', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress2" class="col-sm-2 control-label">{$LANG.clientareaaddress2}</label>
    <div class="col-sm-10">
      <input type="text" name="address2" id="inputAddress2" value="{$clientaddress2}"{if in_array('address2', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputCity" class="col-sm-2 control-label">{$LANG.clientareacity}</label>
    <div class="col-sm-10">
      <input type="text" name="city" id="inputCity" value="{$clientcity}"{if in_array('city', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputState" class="col-sm-2 control-label">{$LANG.clientareastate}</label>
    <div class="col-sm-6">
      <input type="text" name="state" id="inputState" value="{$clientstate}"{if in_array('state', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="inputPostcode" class="col-sm-2 control-label">{$LANG.clientareapostcode}</label>
    <div class="col-sm-10">
      <input type="text" name="postcode" id="inputPostcode" value="{$clientpostcode}"{if in_array('postcode', $uneditablefields)} disabled="disabled"{/if} class="form-control" />
    </div>
  </div>
  <div class="form-group">
    <label for="country" class="col-sm-2 control-label">{$LANG.clientareacountry}</label>
    <div class="col-sm-6">
      {$clientcountriesdropdown}
    </div>
  </div>
  <hr>
  <div class="form-group">
    <label for="inputPaymentMethod" class="col-sm-2 control-label">{$LANG.paymentmethod}</label>
    <div class="col-sm-6">
      <select name="paymentmethod" id="inputPaymentMethod" class="form-control">
        <option value="none">{$LANG.paymentmethoddefault}</option>
        {foreach from=$paymentmethods item=method}
        <option value="{$method.sysname}"{if $method.sysname eq $defaultpaymentmethod} selected="selected"{/if}>{$method.name}</option>
        {/foreach}
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputBillingContact" class="col-sm-2 control-label">{$LANG.defaultbillingcontact}</label>
    <div class="col-sm-6">
      <select name="billingcid" id="inputBillingContact" class="form-control">
        <option value="0">{$LANG.usedefaultcontact}</option>
        {foreach from=$contacts item=contact}
        <option value="{$contact.id}"{if $contact.id eq $billingcid} selected="selected"{/if}>{$contact.name}</option>
        {/foreach}
      </select>
    </div>
  </div>
  {if $customfields}
  {foreach from=$customfields key=num item=customfield}
  <div class="form-group">
    <label for="customfield{$customfield.id}" class="col-sm-2 control-label">{$customfield.name}</label>
    <div class="col-sm-6">
      <div class="control">
        {$customfield.input} {$customfield.description}
      </div>
    </div>
  </div>
  {/foreach}
  {/if}
  {if $emailoptoutenabled}
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox">  <input type="checkbox" value="1" name="emailoptout" id="inputEmailOptOut" {if $emailoptout} checked{/if} /> {$LANG.emailoptoutdesc}
        </label>
      </div>
    </div>
  </div>
  {/if}
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input class="btn btn-primary" type="submit" name="save" value="{$LANG.clientareasavechanges}" />
    </div>
  </div>
</form>
