$(function(){
	var head = document.head
	  , link = document.createElement('link');

	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = "/modules/addons/hexa_extras/assets/fonts/flow.css";

	head.appendChild(link);

	init_iconlist_load_data(true);
	$('.add_more_btn').show();
});
function initform(){
		var flowcart_items = $('[name=flowcart_items]').val();

	if(flowcart_items.length>0){
		var items = $.parseJSON(flowcart_items);
	
		var i;
	
		i = 0;
		$.each(items, function(i, val) {
		   if (i==0){
	
		   }else{
		   		addmore_flowcart_gid();
		   } 	
	       i++;
	    });
	
		i = 0;
		$( "#savesetting [name^=flowcart_icon]" ).each(function( index ) {
			$(this).val(items[i].icon);		
			i++;		
		});	
	
		i = 0;
		$( "#savesetting [name^=flowcart_gid]" ).each(function( index ) {
			$(this).val(items[i].gid);	
			i++;
		});

		i = 0;
		$( "#savesetting [name^=flowcart_product_column_width]" ).each(function( index ) {
			$(this).val(items[i].column_width);	
			i++;
		});		
	}

	init_iconlist();
}

function addmore_flowcart_gid(){
	var c = $("#flowcard_holder .flowcart_icon").length;


	$('#flowcard_holder').append($('#f_template').html());


	rename_flowcart_gid();	
}


function remove_flowcart_gid(e){
	$(e).parents('.gid-row').remove();

	
	rename_flowcart_gid();
}

function rename_flowcart_gid(){
	var i; i = 2;

	$( "#flowcard_holder .flowcart_icon" ).each(function( index ) {
	  	$(this).text('Flowcart Icon '+i);
	  	i++;		
	});	

	i = 2;
	$( "#flowcard_holder .flowcart_gid" ).each(function( index ) {
	  	$(this).text('Flowcart gid '+i);
	  	i++;		
	});	

	i = 2;
	$( "#flowcard_holder .flowcart_width" ).each(function( index ) {
	  	$(this).text('Product Column Width '+i);
	  	i++;		
	});	
}

function vasltojson(){
	var items = new Array();

	var icons_c = $( "#savesetting [name^=flowcart_icon]" ).length;

	$( "#savesetting [name^=flowcart_icon]" ).each(function( index ) {
		var o = new Object;
		o.icon = $(this).val();
		items.push(o);
		
	});	
	var i = 0;
	$( "#savesetting [name^=flowcart_gid]" ).each(function( index ) {
		
		items[i].gid = $(this).val();
		
		i++;
	});

	var i = 0;
	$( "#savesetting [name^=flowcart_product_column_width]" ).each(function( index ) {
		
		items[i].column_width = $(this).val();
		
		i++;
	});

	$('[name=flowcart_items]').val(JSON.stringify(items));

}

function init_iconlist_load_data(vinitform){

		$.getJSON( "../modules/addons/hexa_extras/assets/json/icons.json", function( data ) {

			$('.toinit_selectpicker').each(function(key, value) {   
					var select =  $(this);
					console.log(select);
					$.each(data, function(key_icon, value_icon) {   
					     $(select)
					         .append($("<option></option>")
					         .attr("value",value_icon)
					         .attr("data-icon",value_icon)
					         .text(value_icon)); 
					});
			});

			if(vinitform){	
				initform();
			}
			else{
				init_iconlist();
			}		

		});

}


function init_iconlist(){
	$('.toinit_selectpicker').selectpicker({size: 4});	
	$('.toinit_selectpicker').removeClass('toinit_selectpicker');
}