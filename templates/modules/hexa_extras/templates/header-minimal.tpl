  <div class="hexa-container hidden-xs"><a href="contact.php" class="btn btn-xs btn-default hexa-btn" data-original-title="{$LANG.contactus}"><i class="fa fa-send-o"></i></a></div>
  {if $languagechangeenabled && count($locales) > 1}
  <div class="hexa-container hexa-container-lang hidden-xs">
  <div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle hexa-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa icon-globe"></i>
  </button>
  <ul class="dropdown-menu">
         {foreach from=$locales item=locale}
                    {if $LANG.chooselanguage == $locale.localisedName}
                                  <li><a class="active" href="{$currentpagelinkback}language={$locale.language}"  data-lang="{$locale.language}">{$locale.localisedName}</a></li>
                                  {else}
                                  <li><a href="{$currentpagelinkback}language={$locale.language}"  data-lang="{$locale.language}" >{$locale.localisedName}</a></li>
                    {/if}
         {/foreach}
  </ul>
</div>
  </div>
{/if}
 <div class="login-wrapper">
  <div class="container">
   <div class="row">
     <div class="col-md-4 col-md-offset-4 logo-page">
      <a href="index.php" title="{$companyname}">
        {if !isset($hexa_logo)}
          <span class="logo"><span aria-hidden="true" class="icon icon-map"></span> {$companyname}</span>
        {else}
          <span class="logo">
            <img {if isset($hexa_logo_width)} style="width:{$hexa_logo_width}px" {/if} src="{$hexa_logo}">
          </span>
        {/if}
      </a>
    </div>
  </div>
