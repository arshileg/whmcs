<?php
global $hexa_extras_options_fields;
?>
<div id="tabs">
               <ul role="tablist" class="nav nav-tabs">
               <li <?php if (
                        $_REQUEST['action'] != "flowcart_page" 
                        && $_REQUEST['action'] != "information_page" 
                        && $_REQUEST['action'] != "customize_page"
                        && $_REQUEST['action'] != "fonts_page"
                        && $_REQUEST['action'] != "menu_page"
                        ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=general_page">General</a></li>

                <li <?php if ($_REQUEST['action'] == "flowcart_page" ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=flowcart_page">Flowcart</a></li>

                <li <?php if ($_REQUEST['action'] == "customize_page" ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=customize_page">Customize</a></li>

                <li <?php if ($_REQUEST['action'] == "fonts_page" ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=fonts_page">Fonts</a></li>
                
                <li <?php if ($_REQUEST['action'] == "menu_page" ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=menu_page">Menu</a></li>

                <li <?php if ($_REQUEST['action'] == "information_page" ) { ?> class="active" 
                <?php } else { ?> class="tabs" <?php } ?>  ><a  role="tab" href="addonmodules.php?module=hexa_extras&action=information_page">
                <?php echo hexa_check_version($hexa_extras_options_fields); ?>Information</a></li>
               

                </ul>  
</div>