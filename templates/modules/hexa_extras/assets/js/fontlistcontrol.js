var google_font_list = null;


function fonts_fontchanged(e){
	fonts_setoptions();
}

function fonts_charsetchanged(e){
	fonts_reset_charsets(e);
}

function fonts_reset_charsets(e){
	var charsets;
	charsets = '';
	$( e ).parents('.font_control').find('.charsets input:checked').each(function( index ) {
			charsets += $(this).attr('name')+',';	
	});

	if(charsets.length>0) charsets = charsets.substring(0, charsets.length - 1);

	$( e ).parents('.font_control').find('.charsets_value').val(charsets);
}

function fonts_setoptions(){
	$( ".dropdownfontlist" ).each(function( index ) {


		if( $(this).val().length > 0 ){
				if(typeof google_font_list[$(this).val()] === 'undefined') {
				    // does not exist
				    $(this).parents('.font_control').find('.options_holder .charsets').html('');
				    $(this).parents('.font_control').find('.options_holder .charsets-label').hide();

				}
				else {
				    // does exist

				    var chsets = '';

				    $.each(google_font_list[$(this).val()].subsets,function( index, charset ) {
				    		
				    		chsets += fonts_render_checkbox(charset.id,charset.name,false);
				    });


				    $(this).parents('.font_control').find('.options_holder .charsets').html(chsets);
				    $(this).parents('.font_control').find('.options_holder .charsets-label').show();
				    fonts_load_charsets(this);
				}
		}

	});
}

function fonts_load_charsets(e){
	var charsets;
	var element;
	element = $(e).parents('.font_control').find('.options_holder .charsets');
	charsets = $( e ).parents('.font_control').find('.charsets_value').val();
	if(charsets.length>0) {
		ch_list = charsets.split(',');

		$.each(ch_list,function( index, charset ) {
			$(element).find('input[name='+charset+']').attr('checked','checked');
		});
	}
}


$(function(){
	$.getJSON( "../modules/addons/hexa_extras/assets/json/googlefonts.json", function( data ) {

		google_font_list = data;

		fonts_setoptions();

	});
});

function fonts_render_checkbox(key,name,val){
  var html; 
  var checked = '';

  if (val){
  	checked = ' checked ';
  }  

  html = '<div class="checkbox"><label><input type="checkbox" onchange="fonts_charsetchanged(this);" name="'+key+'" '+checked+' >'+name+
    	 '</label></div>';

  return html;  	 
}