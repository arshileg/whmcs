<?php

ini_set('display_errors', 0);

require_once __DIR__ . '/init.php';
require_once __DIR__ . '/includes/gatewayfunctions.php';
require_once __DIR__ . '/includes/invoicefunctions.php';

// Detect module name from filename.
$gatewayModuleName = 'payge';

// Fetch gateway configuration parameters.
$gatewayParams = getGatewayVariables($gatewayModuleName);

// Die if module is not active.
if (!$gatewayParams['type']) {
    die("Module Not Activated");
}

// $gatewayParams['pass']

$check = hash('sha256', $_REQUEST['status'].$_REQUEST['transactioncode'].$_REQUEST['amount'].$_REQUEST['currency'].$_REQUEST['ordercode'].$_REQUEST['paymethod'].$_REQUEST['payedamount'].$_REQUEST['customdata'].$_REQUEST['testmode'].$gatewayParams['pass']);


if ( ($_REQUEST['status']=='COMPLETED') && ($check==strtolower($_REQUEST['check'])) ) {

    checkCbInvoiceID($_REQUEST['customdata'], $gatewayParams['name']);
	
	checkCbTransID($_REQUEST['transactioncode']);
	
	$Tansactionlog = array('status' => $_REQUEST['status'], 'transactioncode' => $_REQUEST['transactioncode'], 'amount' => $_REQUEST['amount'], 'currency' => $_REQUEST['currency'], 'ordercode' => $_REQUEST['ordercode'], 'paymethod' => $_REQUEST['paymethod'], 'customdata' => $_REQUEST['customdata'], 'payedamount' => $_REQUEST['payedamount'], 'testmode' => $_REQUEST['testmode'], 'check' => $_REQUEST['check']);
	
	logTransaction($gatewayParams['name'], $Tansactionlog, $_REQUEST['status']);

    $amount = $_REQUEST['amount'] / 100;
	
	addInvoicePayment($_REQUEST['customdata'], $_REQUEST['transactioncode'], $amount, 0, $gatewayModuleName);


    $xmlcheck = hash('sha256', '0'.'Success'.$_REQUEST['transactioncode'].$gatewayParams['pass']);
    $xml = '<result>';
    $xml.= '<resultcode>0</resultcode>';
    $xml.= '<resultdesc>Success</resultdesc>';
    $xml.= '<check>'.$xmlcheck.'</check>';
    $xml.= '<data>Invoice: '.$_REQUEST["customdata"].'</data>';
    $xml.= '</result>';

    echo $xml;

}

?>