<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>ტრანზაქციის დამუშავება</title>
		<style type="text/css">
			/* @import url("//hostnodes.ge/fonts/stylesheet.css"); */
			@import url("/fonts/bpg-arial.min.css");
			@import url("/fonts/bpg-nino-mtavruli-book.min.css");

			.robotocaps {
				font-family: "BPG Nino Mtavruli Book", sans-serif;
				font-weight: normal;
				font-style: normal;
			}
			.robotonormal {
				font-family: "BPG Arial", sans-serif;
				font-weight: normal;
				font-style: normal;
			}
			body {
				background: url('//hostnodes.ge/wp-content/uploads/revslider/slider3/slider3-bg1.png') no-repeat center center fixed;
				background-size: cover;
			}
			a {
				color: #719ed4;
			}
		</style>
    </head>
    <body>
		<div style="text-align: center; width: 350px; height: 300px; margin: auto; position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
			<img src="//hostnodes.ge/wp-content/uploads/2016/04/Logo_Final_hn4.png" alt="HostNodes LTD">
			<div style="background-color: #eee; padding: 15px; border-radius: 5px; margin-top: 10px;">
				
					<h3 style="text-align: center; font-weight: bold;" class="robotocaps">ტრანზაქციის დამუშავება</h3>
				
				
				<?php
					$good = '<span style="color: green;">დადებითი</span>';
					$bad = '<span style="color: red;">უარყოფითი</span>';
				?>

				
					<h4 style="text-align: center;" class="robotonormal">ტრანზაქციის სტატუსი: <?php if($_REQUEST['status']=='COMPLETED'){ echo $good; } else { echo $bad; } ?></h4>
				
					<h5 style="text-align: center;" class="robotonormal"><a href="//<?php echo $_SERVER['HTTP_HOST']; ?>/clientarea.php?action=invoices">ინვოისების გვერდი</a></h5>
			</div>
		</div>
	</body>
</html>