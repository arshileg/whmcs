<?php
/* Smarty version 3.1.29, created on 2018-08-08 10:16:30
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6ac2fe4905e6_70798613',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533723390,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6ac2fe4905e6_70798613 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>Recently a request was submitted to reset your password for our client area. If you did not request this, please ignore this email. It will expire and become useless in 2 hours time.</p>
<p>To reset your password, please visit the url below:<br /><a href="<?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>
</a></p>
<p>When you visit the link above, you will have the opportunity to choose a new password.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
