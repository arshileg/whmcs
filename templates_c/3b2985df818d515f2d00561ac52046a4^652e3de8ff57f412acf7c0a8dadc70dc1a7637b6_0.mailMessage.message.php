<?php
/* Smarty version 3.1.29, created on 2018-08-08 17:42:54
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b2b9e327269_95083981',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533750174,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b2b9e327269_95083981 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ჩვენი მივიღეთ თქვენი შეკვეთა, რომელიც მოკლე დროში დამუშავდება. შეკვეთის დეტალები მოცემულია ქვემოთ:</p>
<p>შეკვეთის ნომერი: <?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['order_details']->value;?>
</p>
<p>თქვენი ანგარიშის მოწყობის შემდგომ, ჩვენგან მიიღებთ წერილს. თუკი რაიმე დეტალის დაზუსტება გსურთ, მოგვმართეთ შეკვეთის ნომრით. </p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
