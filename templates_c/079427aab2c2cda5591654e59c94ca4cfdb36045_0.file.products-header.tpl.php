<?php
/* Smarty version 3.1.29, created on 2018-08-03 03:06:30
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/products-header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63c6b61a54d8_64932857',
  'file_dependency' => 
  array (
    '079427aab2c2cda5591654e59c94ca4cfdb36045' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/products-header.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63c6b61a54d8_64932857 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><style>
#flow-menu > li, #stepbar > li{
   width: <?php echo $_smarty_tpl->tpl_vars['width_flowcart_items']->value;?>
%;
}
</style>

<div class="hidden-xs">
<ul id="flow-menu" class="mb-1">
  <li class="dashboard dropdown">
    <a href="#" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
    <ul class="dropdown-menu">

      <?php if (isset($_smarty_tpl->tpl_vars['flowcart_items_menu']->value) && count($_smarty_tpl->tpl_vars['flowcart_items_menu']->value) > 0) {?>
          <?php
$_from = $_smarty_tpl->tpl_vars['flowcart_items_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
              <li><a href="cart.php?gid=<?php echo $_smarty_tpl->tpl_vars['item']->value['gid'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></li>
          <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
          <li class="divider"></li>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>
      <li><a href="<?php echo $_SERVER['PHP_SELF'];?>
?gid=addons"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductaddons'];?>
</a></li>
      <?php if ($_smarty_tpl->tpl_vars['renewalsenabled']->value) {?><li><a href="<?php echo $_SERVER['PHP_SELF'];?>
?gid=renewals"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewals'];?>
</a><li><?php }?>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['registerdomainenabled']->value) {?><li><a href="<?php echo $_SERVER['PHP_SELF'];?>
?a=add&domain=register"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['registerdomain'];?>
</a></li><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['transferdomainenabled']->value) {?><li><a href="<?php echo $_SERVER['PHP_SELF'];?>
?a=add&domain=transfer"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['transferdomain'];?>
</a></li><?php }?>
      <li class="divider"></li>
      <li><a href="<?php echo $_SERVER['PHP_SELF'];?>
?a=view"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['viewcart'];?>
</a></li>
    </ul>
  </li>

  <?php if (isset($_smarty_tpl->tpl_vars['flowcart_items']->value)) {?>
    <?php if (isset($_smarty_tpl->tpl_vars["items_c"])) {$_smarty_tpl->tpl_vars["items_c"] = clone $_smarty_tpl->tpl_vars["items_c"];
$_smarty_tpl->tpl_vars["items_c"]->value = "0"; $_smarty_tpl->tpl_vars["items_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["items_c"] = new Smarty_Variable("0", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "items_c", 0);?>
    <?php
$_from = $_smarty_tpl->tpl_vars['flowcart_items']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
        <?php if ($_smarty_tpl->tpl_vars['items_c']->value == 6) {?>
            <?php break 1;?>
        <?php }?>
        <li class="group-first<?php if ($_smarty_tpl->tpl_vars['gid']->value == $_smarty_tpl->tpl_vars['item']->value['gid']) {?> active animatedCart fadeInCart<?php }?>">
          <a href="cart.php?gid=<?php echo $_smarty_tpl->tpl_vars['item']->value['gid'];?>
">
            <span class="hidden-sm flow-icon <?php if (isset($_smarty_tpl->tpl_vars['item']->value['icon'])) {?> <?php echo $_smarty_tpl->tpl_vars['item']->value['icon'];?>
 <?php }?> "></span><span class="flowtitle"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</span>
          </a>
        </li>
        <?php if (isset($_smarty_tpl->tpl_vars["items_c"])) {$_smarty_tpl->tpl_vars["items_c"] = clone $_smarty_tpl->tpl_vars["items_c"];
$_smarty_tpl->tpl_vars["items_c"]->value = $_smarty_tpl->tpl_vars['items_c']->value+1; $_smarty_tpl->tpl_vars["items_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["items_c"] = new Smarty_Variable($_smarty_tpl->tpl_vars['items_c']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "items_c", 0);?>
    <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
?>

  <?php }?>
</ul>
</div>

<div class="form-group visible-xs">
<div class="btn-group btn-block">
    <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartchooseanothercategory'];?>
 <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <?php
$_from = $_smarty_tpl->tpl_vars['productgroups']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_productgroup_2_saved_item = isset($_smarty_tpl->tpl_vars['productgroup']) ? $_smarty_tpl->tpl_vars['productgroup'] : false;
$__foreach_productgroup_2_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['productgroup'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['productgroup']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['productgroup']->value) {
$_smarty_tpl->tpl_vars['productgroup']->_loop = true;
$__foreach_productgroup_2_saved_local_item = $_smarty_tpl->tpl_vars['productgroup'];
?>
            <li><a href="cart.php?gid=<?php echo $_smarty_tpl->tpl_vars['productgroup']->value['gid'];?>
"><?php echo $_smarty_tpl->tpl_vars['productgroup']->value['name'];?>
</a></li>
        <?php
$_smarty_tpl->tpl_vars['productgroup'] = $__foreach_productgroup_2_saved_local_item;
}
if ($__foreach_productgroup_2_saved_item) {
$_smarty_tpl->tpl_vars['productgroup'] = $__foreach_productgroup_2_saved_item;
}
if ($__foreach_productgroup_2_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_productgroup_2_saved_key;
}
?>
        <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>
            <li><a href="cart.php?gid=addons"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductaddons'];?>
</a></li>
            <?php if ($_smarty_tpl->tpl_vars['renewalsenabled']->value) {?>
                <li><a href="cart.php?gid=renewals"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewals'];?>
</a></li>
            <?php }?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['registerdomainenabled']->value) {?>
            <li><a href="cart.php?a=add&domain=register"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['registerdomain'];?>
</a></li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['transferdomainenabled']->value) {?>
            <li><a href="cart.php?a=add&domain=transfer"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['transferdomain'];?>
</a></li>
        <?php }?>
        <li><a href="cart.php?a=checkout"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['viewcart'];?>
</a></li>
    </ul>
</div>
</div>
<?php }
}
