<?php
/* Smarty version 3.1.29, created on 2018-02-18 05:15:20
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/modules/servers/cpanel/overview.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a88d3a8c52844_04605585',
  'file_dependency' => 
  array (
    'e2432689de47f4bf386b7f43bdc247a1fed86400' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/modules/servers/cpanel/overview.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a88d3a8c52844_04605585 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><link href="modules/servers/cpanel/css/client.css" rel="stylesheet">
<?php echo '<script'; ?>
 src="modules/servers/cpanel/js/client.js"><?php echo '</script'; ?>
>

<div class="row">
    <div class="col-md-6">

        <div class="panel panel-default" id="cPanelPackagePanel">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['packageDomain'];?>
</h3>
            </div>
            <div class="panel-body text-center">

                <div class="cpanel-package-details">
                    <em><?php echo $_smarty_tpl->tpl_vars['groupname']->value;?>
</em>
                    <h4 style="margin:0;"><?php echo $_smarty_tpl->tpl_vars['product']->value;?>
</h4>
                    <a href="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
" target="_blank">www.<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
</a>
                </div>

                <p>
                    <a href="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
" class="btn btn-default btn-sm" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['visitwebsite'];?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['domainId']->value) {?>
                        <a href="clientarea.php?action=domaindetails&id=<?php echo $_smarty_tpl->tpl_vars['domainId']->value;?>
" class="btn btn-success btn-sm" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['managedomain'];?>
</a>
                    <?php }?>
                    <input type="button" onclick="popupWindow('whois.php?domain=<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
','whois',650,420);return false;" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['whoisinfo'];?>
" class="btn btn-info btn-sm" />
                </p>

            </div>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['availableAddonProducts']->value) {?>
            <div class="panel panel-default" id="cPanelExtrasPurchasePanel">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['addonsExtras'];?>
</h3>
                </div>
                <div class="panel-body text-center">

                    <form method="post" action="cart.php?a=add" class="form-inline">
                        <input type="hidden" name="serviceid" value="<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
" />
                        <select name="aid" class="form-control input-sm">
                        <?php
$_from = $_smarty_tpl->tpl_vars['availableAddonProducts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addonName_0_saved_item = isset($_smarty_tpl->tpl_vars['addonName']) ? $_smarty_tpl->tpl_vars['addonName'] : false;
$__foreach_addonName_0_saved_key = isset($_smarty_tpl->tpl_vars['addonId']) ? $_smarty_tpl->tpl_vars['addonId'] : false;
$_smarty_tpl->tpl_vars['addonName'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addonId'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addonName']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addonId']->value => $_smarty_tpl->tpl_vars['addonName']->value) {
$_smarty_tpl->tpl_vars['addonName']->_loop = true;
$__foreach_addonName_0_saved_local_item = $_smarty_tpl->tpl_vars['addonName'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['addonId']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['addonName']->value;?>
</option>
                        <?php
$_smarty_tpl->tpl_vars['addonName'] = $__foreach_addonName_0_saved_local_item;
}
if ($__foreach_addonName_0_saved_item) {
$_smarty_tpl->tpl_vars['addonName'] = $__foreach_addonName_0_saved_item;
}
if ($__foreach_addonName_0_saved_key) {
$_smarty_tpl->tpl_vars['addonId'] = $__foreach_addonName_0_saved_key;
}
?>
                        </select>
                        <button type="submit" class="btn btn-default btn-sm">
                            <i class="fa fa-shopping-cart"></i>
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['purchaseActivate'];?>

                        </button>
                    </form>

                </div>
            </div>
        <?php }?>

    </div>
    <div class="col-md-6">

        <div class="panel panel-default" id="cPanelUsagePanel">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageStats'];?>
</h3>
            </div>
            <div class="panel-body text-center cpanel-usage-stats">

                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1 col-xs-6" id="diskUsage">
                        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['diskUsage'];?>
</strong>
                        <br /><br />
                        <input type="text" value="<?php echo substr($_smarty_tpl->tpl_vars['diskpercent']->value,0,-1);?>
" class="usage-dial" data-fgColor="#5dd0ed" data-bgColor="#999" data-angleOffset="-125" data-angleArc="250" data-min="0" data-max="100" data-readOnly="true" data-width="100" data-height="80" />
                        <br /><br />
                        <?php echo $_smarty_tpl->tpl_vars['diskusage']->value;?>
 M / <?php echo $_smarty_tpl->tpl_vars['disklimit']->value;?>
 M
                    </div>
                    <div class="col-sm-5 col-xs-6" id="bandwidthUsage">
                        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['bandwidthUsage'];?>
</strong>
                        <br /><br />
                        <input type="text" value="<?php echo substr($_smarty_tpl->tpl_vars['bwpercent']->value,0,-1);?>
" class="usage-dial" data-fgColor="#5dd0ed" data-bgColor="#999" data-angleOffset="-125" data-angleArc="250" data-min="0" data-max="100" data-readOnly="true" data-width="100" data-height="80" />
                        <br /><br />
                        <?php echo $_smarty_tpl->tpl_vars['bwusage']->value;?>
 M / <?php echo $_smarty_tpl->tpl_vars['bwlimit']->value;?>
 M
                    </div>
                </div>

                <?php if (substr($_smarty_tpl->tpl_vars['bwpercent']->value,0,-1) > 75) {?>
                    <div class="text-danger limit-near">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageStatsBwLimitNear'];?>

                        <?php if ($_smarty_tpl->tpl_vars['packagesupgrade']->value) {?>
                            <a href="upgrade.php?type=package&id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
" class="btn btn-xs btn-danger">
                                <i class="fa fa-arrow-circle-up"></i>
                                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageUpgradeNow'];?>

                            </a>
                        <?php }?>
                    </div>
                <?php } elseif (substr($_smarty_tpl->tpl_vars['diskpercent']->value,0,-1) > 75) {?>
                    <div class="text-danger limit-near">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageStatsDiskLimitNear'];?>

                        <?php if ($_smarty_tpl->tpl_vars['packagesupgrade']->value) {?>
                            <a href="upgrade.php?type=package&id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
" class="btn btn-xs btn-danger">
                                <i class="fa fa-arrow-circle-up"></i>
                                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageUpgradeNow'];?>

                            </a>
                        <?php }?>
                    </div>
                <?php } else { ?>
                    <div class="text-info limit-near">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usageLastUpdated'];?>
 <?php echo $_smarty_tpl->tpl_vars['lastupdate']->value;?>

                    </div>
                <?php }?>

                <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.knob.js"><?php echo '</script'; ?>
>
                <?php echo '<script'; ?>
 type="text/javascript">
                jQuery(function() {
                    jQuery(".usage-dial").knob({
                        'format': function (value) {
                            return value + '%';
                        }
                    });
                });
                <?php echo '</script'; ?>
>

            </div>
        </div>

    </div>
</div>

<?php
$_from = $_smarty_tpl->tpl_vars['hookOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_1_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_1_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
    <div>
        <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

    </div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_local_item;
}
if ($__foreach_output_1_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_item;
}
?>

<?php if ($_smarty_tpl->tpl_vars['systemStatus']->value == 'Active') {?>

    <div class="panel panel-default" id="cPanelQuickShortcutsPanel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['quickShortcuts'];?>
</h3>
        </div>
        <div class="panel-body text-center">

            <div class="row cpanel-feature-row">
                <div class="col-sm-3 col-xs-6" id="cPanelEmailAccounts">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Email_Accounts" target="_blank">
                        <img src="modules/servers/cpanel/img/email_accounts.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['emailAccounts'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelForwarders">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Email_Forwarders" target="_blank">
                        <img src="modules/servers/cpanel/img/forwarders.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['forwarders'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelAutoResponders">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Email_AutoResponders" target="_blank">
                        <img src="modules/servers/cpanel/img/autoresponders.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['autoresponders'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelFileManager">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=FileManager_Home" target="_blank">
                        <img src="modules/servers/cpanel/img/file_manager.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['fileManager'];?>

                    </a>
                </div>
            </div>
            <div class="row cpanel-feature-row">
                <div class="col-sm-3 col-xs-6" id="cPanelBackup">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Backups_Home" target="_blank">
                        <img src="modules/servers/cpanel/img/backup.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['backup'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelSubdomains">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Domains_SubDomains" target="_blank">
                        <img src="modules/servers/cpanel/img/subdomains.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['subdomains'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelAddonDomains">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Domains_AddonDomains" target="_blank">
                        <img src="modules/servers/cpanel/img/addon_domains.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['addonDomains'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelCronJobs">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Cron_Home" target="_blank">
                        <img src="modules/servers/cpanel/img/cron_jobs.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['cronJobs'];?>

                    </a>
                </div>
            </div>
            <div class="row cpanel-feature-row">
                <div class="col-sm-3 col-xs-6" id="cPanelMySQLDatabases">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Database_MySQL" target="_blank">
                        <img src="modules/servers/cpanel/img/mysql_databases.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['mysqlDatabases'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelPhpMyAdmin">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Database_phpMyAdmin" target="_blank">
                        <img src="modules/servers/cpanel/img/php_my_admin.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['phpMyAdmin'];?>

                    </a>
                </div>
                <div class="col-sm-3 col-xs-6" id="cPanelAwstats">
                    <a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
&amp;dosinglesignon=1&amp;app=Stats_AWStats" target="_blank">
                        <img src="modules/servers/cpanel/img/awstats.png" />
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['awstats'];?>

                    </a>
                </div>
            </div>

        </div>
    </div>

    <div class="panel panel-default" id="cPanelQuickEmailPanel">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['createEmailAccount'];?>
</h3>
        </div>
        <div class="panel-body">

            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['cPanel']['emailAccountCreateSuccess'],'textcenter'=>true,'hide'=>true,'idname'=>"emailCreateSuccess",'additionalClasses'=>"email-create-feedback"), 0, true);
?>


            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"danger",'msg'=>($_smarty_tpl->tpl_vars['LANG']->value['cPanel']['emailAccountCreateFailed']).(' <span id="emailCreateFailedErrorMsg"></span>'),'textcenter'=>true,'hide'=>true,'idname'=>"emailCreateFailed",'additionalClasses'=>"email-create-feedback"), 0, true);
?>


            <form id="frmCreateEmailAccount" onsubmit="doEmailCreate();return false">
                <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['serviceid']->value;?>
" />
                <input type="hidden" name="email_quota" value="250" />
                <div class="row">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" name="email_prefix" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['usernamePlaceholder'];?>
">
                            <span class="input-group-addon">@<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <input type="password" name="email_pw" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['passwordPlaceholder'];?>
">
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary btn-block" />
                            <i class="fa fa-plus" id="btnCreateLoader"></i>
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['create'];?>

                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

<?php } else { ?>

    <div class="alert alert-warning text-center" role="alert" id="cPanelSuspendReasonPanel">
        <?php if ($_smarty_tpl->tpl_vars['suspendreason']->value) {?>
            <strong><?php echo $_smarty_tpl->tpl_vars['suspendreason']->value;?>
</strong><br />
        <?php }?>
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['packageNotActive'];?>
 <?php echo $_smarty_tpl->tpl_vars['status']->value;?>
.<br />
        <?php if ($_smarty_tpl->tpl_vars['systemStatus']->value == "Pending") {?>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['statusPendingNotice'];?>

        <?php } elseif ($_smarty_tpl->tpl_vars['systemStatus']->value == "Suspended") {?>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['statusSuspendedNotice'];?>

        <?php }?>
    </div>

<?php }?>

<div class="panel panel-default" id="cPanelBillingOverviewPanel">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cPanel']['billingOverview'];?>
</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-5">
                <?php if ($_smarty_tpl->tpl_vars['firstpaymentamount']->value != $_smarty_tpl->tpl_vars['recurringamount']->value) {?>
                    <div class="row" id="firstPaymentAmount">
                        <div class="col-xs-6 text-right" >
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['firstpaymentamount'];?>

                        </div>
                        <div class="col-xs-6">
                            <?php echo $_smarty_tpl->tpl_vars['firstpaymentamount']->value;?>

                        </div>
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['billingcycle']->value != $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermonetime'] && $_smarty_tpl->tpl_vars['billingcycle']->value != $_smarty_tpl->tpl_vars['LANG']->value['orderfree']) {?>
                    <div class="row" id="recurringAmount">
                        <div class="col-xs-6 text-right">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['recurringamount'];?>

                        </div>
                        <div class="col-xs-6">
                            <?php echo $_smarty_tpl->tpl_vars['recurringamount']->value;?>

                        </div>
                    </div>
                <?php }?>
                <div class="row" id="billingCycle">
                    <div class="col-xs-6 text-right">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderbillingcycle'];?>

                    </div>
                    <div class="col-xs-6">
                        <?php echo $_smarty_tpl->tpl_vars['billingcycle']->value;?>

                    </div>
                </div>
                <div class="row" id="paymentMethod">
                    <div class="col-xs-6 text-right">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>

                    </div>
                    <div class="col-xs-6">
                        <?php echo $_smarty_tpl->tpl_vars['paymentmethod']->value;?>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row" id="registrationDate">
                    <div class="col-xs-6 col-md-5 text-right">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingregdate'];?>

                    </div>
                    <div class="col-xs-6 col-md-7">
                        <?php echo $_smarty_tpl->tpl_vars['regdate']->value;?>

                    </div>
                </div>
                <div class="row" id="nextDueDate">
                    <div class="col-xs-6 col-md-5 text-right">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>

                    </div>
                    <div class="col-xs-6 col-md-7">
                        <?php echo $_smarty_tpl->tpl_vars['nextduedate']->value;?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php }
}
