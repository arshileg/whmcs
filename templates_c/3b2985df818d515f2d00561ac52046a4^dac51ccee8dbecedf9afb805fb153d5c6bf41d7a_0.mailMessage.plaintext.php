<?php
/* Smarty version 3.1.29, created on 2018-08-08 17:42:54
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b2b9e32e5a4_10700878',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533750174,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b2b9e32e5a4_10700878 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ჩვენი მივიღეთ თქვენი შეკვეთა, რომელიც მოკლე დროში დამუშავდება. შეკვეთის დეტალები მოცემულია ქვემოთ:


შეკვეთის ნომერი: <?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>



<?php echo $_smarty_tpl->tpl_vars['order_details']->value;?>



თქვენი ანგარიშის მოწყობის შემდგომ, ჩვენგან მიიღებთ წერილს. თუკი რაიმე დეტალის დაზუსტება გსურთ, მოგვმართეთ შეკვეთის ნომრით. 


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
