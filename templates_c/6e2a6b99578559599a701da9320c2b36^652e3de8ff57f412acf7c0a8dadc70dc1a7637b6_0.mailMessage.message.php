<?php
/* Smarty version 3.1.29, created on 2018-08-08 18:06:37
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b312d25b079_19621204',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533751597,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b312d25b079_19621204 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,<br /><br />We have now removed your email address from our mailing list.<br /><br />If this was a mistake or you change your mind, you can re-subscribe at any time from the My Details section of our client area.<br /><br /><a href="<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/clientarea.php?action=details"><?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/clientarea.php?action=details</a><br /><br /><?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
