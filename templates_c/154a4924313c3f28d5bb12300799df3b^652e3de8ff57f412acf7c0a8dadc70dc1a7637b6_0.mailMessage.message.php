<?php
/* Smarty version 3.1.29, created on 2018-05-16 17:44:30
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5afc35be9e3a29_50601832',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1526478270,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5afc35be9e3a29_50601832 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>As you requested, your password for our client area has now been reset. Your new login details are as follows:</p>
<p><?php echo $_smarty_tpl->tpl_vars['whmcs_link']->value;?>
<br />Email: <?php echo $_smarty_tpl->tpl_vars['client_email']->value;?>
<br />Password: <?php echo $_smarty_tpl->tpl_vars['client_password']->value;?>
</p>
<p>To change your password to something more memorable, after logging in go to My Details &gt; Change Password.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
