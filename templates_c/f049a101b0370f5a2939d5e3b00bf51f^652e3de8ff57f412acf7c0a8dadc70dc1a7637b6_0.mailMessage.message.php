<?php
/* Smarty version 3.1.29, created on 2018-08-07 06:49:23
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6940f3563524_99607071',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533624563,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6940f3563524_99607071 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p align="center"><strong>სრულად გაეცანით წერილს და შეინახეთ მისი ასლი</strong></p>
<p>მოხარულები ვართ, რადგან ჩვენ აგვირჩიეთ! თქვენი ჰოსტინგ ანგარიში მზადაა, ეს წერილი კი შეიცავს ყველა საჭირო ინფორმაცია, რომელიც მისი მართვისთვის გამოგადგებათ.</p>
<p>თუკი თქვენ რეგისტრაციისას მოითხოვეთ დომეინი, მხედველობაში მიიღეთ ის, რომ დომეინი ინტერნეტში უცბადვე არ გავრცელდება.  "დომეინის გავრცელების პროცესს" სჭირდება 1-48 საათამდე. სანამ თქვენი დომეინი არ გავრცელდება, მანამ თქვენი საიტი და ელ.ფოსტა არ იფუნქციონირებს. ჩვენ კი მოგაწვდით დროებით მისამართებს, რათა შეძლოთ თქვენი საიტი წინასწარი ხილვა და საჭირო ფაილების ატვირთვა. </p>
<p><strong>ახალი ანგარიშის დეტალები</strong></p>
<p>ჰოსტინგ პაკეტი: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>
<br />დომეინი: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
<br />პირველადი გადახდის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['service_first_payment_amount']->value;?>
<br />პერიოდულად გადასახდელი თანხა: <?php echo $_smarty_tpl->tpl_vars['service_recurring_amount']->value;?>
<br />გადახდის ციკლი: <?php echo $_smarty_tpl->tpl_vars['service_billing_cycle']->value;?>
<br />შემდგომი გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>
</p>
<p><strong>დეტალები შესვლისთვის</strong></p>
<p>Username: <?php echo $_smarty_tpl->tpl_vars['service_username']->value;?>
<br />Password: <?php echo $_smarty_tpl->tpl_vars['service_password']->value;?>
</p>
<p>საკონტროლო პანელის მისამართი: <a href="http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
:2082/">http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
:2082/</a><br />დომეინის მიბმის შემდგომ, შეგიძლიათ გამოიყენოთ შემდეგი მისამართი: <a href="http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
:2082/">http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
:2082/</a></p>
<p><strong>ინფორმაცია სერვერზე</strong></p>
<p>სერვერის დასახელები: <?php echo $_smarty_tpl->tpl_vars['service_server_name']->value;?>
<br />სერვერის IP: <?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
</p>
<p>თუკი თქვენ იყენებთ არსებულ დომეინს თქვენი ახალი ანგარიშისთვის, მოგიწევთ ამ დომეინისთვის nameserver-ების შეცვლა. </p>
<p>Nameserver 1: <?php echo $_smarty_tpl->tpl_vars['service_ns1']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns1_ip']->value;?>
)<br />Nameserver 2: <?php echo $_smarty_tpl->tpl_vars['service_ns2']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns2_ip']->value;?>
)<?php if ($_smarty_tpl->tpl_vars['service_ns3']->value) {?><br />Nameserver 3: <?php echo $_smarty_tpl->tpl_vars['service_ns3']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns3_ip']->value;?>
)<?php }
if ($_smarty_tpl->tpl_vars['service_ns4']->value) {?><br />Nameserver 4: <?php echo $_smarty_tpl->tpl_vars['service_ns4']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns4_ip']->value;?>
)<?php }?></p>
<p><strong>საიტზე ატვირთვა</strong></p>
<p>დროებით, თქვენ შეგიძლიათ გამოიყენოთ ქვემოთ მოცემული მისამართები საიტის სამართავად: </p>
<p>დროებითი FTP Hostname: <?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
<br />დროებითი მისამართი საიტზე წვდომისთვის: <a href="http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
/~<?php echo $_smarty_tpl->tpl_vars['service_username']->value;?>
/">http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
/~<?php echo $_smarty_tpl->tpl_vars['service_username']->value;?>
/</a></p>
<p>დომეინის მიბმის შემდეგ კი შეგიძლიათ გამოიყენოთ ქვემოთ მოცემული მისამართები:</p>
<p>FTP Hostname: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
<br />Webpage URL: <a href="http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
">http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
</a></p>
<p><strong>ელ.ფოსტის პარამეტრები</strong></p>
<p>ელ.ფოსტის ანგარიშების მოსაწყობა, თქვენს საფოსტო პროგრამაში უნდა გამოიყენოთ შემდეგი დასაკავშირებელი მონაცემები: </p>
<p>POP3 Host Address: mail.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
<br />SMTP Host Address: mail.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
<br />Username: ელ.ფოსტა, რომლის შემოწმებაც გსურთ<br />Password: პაროლი, რომელსაც საკონტროლო პანელში იყენებთ</p>
<p>გმადლობთ, რომ აგვირჩიეთ.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
