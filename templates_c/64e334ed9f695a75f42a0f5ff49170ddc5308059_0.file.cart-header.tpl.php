<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:34:20
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/cart-header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63b11cb2c3b0_68462741',
  'file_dependency' => 
  array (
    '64e334ed9f695a75f42a0f5ff49170ddc5308059' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/cart-header.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63b11cb2c3b0_68462741 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['step']->value) {?>
<ul id="stepbar" class="mb-1 nav nav-wizard <?php if (!$_smarty_tpl->tpl_vars['flowcart_hide_domain_step']->value) {?>steps4<?php } else { ?>steps3<?php }?>">
  <li class="step1 <?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?>active<?php } elseif ($_smarty_tpl->tpl_vars['step']->value > 1) {?>done<?php }?>"><a href="cart.php"><?php if ($_smarty_tpl->tpl_vars['step']->value > 1) {?><i class="fa fa-check"></i><?php }?> 1. <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartproduct'];?>
</span><?php if ($_GET['action'] != "confproduct") {?></a><?php }?>
  </li>
  <?php if (!$_smarty_tpl->tpl_vars['flowcart_hide_domain_step']->value) {?>
  <li class="step2 <?php if ($_smarty_tpl->tpl_vars['step']->value == 2) {?>active<?php } elseif ($_smarty_tpl->tpl_vars['step']->value > 2) {?>done<?php }?>">
    <?php if ($_smarty_tpl->tpl_vars['step']->value > 2) {?>
    <i class="fa fa-fw fa-check"></i>
      <?php } else { ?>
      <?php }?> 2. <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartdomainconfig'];?>
</span>
      <?php if ($_smarty_tpl->tpl_vars['step']->value > 2) {?>
    <?php } else { ?>
    <?php }?>
  </li>
  <?php }?>
  <li class="step3<?php if ($_smarty_tpl->tpl_vars['step']->value == 3) {?> active<?php } elseif ($_smarty_tpl->tpl_vars['step']->value > 3) {?>done<?php }?>">
    <?php if ($_smarty_tpl->tpl_vars['step']->value > 3) {?>
  <i class="fa fa-fw fa-check"></i>
      <?php } else { ?>
      <?php }?>
      <?php if (!$_smarty_tpl->tpl_vars['flowcart_hide_domain_step']->value) {?>3. <?php } else { ?>2.<?php }?> <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartsettings'];?>
</span>
      <?php if ($_smarty_tpl->tpl_vars['step']->value > 3) {?>
    <?php } else { ?>
    <?php }?>
  </li>
  <li class="step4<?php if ($_smarty_tpl->tpl_vars['step']->value == 4) {?> active<?php }?>">
<a href="cart.php?a=checkout"><?php if (!$_smarty_tpl->tpl_vars['flowcart_hide_domain_step']->value) {?>4.<?php } else { ?>3.<?php }?> <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartreviewcheckout'];?>
</span></a>
</li>
</ul>
<?php }
}
}
