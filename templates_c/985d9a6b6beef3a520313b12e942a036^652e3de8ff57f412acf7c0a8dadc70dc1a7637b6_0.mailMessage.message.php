<?php
/* Smarty version 3.1.29, created on 2018-08-09 00:00:09
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b840960f604_62475278',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533772809,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b840960f604_62475278 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>This is a billing notice that your invoice no. <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
 which was generated on <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
 is now overdue.</p>
<p>Your payment method is: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>
</p>
<p>Invoice: <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
<br /> Balance Due: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>
<br /> Due Date: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>
</p>
<p>თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
