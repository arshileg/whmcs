<?php
/* Smarty version 3.1.29, created on 2018-08-09 08:48:46
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6bffeeee8114_31505346',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533804526,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6bffeeee8114_31505346 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email. The details of your ticket are shown below.


Subject: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>

 Priority: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>

 Status: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>



You can view the ticket at any time at <?php echo $_smarty_tpl->tpl_vars['ticket_link']->value;?>



<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
