<?php
/* Smarty version 3.1.29, created on 2018-08-12 00:00:09
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6f78896f51e1_11139962',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1534032009,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6f78896f51e1_11139962 ($_smarty_tpl) {
$template = $_smarty_tpl;
?> 


ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ეს გახლავთ რიგით მესამე და საბოლოო შეხსენებითი წერილი: ინვოისი N<?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
, რომელიც დაგენერირდა შემდეგი თარიღით <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
 - საჭიროებს გადახდას. თანხის არ ან ვერ გადახდა გულისხმობს ანგარიშის გათიშვას.


თქვენ მიერ არჩეული გადახდის მეთოდი: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>



ინვოისი: <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>

გადასახადის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>

გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>



თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>



<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
