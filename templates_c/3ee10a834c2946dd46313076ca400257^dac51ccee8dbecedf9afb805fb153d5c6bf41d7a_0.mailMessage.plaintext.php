<?php
/* Smarty version 3.1.29, created on 2018-08-10 06:04:29
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d2aed6f9157_68090445',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533881069,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d2aed6f9157_68090445 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,

თქვენი მოთხოვნისამებრ პაროლი აღდგენილია. მომხმარებლის არეში შესასვლელი მონაცემები მითითებულია ქვევით:


<?php echo $_smarty_tpl->tpl_vars['whmcs_link']->value;?>

ელ.ფოსტა: <?php echo $_smarty_tpl->tpl_vars['client_email']->value;?>

პაროლი: <?php echo $_smarty_tpl->tpl_vars['client_password']->value;?>



იმისათვის, რომ დაგენერირებული პაროლი შეცვალოთ დასამახსოვრებელი ვარიანტით, მომხმარებლის არეში შესვლის შემდეგ - გადადით გვერდზე "ანგარიშის მონაცემების ჩასწორება" > "პაროლის შეცვლა".


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
