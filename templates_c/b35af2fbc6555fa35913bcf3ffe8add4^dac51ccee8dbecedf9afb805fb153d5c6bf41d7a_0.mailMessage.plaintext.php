<?php
/* Smarty version 3.1.29, created on 2018-08-06 09:40:13
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b68177ddae0a4_15366336',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533548413,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b68177ddae0a4_15366336 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო<?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
, 

ეს გახლავთ გადახდის ჩეკი შემდეგი ინვოისისთვის ნომრით # <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
, რომელიც დაგენერირდა შემდეგი თარიღით <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>


 <?php echo $_smarty_tpl->tpl_vars['invoice_html_contents']->value;?>
 

თანხის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_last_payment_amount']->value;?>
 
ტრანზაქციის #: <?php echo $_smarty_tpl->tpl_vars['invoice_last_payment_transid']->value;?>
 
სულ გადახდილია: <?php echo $_smarty_tpl->tpl_vars['invoice_amount_paid']->value;?>
 
დარჩენილი ბალანსი: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>
 
სტატუსი: <?php echo $_smarty_tpl->tpl_vars['invoice_status']->value;?>
 

შეგიძლიათ ნებისმიერ დროს მიმოიხილოთ თქვენი ინვოისები მომხმარებლის არეში შესვლით. 

შენიშვნა: ეს გახლავთ გადახდის დამადასტურებელი ოფიციალური წერილ <?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
