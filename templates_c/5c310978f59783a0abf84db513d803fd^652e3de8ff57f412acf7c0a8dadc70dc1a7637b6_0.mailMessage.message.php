<?php
/* Smarty version 3.1.29, created on 2018-08-11 00:00:10
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6e270aa53d15_20315215',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533945610,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6e270aa53d15_20315215 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ეს გახლავთ რიგით მეორე შეხსენებითი წერილი: ინვოისი N<?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
, რომელიც დაგენერირდა შემდეგი თარიღით <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
 - საჭიროებს გადახდას.</p>
<p>თქვენ მიერ არჩეული გადახდის მეთოდი: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>
</p>
<p>ინვოისი: <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
<br /> გადასახადის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>
<br /> გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>
</p>
<p>თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
