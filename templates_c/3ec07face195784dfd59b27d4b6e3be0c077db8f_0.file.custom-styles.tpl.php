<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:34:20
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/custom-styles.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63b11caa6ca2_48332407',
  'file_dependency' => 
  array (
    '3ec07face195784dfd59b27d4b6e3be0c077db8f' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/custom-styles.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63b11caa6ca2_48332407 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><style type="text/css">
<?php if (isset($_smarty_tpl->tpl_vars['flowcart_primary_color']->value)) {?>
#flow-menu li.group-first .flow-icon, #flow-menu li.group-second .flow-icon,#flow-menu li.group-third .flow-icon,#flow-menu li.group-fourth .flow-icon { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
#stepbar > li.active > a,#stepbar > li.active > span, #flow-menu > li.active > a { background-color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; color: #fff; }
ul.nav-wizard li.active { color:#fff; background: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
ul.nav-wizard li.active:after { border-left:16px solid <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
ul.nav-wizard li.active a,ul.nav-wizard li.active a:active,ul.nav-wizard li.active a:visited,ul.nav-wizard li.active a:focus { color: #3a87ad; background: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
;}
.well-primary,.nav-tabs > li.active > a,.nav-tabs > li.active > a:hover,.nav-tabs > li.active > a:focus { border-top:2px <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
 solid; }
.fa-primary,.btn-link, .btn-link a:hover, .btn-link:hover, .btn-link:focus { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
.btn-default:hover, .btn-default:focus { color: #fff; background-color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; border-color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
;}
.order-row h6, .order-row .order-price, .order-row .cart-features a, .order-row h6 a { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
.well-primary, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border-top-color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }
.loader .fa, .pricing .fa { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_primary_color']->value;?>
; }

<?php }
if (isset($_smarty_tpl->tpl_vars['flowcart_secondary_color']->value)) {?>
.currency-product a { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_secondary_color']->value;?>
; }
input.cycle-1:checked+label.radio i, input.cycle-curr:checked+label.radio i, input.cycle-1:checked+label.radio i { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_secondary_color']->value;?>
; }
.fa-money.active { color: <?php echo $_smarty_tpl->tpl_vars['flowcart_secondary_color']->value;?>
; }
.bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-success, .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-success { background: <?php echo $_smarty_tpl->tpl_vars['flowcart_secondary_color']->value;?>
; }
<?php }
if ($_smarty_tpl->tpl_vars['flowcart_hide_domain_step']->value) {?>
#stepbar > li {
	width: 33.333%;
}
<?php }?>

#flow-menu li.dashboard.flowcart_items_menu, #stepbar li.dashboard.flowcart_items_menu
{
	right: 0 ;
	left: auto;
	z-index: 10000; 
}
#flow-menu > .flowcart_items_menu li .flow-icon 
{
	font-size: 14px;
	color: #333;
	padding-right: 10px;
}
.productGroupfeatures{
	padding-left: 0;
}
.productGroupfeatures li{
	float: left;
	padding-right: 10px;
}
.productGroupfeatures i{
	padding-right: 5px;
}
</style><?php }
}
