<?php
/* Smarty version 3.1.29, created on 2018-08-03 22:52:12
  from "/home/hostnodesnet/public_html/templates/orderforms/boxes/error.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b64dc9c5683b5_17548813',
  'file_dependency' => 
  array (
    '2e15ee2a5af16c217962951103d2c82ae19d29cd' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/boxes/error.tpl',
      1 => 1515132966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b64dc9c5683b5_17548813 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $_smarty_tpl->tpl_vars['carttpl']->value;?>
/style.css" />

<div id="order-boxes">

    <div class="header-lined">
        <h1><?php echo $_smarty_tpl->tpl_vars['errortitle']->value;?>
</h1>
    </div>

    <p><?php echo $_smarty_tpl->tpl_vars['errormsg']->value;?>
</p>

    <p class="text-center">
        <a href="javascript:history.go(-1)" class="btn btn-default">&laquo; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['problemgoback'];?>
</a>
    </p>

</div>
<?php }
}
