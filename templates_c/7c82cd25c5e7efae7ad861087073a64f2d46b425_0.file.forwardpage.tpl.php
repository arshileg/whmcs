<?php
/* Smarty version 3.1.29, created on 2018-02-28 16:02:06
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/forwardpage.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a969a3e0da614_02819726',
  'file_dependency' => 
  array (
    '7c82cd25c5e7efae7ad861087073a64f2d46b425' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/forwardpage.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a969a3e0da614_02819726 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><div class="container">
  <div class="alert alert-block alert-warning textcenter">
    <h4><i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i> <?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h4>
  </div>

  <div id="submitfrm">
    <div class="form-group"><?php echo $_smarty_tpl->tpl_vars['code']->value;?>
</div>
    <form method="post" action="<?php if ($_smarty_tpl->tpl_vars['invoiceid']->value) {?>viewinvoice.php?id=<?php echo $_smarty_tpl->tpl_vars['invoiceid']->value;
} else { ?>clientarea.php<?php }?>">
    </form>
  </div>
</div>


<?php echo '<script'; ?>
 language="javascript">
  setTimeout("autoForward()", 5000);
  function autoForward() {
    var submitForm = jQuery("#submitfrm").find("form:first");
    submitForm.submit();
  }
<?php echo '</script'; ?>
>

<?php }
}
