<?php
/* Smarty version 3.1.29, created on 2018-08-03 17:34:40
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaquotes.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6492302d9366_36032364',
  'file_dependency' => 
  array (
    '17c5e53872453b930987bd23622f5cf35e89fce5' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaquotes.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6492302d9366_36032364 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['quotestitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['quotesintro'],'icon'=>'calculator'), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"QuotesList",'noSortColumns'=>"5",'filterColumn'=>"4"), 0, true);
?>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableQuotesList').removeClass('hidden').DataTable();
        <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'id') {?>
            table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'date') {?>
            table.order(2, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'validuntil') {?>
            table.order(3, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'stage') {?>
            table.order(4, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php }?>
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
<?php echo '</script'; ?>
>
<div class="table-container clearfix">
    <div class="panel panel-default panel-datatable">
    <div class="panel-heading clearfix"> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('sidebar'=>$_smarty_tpl->tpl_vars['primarySidebar']->value), 0, true);
?>
</div>
    <table id="tableQuotesList" class="table table-list hidden">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotenumber'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotesubject'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotedatecreated'];?>
</th>
                <th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotevaliduntil'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotestage'];?>
</th>
                <th>&nbsp;</th>
                <th class="responsive-edit-button" style="display: none;"></th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['quotes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_quote_0_saved_item = isset($_smarty_tpl->tpl_vars['quote']) ? $_smarty_tpl->tpl_vars['quote'] : false;
$_smarty_tpl->tpl_vars['quote'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['quote']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['quote']->value) {
$_smarty_tpl->tpl_vars['quote']->_loop = true;
$__foreach_quote_0_saved_local_item = $_smarty_tpl->tpl_vars['quote'];
?>
                <tr>
                    <td><a href="dl.php?type=q&id=<?php echo $_smarty_tpl->tpl_vars['quote']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['quote']->value['id'];?>
</a>

                    <ul class="cell-inner-list visible-sm visible-xs small">
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotesubject'];?>
</span>: <?php echo $_smarty_tpl->tpl_vars['service']->value['nextduedate'];?>
</li>
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotedatecreated'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['quote']->value['datecreated'];?>
</li>

                    <li class="hidden-sm"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotevaliduntil'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['quote']->value['validuntil'];?>
</li>
                    </ul>

                    </td>

                    <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['quote']->value['subject'];?>
</td>
                    <td class="hidden-sm hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['quote']->value['normalisedDateCreated'];?>
</span><?php echo $_smarty_tpl->tpl_vars['quote']->value['datecreated'];?>
</td>
                    <td class="hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['quote']->value['normalisedValidUntil'];?>
</span><?php echo $_smarty_tpl->tpl_vars['quote']->value['validuntil'];?>
</td>
                    <td><span class="label status status-<?php echo $_smarty_tpl->tpl_vars['quote']->value['stageClass'];?>
"><?php echo $_smarty_tpl->tpl_vars['quote']->value['stage'];?>
</span></td>
                    <td class="text-right">
                        <form method="submit" action="dl.php">
                            <input type="hidden" name="type" value="q" />
                            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['quote']->value['id'];?>
" />
                            <input type="button" class="btn btn-link" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['quoteview'];?>
" onclick="window.location='viewquote.php?id=<?php echo $_smarty_tpl->tpl_vars['quote']->value['id'];?>
'" /> <button type="submit" class="btn btn-link"><i class="fa fa-download"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['quotedownload'];?>
</button>
                        </form>
                    </td>
                    <td class="responsive-edit-button" style="display: none;">
                        <a href="viewquote.php?id=<?php echo $_smarty_tpl->tpl_vars['quote']->value['id'];?>
" class="btn btn-block btn-info">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['manageproduct'];?>

                        </a>
                    </td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['quote'] = $__foreach_quote_0_saved_local_item;
}
if ($__foreach_quote_0_saved_item) {
$_smarty_tpl->tpl_vars['quote'] = $__foreach_quote_0_saved_item;
}
?>
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
	</div>
    </div>
</div>
<?php }
}
