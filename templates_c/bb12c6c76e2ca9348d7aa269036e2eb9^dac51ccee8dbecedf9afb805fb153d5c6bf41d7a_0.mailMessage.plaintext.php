<?php
/* Smarty version 3.1.29, created on 2018-07-03 13:33:27
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b3b42e7d4ad50_77447941',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1530610407,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3b42e7d4ad50_77447941 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>A new cancellation request has been submitted.

Client ID: <?php echo $_smarty_tpl->tpl_vars['client_id']->value;?>

Client Name: <?php echo $_smarty_tpl->tpl_vars['clientname']->value;?>

Service ID: <?php echo $_smarty_tpl->tpl_vars['service_id']->value;?>

Product Name: <?php echo $_smarty_tpl->tpl_vars['product_name']->value;?>

Cancellation Type: <?php echo $_smarty_tpl->tpl_vars['service_cancellation_type']->value;?>

Cancellation Reason: <?php echo $_smarty_tpl->tpl_vars['service_cancellation_reason']->value;?>


<?php echo $_smarty_tpl->tpl_vars['whmcs_admin_link']->value;
}
}
