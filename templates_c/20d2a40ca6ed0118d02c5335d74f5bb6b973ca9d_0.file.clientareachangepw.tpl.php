<?php
/* Smarty version 3.1.29, created on 2018-02-28 16:35:24
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareachangepw.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a96a20c4f5195_06658802',
  'file_dependency' => 
  array (
    '20d2a40ca6ed0118d02c5335d74f5bb6b973ca9d' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareachangepw.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a96a20c4f5195_06658802 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareanavchangepw']), 0, true);
?>


<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/clientareadetailslinks.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<?php if ($_smarty_tpl->tpl_vars['successful']->value) {?>
<div class="alert alert-success">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['changessavedsuccessfully'];?>
</p>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
    <p class="bold"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
    <ul>
        <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

    </ul>
</div>
<?php }?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=changepw" class="form-horizontal p-2">
  <div class="form-group">
    <label for="existingpw" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['existingpassword'];?>
</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="existingpw" id="existingpw" autocomplete="off" />
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['newpassword'];?>
</label>
    <div class="col-sm-10">
      <input type="password" name="newpw" class="form-control" id="password" autocomplete="off" />
    </div>
  </div>

  <div class="form-group">
    <label for="confirmpw" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['confirmnewpassword'];?>
</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="confirmpw" id="confirmpw" autocomplete="off" />
    </div>
  </div>

  <div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pwstrength.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  </div>
</div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" />
    </div>
  </div>
</form>
<?php }
}
