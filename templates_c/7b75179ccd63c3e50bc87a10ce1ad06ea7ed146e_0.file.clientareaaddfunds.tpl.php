<?php
/* Smarty version 3.1.29, created on 2018-08-07 15:10:39
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaaddfunds.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b69b66f5b8247_21912087',
  'file_dependency' => 
  array (
    '7b75179ccd63c3e50bc87a10ce1ad06ea7ed146e' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaaddfunds.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b69b66f5b8247_21912087 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><h3 class="page-header"><span aria-hidden="true" class="icon icon-wallet"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfunds'];?>
 <i class="fa fa-info-circle animated bounce show-info"></i>
</h3>
<blockquote class="page-information hidden">
	<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsintro'];?>
. <?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsdescription'];?>
</p>
</blockquote>
<?php if ($_smarty_tpl->tpl_vars['addfundsdisabled']->value) {?>
<div class="alert alert-danger">
	<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaaddfundsdisabled'];?>
</p>
</div>
<?php } else {
if ($_smarty_tpl->tpl_vars['notallowed']->value || $_smarty_tpl->tpl_vars['errormessage']->value) {?><div class="alert alert-danger">
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
<ul>
	<li><?php if ($_smarty_tpl->tpl_vars['notallowed']->value) {
echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaaddfundsnotallowed'];
} else {
echo $_smarty_tpl->tpl_vars['errormessage']->value;
}?></li>
</ul>
</div><?php }?>
<div class="row px-1 pt-1">
	<div class="col-md-4">
		<div class="info-box  bg-info  text-white">
			<div class="info-icon bg-info-dark">

				<span aria-hidden="true" class="icon icon-arrow-down"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsminimum'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['minimumamount']->value;?>
</span></p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="info-box  bg-warn  text-white">
			<div class="info-icon bg-warn-dark">
				<span aria-hidden="true" class="icon icon-arrow-up"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsmaximum'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['maximumamount']->value;?>
</span></p>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="info-box  bg-inactive  text-white">
			<div class="info-icon bg-inactive-dark">
				<span aria-hidden="true" class="icon icon-chart"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsmaximumbalance'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['maximumbalance']->value;?>
</span></p>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
<div class="col-md-6 col-md-offset-3">
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=addfunds">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label" for="currentbalance"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['statscreditbalance'];?>
</label>
				<input class="form-control" name="currentbalance" type="text" placeholder="<?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['creditbalance'];?>
" readonly>
				<p class="help-block"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsnonrefundable'];?>
</p>
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label" for="amount"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfundsamount'];?>
:</label>
				<input type="text" class="form-control"  name="amount" id="amount" value="<?php echo $_smarty_tpl->tpl_vars['amount']->value;?>
" />
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label" for="paymentmethod"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
:</label>
				<select name="paymentmethod" class="form-control" id="paymentmethod">
					<?php
$_from = $_smarty_tpl->tpl_vars['gateways']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gateway_0_saved_item = isset($_smarty_tpl->tpl_vars['gateway']) ? $_smarty_tpl->tpl_vars['gateway'] : false;
$_smarty_tpl->tpl_vars['gateway'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gateway']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['gateway']->value) {
$_smarty_tpl->tpl_vars['gateway']->_loop = true;
$__foreach_gateway_0_saved_local_item = $_smarty_tpl->tpl_vars['gateway'];
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['gateway']->value['sysname'];?>
"><?php echo $_smarty_tpl->tpl_vars['gateway']->value['name'];?>
</option>
					<?php
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_0_saved_local_item;
}
if ($__foreach_gateway_0_saved_item) {
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_0_saved_item;
}
?>
				</select>
			</div>
		</div>
	</div>
	<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfunds'];?>
" class="btn btn-lg btn-primary btn-block" />
</form>
</div>
</div>
<?php }
}
}
