<?php
/* Smarty version 3.1.29, created on 2018-08-10 06:02:06
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d2a5e4ffc96_55745243',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533880926,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d2a5e4ffc96_55745243 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო<?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>მადლობა <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
ს ანგარიშის შექმნისთვის.</p>
<p>ეწვიეთ ქვემოთ მოცემულ ბმულს და შედით თქვენს ანგარიშში, რათა დაადასტუროთ ელ.ფოსტა და დაასრულოთ რეგისტრაცია.</p>
<p><?php echo $_smarty_tpl->tpl_vars['client_email_verification_link']->value;?>
</p>
<p>ეს წერილი თქვენ გამოგეგზავნათ, რადგან ახლახანს ამ ელ.ფოსტით ანგარიში შეიქმნა ან შეიცვალა. თუკი ეს თქვენ არ გაგიკეთებიათ - დაგვიკავშირდით.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
