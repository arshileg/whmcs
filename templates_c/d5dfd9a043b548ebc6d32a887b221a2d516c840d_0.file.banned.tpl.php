<?php
/* Smarty version 3.1.29, created on 2018-06-28 21:16:42
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/banned.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b3517fa8b8022_26872927',
  'file_dependency' => 
  array (
    'd5dfd9a043b548ebc6d32a887b221a2d516c840d' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/banned.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3517fa8b8022_26872927 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['accessdenied'],'icon'=>'ban'), 0, true);
?>

<div class="alert alert-danger">
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bannedyourip'];?>
 <?php echo $_smarty_tpl->tpl_vars['ip']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['bannedhasbeenbanned'];?>
</p>
    <ul>
        <li><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bannedbanreason'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['reason']->value;?>
</strong></li>
    	<li><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bannedbanexpires'];?>
: <?php echo $_smarty_tpl->tpl_vars['expires']->value;?>
</li>
    </ul>
</div>
<?php }
}
