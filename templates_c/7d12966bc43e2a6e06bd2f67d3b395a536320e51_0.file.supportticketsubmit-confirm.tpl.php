<?php
/* Smarty version 3.1.29, created on 2018-08-04 00:49:55
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-confirm.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b64f83309a1a7_46488477',
  'file_dependency' => 
  array (
    '7d12966bc43e2a6e06bd2f67d3b395a536320e51' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-confirm.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b64f83309a1a7_46488477 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['navopenticket'],'icon'=>'support'), 0, true);
?>

<div class="alert alert-success">
<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketcreated'];?>
 <a href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
">#<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
</a></h4>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketcreateddesc'];?>
</p>
</div>
<?php }
}
