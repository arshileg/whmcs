<?php
/* Smarty version 3.1.29, created on 2018-08-08 10:08:04
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/pwresetvalidation.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6ac1045f5282_02819278',
  'file_dependency' => 
  array (
    '4a1efe48ffd7fe44ac04c0a43492f1b403fc54e1' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/pwresetvalidation.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6ac1045f5282_02819278 ($_smarty_tpl) {
$template = $_smarty_tpl;
?> <div class="row">
  <div class="col-md-4 col-md-offset-4 box">
    <?php if ($_smarty_tpl->tpl_vars['invalidlink']->value) {?>
    <p class="text-danger bg-danger text-alert"><?php echo $_smarty_tpl->tpl_vars['invalidlink']->value;?>
</p>
    <?php } elseif ($_smarty_tpl->tpl_vars['success']->value) {?>
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetvalidationsuccess'];?>
</h4>
    <p class="text-success bg-success"><?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['pwresetsuccessdesc'],'<a href="clientarea.php">','</a>');?>
</p>
    <?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
    <p class="text-danger bg-danger text-alert"><strong><span aria-hidden="true" class="icon icon-ban"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['warning'];?>
</strong></p><br>
    <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

    <?php }?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=pwreset">
      <input type="hidden" name="key" id="key" value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" />
      <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetenternewpw'];?>
</h5>
      <div class="form-group">
        <div class="input-group input-group-lg">
          <span class="input-group-addon"><span aria-hidden="true" class="icon icon-lock"></span></span>
          <input type="password" class="form-control" name="newpw" id="password" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['newpassword'];?>
" autocomplete="off" />
        </div>
      </div>
      <div class="form-group">
        <div class="input-group input-group-lg">
          <span class="input-group-addon"><span aria-hidden="true" class="icon icon-lock"></span></span>
          <input type="password" class="form-control" name="confirmpw" id="confirmpw" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['confirmnewpassword'];?>
" autocomplete="off" />
        </div>
      </div>
      <div class="form-group">
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pwstrength.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

      </div>
      <div class="form-group">
        <input class="btn btn-primary btn-block" type="submit" name="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" />
        <input class="btn btn-default btn-block" type="reset" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cancel'];?>
" />
      </div>
  </form>
  <?php }?>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?>
<div class="languageblock">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/languageblock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php }
}
}
