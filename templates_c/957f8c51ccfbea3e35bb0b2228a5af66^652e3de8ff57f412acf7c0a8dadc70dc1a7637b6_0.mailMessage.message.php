<?php
/* Smarty version 3.1.29, created on 2018-08-11 00:00:21
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6e2715b4ebb2_03575169',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533945621,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6e2715b4ebb2_03575169 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>თქვენი "დახმარების ბილეთი" დაიხურა.</p>
<p>დიდად დავაფასებთ თუკი მცირე დროს დაგვითმობთ და შეაფასებთ გაწეულ მომსახურეობას: </p>
<p><a href="<?php echo $_smarty_tpl->tpl_vars['ticket_url']->value;?>
&amp;feedback=1"><?php echo $_smarty_tpl->tpl_vars['ticket_url']->value;?>
&amp;feedback=1</a></p>
<p>ჩვენ ვზრუნავთ ხარისხის გაუმჯობესებაზე, ასე რომ, ველით თქვენს რჩევებს.</p>
<p>გმადლობთ, რომ სარგებლობთ ჩვენი სერვისით.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
