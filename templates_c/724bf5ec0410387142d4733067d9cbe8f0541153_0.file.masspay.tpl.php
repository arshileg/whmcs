<?php
/* Smarty version 3.1.29, created on 2018-08-05 14:22:58
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/masspay.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b670842c90696_59929905',
  'file_dependency' => 
  array (
    '724bf5ec0410387142d4733067d9cbe8f0541153' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/masspay.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b670842c90696_59929905 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['masspaytitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['masspayintro']), 0, true);
?>

<form method="post" action="clientarea.php?action=masspay">
    <input type="hidden" name="geninvoice" value="true" />
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdescription'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesamount'];?>
</th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['invoiceitems']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_invoiceitem_0_saved_item = isset($_smarty_tpl->tpl_vars['invoiceitem']) ? $_smarty_tpl->tpl_vars['invoiceitem'] : false;
$__foreach_invoiceitem_0_saved_key = isset($_smarty_tpl->tpl_vars['invid']) ? $_smarty_tpl->tpl_vars['invid'] : false;
$_smarty_tpl->tpl_vars['invoiceitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['invid'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['invoiceitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['invid']->value => $_smarty_tpl->tpl_vars['invoiceitem']->value) {
$_smarty_tpl->tpl_vars['invoiceitem']->_loop = true;
$__foreach_invoiceitem_0_saved_local_item = $_smarty_tpl->tpl_vars['invoiceitem'];
?>
            <tr>
                <td colspan="2">
                    <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicenumber'];?>
 <?php echo $_smarty_tpl->tpl_vars['invid']->value;?>
</strong>
                    <input type="hidden" name="invoiceids[]" value="<?php echo $_smarty_tpl->tpl_vars['invid']->value;?>
" />
                </td>
            </tr>
            <?php
$_from = $_smarty_tpl->tpl_vars['invoiceitem']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_1_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_1_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['item']->value['description'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['item']->value['amount'];?>
</td>
            </tr>
            <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_local_item;
}
if ($__foreach_item_1_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_1_saved_item;
}
?>
            <?php
$_smarty_tpl->tpl_vars['invoiceitem'] = $__foreach_invoiceitem_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['invoiceitem']->_loop) {
?>
            <tr>
                <td colspan="6"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['norecordsfound'];?>
</td>
            </tr>
            <?php
}
if ($__foreach_invoiceitem_0_saved_item) {
$_smarty_tpl->tpl_vars['invoiceitem'] = $__foreach_invoiceitem_0_saved_item;
}
if ($__foreach_invoiceitem_0_saved_key) {
$_smarty_tpl->tpl_vars['invid'] = $__foreach_invoiceitem_0_saved_key;
}
?>
            <tr class="subtotal">
                <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicessubtotal'];?>
:</td>
                <td><?php echo $_smarty_tpl->tpl_vars['subtotal']->value;?>
</td>
            </tr>
            <?php if ($_smarty_tpl->tpl_vars['tax']->value) {?><tr>
            <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestax'];?>
:</td>
            <td><?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</td>
        </tr><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['tax2']->value) {?><tr>
        <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestax'];?>
 2:</td>
        <td><?php echo $_smarty_tpl->tpl_vars['tax2']->value;?>
</td>
    </tr><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['credit']->value) {?><tr>
    <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicescredit'];?>
:</td>
    <td><?php echo $_smarty_tpl->tpl_vars['credit']->value;?>
</td>
</tr><?php }
if ($_smarty_tpl->tpl_vars['partialpayments']->value) {?><tr>
<td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicespartialpayments'];?>
:</td>
<td><?php echo $_smarty_tpl->tpl_vars['partialpayments']->value;?>
</td>
</tr><?php }?>
<tr>
    <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestotaldue'];?>
:</td>
    <td><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td>
</tr>
</tbody>
</table>
<div class="row">
<div class="col-md-6 col-md-offset-3">
<h3><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
</h3>
<div class="well">
<div class="form-group">
<?php
$_from = $_smarty_tpl->tpl_vars['gateways']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gateway_2_saved_item = isset($_smarty_tpl->tpl_vars['gateway']) ? $_smarty_tpl->tpl_vars['gateway'] : false;
$__foreach_gateway_2_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['gateway'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gateway']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['gateway']->value) {
$_smarty_tpl->tpl_vars['gateway']->_loop = true;
$__foreach_gateway_2_saved_local_item = $_smarty_tpl->tpl_vars['gateway'];
?>
<div class="radio-inline">
    <label>
        <input type="radio" class="radio inline" name="paymentmethod" value="<?php echo $_smarty_tpl->tpl_vars['gateway']->value['sysname'];?>
"<?php if ($_smarty_tpl->tpl_vars['gateway']->value['sysname'] == $_smarty_tpl->tpl_vars['defaultgateway']->value) {?> checked<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['gateway']->value['name'];?>

    </label>
</div>
<?php
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_2_saved_local_item;
}
if ($__foreach_gateway_2_saved_item) {
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_2_saved_item;
}
if ($__foreach_gateway_2_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_gateway_2_saved_key;
}
?>
</div>
<div class="form-group">
<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['masspaymakepayment'];?>
" class="btn btn-success btn-lg btn-block" />
</div>
</div>
</div>
</div>
</form>
<?php }
}
