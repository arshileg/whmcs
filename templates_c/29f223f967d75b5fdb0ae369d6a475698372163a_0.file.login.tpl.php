<?php
/* Smarty version 3.1.29, created on 2018-08-03 04:41:52
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63dd106a90a3_08192456',
  'file_dependency' => 
  array (
    '29f223f967d75b5fdb0ae369d6a475698372163a' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/login.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63dd106a90a3_08192456 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 0) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['login'],'icon'=>'login'), 0, true);
?>

<?php }?>
 <div class="row py-1">
  <div class="col-md-4 col-md-offset-4 box">
    <div class="content-wrap">
      <?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?><div class="text-right py-2"><a class="text-uppercase" href="register.php"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['signup'];?>
</a></div><?php }?>
      <p class="text-danger bg-danger text-alert <?php if (!$_smarty_tpl->tpl_vars['incorrect']->value) {?> displaynone<?php }?>" id="login-error"><strong><span aria-hidden="true" class="icon icon-ban"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['warning'];?>
</strong><br/><?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginincorrect'];?>
</p>
       <div class="providerLinkingFeedback"></div>
      <?php if ($_smarty_tpl->tpl_vars['verificationId']->value && empty($_smarty_tpl->tpl_vars['transientDataName']->value)) {?>
      <p class="text-danger bg-danger"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['verificationKeyExpired'];?>
</p>
      <?php } elseif ($_smarty_tpl->tpl_vars['ssoredirect']->value) {?>
      <p class="text-info bg-info"><i class="fa fa-external-link-square"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['sso']['redirectafterlogin'];?>
</p>
      <?php }?>
      <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['systemurl']->value;?>
dologin.php">
        <div class="form-group">
          <div class="input-group input-group-lg">
            <span class="input-group-addon"><span aria-hidden="true" class="icon icon-envelope"></span></span>
            <input class="form-control" name="username" type="text" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginemail'];?>
">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group input-group-lg">
            <span class="input-group-addon"><span aria-hidden="true" class="icon icon-lock"></span></span>
            <input class="form-control" name="password" type="password" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginpassword'];?>
" autocomplete="off">
          </div>
        </div>
        <a href="pwreset.php" class="forgot"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginforgotteninstructions'];?>
</a>
       <div class="form-group text-left">
       <label class="checkbox-inline">
         <input type="checkbox" name="rememberme" id="rememberme"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginrememberme'];?>

       </label>
     </div>
        <div class="row">
         <div class="col-md-12">
          <div class="form-group"><input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginbutton'];?>
"></div>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="col-md-4 col-md-offset-4 box box-inverse <?php if (!$_smarty_tpl->tpl_vars['linkableProviders']->value) {?> hidden<?php }?>">
  <div class="content-wrap">
  <h6 class="py-1 m-0"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['signinwith'];?>
</h6>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/linkedaccounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('linkContext'=>"login",'customFeedback'=>true), 0, true);
?>

</div>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?>
<div class="languageblock">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/languageblock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php }
}
}
