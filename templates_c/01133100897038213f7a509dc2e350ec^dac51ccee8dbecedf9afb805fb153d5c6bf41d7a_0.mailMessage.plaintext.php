<?php
/* Smarty version 3.1.29, created on 2018-08-06 09:45:35
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6818bf495197_81829435',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533548735,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6818bf495197_81829435 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,

Your email to our support system could not be accepted because it was not recognized as coming from an email address belonging to one of our customers.  If you need assistance, please email from the address you registered with us that you use to login to our client area.

<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
