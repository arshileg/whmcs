<?php
/* Smarty version 3.1.29, created on 2018-05-24 14:39:11
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b06964f6f8284_61543473',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1527158351,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b06964f6f8284_61543473 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
, 

Thank you for your domain transfer order. Your order has been received and we have now initiated the transfer process. The details of the domain purchase are below: 

Domain: <?php echo $_smarty_tpl->tpl_vars['domain_name']->value;?>

Registration Length: <?php echo $_smarty_tpl->tpl_vars['domain_reg_period']->value;?>

Transfer Price: <?php echo $_smarty_tpl->tpl_vars['domain_first_payment_amount']->value;?>

Renewal Price: <?php echo $_smarty_tpl->tpl_vars['domain_recurring_amount']->value;?>

Next Due Date: <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>
 

You may login to your client area at <?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
