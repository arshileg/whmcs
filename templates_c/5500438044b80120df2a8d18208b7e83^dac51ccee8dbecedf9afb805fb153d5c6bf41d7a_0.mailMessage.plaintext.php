<?php
/* Smarty version 3.1.29, created on 2018-08-09 08:48:47
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6bffef2f3644_38420309',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533804527,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6bffef2f3644_38420309 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['newTicket']->value) {?>
    Ticket #<?php echo $_smarty_tpl->tpl_vars['whmcs_admin_url']->value;?>
supporttickets.php?action=viewticket&id=<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>



    
        Client: <?php echo $_smarty_tpl->tpl_vars['client_name']->value;
if ($_smarty_tpl->tpl_vars['client_id']->value) {?> #<?php echo $_smarty_tpl->tpl_vars['client_id']->value;
}?>
        Department: <?php echo $_smarty_tpl->tpl_vars['ticket_department']->value;?>

        Subject: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>

        Priority: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>

    


    
        <?php echo $_smarty_tpl->tpl_vars['newTicket']->value;?>

    
<?php } else { ?>
    Ticket #<?php echo $_smarty_tpl->tpl_vars['whmcs_admin_url']->value;?>
supporttickets.php?action=viewticket&id=<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>




    <?php if ($_smarty_tpl->tpl_vars['changes']->value) {?>
        
            
                <?php
$_from = $_smarty_tpl->tpl_vars['changes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_change_0_saved_item = isset($_smarty_tpl->tpl_vars['change']) ? $_smarty_tpl->tpl_vars['change'] : false;
$_smarty_tpl->tpl_vars['change'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['change']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['change']->key => $_smarty_tpl->tpl_vars['change']->value) {
$_smarty_tpl->tpl_vars['change']->_loop = true;
$__foreach_change_0_saved_local_item = $_smarty_tpl->tpl_vars['change'];
?>
                    
                        <?php echo $_smarty_tpl->tpl_vars['change']->key;?>
:
                        
                            <?php echo $_smarty_tpl->tpl_vars['change']->value['old'];?>

                             
                            <?php echo $_smarty_tpl->tpl_vars['change']->value['new'];?>

                        
                    
                <?php
$_smarty_tpl->tpl_vars['change'] = $__foreach_change_0_saved_local_item;
}
if ($__foreach_change_0_saved_item) {
$_smarty_tpl->tpl_vars['change'] = $__foreach_change_0_saved_item;
}
?>
            
        
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['newReply']->value) {?>
        
            <?php echo $_smarty_tpl->tpl_vars['newReply']->value;?>

        
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['newNote']->value) {?>
        
            <?php echo $_smarty_tpl->tpl_vars['newNote']->value;?>

        
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['newAttachments']->value) {
echo $_smarty_tpl->tpl_vars['newAttachments']->value;
}
}?>

    You can respond to this ticket by simply replying to this email or through the admin area at the url below.

    <?php echo $_smarty_tpl->tpl_vars['whmcs_admin_url']->value;?>
supporttickets.php?action=viewticket&id=<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>

        <?php echo $_smarty_tpl->tpl_vars['whmcs_admin_url']->value;?>
supporttickets.php?action=viewticket&id=<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;
}
}
