<?php
/* Smarty version 3.1.29, created on 2018-08-08 18:06:37
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b312d268e01_85155414',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533751597,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b312d268e01_85155414 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,

We have now removed your email address from our mailing list.

If this was a mistake or you change your mind, you can re-subscribe at any time from the My Details section of our client area.

<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/clientarea.php?action=details

<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
