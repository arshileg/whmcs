<?php
/* Smarty version 3.1.29, created on 2018-05-24 14:39:11
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b06964f6f19a4_76504768',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1527158351,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b06964f6f19a4_76504768 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
, </p><p>Thank you for your domain transfer order. Your order has been received and we have now initiated the transfer process. The details of the domain purchase are below: </p><p>Domain: <?php echo $_smarty_tpl->tpl_vars['domain_name']->value;?>
<br />Registration Length: <?php echo $_smarty_tpl->tpl_vars['domain_reg_period']->value;?>
<br />Transfer Price: <?php echo $_smarty_tpl->tpl_vars['domain_first_payment_amount']->value;?>
<br />Renewal Price: <?php echo $_smarty_tpl->tpl_vars['domain_recurring_amount']->value;?>
<br />Next Due Date: <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>
 </p><p>You may login to your client area at <a href="<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
</a> to manage your domain. </p><p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
 </p><?php }
}
