<?php
/* Smarty version 3.1.29, created on 2018-08-03 02:43:45
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/knowledgebase.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63c161ed1608_87886682',
  'file_dependency' => 
  array (
    'ec982c7d151b1d8ed37670e86f01273b4e4b5cba' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/knowledgebase.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63c161ed1608_87886682 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/home/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasetitle'],'icon'=>'notebook'), 0, true);
?>

<form role="form" method="post" action="<?php echo routePath('knowledgebase-search');?>
">
  <div class="input-group kb-search p-1">
    <input type="text" id="inputKnowledgebaseSearch" name="search" class="form-control" placeholder="What can we help you with?" />
    <span class="input-group-btn">
      <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
    </span>
  </div>
</form>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasecategories']), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['kbcats']->value) {?>
<div class="row px-1">
  <?php
$_from = $_smarty_tpl->tpl_vars['kbcats']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbcats_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbcats']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbcats'] : false;
$__foreach_kbcats_0_saved_item = isset($_smarty_tpl->tpl_vars['kbcat']) ? $_smarty_tpl->tpl_vars['kbcat'] : false;
$_smarty_tpl->tpl_vars['kbcat'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_kbcats'] = new Smarty_Variable(array('iteration' => 0));
$_smarty_tpl->tpl_vars['kbcat']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['kbcat']->value) {
$_smarty_tpl->tpl_vars['kbcat']->_loop = true;
$_smarty_tpl->tpl_vars['__smarty_foreach_kbcats']->value['iteration']++;
$__foreach_kbcats_0_saved_local_item = $_smarty_tpl->tpl_vars['kbcat'];
?>
  <div class="col-md-4">
    <h4><a href="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbcat']->value['id'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbcat']->value['urlfriendlyname'];
$_tmp2=ob_get_clean();
echo routePath('knowledgebase-category-view',$_tmp1,$_tmp2);?>
">
      <span class="glyphicon glyphicon-folder-close"></span>
      <?php echo $_smarty_tpl->tpl_vars['kbcat']->value['name'];?>
 <span class="badge"><?php echo $_smarty_tpl->tpl_vars['kbcat']->value['numarticles'];?>
</span>
    </a></h4>
    <p><?php echo $_smarty_tpl->tpl_vars['kbcat']->value['description'];?>
</p>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbcats']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbcats']->value['iteration'] : null) % 3 == 0) {?>
</div><div class="row px-1">
  <?php }?>
  <?php
$_smarty_tpl->tpl_vars['kbcat'] = $__foreach_kbcats_0_saved_local_item;
}
if ($__foreach_kbcats_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_kbcats'] = $__foreach_kbcats_0_saved;
}
if ($__foreach_kbcats_0_saved_item) {
$_smarty_tpl->tpl_vars['kbcat'] = $__foreach_kbcats_0_saved_item;
}
?>
</div>
<?php } else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasenoarticles'],'textcenter'=>true), 0, true);
?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['kbmostviews']->value) {?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasepopular']), 0, true);
?>

<?php
$_from = $_smarty_tpl->tpl_vars['kbmostviews']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbmostviews_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews'] : false;
$__foreach_kbmostviews_1_saved_item = isset($_smarty_tpl->tpl_vars['kbarticle']) ? $_smarty_tpl->tpl_vars['kbarticle'] : false;
$__foreach_kbmostviews_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['kbarticle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews'] = new Smarty_Variable(array('iteration' => 0));
$__foreach_kbmostviews_1_iteration=0;
$_smarty_tpl->tpl_vars['kbarticle']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['kbarticle']->value) {
$_smarty_tpl->tpl_vars['kbarticle']->_loop = true;
$__foreach_kbmostviews_1_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['last'] = $__foreach_kbmostviews_1_iteration == $__foreach_kbmostviews_1_total;
$__foreach_kbmostviews_1_saved_local_item = $_smarty_tpl->tpl_vars['kbarticle'];
if ((1 & (isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['iteration'] : null))) {?>
<div class="row row-eq-height px-1">
  <?php }?>
    <div class="col-md-6">
      <div class="well well-sm">
        <h4><a href="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['id'];
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['urlfriendlytitle'];
$_tmp4=ob_get_clean();
echo routePath('knowledgebase-article-view',$_tmp3,$_tmp4);?>
">
          <span class="glyphicon glyphicon-file"></span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];?>

        </a></h4>
        <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['kbarticle']->value['article'],240,"...");?>
</p>
      </div>
    </div>
<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['iteration'] : null) % 2 == 0 || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews']->value['last'] : null)) {?>
</div>
<?php }
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbmostviews_1_saved_local_item;
}
if ($__foreach_kbmostviews_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_kbmostviews'] = $__foreach_kbmostviews_1_saved;
}
if ($__foreach_kbmostviews_1_saved_item) {
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbmostviews_1_saved_item;
}
}
}
}
