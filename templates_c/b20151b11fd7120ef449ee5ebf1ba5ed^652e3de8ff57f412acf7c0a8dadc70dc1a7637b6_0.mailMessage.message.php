<?php
/* Smarty version 3.1.29, created on 2018-08-01 00:00:44
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b60bfec05c719_39202804',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533067244,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b60bfec05c719_39202804 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ეს არის თქვენი რეფერალური სისტემის თვიური ანგარიში. თქვენ  ნებისმიერ დროს  შეგიძლიათ იხილოთ რეფერალები სტატისტიკა მომხმარებლის არეში შესვლით. </p>
<p>რეფერალურ ბმულზე გადასული ვიზიტორები: <?php echo $_smarty_tpl->tpl_vars['affiliate_total_visits']->value;?>
<br /> ამჟამად გამომუშავებული: <?php echo $_smarty_tpl->tpl_vars['affiliate_balance']->value;?>
<br /> გამოტანილი თანხის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['affiliate_withdrawn']->value;?>
</p>
<p><strong>თვის ახალი რეფერალები</strong></p>
<p><?php echo $_smarty_tpl->tpl_vars['affiliate_referrals_table']->value;?>
</p>
<p>გახსოვდეთ, თქვენ შეგიძლიათ ახალი კლიენტების მოზიდვა შემდეგი რეფერალური ლინკით: <?php echo $_smarty_tpl->tpl_vars['affiliate_referral_url']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
