<?php
/* Smarty version 3.1.29, created on 2018-08-12 00:00:19
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6f7893794eb2_77586658',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1534032019,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6f7893794eb2_77586658 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


This is a notification to let you know that we are changing the status of your ticket #<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>
 to Closed as we have not received a response from you in over <?php echo $_smarty_tpl->tpl_vars['ticket_auto_close_time']->value;?>
 hours.


Subject: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>

Department: <?php echo $_smarty_tpl->tpl_vars['ticket_department']->value;?>

Priority: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>

Status: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>



If you have any further questions then please just reply to re-open the ticket.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
