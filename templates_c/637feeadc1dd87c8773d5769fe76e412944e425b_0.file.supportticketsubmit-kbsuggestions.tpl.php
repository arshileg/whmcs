<?php
/* Smarty version 3.1.29, created on 2018-08-04 00:48:55
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-kbsuggestions.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b64f7f70c66f9_78267742',
  'file_dependency' => 
  array (
    '637feeadc1dd87c8773d5769fe76e412944e425b' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-kbsuggestions.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b64f7f70c66f9_78267742 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><h4><span class="glyphicon glyphicon-warning-sign text-danger"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['kbsuggestions'];?>
</h4>
<p><small><?php echo $_smarty_tpl->tpl_vars['LANG']->value['kbsuggestionsexplanation'];?>
</small></p>
<p><?php
$_from = $_smarty_tpl->tpl_vars['kbarticles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbarticle_0_saved_item = isset($_smarty_tpl->tpl_vars['kbarticle']) ? $_smarty_tpl->tpl_vars['kbarticle'] : false;
$_smarty_tpl->tpl_vars['kbarticle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['kbarticle']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['kbarticle']->value) {
$_smarty_tpl->tpl_vars['kbarticle']->_loop = true;
$__foreach_kbarticle_0_saved_local_item = $_smarty_tpl->tpl_vars['kbarticle'];
?>
<span class="glyphicon glyphicon-book"></span> <a href="knowledgebase.php?action=displayarticle&id=<?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['id'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];?>
</a> - <?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['article'];?>
...<br>
<?php
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticle_0_saved_local_item;
}
if ($__foreach_kbarticle_0_saved_item) {
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticle_0_saved_item;
}
?></p><?php }
}
