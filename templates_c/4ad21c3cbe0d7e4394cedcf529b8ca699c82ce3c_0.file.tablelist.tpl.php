<?php
/* Smarty version 3.1.29, created on 2018-02-18 05:07:45
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/includes/tablelist.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a88d1e121e1a8_93545739',
  'file_dependency' => 
  array (
    '4ad21c3cbe0d7e4394cedcf529b8ca699c82ce3c' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/includes/tablelist.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a88d1e121e1a8_93545739 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/dataTables.bootstrap.css">
<?php echo '<script'; ?>
 type="text/javascript" charset="utf8" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" charset="utf8" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
jQuery(document).ready( function () {
    var table = jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
").DataTable({
        "dom": '<"listtable"fit>pl',<?php if (isset($_smarty_tpl->tpl_vars['noPagination']->value) && $_smarty_tpl->tpl_vars['noPagination']->value) {?>
        "paging": false,<?php }
if (isset($_smarty_tpl->tpl_vars['noInfo']->value) && $_smarty_tpl->tpl_vars['noInfo']->value) {?>
        "info": false,<?php }
if (isset($_smarty_tpl->tpl_vars['noSearch']->value) && $_smarty_tpl->tpl_vars['noSearch']->value) {?>
        "filter": false,<?php }?>
        "bInfo": false,
        "oLanguage": {
            "sEmptyTable":     "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['norecordsfound'];?>
",
            "sInfo":           "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableshowing'];?>
",
            "sInfoEmpty":      "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableempty'];?>
",
            "sInfoFiltered":   "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablefiltered'];?>
",
            "sInfoPostFix":    "",
            "sInfoThousands":  ",",
            "sLengthMenu":     "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablelength'];?>
",
            "sLoadingRecords": "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableloading'];?>
",
            "sProcessing":     "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableprocessing'];?>
",
            "sSearch":         "",
            "sZeroRecords":    "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['norecordsfound'];?>
",
            "oPaginate": {
                "sFirst":    "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablepagesfirst'];?>
",
                "sLast":     "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablepageslast'];?>
",
                "sNext":     "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablepagesnext'];?>
",
                "sPrevious": "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tablepagesprevious'];?>
"
            }
        },
        "pageLength": 10,
        "order": [
            [ <?php if (isset($_smarty_tpl->tpl_vars['startOrderCol']->value) && $_smarty_tpl->tpl_vars['startOrderCol']->value) {
echo $_smarty_tpl->tpl_vars['startOrderCol']->value;
} else { ?>0<?php }?>, "asc" ]
        ],
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableviewall'];?>
"]
        ],
        "aoColumnDefs": [
            {
                "bSortable": false,
                "aTargets": [ <?php if (isset($_smarty_tpl->tpl_vars['noSortColumns']->value) && $_smarty_tpl->tpl_vars['noSortColumns']->value !== '') {
echo $_smarty_tpl->tpl_vars['noSortColumns']->value;
}?> ]
            },
            {
                "sType": "string",
                "aTargets": [ <?php if (isset($_smarty_tpl->tpl_vars['filterColumn']->value) && $_smarty_tpl->tpl_vars['filterColumn']->value) {
echo $_smarty_tpl->tpl_vars['filterColumn']->value;
}?> ]
            }
        ],
        "stateSave": true
    });
    jQuery(".dataTables_filter input").attr("placeholder", "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['tableentersearchterm'];?>
");

    <?php if (isset($_smarty_tpl->tpl_vars['filterColumn']->value) && isset($_smarty_tpl->tpl_vars['filterColumn']->value)) {?>
    // highlight remembered filter on page re-load
    var rememberedFilterTerm = table.state().columns[<?php echo $_smarty_tpl->tpl_vars['filterColumn']->value;?>
].search.search;
    if (rememberedFilterTerm) {
        jQuery(".view-filter-btns a span").each(function(index) {
            if (jQuery(this).text().trim() == rememberedFilterTerm) {
                jQuery(this).parent('a').addClass('active');
                jQuery(this).parent('a').find('i').addClass('fa-dot-circle-o').removeClass('fa-circle-o');
            }
        });
    }
    <?php }?>



jQuery( "<div class=\"panel-footer\">" ).insertAfter( jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .listtable") );

jQuery("<div class=\"clearfix\">").prependTo("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .panel-footer");
jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .dataTables_length").prependTo("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .panel-footer");
jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .dataTables_paginate").prependTo("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .panel-footer");

jQuery(jQuery('<div id='+"#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_tmp"+'>').append(jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .dataTables_filter").clone()).html())
            .insertAfter( jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper").parents('div.panel').find(".elemetsholder")   );
jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .dataTables_filter").hide();
jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_wrapper .dataTables_filter").attr('id',"table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter_h");

jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter_h input").attr('id',"table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter_hi");

    var filter = document.getElementById("table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter_hi");

    jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter input").keyup(function() {
        filter.value = this.value;
        jQuery(filter).change();
        jQuery(filter).keyup();
    });

jQuery("#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter input").addClass('input_search');

<?php if (isset($_smarty_tpl->tpl_vars['filterColumn']->value) && isset($_smarty_tpl->tpl_vars['filterColumn']->value)) {?>



    jQuery(jQuery('<div id='+"#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_filter"+'>').append(jQuery('.filter_top').clone()).html()).insertAfter( "#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
_info" );
    jQuery('.filter_top:last').show();

    jQuery(".view-filter-btns a").click(function() {
    var filterValue = jQuery(this).find("span").html().trim();
    var dataTable = jQuery('#table<?php echo $_smarty_tpl->tpl_vars['tableName']->value;?>
').DataTable();
    if (jQuery(this).hasClass('active')) {
        <?php if (!isset($_smarty_tpl->tpl_vars['dontControlActiveClass']->value) || !$_smarty_tpl->tpl_vars['dontControlActiveClass']->value) {?>
            jQuery(this).removeClass('active');
            jQuery(this).find(jQuery("i.fa.fa-dot-circle-o")).addClass('fa-circle-o').removeClass('fa-dot-circle-o');
        <?php }?>
        dataTable.column(<?php echo $_smarty_tpl->tpl_vars['filterColumn']->value;?>
).search('').draw();
    } else {
        <?php if (!isset($_smarty_tpl->tpl_vars['dontControlActiveClass']->value) || !$_smarty_tpl->tpl_vars['dontControlActiveClass']->value) {?>
            jQuery('.view-filter-btns .list-group-item').removeClass('active');
            jQuery('.view-filter-btns a').removeClass('active');
            jQuery('i.fa.fa-dot-circle-o').addClass('fa-circle-o').removeClass('fa-dot-circle-o');
            jQuery(this).addClass('active');
            jQuery(this).find(jQuery("i.fa.fa-circle-o")).addClass('fa-dot-circle-o').removeClass('fa-circle-o');
        <?php }?>
        dataTable.column(<?php echo $_smarty_tpl->tpl_vars['filterColumn']->value;?>
).search(filterValue, false, false, false).draw();
    }
});


<?php }?>


} );

<?php echo '</script'; ?>
>
<?php }
}
