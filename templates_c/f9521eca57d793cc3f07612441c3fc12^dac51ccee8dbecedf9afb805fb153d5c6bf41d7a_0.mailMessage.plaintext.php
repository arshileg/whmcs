<?php
/* Smarty version 3.1.29, created on 2018-08-10 05:48:46
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d273e400ba8_17066000',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533880126,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d273e400ba8_17066000 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ეს შეხსენებითი შეტყობინება ეხება შემდეგ თარიღით დაგენერირებულ ინვოისს:  <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
.


თქვენი არჩეული გადახდის მეთოდია: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>



ინვოისი #<?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>

 გადასახადის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_total']->value;?>

 გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>



ინვოისის პროდუქტები


<?php echo $_smarty_tpl->tpl_vars['invoice_html_contents']->value;?>
 
 ------------------------------------------------------


თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>



<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
