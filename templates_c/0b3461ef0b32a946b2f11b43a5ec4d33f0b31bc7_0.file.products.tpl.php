<?php
/* Smarty version 3.1.29, created on 2018-02-18 05:03:05
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/products.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a88d0c99439f8_42507884',
  'file_dependency' => 
  array (
    '0b3461ef0b32a946b2f11b43a5ec4d33f0b31bc7' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/products.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/products-header.tpl' => 1,
  ),
),false)) {
function content_5a88d0c99439f8_42507884 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
if (!is_callable('smarty_modifier_regex_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.regex_replace.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/products-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<?php if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "6"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("6", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
if (isset($_smarty_tpl->tpl_vars["itemsprow_c"])) {$_smarty_tpl->tpl_vars["itemsprow_c"] = clone $_smarty_tpl->tpl_vars["itemsprow_c"];
$_smarty_tpl->tpl_vars["itemsprow_c"]->value = 0; $_smarty_tpl->tpl_vars["itemsprow_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow_c"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow_c", 0);
if (isset($_smarty_tpl->tpl_vars["col"])) {$_smarty_tpl->tpl_vars["col"] = clone $_smarty_tpl->tpl_vars["col"];
$_smarty_tpl->tpl_vars["col"]->value = flowcart_group_column_width($_smarty_tpl->tpl_vars['gid']->value,$_smarty_tpl->tpl_vars['group_column_width']->value); $_smarty_tpl->tpl_vars["col"]->nocache = null;
} else $_smarty_tpl->tpl_vars["col"] = new Smarty_Variable(flowcart_group_column_width($_smarty_tpl->tpl_vars['gid']->value,$_smarty_tpl->tpl_vars['group_column_width']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "col", 0);
if ($_smarty_tpl->tpl_vars['col']->value == 'col-md-15') {
if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "5"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("5", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
} elseif ($_smarty_tpl->tpl_vars['col']->value == 'col-md-3') {
if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "4"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("4", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
} elseif ($_smarty_tpl->tpl_vars['col']->value == 'col-md-4') {
if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "3"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("3", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
} elseif ($_smarty_tpl->tpl_vars['col']->value == 'col-md-6') {
if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "2"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("2", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
} elseif ($_smarty_tpl->tpl_vars['col']->value == 'col-md-12') {
if (isset($_smarty_tpl->tpl_vars["itemsprow"])) {$_smarty_tpl->tpl_vars["itemsprow"] = clone $_smarty_tpl->tpl_vars["itemsprow"];
$_smarty_tpl->tpl_vars["itemsprow"]->value = "1"; $_smarty_tpl->tpl_vars["itemsprow"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow"] = new Smarty_Variable("1", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow", 0);
}
if (!$_smarty_tpl->tpl_vars['loggedin']->value && $_smarty_tpl->tpl_vars['currencies']->value) {?>
<div class="row">
  <div class="col-md-12">
    <div class="pull-right currency-product">
      <?php
$_from = $_smarty_tpl->tpl_vars['currencies']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_curr_0_saved_item = isset($_smarty_tpl->tpl_vars['curr']) ? $_smarty_tpl->tpl_vars['curr'] : false;
$_smarty_tpl->tpl_vars['curr'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['curr']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['curr']->value) {
$_smarty_tpl->tpl_vars['curr']->_loop = true;
$__foreach_curr_0_saved_local_item = $_smarty_tpl->tpl_vars['curr'];
?>
      <a href="cart.php?gid=<?php echo $_smarty_tpl->tpl_vars['gid']->value;?>
&currency=<?php echo $_smarty_tpl->tpl_vars['curr']->value['id'];?>
"><i class="fa fa-money<?php if ($_smarty_tpl->tpl_vars['curr']->value['id'] == $_smarty_tpl->tpl_vars['currency']->value['id']) {?> active<?php }?>"></i> <span><?php echo $_smarty_tpl->tpl_vars['curr']->value['code'];?>
</span></a>
      <?php
$_smarty_tpl->tpl_vars['curr'] = $__foreach_curr_0_saved_local_item;
}
if ($__foreach_curr_0_saved_item) {
$_smarty_tpl->tpl_vars['curr'] = $__foreach_curr_0_saved_item;
}
?>
    </div>
  </div>
</div>
<?php }?>
<div class="cart-wrapper">
<div id="product-packages">

<?php if ($_smarty_tpl->tpl_vars['flowcart_show_Product_Group_Headline']->value || $_smarty_tpl->tpl_vars['flowcart_show_Product_Group_Tagline']->value) {?>
<div class="row">
<div class="col-md-12">
        <div class="header-lined">
        <?php if ($_smarty_tpl->tpl_vars['flowcart_show_Product_Group_Headline']->value) {?>
                <h1>
                    <?php if ($_smarty_tpl->tpl_vars['productGroup']->value['headline']) {?>
                        <?php echo $_smarty_tpl->tpl_vars['productGroup']->value['headline'];?>

                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['productGroup']->value['name'];?>

                    <?php }?>
                </h1>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['flowcart_show_Product_Group_Tagline']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['productGroup']->value['tagline']) {?>
                    <p><?php echo $_smarty_tpl->tpl_vars['productGroup']->value['tagline'];?>
</p>
                <?php }?>
        <?php }?>
            </div>
</div>
</div>
<?php }?>

<?php if (count($_smarty_tpl->tpl_vars['productGroup']->value['features']) > 0 && $_smarty_tpl->tpl_vars['flowcart_show_Group_Features']->value) {?>
    <div class="includes-container">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="head-area">
                    <span>
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['whatIsIncluded'];?>

                    </span>
                </div>
                <ul id="list-contents" class="list-contents productGroupfeatures">
                    <?php
$_from = $_smarty_tpl->tpl_vars['productGroup']->value['features'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_features_1_saved_item = isset($_smarty_tpl->tpl_vars['features']) ? $_smarty_tpl->tpl_vars['features'] : false;
$_smarty_tpl->tpl_vars['features'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['features']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['features']->value) {
$_smarty_tpl->tpl_vars['features']->_loop = true;
$__foreach_features_1_saved_local_item = $_smarty_tpl->tpl_vars['features'];
?>
                        <li class="<?php echo $_smarty_tpl->tpl_vars['flowcart_features_columns']->value;?>
"><i class="flow-icon <?php echo $_smarty_tpl->tpl_vars['flowcart_feature_icon']->value;?>
"></i><?php echo $_smarty_tpl->tpl_vars['features']->value['feature'];?>
</li>
                    <?php
$_smarty_tpl->tpl_vars['features'] = $__foreach_features_1_saved_local_item;
}
if ($__foreach_features_1_saved_item) {
$_smarty_tpl->tpl_vars['features'] = $__foreach_features_1_saved_item;
}
?>
                </ul>
            </div>
        </div>
    </div>
<?php }?>

<?php
$_from = $_smarty_tpl->tpl_vars['hookAboveProductsOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_2_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_2_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
  <div>
      <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

  </div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_2_saved_local_item;
}
if ($__foreach_output_2_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_2_saved_item;
}
?>

    <?php
$_from = $_smarty_tpl->tpl_vars['products']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mainProducts_3_saved_item = isset($_smarty_tpl->tpl_vars['product']) ? $_smarty_tpl->tpl_vars['product'] : false;
$__foreach_mainProducts_3_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['product'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['product']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
$__foreach_mainProducts_3_saved_local_item = $_smarty_tpl->tpl_vars['product'];
?>
    <?php if ($_smarty_tpl->tpl_vars['itemsprow_c']->value == 0) {?>
    <div class="row pricing">
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['num']->value == 0) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "first"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("first", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php } elseif ($_smarty_tpl->tpl_vars['num']->value == 1) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "second"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("second", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php } elseif ($_smarty_tpl->tpl_vars['num']->value == 2) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "third"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("third", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php } elseif ($_smarty_tpl->tpl_vars['num']->value == 3) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "fourth"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("fourth", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php } elseif ($_smarty_tpl->tpl_vars['num']->value == 4) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "fifth"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("fifth", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php } elseif ($_smarty_tpl->tpl_vars['num']->value == 5) {
if (isset($_smarty_tpl->tpl_vars["order"])) {$_smarty_tpl->tpl_vars["order"] = clone $_smarty_tpl->tpl_vars["order"];
$_smarty_tpl->tpl_vars["order"]->value = "sixth"; $_smarty_tpl->tpl_vars["order"]->nocache = null;
} else $_smarty_tpl->tpl_vars["order"] = new Smarty_Variable("sixth", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "order", 0);?>
    <?php }?>
    <div class="<?php echo $_smarty_tpl->tpl_vars['col']->value;?>
 fadeInCart animatedProduct pricing-<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
">
      <div class="plan"><div class="header">
        <h4><?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
</h4>
      </div>
      <ul>
        <?php
$_from = $_smarty_tpl->tpl_vars['product']->value['features'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_4_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_4_saved_key = isset($_smarty_tpl->tpl_vars['feature']) ? $_smarty_tpl->tpl_vars['feature'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['feature'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['feature']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$__foreach_value_4_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
        <li>
          <?php if (strstr($_smarty_tpl->tpl_vars['value']->value,'YES')) {
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['value']->value,'YES','<i class="fa fa-lg fa-check"></i>');?>
 <small><?php echo $_smarty_tpl->tpl_vars['feature']->value;?>
</small>
          <?php } elseif (strstr($_smarty_tpl->tpl_vars['value']->value,'NO')) {
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['value']->value,'NO','<i class="fa fa-lg fa-times"></i>');?>
 <small><?php echo $_smarty_tpl->tpl_vars['feature']->value;?>
</small>
          <?php } else { ?> <strong><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</strong> <small><?php echo $_smarty_tpl->tpl_vars['feature']->value;?>
</small><?php }?>
        </li>
        <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_4_saved_local_item;
}
if ($__foreach_value_4_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_4_saved_item;
}
if ($__foreach_value_4_saved_key) {
$_smarty_tpl->tpl_vars['feature'] = $__foreach_value_4_saved_key;
}
?>
      </ul>
      <p>
              <?php if ($_smarty_tpl->tpl_vars['flowcart_filter_BR_line_break']->value) {?>
                <?php echo smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['product']->value['featuresdesc'],"/(<br>|<br [^>]*>|<\\/>)/",'');?>

              <?php } else { ?>
                <?php echo trim(nl2br($_smarty_tpl->tpl_vars['product']->value['featuresdesc']));?>

              <?php }?>
      </p>
      <div class="price">
        <?php if ($_smarty_tpl->tpl_vars['product']->value['bid']) {?>
        <?php if ($_smarty_tpl->tpl_vars['product']->value['displayprice']) {?>
        <h5><span class="symbol"><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];?>
</span><?php echo $_smarty_tpl->tpl_vars['product']->value['displayprice'];?>
</h5>
        <?php }?>
        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bundledeal'];?>
</h6>
        <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['paytype'] == "free") {?>
        <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderfree'];?>
</h5><h6>&nbsp;</h6>
        <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['paytype'] == "onetime") {?>
        <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['onetime'];?>
</h5>
        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermonetime'];?>
</h6>
        <?php } else { ?>
          <?php if ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "monthly") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermmonthly'];?>
</h6>
          <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "quarterly") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermquarterly'];?>
</h6>
          <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "semiannually") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermsemiannually'];?>
</h6>
          <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "annually") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermannually'];?>
</h6>
          <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "biennially") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermbiennially'];?>
</h6>
          <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['cycle'] == "triennially") {?>
          <h5><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['price'];?>
</h5>
          <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermtriennially'];?>
</h6>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['setupFee']) {?>
              <small><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['minprice']['setupFee']->toPrefixed();?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];?>
</small>
          <?php }?>

        <?php }?>
      </div>
      <a class="btn btn-default btn-sm <?php if ($_smarty_tpl->tpl_vars['product']->value['qty'] == '0') {?> disabled<?php }?>" href="<?php echo $_SERVER['PHP_SELF'];?>
?a=add&<?php if ($_smarty_tpl->tpl_vars['product']->value['bid']) {?>bid=<?php echo $_smarty_tpl->tpl_vars['product']->value['bid'];
} else { ?>pid=<?php echo $_smarty_tpl->tpl_vars['product']->value['pid'];
}?>"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
</a></div>
    </div>

    <?php if (isset($_smarty_tpl->tpl_vars["itemsprow_c"])) {$_smarty_tpl->tpl_vars["itemsprow_c"] = clone $_smarty_tpl->tpl_vars["itemsprow_c"];
$_smarty_tpl->tpl_vars["itemsprow_c"]->value = $_smarty_tpl->tpl_vars['itemsprow_c']->value+1; $_smarty_tpl->tpl_vars["itemsprow_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow_c"] = new Smarty_Variable($_smarty_tpl->tpl_vars['itemsprow_c']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow_c", 0);?>
    <?php if ($_smarty_tpl->tpl_vars['itemsprow_c']->value == $_smarty_tpl->tpl_vars['itemsprow']->value) {?>
      </div>
      <?php if (isset($_smarty_tpl->tpl_vars["itemsprow_c"])) {$_smarty_tpl->tpl_vars["itemsprow_c"] = clone $_smarty_tpl->tpl_vars["itemsprow_c"];
$_smarty_tpl->tpl_vars["itemsprow_c"]->value = 0; $_smarty_tpl->tpl_vars["itemsprow_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow_c"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow_c", 0);?>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['product'] = $__foreach_mainProducts_3_saved_local_item;
}
if ($__foreach_mainProducts_3_saved_item) {
$_smarty_tpl->tpl_vars['product'] = $__foreach_mainProducts_3_saved_item;
}
if ($__foreach_mainProducts_3_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_mainProducts_3_saved_key;
}
?>
    <?php if ($_smarty_tpl->tpl_vars['itemsprow_c']->value != $_smarty_tpl->tpl_vars['itemsprow']->value) {?>
      </div>
      <?php if (isset($_smarty_tpl->tpl_vars["itemsprow_c"])) {$_smarty_tpl->tpl_vars["itemsprow_c"] = clone $_smarty_tpl->tpl_vars["itemsprow_c"];
$_smarty_tpl->tpl_vars["itemsprow_c"]->value = 0; $_smarty_tpl->tpl_vars["itemsprow_c"]->nocache = null;
} else $_smarty_tpl->tpl_vars["itemsprow_c"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "itemsprow_c", 0);?>
    <?php }?>

<?php
$_from = $_smarty_tpl->tpl_vars['hookBelowProductsOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_5_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_5_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
 <div>
    <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

 </div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_5_saved_local_item;
}
if ($__foreach_output_5_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_5_saved_item;
}
?>

</div>
</div>
<?php }
}
