<?php
/* Smarty version 3.1.29, created on 2018-04-05 16:23:09
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/addons.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ac6152dc1ea55_43062770',
  'file_dependency' => 
  array (
    '954348ad187d8500c5b29dce7b15bdeaf7c5af6c' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/addons.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/products-header.tpl' => 1,
  ),
),false)) {
function content_5ac6152dc1ea55_43062770 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/products-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<div class="row flowcart-row">
<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductaddons'];?>
</h4>
    <?php if (count($_smarty_tpl->tpl_vars['addons']->value) == 0) {?>
    <div class="alert alert-warning text-center" role="alert">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductaddonsnone'];?>

    </div>
    <p class="text-center">
      <a href="clientarea.php" class="btn btn-default">
        <i class="fa fa-arrow-circle-left"></i>
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['returnToClientArea'];?>

      </a>
    </p>
    <?php }?>
    <div class="products text-center">
      <div class="row">
        <?php
$_from = $_smarty_tpl->tpl_vars['addons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_0_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$__foreach_addon_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_0_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
        <div class="col-md-4">
        <div class="well well-lg well-primary clearfix">
          <div class="product clearfix" id="product<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
">
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?a=add">
              <input type="hidden" name="aid" value="<?php echo $_smarty_tpl->tpl_vars['addon']->value['id'];?>
" />
              <header>
                <h4><?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>
</h4s>
                <?php if ($_smarty_tpl->tpl_vars['product']->value['qty']) {?>
                <span class="qty">
                  <?php echo $_smarty_tpl->tpl_vars['product']->value['qty'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderavailable'];?>

                </span>
                <?php }?>
              </header>
              <div class="product-desc">
                <p><?php echo $_smarty_tpl->tpl_vars['addon']->value['description'];?>
</p>
                <div class="form-group">
                  <select name="productid" id="inputProductId<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" class="form-control">
                    <?php
$_from = $_smarty_tpl->tpl_vars['addon']->value['productids'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_product_1_saved_item = isset($_smarty_tpl->tpl_vars['product']) ? $_smarty_tpl->tpl_vars['product'] : false;
$_smarty_tpl->tpl_vars['product'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['product']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
$__foreach_product_1_saved_local_item = $_smarty_tpl->tpl_vars['product'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['product']->value['id'];?>
">
                      <?php echo $_smarty_tpl->tpl_vars['product']->value['product'];
if ($_smarty_tpl->tpl_vars['product']->value['domain']) {?> - <?php echo $_smarty_tpl->tpl_vars['product']->value['domain'];
}?>
                    </option>
                    <?php
$_smarty_tpl->tpl_vars['product'] = $__foreach_product_1_saved_local_item;
}
if ($__foreach_product_1_saved_item) {
$_smarty_tpl->tpl_vars['product'] = $__foreach_product_1_saved_item;
}
?>
                  </select>
                </div>
              </div>
              <footer>
                <div class="product-pricing h4 p-1">
                  <?php if ($_smarty_tpl->tpl_vars['addon']->value['free']) {?>
                  <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderfree'];?>

                  <?php } else { ?>
                  <span class="price"><?php echo $_smarty_tpl->tpl_vars['addon']->value['recurringamount'];?>
 <?php echo $_smarty_tpl->tpl_vars['addon']->value['billingcycle'];?>
</span>
                  <?php if ($_smarty_tpl->tpl_vars['addon']->value['setupfee']) {?><br />+ <?php echo $_smarty_tpl->tpl_vars['addon']->value['setupfee'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];
}?>
                  <?php }?>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-success btn-block btn-lg"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
</button>
              </div>
              </footer>
            </form>
          </div>
        </div>
        </div>
     <?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_0_saved_local_item;
}
if ($__foreach_addon_0_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_0_saved_item;
}
if ($__foreach_addon_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_addon_0_saved_key;
}
?>
      </div>
    </div>
  </div>
<?php }
}
