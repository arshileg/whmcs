<?php
/* Smarty version 3.1.29, created on 2018-08-04 12:31:38
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/upgrade.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b659caa5f0747_33297611',
  'file_dependency' => 
  array (
    '714e659683f43d37627f000e88914e3aea6066eb' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/upgrade.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b659caa5f0747_33297611 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['upgradedowngradepackage']), 0, true);
?>

<div class="alert alert-block alert-info">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderproduct'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['groupname']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['productname']->value;?>
</strong><?php if ($_smarty_tpl->tpl_vars['domain']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
)<?php }?></p>
</div>
<?php if ($_smarty_tpl->tpl_vars['overdueinvoice']->value) {?>
<div class="row mx-1">
<div class="col-md-12">
<div class="well"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradeerroroverdueinvoice'];?>
</div>
<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" onclick="window.location='clientarea.php?action=productdetails&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
'" class="btn btn-default" />
</div>
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['existingupgradeinvoice']->value) {?>
<div class="row px-1">
<div class="col-md-12">
<div class="well"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradeexistingupgradeinvoice'];?>
</div>
<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" onclick="window.location='clientarea.php?action=productdetails&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
'" class="btn btn-link" />
<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['submitticketdescription'];?>
" onclick="window.location='submitticket.php'" class="btn btn-primary" />
</div>
</div>
<?php } else {
if ($_smarty_tpl->tpl_vars['type']->value == "package") {?>
<h6 class="px-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradechoosepackage'];?>
</h6>
<div class="row px-1">
<?php
$_from = $_smarty_tpl->tpl_vars['upgradepackages']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_upgradepackage_0_saved_item = isset($_smarty_tpl->tpl_vars['upgradepackage']) ? $_smarty_tpl->tpl_vars['upgradepackage'] : false;
$__foreach_upgradepackage_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['upgradepackage'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['upgradepackage']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['upgradepackage']->value) {
$_smarty_tpl->tpl_vars['upgradepackage']->_loop = true;
$__foreach_upgradepackage_0_saved_local_item = $_smarty_tpl->tpl_vars['upgradepackage'];
?>
<div class="col-md-4">
<div class="well clearfix text-center">
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
">
<input type="hidden" name="step" value="2">
<input type="hidden" name="type" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
<input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pid'];?>
">
<h5><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['groupname'];?>
 - <?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['name'];?>
</h5>
<?php if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['type'] == "free") {?>
<h3><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderfree'];?>
</h3>
<input type="hidden" name="billingcycle" value="free">
<?php } elseif ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['type'] == "onetime") {
echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['onetime'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermonetime'];?>

<input type="hidden" name="billingcycle" value="onetime">
<?php } elseif ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['type'] == "recurring") {?>
<div class="form-group">
<select name="billingcycle" class="form-control">
<?php if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['monthly']) {?><option value="monthly"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['monthly'];?>
</option><?php }
if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['quarterly']) {?><option value="quarterly"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['quarterly'];?>
</option><?php }
if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['semiannually']) {?><option value="semiannually"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['semiannually'];?>
</option><?php }
if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['annually']) {?><option value="annually"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['annually'];?>
</option><?php }
if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['biennially']) {?><option value="biennially"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['biennially'];?>
</option><?php }
if ($_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['triennially']) {?><option value="triennially"><?php echo $_smarty_tpl->tpl_vars['upgradepackage']->value['pricing']['triennially'];?>
</option><?php }?>
</select>
</div>
<?php }?>
<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradedowngradechooseproduct'];?>
" class="btn btn-default" />
</div>
</div>
</form>
<?php
$_smarty_tpl->tpl_vars['upgradepackage'] = $__foreach_upgradepackage_0_saved_local_item;
}
if ($__foreach_upgradepackage_0_saved_item) {
$_smarty_tpl->tpl_vars['upgradepackage'] = $__foreach_upgradepackage_0_saved_item;
}
if ($__foreach_upgradepackage_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_upgradepackage_0_saved_key;
}
?>
</div>


<?php } elseif ($_smarty_tpl->tpl_vars['type']->value == "configoptions") {?>

<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradechooseconfigoptions'];?>
</p>

<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
    <ul>
        <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

    </ul>
</div>
<?php }?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
">
<input type="hidden" name="step" value="2" />
<input type="hidden" name="type" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" />
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" />

<table class="table table-striped table-framed">
    <thead>
        <tr>
            <th></th>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradecurrentconfig'];?>
</th>
            <th></th>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradenewconfig'];?>
</th>
        </tr>
    </thead>
    <tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['configoptions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_configoption_1_saved_item = isset($_smarty_tpl->tpl_vars['configoption']) ? $_smarty_tpl->tpl_vars['configoption'] : false;
$__foreach_configoption_1_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['configoption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['configoption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['configoption']->value) {
$_smarty_tpl->tpl_vars['configoption']->_loop = true;
$__foreach_configoption_1_saved_local_item = $_smarty_tpl->tpl_vars['configoption'];
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</td>
            <td class="textcenter">
<?php if ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 1 || $_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 2) {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedname'];?>

<?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 3) {
if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {
echo $_smarty_tpl->tpl_vars['LANG']->value['yes'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['no'];
}
} elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 4) {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedqty'];?>
 x <?php echo $_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name'];?>

<?php }?>
            </td>
            <td class="textcenter">=></td>
            <td class="textcenter">
<?php if ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 1 || $_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 2) {?>
<select name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]">
<?php
$_from = $_smarty_tpl->tpl_vars['configoption']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_2_saved_item = isset($_smarty_tpl->tpl_vars['option']) ? $_smarty_tpl->tpl_vars['option'] : false;
$__foreach_option_2_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['option'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
$__foreach_option_2_saved_local_item = $_smarty_tpl->tpl_vars['option'];
if ($_smarty_tpl->tpl_vars['option']->value['selected']) {?><option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['id'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradenochange'];?>
</option><?php } else { ?><option value="<?php echo $_smarty_tpl->tpl_vars['option']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['option']->value['nameonly'];?>
 <?php echo $_smarty_tpl->tpl_vars['option']->value['price'];
}?></option>
<?php
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_2_saved_local_item;
}
if ($__foreach_option_2_saved_item) {
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_2_saved_item;
}
if ($__foreach_option_2_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_option_2_saved_key;
}
?>
</select>
<?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 3) {?>
<input type="checkbox" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="1"<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {?> checked<?php }?>> <?php echo $_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name'];?>

<?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 4) {?>
<input type="text" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['configoption']->value['selectedqty'];?>
" size="5"> x <?php echo $_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name'];?>

<?php }?>
            </td>
        </tr>
<?php
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_1_saved_local_item;
}
if ($__foreach_configoption_1_saved_item) {
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_1_saved_item;
}
if ($__foreach_configoption_1_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_configoption_1_saved_key;
}
?>
    </tbody>
</table>
<p><input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordercontinuebutton'];?>
" class="btn btn-primary" /></p>
</form>
<?php }
}
}
}
