<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:28:22
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/pageheader.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63afb68bd7f7_59169094',
  'file_dependency' => 
  array (
    '697925faea04b92081d34702c6ab224fd8987acb' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/pageheader.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63afb68bd7f7_59169094 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><div class="row">
<div class="col-md-12">
<h3 class="page-header">
<?php if ($_smarty_tpl->tpl_vars['icon']->value) {?><span aria-hidden="true" class="icon icon-<?php echo $_smarty_tpl->tpl_vars['icon']->value;?>
"></span><?php }?> <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['desc']->value) {?><i class="fa fa-angle-down show-info" aria-hidden="true"></i><?php }?>
</h3>
<?php if ($_smarty_tpl->tpl_vars['desc']->value) {?>
<blockquote class="page-information hidden">
<p><?php echo $_smarty_tpl->tpl_vars['desc']->value;?>
</p>
</blockquote>
<?php }?>
</div>
</div>
<?php }
}
