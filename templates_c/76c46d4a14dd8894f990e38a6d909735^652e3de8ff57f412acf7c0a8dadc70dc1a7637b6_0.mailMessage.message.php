<?php
/* Smarty version 3.1.29, created on 2018-05-26 18:42:31
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b097257b76fd1_41863678',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1527345751,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b097257b76fd1_41863678 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ახლახანს, მომხმარებლის არიდან გამოიგზავნა პაროლის აღდგენის მოთხოვნა. თუკი ეს თქვენ არ მოგითხოვიათ, დააიგნორეთ წერილი.  ახალი პაროლი 2 საათში გამოუსადეგარი გახდება.</p>
<p>პაროლის აღსადგენად ეწვიეთ ქვემოთ მოცემულ ბმულს: <br /><a href="<?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>
</a></p>
<p>ზემოთ მოცემულ ბმულზე გადასვლის შემდეგ, თქვენ გექნებათ შესაძლებლობა აირჩიოთ ახალი პაროლი. </p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
