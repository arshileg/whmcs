<?php
/* Smarty version 3.1.29, created on 2018-03-13 15:50:23
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketslist.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5aa7baff685cf9_14166149',
  'file_dependency' => 
  array (
    '608d4cfdc825584d650ac78bb0935a35bcddd88e' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketslist.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5aa7baff685cf9_14166149 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareanavsupporttickets'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['supportticketsintro'],'icon'=>'support'), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"TicketsList",'filterColumn'=>"2",'noSortColumns'=>"4"), 0, true);
?>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableTicketsList').removeClass('hidden').DataTable();
        <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'did' || $_smarty_tpl->tpl_vars['orderby']->value == 'dept') {?>
            table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'subject' || $_smarty_tpl->tpl_vars['orderby']->value == 'title') {?>
            table.order(1, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'status') {?>
            table.order(2, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'lastreply') {?>
            table.order(3, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php }?>
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
<?php echo '</script'; ?>
>
<div class="table-container clearfix">
    <div class="panel panel-default panel-datatable">
    <div class="panel-heading clearfix"> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('sidebar'=>$_smarty_tpl->tpl_vars['primarySidebar']->value), 0, true);
?>
</div>
    <table id="tableTicketsList" class="table table-list hidden">
        <thead>
            <tr>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsdepartment'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketssubject'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsstatus'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketlastupdated'];?>
</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['tickets']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ticket_0_saved_item = isset($_smarty_tpl->tpl_vars['ticket']) ? $_smarty_tpl->tpl_vars['ticket'] : false;
$_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['ticket']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
$__foreach_ticket_0_saved_local_item = $_smarty_tpl->tpl_vars['ticket'];
?>
                <tr>
                    <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['department'];?>
</td>
                    <td><a href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['tid'];?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['c'];?>
"><?php if ($_smarty_tpl->tpl_vars['ticket']->value['unread']) {?><strong><?php }?>#<?php echo $_smarty_tpl->tpl_vars['ticket']->value['tid'];?>
 - <?php echo $_smarty_tpl->tpl_vars['ticket']->value['subject'];
if ($_smarty_tpl->tpl_vars['ticket']->value['unread']) {?></strong><?php }?></a>

                <ul class="cell-inner-list visible-sm visible-xs small">
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketlastupdated'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['lastreply'];?>
</li>
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsdepartment'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['department'];?>
</li>
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgency'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['urgency'];?>
</li>

                </ul>

                    </td>
                    <td><span class="label <?php if (is_null($_smarty_tpl->tpl_vars['ticket']->value['statusColor'])) {?>label-<?php echo $_smarty_tpl->tpl_vars['ticket']->value['statusClass'];?>
"<?php } else { ?>status-custom" style="border-color: <?php echo $_smarty_tpl->tpl_vars['ticket']->value['statusColor'];?>
; color: <?php echo $_smarty_tpl->tpl_vars['ticket']->value['statusColor'];?>
"<?php }?>><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['ticket']->value['status']);?>
</span></td>
                    <td class="hidden-sm hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['normalisedLastReply'];?>
</span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['lastreply'];?>
</td>
                    <td class="text-right"><a class="btn btn-link" href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['tid'];?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['c'];?>
"><i class="fa fa-chevron-right fa-lg pull-right flip"></i></a>
                    </td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['ticket'] = $__foreach_ticket_0_saved_local_item;
}
if ($__foreach_ticket_0_saved_item) {
$_smarty_tpl->tpl_vars['ticket'] = $__foreach_ticket_0_saved_item;
}
?>
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
    </div>
    </div>
</div>
<?php }
}
