<?php
/* Smarty version 3.1.29, created on 2018-02-18 01:14:43
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/knowledgebasecat.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a889b43dd78b6_34203105',
  'file_dependency' => 
  array (
    '7c68243ee7b1479e3030a7befe54dc2fbcde9804' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/knowledgebasecat.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a889b43dd78b6_34203105 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasetitle'],'icon'=>'notebook'), 0, true);
?>

<p class="px-1"><small><?php echo $_smarty_tpl->tpl_vars['breadcrumbnav']->value;?>
</small></p>
  <form role="form" method="post" action="<?php echo routePath('knowledgebase-search');?>
">
      <div class="input-group kb-search p-1">
          <input type="text"  id="inputKnowledgebaseSearch" name="search" class="form-control" placeholder="What can we help you with?" value="<?php echo $_smarty_tpl->tpl_vars['searchterm']->value;?>
" />
          <span class="input-group-btn">
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </span>
      </div>
  </form>

  <?php if ($_smarty_tpl->tpl_vars['kbcats']->value) {?>
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasecategories']), 0, true);
?>


      <div class="row px-1 kbcategories">
          <?php
$_from = $_smarty_tpl->tpl_vars['kbcats']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbasecats_0_saved_item = isset($_smarty_tpl->tpl_vars['kbcat']) ? $_smarty_tpl->tpl_vars['kbcat'] : false;
$_smarty_tpl->tpl_vars['kbcat'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['kbcat']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['kbcat']->value) {
$_smarty_tpl->tpl_vars['kbcat']->_loop = true;
$__foreach_kbasecats_0_saved_local_item = $_smarty_tpl->tpl_vars['kbcat'];
?>
              <div class="col-md-4">
                  <a href="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbcat']->value['id'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbcat']->value['urlfriendlyname'];
$_tmp2=ob_get_clean();
echo routePath('knowledgebase-category-view',$_tmp1,$_tmp2);?>
">
                      <span class="glyphicon glyphicon-folder-close"></span> <?php echo $_smarty_tpl->tpl_vars['kbcat']->value['name'];?>
 <span class="badge badge-info"><?php echo $_smarty_tpl->tpl_vars['kbcat']->value['numarticles'];?>
</span>
                  </a>
                  <p><?php echo $_smarty_tpl->tpl_vars['kbcat']->value['description'];?>
</p>
              </div>
          <?php
$_smarty_tpl->tpl_vars['kbcat'] = $__foreach_kbasecats_0_saved_local_item;
}
if ($__foreach_kbasecats_0_saved_item) {
$_smarty_tpl->tpl_vars['kbcat'] = $__foreach_kbasecats_0_saved_item;
}
?>
      </div>
  <?php }?>

  <?php if ($_smarty_tpl->tpl_vars['kbarticles']->value || !$_smarty_tpl->tpl_vars['kbcats']->value) {?>
      <?php if ($_smarty_tpl->tpl_vars['tag']->value) {?>
          <h2><?php echo $_smarty_tpl->tpl_vars['LANG']->value['kbviewingarticlestagged'];?>
 '<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
'</h2>
      <?php } else { ?>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasearticles']), 0, true);
?>

      <?php }?>


          <?php
$_from = $_smarty_tpl->tpl_vars['kbarticles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbarticles_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles'] : false;
$__foreach_kbarticles_1_saved_item = isset($_smarty_tpl->tpl_vars['kbarticle']) ? $_smarty_tpl->tpl_vars['kbarticle'] : false;
$__foreach_kbarticles_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['kbarticle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles'] = new Smarty_Variable(array('iteration' => 0));
$__foreach_kbarticles_1_iteration=0;
$_smarty_tpl->tpl_vars['kbarticle']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['kbarticle']->value) {
$_smarty_tpl->tpl_vars['kbarticle']->_loop = true;
$__foreach_kbarticles_1_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['last'] = $__foreach_kbarticles_1_iteration == $__foreach_kbarticles_1_total;
$__foreach_kbarticles_1_saved_local_item = $_smarty_tpl->tpl_vars['kbarticle'];
?>
          <?php if ((1 & (isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['iteration'] : null))) {?>
          <div class="row px-1 row-eq-height">
            <?php }?>
          <div class="col-md-6">
          <div class="well well-sm">
              <h4><a href="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['id'];
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['urlfriendlytitle'];
$_tmp4=ob_get_clean();
echo routePath('knowledgebase-article-view',$_tmp3,$_tmp4);?>
">
                  <span class="glyphicon glyphicon-file"></span>&nbsp;<?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];?>

              </a></h4>
              <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['kbarticle']->value['article'],200,"...");?>
</p>
            </div>
          </div>
            <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['iteration'] : null) % 2 == 0 || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles']->value['last'] : null)) {?>
            </div>
            <?php }?>
          <?php
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticles_1_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['kbarticle']->_loop) {
?>
              <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"warning",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasenoarticles'],'textcenter'=>true), 0, true);
?>

          <?php
}
if ($__foreach_kbarticles_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_kbarticles'] = $__foreach_kbarticles_1_saved;
}
if ($__foreach_kbarticles_1_saved_item) {
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticles_1_saved_item;
}
?>
  <?php }
}
}
