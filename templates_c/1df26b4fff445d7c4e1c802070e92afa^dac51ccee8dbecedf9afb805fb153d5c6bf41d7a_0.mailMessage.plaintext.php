<?php
/* Smarty version 3.1.29, created on 2018-08-09 07:25:08
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6bec54626ef0_90409580',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533799508,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6bec54626ef0_90409580 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,

Your email to our support system could not be accepted because we require you to submit all tickets via our online client support portal. You can do this at the URL below.

<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/submitticket.php

<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
