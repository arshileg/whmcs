<?php
/* Smarty version 3.1.29, created on 2018-02-18 12:59:44
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/serverstatus.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a894080586023_19656028',
  'file_dependency' => 
  array (
    'bde6994ba86cb457ce92372a8eb84f0c1f5a7bb5' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/serverstatus.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a894080586023_19656028 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['networkstatustitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['networkstatusintro'],'icon'=>'feed'), 0, true);
?>

<div class="row px-1 pt-1">
	<div class="col-md-4">
    <a href="<?php echo $_SERVER['PHP_SELF'];?>
?view=open">
		<div class="info-box  bg-info  text-white">
			<div class="info-icon bg-info-dark">
				<span aria-hidden="true" class="icon icon-folder-alt"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesstatusopen'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['opencount']->value;?>
</span></p>
			</div>
		</div>
    </a>
	</div>
	<div class="col-md-4">
    <a href="<?php echo $_SERVER['PHP_SELF'];?>
?view=scheduled">
		<div class="info-box  bg-warn  text-white">
			<div class="info-icon bg-warn-dark">
				<span aria-hidden="true" class="icon icon-event"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesstatusscheduled'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['scheduledcount']->value;?>
</span></p>
			</div>
		</div>
  </a>
	</div>
	<div class="col-md-4">
    <a href="<?php echo $_SERVER['PHP_SELF'];?>
?view=resolved">
		<div class="info-box  bg-inactive  text-white">
			<div class="info-icon bg-inactive-dark">
				<span aria-hidden="true" class="icon icon-folder"></span>
			</div>
			<div class="info-details">
				<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesstatusresolved'];?>
</h4>
				<p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['resolvedcount']->value;?>
</span></p>
			</div>
		</div>
	</div>
</a>
</div>
<hr>


<div class="row px-1">
<div class="col-md-12">

<?php
$_from = $_smarty_tpl->tpl_vars['issues']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_issue_0_saved_item = isset($_smarty_tpl->tpl_vars['issue']) ? $_smarty_tpl->tpl_vars['issue'] : false;
$_smarty_tpl->tpl_vars['issue'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['issue']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['issue']->value) {
$_smarty_tpl->tpl_vars['issue']->_loop = true;
$__foreach_issue_0_saved_local_item = $_smarty_tpl->tpl_vars['issue'];
?>

<?php if ($_smarty_tpl->tpl_vars['issue']->value['clientaffected']) {?>
<div class="alert-message block-message alert-warning">
  <?php }?>
  <h3><?php echo $_smarty_tpl->tpl_vars['issue']->value['title'];?>
 (<?php echo $_smarty_tpl->tpl_vars['issue']->value['status'];?>
)</h3>
  <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesaffecting'];?>
 <?php echo $_smarty_tpl->tpl_vars['issue']->value['type'];?>
</strong> - <?php if ($_smarty_tpl->tpl_vars['issue']->value['type'] == $_smarty_tpl->tpl_vars['LANG']->value['networkissuestypeserver']) {
echo $_smarty_tpl->tpl_vars['issue']->value['server'];
} else {
echo $_smarty_tpl->tpl_vars['issue']->value['affecting'];
}?> | <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuespriority'];?>
</strong> - <?php echo $_smarty_tpl->tpl_vars['issue']->value['priority'];?>
</span></p>
  <blockquote>
    <?php echo $_smarty_tpl->tpl_vars['issue']->value['description'];?>

  </blockquote>
  <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesdate'];?>
</strong> - <?php echo $_smarty_tpl->tpl_vars['issue']->value['startdate'];
if ($_smarty_tpl->tpl_vars['issue']->value['enddate']) {?> - <?php echo $_smarty_tpl->tpl_vars['issue']->value['enddate'];
}?></p>
  <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissueslastupdated'];?>
</strong> - <?php echo $_smarty_tpl->tpl_vars['issue']->value['lastupdate'];?>
</p>

  <?php if ($_smarty_tpl->tpl_vars['issue']->value['clientaffected']) {?></div><?php }?>

  <?php
$_smarty_tpl->tpl_vars['issue'] = $__foreach_issue_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['issue']->_loop) {
?>

  <p><small><?php echo $_smarty_tpl->tpl_vars['noissuesmsg']->value;?>
</small></p>

  <?php
}
if ($__foreach_issue_0_saved_item) {
$_smarty_tpl->tpl_vars['issue'] = $__foreach_issue_0_saved_item;
}
?>

  <ul class="pagination">
    <li class="prev<?php if (!$_smarty_tpl->tpl_vars['prevpage']->value) {?> disabled<?php }?>"><a href="<?php if ($_smarty_tpl->tpl_vars['prevpage']->value) {
echo $_SERVER['PHP_SELF'];?>
?<?php if ($_smarty_tpl->tpl_vars['view']->value) {?>view=<?php echo $_smarty_tpl->tpl_vars['view']->value;?>
&amp;<?php }?>page=<?php echo $_smarty_tpl->tpl_vars['prevpage']->value;
} else { ?>javascript:return false;<?php }?>">&larr; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['previouspage'];?>
</a></li>
    <li class="next<?php if (!$_smarty_tpl->tpl_vars['nextpage']->value) {?> disabled<?php }?>"><a href="<?php if ($_smarty_tpl->tpl_vars['nextpage']->value) {
echo $_SERVER['PHP_SELF'];?>
?<?php if ($_smarty_tpl->tpl_vars['view']->value) {?>view=<?php echo $_smarty_tpl->tpl_vars['view']->value;?>
&amp;<?php }?>page=<?php echo $_smarty_tpl->tpl_vars['nextpage']->value;
} else { ?>javascript:return false;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['nextpage'];?>
 &rarr;</a></li>
  </ul>

  <?php if ($_smarty_tpl->tpl_vars['servers']->value) {?>

  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['serverstatustitle']), 0, true);
?>


  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusheadingtext'];?>
</p>

  
  <?php echo '<script'; ?>
>
    function getStats(num) {
      jQuery.post('serverstatus.php', 'getstats=1&num='+num, function(data) {
        jQuery("#load"+num).html(data.load);
        jQuery("#uptime"+num).html(data.uptime);
      },'json');
    }
    function checkPort(num,port) {
      jQuery.post('serverstatus.php', 'ping=1&num='+num+'&port='+port, function(data) {
        jQuery("#port"+port+"_"+num).html(data);
      });
    }
  <?php echo '</script'; ?>
>
  

  <table class="table table-striped table-framed">
    <thead>
      <tr>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['servername'];?>
</th>
        <th>HTTP</th>
        <th>FTP</th>
        <th>POP3</th>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusphpinfo'];?>
</th>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusserverload'];?>
</th>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusuptime'];?>
</th>
      </tr>
    </thead>
    <tbody>
      <?php
$_from = $_smarty_tpl->tpl_vars['servers']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_server_1_saved_item = isset($_smarty_tpl->tpl_vars['server']) ? $_smarty_tpl->tpl_vars['server'] : false;
$__foreach_server_1_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['server'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['server']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['server']->value) {
$_smarty_tpl->tpl_vars['server']->_loop = true;
$__foreach_server_1_saved_local_item = $_smarty_tpl->tpl_vars['server'];
?>
      <tr>
        <td><?php echo $_smarty_tpl->tpl_vars['server']->value['name'];?>
</td>
        <td id="port80_<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><img src="images/loadingsml.gif" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
" /></td>
        <td id="port21_<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><img src="images/loadingsml.gif" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
" /></td>
        <td id="port110_<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><img src="images/loadingsml.gif" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
" /></td>
        <td><a href="<?php echo $_smarty_tpl->tpl_vars['server']->value['phpinfourl'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusphpinfo'];?>
</a></td>
        <td id="load<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><img src="images/loadingsml.gif" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
" /></td>
        <td id="uptime<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><img src="images/loadingsml.gif" alt="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
" /><?php echo '<script'; ?>
> checkPort(<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
,80); checkPort(<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
,21); checkPort(<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
,110); getStats(<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
); <?php echo '</script'; ?>
></td>
      </tr>
      <?php
$_smarty_tpl->tpl_vars['server'] = $__foreach_server_1_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['server']->_loop) {
?>
      <tr>
        <td colspan="7"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverstatusnoservers'];?>
</td>
      </tr>
      <?php
}
if ($__foreach_server_1_saved_item) {
$_smarty_tpl->tpl_vars['server'] = $__foreach_server_1_saved_item;
}
if ($__foreach_server_1_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_server_1_saved_key;
}
?>
    </tbody>
  </table>
  <?php }
if ($_smarty_tpl->tpl_vars['loggedin']->value) {?><div class="alert alert-info"><p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkissuesaffectingyourservers'];?>
</p></div><?php }?>
</div>
</div>
<?php }
}
