<?php
/* Smarty version 3.1.29, created on 2018-06-08 16:15:20
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/affiliatessignup.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b1a7358c802e4_96189518',
  'file_dependency' => 
  array (
    '3574c46c824d41dbae253df53f6a4caec780350c' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/affiliatessignup.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b1a7358c802e4_96189518 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['affiliatesystemenabled']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatesactivate'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatesignupintro'],'icon'=>'diamond'), 0, true);
?>

<div class="row py-2">
<div class="col-md-6 col-md-offset-3">
<ul class="list-group">
  <li class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesignupinfo1'];?>
</li>
  <li class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesignupinfo2'];?>
</li>
  <li class="list-group-item"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesignupinfo3'];?>
</li>
</ul>
<div class="form-group">
<form method="post" action="affiliates.php">
<input type="hidden" name="activate" value="true" />
<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesactivate'];?>
" class="btn btn-default btn-block" />
</form>
</div>
</div>
</div>

<?php } else { ?>
   <div class="row">
      <div class="col-md-12">
        <h3 class="page-header"><span aria-hidden="true" class="icon icon-users"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatestitle'];?>
 </h3>
      </div>
    </div>
<div class="alert alert-warning">
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesdisabled'];?>
</p>
</div>
<?php }
}
}
