<?php
/* Smarty version 3.1.29, created on 2018-05-21 22:34:50
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b03114af2afc0_58716203',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1526927690,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b03114af2afc0_58716203 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ეს წერილი ადასტურებს იმას, რომ დომეინის შესყიდვა წარმატებით დასრულდა. დეტალები შესყიდულ დომეინზე იხილეთ ქვევით:


რეგისტრაციის თარიღი: <?php echo $_smarty_tpl->tpl_vars['domain_reg_date']->value;?>

 დომეინი: <?php echo $_smarty_tpl->tpl_vars['domain_name']->value;?>

 რეგისტრაციის პერიოდი: <?php echo $_smarty_tpl->tpl_vars['domain_reg_period']->value;?>

გადახდილი თანხი ოდენობა: <?php echo $_smarty_tpl->tpl_vars['domain_first_payment_amount']->value;?>

 შემდგომი გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>



ახალი დომეინის სამართავად შეგიძლიათა გადახვიდეთ მომხმარებლის არეში შემდეგი ბმულით.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
