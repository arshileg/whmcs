<?php
/* Smarty version 3.1.29, created on 2018-06-08 16:17:23
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/affiliates.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b1a73d37d5219_41622255',
  'file_dependency' => 
  array (
    '7eaebe42d9552ad906d2846315cd6c29a4e14491' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/affiliates.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b1a73d37d5219_41622255 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['inactive']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatestitle'],'icon'=>'diamond'), 0, true);
?>

<div class="alert alert-warning">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesdisabled'];?>
</p>
</div>
<?php } else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatestitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatesrealtime'],'icon'=>'diamond'), 0, true);
?>

<div class="row px-1 py-2">
  <div class="col-md-12">
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesreferallink'];?>
</h4>
    <div class="form-group">
      <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['referrallink']->value;?>
" class="form-control input-sm" />
    </div>
  </div>
</div>
<div class="row px-1">
  <div class="col-md-4">
    <div class="well well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesvisitorsreferred'];?>

      <span class="label label-default pull-right flip"><?php echo $_smarty_tpl->tpl_vars['visitors']->value;?>
</span>
    </div>
  </div>
  <div class="col-md-4">
    <div class="well  well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatessignups'];?>

      <span class="label label-success pull-right flip"><?php echo $_smarty_tpl->tpl_vars['signups']->value;?>
</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesconversionrate'];?>

      <span class="label label-info pull-right flip"><?php echo $_smarty_tpl->tpl_vars['conversionrate']->value;?>
%</span>
    </div>
  </div>
</div>

<div class="row px-1">
  <div class="col-md-4">
    <div class="well  well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatescommissionspending'];?>

      <span class="label label-default pull-right flip"><?php echo $_smarty_tpl->tpl_vars['pendingcommissions']->value;?>
</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatescommissionsavailable'];?>

      <span class="label label-success pull-right flip"><?php echo $_smarty_tpl->tpl_vars['balance']->value;?>
</span>
    </div>
  </div>

  <div class="col-md-4">
    <div class="well  well-lg">
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliateswithdrawn'];?>

      <span class="label label-info pull-right flip"><?php echo $_smarty_tpl->tpl_vars['withdrawn']->value;?>
</span>
    </div>
  </div>
</div>


<?php if ($_smarty_tpl->tpl_vars['withdrawrequestsent']->value) {?>
<div class="alert alert-success">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliateswithdrawalrequestsuccessful'];?>
</p>
</div>
<?php } else {
if ($_smarty_tpl->tpl_vars['withdrawlevel']->value) {?>
<input type="button" class="btn btn-default btn-sm" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesrequestwithdrawal'];?>
" onclick="window.location='<?php echo $_SERVER['PHP_SELF'];?>
?action=withdrawrequest'" />
<?php }
}?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliatesreferals']), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"AffiliatesList"), 0, true);
?>

<?php echo '<script'; ?>
 type="text/javascript">
jQuery(document).ready( function ()
{
  var table = jQuery('#tableAffiliatesList').removeClass('hidden').DataTable();
  <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'regdate') {?>
  table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
  <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'product') {?>
  table.order(1, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
  <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'amount') {?>
  table.order(2, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
  <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'status') {?>
  table.order(4, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
  <?php }?>
  table.draw();
  jQuery('#tableLoading').addClass('hidden');
});
<?php echo '</script'; ?>
>
<div class="panel panel-default panel-datatable">
  <div class="panel-heading clearfix">
    <div class="filter_top panel panel-default panel-actions view-filter-btns" >
      <div class="elemetsholder ">
        <a class="btn btn-link btn-xs" href="#" ></a>
      </div>
    </div>
  </div>
  <table class="table table-striped table-framed" id="tableAffiliatesList">
    <thead>
      <tr>
        <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatessignupdate'];?>
</th>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderproduct'];?>
</th>
        <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesamount'];?>
</th>
        <th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatescommission'];?>
</th>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesstatus'];?>
</th>
      </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->tpl_vars['referrals']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_referral_0_saved_item = isset($_smarty_tpl->tpl_vars['referral']) ? $_smarty_tpl->tpl_vars['referral'] : false;
$_smarty_tpl->tpl_vars['referral'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['referral']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['referral']->value) {
$_smarty_tpl->tpl_vars['referral']->_loop = true;
$__foreach_referral_0_saved_local_item = $_smarty_tpl->tpl_vars['referral'];
?>
    <tr>
      <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['referral']->value['date'];?>
</td>
      <td><?php echo $_smarty_tpl->tpl_vars['referral']->value['service'];?>


        <ul class="cell-inner-list visible-sm visible-xs small">
          <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatessignupdate'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['referral']->value['date'];?>
</li>
          <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatesamount'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['referral']->value['amountdesc'];?>
</li>
          <li class="hidden-sm"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['affiliatescommission'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['referral']->value['commission'];?>
</li>
        </ul>

      </td>
      <td data-order="<?php echo $_smarty_tpl->tpl_vars['referral']->value['amountnum'];?>
" class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['referral']->value['amountdesc'];?>
</td>
      <td data-order="<?php echo $_smarty_tpl->tpl_vars['referral']->value['commissionnum'];?>
" class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['referral']->value['commission'];?>
</td>
      <td><span class='label status status-<?php echo strtolower($_smarty_tpl->tpl_vars['referral']->value['rawstatus']);?>
'><?php echo $_smarty_tpl->tpl_vars['referral']->value['status'];?>
</span></td>
    </tr>
    <?php
$_smarty_tpl->tpl_vars['referral'] = $__foreach_referral_0_saved_local_item;
}
if ($__foreach_referral_0_saved_item) {
$_smarty_tpl->tpl_vars['referral'] = $__foreach_referral_0_saved_item;
}
?>
  </table>
  <div class="text-center" id="tableLoading">
    <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
  </div>
</div>
<ul class="pagination px-1">
  <li class="prev<?php if (!$_smarty_tpl->tpl_vars['prevpage']->value) {?> disabled<?php }?>"><a href="<?php if ($_smarty_tpl->tpl_vars['prevpage']->value) {?>affiliates.php?page=<?php echo $_smarty_tpl->tpl_vars['prevpage']->value;
} else { ?>javascript:return false;<?php }?>">&larr; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['previouspage'];?>
</a></li>
  <li class="next<?php if (!$_smarty_tpl->tpl_vars['nextpage']->value) {?> disabled<?php }?>"><a href="<?php if ($_smarty_tpl->tpl_vars['nextpage']->value) {?>affiliates.php?page=<?php echo $_smarty_tpl->tpl_vars['nextpage']->value;
} else { ?>javascript:return false;<?php }?>"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['nextpage'];?>
 &rarr;</a></li>
</ul>
<?php if ($_smarty_tpl->tpl_vars['affiliatelinkscode']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['affiliateslinktous']), 0, true);
?>

<div class="textcenter">
  <?php echo $_smarty_tpl->tpl_vars['affiliatelinkscode']->value;?>

</div>
<?php }
}
}
}
