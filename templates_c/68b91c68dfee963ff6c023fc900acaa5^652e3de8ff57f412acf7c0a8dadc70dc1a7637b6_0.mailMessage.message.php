<?php
/* Smarty version 3.1.29, created on 2018-07-25 00:00:02
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b5785427e3e31_98271914',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1532462402,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5785427e3e31_98271914 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>Re: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;
if ($_smarty_tpl->tpl_vars['service_domain']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
)<?php }?></p>
<p>
    Recently you placed an upgrade order with us.<br>
    Today your automated renewal invoice has been generated for the product/service listed above which has invalidated the upgrade quote and invoice.<br>
    As a result, your upgrade order has now been cancelled.
</p>
<p>Should you wish to continue with the upgrade, we ask that you please first pay the renewal invoice, after which you will be able to order the upgrade again and simply pay the difference.</p>
<p>We thank you for your business.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
