<?php
/* Smarty version 3.1.29, created on 2018-08-03 04:41:52
  from "/home/hostnodesnet/public_html/modules/addons/hexa_extras/templates/header-minimal.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63dd10624379_82913675',
  'file_dependency' => 
  array (
    'e44c387f4396d786a7e2454dc76d8c11d6746c61' => 
    array (
      0 => '/home/hostnodesnet/public_html/modules/addons/hexa_extras/templates/header-minimal.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63dd10624379_82913675 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>  <div class="hexa-container hidden-xs"><a href="contact.php" class="btn btn-xs btn-default hexa-btn" data-original-title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['contactus'];?>
"><i class="fa fa-send-o"></i></a></div>
  <?php if ($_smarty_tpl->tpl_vars['languagechangeenabled']->value && count($_smarty_tpl->tpl_vars['locales']->value) > 1) {?>
  <div class="hexa-container hexa-container-lang hidden-xs">
  <div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle hexa-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa icon-globe"></i>
  </button>
  <ul class="dropdown-menu">
         <?php
$_from = $_smarty_tpl->tpl_vars['locales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_locale_0_saved_item = isset($_smarty_tpl->tpl_vars['locale']) ? $_smarty_tpl->tpl_vars['locale'] : false;
$_smarty_tpl->tpl_vars['locale'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['locale']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->_loop = true;
$__foreach_locale_0_saved_local_item = $_smarty_tpl->tpl_vars['locale'];
?>
                    <?php if ($_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'] == $_smarty_tpl->tpl_vars['locale']->value['localisedName']) {?>
                                  <li><a class="active" href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                                  <?php } else { ?>
                                  <li><a href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
" ><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                    <?php }?>
         <?php
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_local_item;
}
if ($__foreach_locale_0_saved_item) {
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_item;
}
?>
  </ul>
</div>
  </div>
<?php }?>
 <div class="login-wrapper">
  <div class="container">
   <div class="row">
     <div class="col-md-4 col-md-offset-4 logo-page">
      <a href="index.php" title="<?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
">
        <?php if (!isset($_smarty_tpl->tpl_vars['hexa_logo']->value)) {?>
          <span class="logo"><span aria-hidden="true" class="icon icon-map"></span> <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
</span>
        <?php } else { ?>
          <span class="logo">
            <img <?php if (isset($_smarty_tpl->tpl_vars['hexa_logo_width']->value)) {?> style="width:<?php echo $_smarty_tpl->tpl_vars['hexa_logo_width']->value;?>
px" <?php }?> src="<?php echo $_smarty_tpl->tpl_vars['hexa_logo']->value;?>
">
          </span>
        <?php }?>
      </a>
    </div>
  </div>
<?php }
}
