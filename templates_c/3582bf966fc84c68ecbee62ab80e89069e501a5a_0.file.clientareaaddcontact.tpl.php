<?php
/* Smarty version 3.1.29, created on 2018-02-28 12:09:17
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareaaddcontact.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9663ada55f65_55007844',
  'file_dependency' => 
  array (
    '3582bf966fc84c68ecbee62ab80e89069e501a5a' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareaaddcontact.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9663ada55f65_55007844 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/StatesDropdown.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareanavcontacts']), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/clientareadetailslinks.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>


<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'errorshtml'=>$_smarty_tpl->tpl_vars['errormessage']->value), 0, true);
?>

<?php }?>

<?php echo '<script'; ?>
>
var stateNotRequired = true;
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/StatesDropdown.js"><?php echo '</script'; ?>
>

  <form class="form-horizontal p-2" role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=contacts">
    <div class="form-group">
      <label for="inputContactID" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareachoosecontact'];?>
</label>
        <div class="col-sm-10">
      <select name="contactid" id="inputContactID" onchange="submit()" class="form-control">
        <?php
$_from = $_smarty_tpl->tpl_vars['contacts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_contact_0_saved_item = isset($_smarty_tpl->tpl_vars['contact']) ? $_smarty_tpl->tpl_vars['contact'] : false;
$_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['contact']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->_loop = true;
$__foreach_contact_0_saved_local_item = $_smarty_tpl->tpl_vars['contact'];
?>
        <option value="<?php echo $_smarty_tpl->tpl_vars['contact']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['contact']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['contact']->value['email'];?>
</option>
        <?php
$_smarty_tpl->tpl_vars['contact'] = $__foreach_contact_0_saved_local_item;
}
if ($__foreach_contact_0_saved_item) {
$_smarty_tpl->tpl_vars['contact'] = $__foreach_contact_0_saved_item;
}
?>
        <option value="new" selected="selected"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavaddcontact'];?>
</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-default"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['go'];?>
</button>
  </div>
</div>
  </form>

<form role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=addcontact" class="form-horizontal p-2">
  <input type="hidden" name="submit" value="true" />
  <div class="form-group">
    <label for="inputFirstName" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareafirstname'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="firstname" id="inputFirstName" value="<?php echo $_smarty_tpl->tpl_vars['contactfirstname']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputLastName" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientarealastname'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="lastname" id="inputLastName" value="<?php echo $_smarty_tpl->tpl_vars['contactlastname']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputCompanyName" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacompanyname'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="companyname" id="inputCompanyName" value="<?php echo $_smarty_tpl->tpl_vars['contactcompanyname']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaemail'];?>
</label>
    <div class="col-sm-10">
      <input type="email" name="email" id="inputEmail" value="<?php echo $_smarty_tpl->tpl_vars['contactemail']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputPhone" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaphonenumber'];?>
</label>
    <div class="col-sm-10">
      <input type="tel" name="phonenumber" id="inputPhone" value="<?php echo $_smarty_tpl->tpl_vars['contactphonenumber']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputAddress1"  class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaaddress1'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="address1" id="inputAddress1" value="<?php echo $_smarty_tpl->tpl_vars['contactaddress1']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputAddress2" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaaddress2'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="address2" id="inputAddress2" value="<?php echo $_smarty_tpl->tpl_vars['contactaddress2']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputCity"  class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacity'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="city" id="inputCity" value="<?php echo $_smarty_tpl->tpl_vars['contactcity']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputState"  class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareastate'];?>
</label>
    <div class="col-sm-6">
      <input type="text" name="state" id="inputState" value="<?php echo $_smarty_tpl->tpl_vars['contactstate']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">
    <label for="inputPostcode"  class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareapostcode'];?>
</label>
    <div class="col-sm-10">
      <input type="text" name="postcode" id="inputPostcode" value="<?php echo $_smarty_tpl->tpl_vars['contactpostcode']->value;?>
" class="form-control" />
    </div>
  </div>

  <div class="form-group">

    <label  class="col-sm-2 control-label" for="country"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacountry'];?>
</label>
    <div class="col-sm-6">
      <?php echo $_smarty_tpl->tpl_vars['countriesdropdown']->value;?>

    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="subaccount" id="inputSubaccountActivate"<?php if ($_smarty_tpl->tpl_vars['subaccount']->value) {?> checked<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['subaccountactivatedesc'];?>

        </label>
      </div>
    </div>
  </div>

  <div id="subacct-container" class="<?php if (!$_smarty_tpl->tpl_vars['subaccount']->value) {?> hidden<?php }?>">
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
      <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['subaccountpermissions'];?>
</h5>
        <?php
$_from = $_smarty_tpl->tpl_vars['allPermissions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_permission_1_saved_item = isset($_smarty_tpl->tpl_vars['permission']) ? $_smarty_tpl->tpl_vars['permission'] : false;
$_smarty_tpl->tpl_vars['permission'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['permission']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['permission']->value) {
$_smarty_tpl->tpl_vars['permission']->_loop = true;
$__foreach_permission_1_saved_local_item = $_smarty_tpl->tpl_vars['permission'];
?>
              <div class="checkbox">
          <label>
            <input type="checkbox" name="permissions[]" value="<?php echo $_smarty_tpl->tpl_vars['permission']->value;?>
"<?php if (in_array($_smarty_tpl->tpl_vars['permission']->value,$_smarty_tpl->tpl_vars['permissions']->value)) {?> checked<?php }?> />
              <?php if (isset($_smarty_tpl->tpl_vars['langPermission'])) {$_smarty_tpl->tpl_vars['langPermission'] = clone $_smarty_tpl->tpl_vars['langPermission'];
$_smarty_tpl->tpl_vars['langPermission']->value = ('subaccountperms').($_smarty_tpl->tpl_vars['permission']->value); $_smarty_tpl->tpl_vars['langPermission']->nocache = null;
} else $_smarty_tpl->tpl_vars['langPermission'] = new Smarty_Variable(('subaccountperms').($_smarty_tpl->tpl_vars['permission']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'langPermission', 0);
echo $_smarty_tpl->tpl_vars['LANG']->value[$_smarty_tpl->tpl_vars['langPermission']->value];?>

          </label>
        </div>
        <?php
$_smarty_tpl->tpl_vars['permission'] = $__foreach_permission_1_saved_local_item;
}
if ($__foreach_permission_1_saved_item) {
$_smarty_tpl->tpl_vars['permission'] = $__foreach_permission_1_saved_item;
}
?>
  </div>
</div>
      <div id="newPassword1" class="form-group has-feedback">
        <label for="inputNewPassword1" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['newpassword'];?>
</label>
        <div class="col-sm-6">
          <input type="password" class="form-control" id="inputNewPassword1" name="password" autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
        </div>
      </div>
      <div id="newPassword2" class="form-group has-feedback">
        <label for="inputNewPassword2" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['confirmnewpassword'];?>
</label>
        <div class="col-sm-6">
          <input type="password" class="form-control" id="inputNewPassword2" name="password2" autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
          <div id="inputNewPassword2Msg">
          </div>
        </div>
      </div>
</div>

<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemails'];?>
</h5>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="generalemails" id="generalemails" value="1"<?php if ($_smarty_tpl->tpl_vars['generalemails']->value) {?> checked<?php }?> />
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemailsgeneral'];?>

        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="productemails" id="productemails" value="1"<?php if ($_smarty_tpl->tpl_vars['productemails']->value) {?> checked<?php }?> />
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemailsproduct'];?>

        </label>
    </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="domainemails" id="domainemails" value="1"<?php if ($_smarty_tpl->tpl_vars['domainemails']->value) {?> checked<?php }?> />
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemailsdomain'];?>

        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="invoiceemails" id="invoiceemails" value="1"<?php if ($_smarty_tpl->tpl_vars['invoiceemails']->value) {?> checked<?php }?> />
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemailsinvoice'];?>

        </label>
    </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="supportemails" id="supportemails" value="1"<?php if ($_smarty_tpl->tpl_vars['supportemails']->value) {?> checked<?php }?> />
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacontactsemailssupport'];?>

        </label>
    </div>
</div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <input class="btn btn-primary" type="submit" name="save" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" />
  </div>
</div>
</form>
<?php }
}
