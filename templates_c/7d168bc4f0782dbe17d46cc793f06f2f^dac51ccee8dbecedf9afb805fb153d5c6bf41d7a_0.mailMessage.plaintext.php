<?php
/* Smarty version 3.1.29, created on 2018-08-10 06:02:06
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d2a5e506168_05387853',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533880926,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d2a5e506168_05387853 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო<?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


მადლობა <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
ს ანგარიშის შექმნისთვის.


ეწვიეთ ქვემოთ მოცემულ ბმულს და შედით თქვენს ანგარიშში, რათა დაადასტუროთ ელ.ფოსტა და დაასრულოთ რეგისტრაცია.


<?php echo $_smarty_tpl->tpl_vars['client_email_verification_link']->value;?>



ეს წერილი თქვენ გამოგეგზავნათ, რადგან ახლახანს ამ ელ.ფოსტით ანგარიში შეიქმნა ან შეიცვალა. თუკი ეს თქვენ არ გაგიკეთებიათ - დაგვიკავშირდით.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
