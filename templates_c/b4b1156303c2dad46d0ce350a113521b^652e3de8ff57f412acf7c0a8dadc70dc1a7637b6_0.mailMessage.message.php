<?php
/* Smarty version 3.1.29, created on 2018-08-09 08:48:46
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6bffeeee14f8_95446644',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533804526,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6bffeeee14f8_95446644 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email. The details of your ticket are shown below.</p>
<p>Subject: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>
<br /> Priority: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>
<br /> Status: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>
</p>
<p>You can view the ticket at any time at <?php echo $_smarty_tpl->tpl_vars['ticket_link']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
