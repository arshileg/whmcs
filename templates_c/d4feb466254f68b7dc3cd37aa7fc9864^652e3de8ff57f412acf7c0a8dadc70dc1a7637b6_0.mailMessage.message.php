<?php
/* Smarty version 3.1.29, created on 2018-08-08 18:18:35
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b33fb150ad8_03441693',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533752315,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b33fb150ad8_03441693 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>We have received your order and will be processing it shortly. The details of the order are below:</p>
<p>Order Number: <strong><?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>
</strong></p>
<p><?php echo $_smarty_tpl->tpl_vars['order_details']->value;?>
</p>
<p>You will receive an email from us shortly once your account has been setup. Please quote your order reference number if you wish to contact us about this order.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
