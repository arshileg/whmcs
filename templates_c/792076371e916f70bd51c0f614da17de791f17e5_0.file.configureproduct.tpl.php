<?php
/* Smarty version 3.1.29, created on 2018-08-05 20:22:05
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/configureproduct.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b675c6d274cf3_89027168',
  'file_dependency' => 
  array (
    '792076371e916f70bd51c0f614da17de791f17e5' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/configureproduct.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/cart-header.tpl' => 1,
  ),
),false)) {
function content_5b675c6d274cf3_89027168 ($_smarty_tpl) {
if (!is_callable('smarty_function_math')) require_once '/home/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_modifier_replace')) require_once '/home/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/cart-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('desc'=>$_smarty_tpl->tpl_vars['LANG']->value['cartproductconfig'],'step'=>3), 0, true);
?>


<?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-12'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-12', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);?>

<?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = 0; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
if ($_smarty_tpl->tpl_vars['pricing']->value['monthly']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}
if ($_smarty_tpl->tpl_vars['pricing']->value['quarterly']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}
if ($_smarty_tpl->tpl_vars['pricing']->value['semiannually']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}
if ($_smarty_tpl->tpl_vars['pricing']->value['annually']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}
if ($_smarty_tpl->tpl_vars['pricing']->value['biennially']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}
if ($_smarty_tpl->tpl_vars['pricing']->value['triennially']) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcyclecount"])) {$_smarty_tpl->tpl_vars["billingcyclecount"] = clone $_smarty_tpl->tpl_vars["billingcyclecount"];
$_smarty_tpl->tpl_vars["billingcyclecount"]->value = $_smarty_tpl->tpl_vars['billingcyclecount']->value+1; $_smarty_tpl->tpl_vars["billingcyclecount"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcyclecount"] = new Smarty_Variable($_smarty_tpl->tpl_vars['billingcyclecount']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcyclecount", 0);
}?>

<?php if ($_smarty_tpl->tpl_vars['billingcyclecount']->value == 2) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-6'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-6', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);
}
if ($_smarty_tpl->tpl_vars['billingcyclecount']->value == 3) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-4'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-4', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);
}
if ($_smarty_tpl->tpl_vars['billingcyclecount']->value == 4) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-3'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-3', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);
}
if ($_smarty_tpl->tpl_vars['billingcyclecount']->value == 5) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-2'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-2', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);
}
if ($_smarty_tpl->tpl_vars['billingcyclecount']->value > 5) {?>
  <?php if (isset($_smarty_tpl->tpl_vars["billingcellsize"])) {$_smarty_tpl->tpl_vars["billingcellsize"] = clone $_smarty_tpl->tpl_vars["billingcellsize"];
$_smarty_tpl->tpl_vars["billingcellsize"]->value = 'col-sm-2'; $_smarty_tpl->tpl_vars["billingcellsize"]->nocache = null;
} else $_smarty_tpl->tpl_vars["billingcellsize"] = new Smarty_Variable('col-sm-2', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "billingcellsize", 0);
}?>

<?php if (isset($_smarty_tpl->tpl_vars["qmonthprice"])) {$_smarty_tpl->tpl_vars["qmonthprice"] = clone $_smarty_tpl->tpl_vars["qmonthprice"];
$_smarty_tpl->tpl_vars["qmonthprice"]->value = 0; $_smarty_tpl->tpl_vars["qmonthprice"]->nocache = null;
} else $_smarty_tpl->tpl_vars["qmonthprice"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "qmonthprice", 0);
echo smarty_function_math(array('assign'=>"qmonthprice",'equation'=>"d / b",'b'=>3,'d'=>$_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['quarterly'],'format'=>"%.2f"),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars["smonthprice"])) {$_smarty_tpl->tpl_vars["smonthprice"] = clone $_smarty_tpl->tpl_vars["smonthprice"];
$_smarty_tpl->tpl_vars["smonthprice"]->value = 0; $_smarty_tpl->tpl_vars["smonthprice"]->nocache = null;
} else $_smarty_tpl->tpl_vars["smonthprice"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "smonthprice", 0);
echo smarty_function_math(array('assign'=>"smonthprice",'equation'=>"d / b",'b'=>6,'d'=>$_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['semiannually'],'format'=>"%.2f"),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars["amonthprice"])) {$_smarty_tpl->tpl_vars["amonthprice"] = clone $_smarty_tpl->tpl_vars["amonthprice"];
$_smarty_tpl->tpl_vars["amonthprice"]->value = 0; $_smarty_tpl->tpl_vars["amonthprice"]->nocache = null;
} else $_smarty_tpl->tpl_vars["amonthprice"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "amonthprice", 0);
echo smarty_function_math(array('assign'=>"amonthprice",'equation'=>"d / b",'b'=>12,'d'=>$_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['annually'],'format'=>"%.2f"),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars["bmonthprice"])) {$_smarty_tpl->tpl_vars["bmonthprice"] = clone $_smarty_tpl->tpl_vars["bmonthprice"];
$_smarty_tpl->tpl_vars["bmonthprice"]->value = 0; $_smarty_tpl->tpl_vars["bmonthprice"]->nocache = null;
} else $_smarty_tpl->tpl_vars["bmonthprice"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "bmonthprice", 0);
echo smarty_function_math(array('assign'=>"bmonthprice",'equation'=>"d / b",'b'=>24,'d'=>$_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['biennially'],'format'=>"%.2f"),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars["tmonthprice"])) {$_smarty_tpl->tpl_vars["tmonthprice"] = clone $_smarty_tpl->tpl_vars["tmonthprice"];
$_smarty_tpl->tpl_vars["tmonthprice"]->value = 0; $_smarty_tpl->tpl_vars["tmonthprice"]->nocache = null;
} else $_smarty_tpl->tpl_vars["tmonthprice"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "tmonthprice", 0);
echo smarty_function_math(array('assign'=>"tmonthprice",'equation'=>"d / b",'b'=>36,'d'=>$_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['triennially'],'format'=>"%.2f"),$_smarty_tpl);?>


<?php if (isset($_smarty_tpl->tpl_vars["monthly_bill"])) {$_smarty_tpl->tpl_vars["monthly_bill"] = clone $_smarty_tpl->tpl_vars["monthly_bill"];
$_smarty_tpl->tpl_vars["monthly_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['monthly']; $_smarty_tpl->tpl_vars["monthly_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["monthly_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['monthly'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "monthly_bill", 0);
if (isset($_smarty_tpl->tpl_vars["quarterly_bill"])) {$_smarty_tpl->tpl_vars["quarterly_bill"] = clone $_smarty_tpl->tpl_vars["quarterly_bill"];
$_smarty_tpl->tpl_vars["quarterly_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['quarterly']; $_smarty_tpl->tpl_vars["quarterly_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["quarterly_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['quarterly'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "quarterly_bill", 0);
if (isset($_smarty_tpl->tpl_vars["semiannually_bill"])) {$_smarty_tpl->tpl_vars["semiannually_bill"] = clone $_smarty_tpl->tpl_vars["semiannually_bill"];
$_smarty_tpl->tpl_vars["semiannually_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['semiannually']; $_smarty_tpl->tpl_vars["semiannually_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["semiannually_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['semiannually'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "semiannually_bill", 0);
if (isset($_smarty_tpl->tpl_vars["annually_bill"])) {$_smarty_tpl->tpl_vars["annually_bill"] = clone $_smarty_tpl->tpl_vars["annually_bill"];
$_smarty_tpl->tpl_vars["annually_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['annually']; $_smarty_tpl->tpl_vars["annually_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["annually_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['annually'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "annually_bill", 0);
if (isset($_smarty_tpl->tpl_vars["biennially_bill"])) {$_smarty_tpl->tpl_vars["biennially_bill"] = clone $_smarty_tpl->tpl_vars["biennially_bill"];
$_smarty_tpl->tpl_vars["biennially_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['biennially']; $_smarty_tpl->tpl_vars["biennially_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["biennially_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['biennially'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "biennially_bill", 0);
if (isset($_smarty_tpl->tpl_vars["triennially_bill"])) {$_smarty_tpl->tpl_vars["triennially_bill"] = clone $_smarty_tpl->tpl_vars["triennially_bill"];
$_smarty_tpl->tpl_vars["triennially_bill"]->value = $_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['triennially']; $_smarty_tpl->tpl_vars["triennially_bill"]->nocache = null;
} else $_smarty_tpl->tpl_vars["triennially_bill"] = new Smarty_Variable($_smarty_tpl->tpl_vars['pricing']->value['rawpricing']['triennially'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "triennially_bill", 0);?>

<?php echo '<script'; ?>
>
var _localLang = {
  'addToCart': '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['LANG']->value['orderForm']['addToCart'], ENT_QUOTES, 'UTF-8', true);?>
',
  'addedToCartRemove': '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['LANG']->value['orderForm']['addedToCartRemove'], ENT_QUOTES, 'UTF-8', true);?>
'
}
<?php echo '</script'; ?>
>
<div class="row flowcart-row">
  <div class="col-md-12 ">

    <form id="frmConfigureProduct">
      <input type="hidden" name="configure" value="true" />
      <input type="hidden" name="i" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" />

      <div class="row">
        <div class="col-md-8">

          <div class="alert alert-danger hidden" role="alert" id="containerProductValidationErrors">
            <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['correctErrors'];?>
:</p>
            <ul id="containerProductValidationErrorsList"></ul>
          </div>

          <?php if ($_smarty_tpl->tpl_vars['pricing']->value['type'] == "recurring") {?>
          <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartchoosecycle'];?>
 - <?php echo $_smarty_tpl->tpl_vars['productinfo']->value['name'];?>
</h4>

            <!--billingcycle data start-->

            <div class="row" id="billingcycle_data">
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['monthly']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="monthly" id="sel-monthly" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "monthly") {?> checked<?php }?>>
            <label for="sel-monthly" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class="cal cal-monthly"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['monthly_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermmonthly'];?>
</span>
            </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['quarterly']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="quarterly" id="sel-quarterly" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "quarterly") {?> checked<?php }?>>
            <label for="sel-quarterly" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class=" cal cal-quarterly"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['quarterly_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermquarterly'];?>
</span>
              <span class="monthly-breakdown">(<?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo $_smarty_tpl->tpl_vars['qmonthprice']->value;
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currency']->value['suffix'],'USD','');?>
/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartmonth'];?>
)</span>
            </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['semiannually']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="semiannually" id="sel-semiannually" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "semiannually") {?> checked<?php }?>>
            <label for="sel-semiannually" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class="cal cal-semiannually"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['semiannually_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermsemiannually'];?>
</span>
              <span class="monthly-breakdown">(<?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo $_smarty_tpl->tpl_vars['smonthprice']->value;
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currency']->value['suffix'],'USD','');?>
/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartmonth'];?>
)</span>
            </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['annually']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="annually" id="sel-annually" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "annually") {?> checked<?php }?>>
            <label for="sel-annually" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class="cal cal-annually"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['annually_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermannually'];?>
</span>
              <span class="monthly-breakdown">(<?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo $_smarty_tpl->tpl_vars['amonthprice']->value;
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currency']->value['suffix'],'USD','');?>
/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartmonth'];?>
)</span>
            </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['biennially']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="biennially" id="sel-biennially" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "biennially") {?> checked<?php }?>>
            <label for="sel-biennially" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class="cal cal-biennially"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['biennially_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermbiennially'];?>
</span>
              <span class="monthly-breakdown">(<?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo $_smarty_tpl->tpl_vars['bmonthprice']->value;
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currency']->value['suffix'],'USD','');?>
/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartmonth'];?>
)</span>
            </label>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['pricing']->value['triennially']) {?>
            <input type="radio" name="billingcycle" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" value="triennially" id="sel-triennially" class="cycle-1"<?php if ($_smarty_tpl->tpl_vars['billingcycle']->value == "triennially") {?> checked<?php }?>>
            <label for="sel-triennially" class="cycle-1 <?php echo $_smarty_tpl->tpl_vars['billingcellsize']->value;?>
 col-xs-6 radio"><div class="cal cal-triennially"><i class="fa fa-calendar-o fa-3x"></i></div><b><?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo number_format($_smarty_tpl->tpl_vars['triennially_bill']->value,2,".",",");?>
 <?php echo $_smarty_tpl->tpl_vars['currency']->value['suffix'];?>
</b><span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermtriennially'];?>
</span>
              <span class="monthly-breakdown">(<?php echo $_smarty_tpl->tpl_vars['currency']->value['prefix'];
echo $_smarty_tpl->tpl_vars['tmonthprice']->value;
echo smarty_modifier_replace($_smarty_tpl->tpl_vars['currency']->value['suffix'],'USD','');?>
/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['flowcartmonth'];?>
)</span>
            </label>
            <?php }?>
            </div>
            <?php }?>
            <!--billingcycle data end-->


        

          <?php if ($_smarty_tpl->tpl_vars['productinfo']->value['type'] == "server") {?>
          <div class="sub-heading">
            <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartconfigserver'];?>
</span>
          </div>

          <div class="field-container">

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputHostname"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverhostname'];?>
</label>
                  <input type="text" name="hostname" class="form-control" id="inputHostname" value="<?php echo $_smarty_tpl->tpl_vars['server']->value['hostname'];?>
" placeholder="servername.yourdomain.com">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputRootpw"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverrootpw'];?>
</label>
                  <input type="password" name="rootpw" class="form-control" id="inputRootpw" value="<?php echo $_smarty_tpl->tpl_vars['server']->value['rootpw'];?>
">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputNs1prefix"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverns1prefix'];?>
</label>
                  <input type="text" name="ns1prefix" class="form-control" id="inputNs1prefix" value="<?php echo $_smarty_tpl->tpl_vars['server']->value['ns1prefix'];?>
" placeholder="ns1">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputNs2prefix"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverns2prefix'];?>
</label>
                  <input type="text" name="ns2prefix" class="form-control" id="inputNs2prefix" value="<?php echo $_smarty_tpl->tpl_vars['server']->value['ns2prefix'];?>
" placeholder="ns2">
                </div>
              </div>
            </div>

          </div>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>
          <div class="sub-heading">
            <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderconfigpackage'];?>
</span>
          </div>
          <div class="product-configurable-options" id="productConfigurableOptions">
            <div class="row">
              <?php
$_from = $_smarty_tpl->tpl_vars['configurableoptions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_configoption_0_saved_item = isset($_smarty_tpl->tpl_vars['configoption']) ? $_smarty_tpl->tpl_vars['configoption'] : false;
$__foreach_configoption_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['configoption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['configoption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['configoption']->value) {
$_smarty_tpl->tpl_vars['configoption']->_loop = true;
$__foreach_configoption_0_saved_local_item = $_smarty_tpl->tpl_vars['configoption'];
?>
              <?php if ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 1) {?>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</label>
                  <select name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" id="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
" class="form-control">
                    <?php
$_from = $_smarty_tpl->tpl_vars['configoption']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_options_1_saved_item = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options'] : false;
$__foreach_options_1_saved_key = isset($_smarty_tpl->tpl_vars['num2']) ? $_smarty_tpl->tpl_vars['num2'] : false;
$_smarty_tpl->tpl_vars['options'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num2'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['options']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num2']->value => $_smarty_tpl->tpl_vars['options']->value) {
$_smarty_tpl->tpl_vars['options']->_loop = true;
$__foreach_options_1_saved_local_item = $_smarty_tpl->tpl_vars['options'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['options']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedvalue'] == $_smarty_tpl->tpl_vars['options']->value['id']) {?> selected="selected"<?php }?>>
                      <?php echo $_smarty_tpl->tpl_vars['options']->value['name'];?>

                    </option>
                    <?php
$_smarty_tpl->tpl_vars['options'] = $__foreach_options_1_saved_local_item;
}
if ($__foreach_options_1_saved_item) {
$_smarty_tpl->tpl_vars['options'] = $__foreach_options_1_saved_item;
}
if ($__foreach_options_1_saved_key) {
$_smarty_tpl->tpl_vars['num2'] = $__foreach_options_1_saved_key;
}
?>
                  </select>
                </div>
              </div>
              <?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 2) {?>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</label>
                  <?php
$_from = $_smarty_tpl->tpl_vars['configoption']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_options_2_saved_item = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options'] : false;
$__foreach_options_2_saved_key = isset($_smarty_tpl->tpl_vars['num2']) ? $_smarty_tpl->tpl_vars['num2'] : false;
$_smarty_tpl->tpl_vars['options'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num2'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['options']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num2']->value => $_smarty_tpl->tpl_vars['options']->value) {
$_smarty_tpl->tpl_vars['options']->_loop = true;
$__foreach_options_2_saved_local_item = $_smarty_tpl->tpl_vars['options'];
?>
                  <?php if ($_smarty_tpl->tpl_vars['flowcart_display_product_radio']->value == 'Inline') {?>
                  <div class="radio">
                  <label>
                    <input type="radio" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['options']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedvalue'] == $_smarty_tpl->tpl_vars['options']->value['id']) {?> checked="checked"<?php }?> />
                    <?php if ($_smarty_tpl->tpl_vars['options']->value['name']) {?>
                    <?php echo $_smarty_tpl->tpl_vars['options']->value['name'];?>

                    <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['enable'];?>

                    <?php }?>
                  </label>
                </div>
                  <?php } else { ?>
                  <label class="radio-inline">
                    <input type="radio" onchange="<?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>updateConfigurableOptions(<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
, this.value);<?php } else { ?>recalctotals();<?php }?>" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['options']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedvalue'] == $_smarty_tpl->tpl_vars['options']->value['id']) {?> checked="checked"<?php }?> />
                    <?php if ($_smarty_tpl->tpl_vars['options']->value['name']) {?>
                    <?php echo $_smarty_tpl->tpl_vars['options']->value['name'];?>

                    <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['enable'];?>

                    <?php }?>
                  </label>
                <?php }?>
                  <?php
$_smarty_tpl->tpl_vars['options'] = $__foreach_options_2_saved_local_item;
}
if ($__foreach_options_2_saved_item) {
$_smarty_tpl->tpl_vars['options'] = $__foreach_options_2_saved_item;
}
if ($__foreach_options_2_saved_key) {
$_smarty_tpl->tpl_vars['num2'] = $__foreach_options_2_saved_key;
}
?>
                </div>
              </div>
              <?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 3) {?>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</label>
                  <br />
                  <label>
                    <input type="checkbox" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" id="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
" value="1"<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {?> checked<?php }?> />
                    <?php if ($_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name']) {?>
                    <?php echo $_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name'];?>

                    <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['enable'];?>

                    <?php }?>
                  </label>
                </div>
              </div>
              <?php } elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 4) {?>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</label>
                  <?php if ($_smarty_tpl->tpl_vars['configoption']->value['qtymaximum']) {?>
                  <?php if (!$_smarty_tpl->tpl_vars['rangesliderincluded']->value) {?>
                  <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/ion.rangeSlider.min.js"><?php echo '</script'; ?>
>
                  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/ion.rangeSlider.css" rel="stylesheet">
                  <link href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/ion.rangeSlider.skinModern.css" rel="stylesheet">
                  <?php if (isset($_smarty_tpl->tpl_vars['rangesliderincluded'])) {$_smarty_tpl->tpl_vars['rangesliderincluded'] = clone $_smarty_tpl->tpl_vars['rangesliderincluded'];
$_smarty_tpl->tpl_vars['rangesliderincluded']->value = true; $_smarty_tpl->tpl_vars['rangesliderincluded']->nocache = null;
} else $_smarty_tpl->tpl_vars['rangesliderincluded'] = new Smarty_Variable(true, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rangesliderincluded', 0);?>
                  <?php }?>
                  <input type="text" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedqty'];
} else {
echo $_smarty_tpl->tpl_vars['configoption']->value['qtyminimum'];
}?>" id="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
" class="form-control" />
                  <?php echo '<script'; ?>
>
                  var sliderTimeoutId = null;
                  var sliderRangeDifference = <?php echo $_smarty_tpl->tpl_vars['configoption']->value['qtymaximum'];?>
 - <?php echo $_smarty_tpl->tpl_vars['configoption']->value['qtyminimum'];?>
;
                  // The largest size that looks nice on most screens.
                  var sliderStepThreshold = 25;
                  // Check if there are too many to display individually.
                  var setLargerMarkers = sliderRangeDifference > sliderStepThreshold;

                  jQuery("#inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
").ionRangeSlider({
                    min: <?php echo $_smarty_tpl->tpl_vars['configoption']->value['qtyminimum'];?>
,
                    max: <?php echo $_smarty_tpl->tpl_vars['configoption']->value['qtymaximum'];?>
,
                    grid: true,
                    grid_snap: setLargerMarkers ? false : true,
                    onChange: function() {
                      if (sliderTimeoutId) {
                        clearTimeout(sliderTimeoutId);
                      }

                      sliderTimeoutId = setTimeout(function() {
                        sliderTimeoutId = null;
                        recalctotals();
                      }, 250);
                    }
                  });
                  <?php echo '</script'; ?>
>
                  <?php } else { ?>
                  <div>
                    <input type="number" name="configoption[<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
]" value="<?php if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedqty'];
} else {
echo $_smarty_tpl->tpl_vars['configoption']->value['qtyminimum'];
}?>" id="inputConfigOption<?php echo $_smarty_tpl->tpl_vars['configoption']->value['id'];?>
" min="<?php echo $_smarty_tpl->tpl_vars['configoption']->value['qtyminimum'];?>
" onchange="recalctotals()" onkeyup="recalctotals()" class="form-control form-control-qty" />
                    <span class="form-control-static form-control-static-inline">
                      x <?php echo $_smarty_tpl->tpl_vars['configoption']->value['options'][0]['name'];?>

                    </span>
                  </div>
                  <?php }?>
                </div>
              </div>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['num']->value%2 != 0) {?>
            </div>
            <div class="row">
              <?php }?>
              <?php
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_0_saved_local_item;
}
if ($__foreach_configoption_0_saved_item) {
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_0_saved_item;
}
if ($__foreach_configoption_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_configoption_0_saved_key;
}
?>
            </div>
          </div>

          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['customfields']->value) {?>

          <div class="sub-heading">
            <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderadditionalrequiredinfo'];?>
</span>
          </div>

          <div class="field-container">
            <?php
$_from = $_smarty_tpl->tpl_vars['customfields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_customfield_3_saved_item = isset($_smarty_tpl->tpl_vars['customfield']) ? $_smarty_tpl->tpl_vars['customfield'] : false;
$_smarty_tpl->tpl_vars['customfield'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['customfield']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['customfield']->value) {
$_smarty_tpl->tpl_vars['customfield']->_loop = true;
$__foreach_customfield_3_saved_local_item = $_smarty_tpl->tpl_vars['customfield'];
?>
            <div class="form-group">
              <label for="customfield<?php echo $_smarty_tpl->tpl_vars['customfield']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['customfield']->value['name'];?>
</label>
              <?php echo $_smarty_tpl->tpl_vars['customfield']->value['input'];?>

              <?php if ($_smarty_tpl->tpl_vars['customfield']->value['description']) {?>
              <span class="field-help-text">
                <?php echo $_smarty_tpl->tpl_vars['customfield']->value['description'];?>

              </span>
              <?php }?>
            </div>
            <?php
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_3_saved_local_item;
}
if ($__foreach_customfield_3_saved_item) {
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_3_saved_item;
}
?>
          </div>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['addons']->value || count($_smarty_tpl->tpl_vars['addonsPromoOutput']->value) > 0) {?>
            <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartavailableaddons'];?>
</h4>

            <?php
$_from = $_smarty_tpl->tpl_vars['addonsPromoOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_4_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_4_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
                <div>
                    <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

                </div>
            <?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_4_saved_local_item;
}
if ($__foreach_output_4_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_4_saved_item;
}
?>

          <div class="row addon-products">
            <?php
$_from = $_smarty_tpl->tpl_vars['addons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_5_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_5_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
            <div class="col-sm-<?php if (count($_smarty_tpl->tpl_vars['addons']->value) > 1) {?>6<?php } else { ?>12<?php }?>">
              <div class="well panel-addons">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="addons[<?php echo $_smarty_tpl->tpl_vars['addon']->value['id'];?>
]"<?php if ($_smarty_tpl->tpl_vars['addon']->value['status']) {?> checked<?php }?> />
                    <?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>

                  </label>
                </div>
                  <?php echo $_smarty_tpl->tpl_vars['addon']->value['description'];?>
 <?php echo $_smarty_tpl->tpl_vars['addon']->value['pricing'];?>

              </div>
            </div>
            <?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_local_item;
}
if ($__foreach_addon_5_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_item;
}
?>
          </div>
          <?php }?>

        </div>
        <div class="col-md-4" id="scrollingPanelContainer">
          <div id="orderSummary">
            <div class="order-summary">
              <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersummary'];?>
 <span class="loader" id="orderSummaryLoader"><i class="fa fa-fw fa-refresh fa-spin"></i></span></h4>
              <div class="well well well-primary">
                <div class="summary-container" id="producttotal"></div>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" id="btnCompleteProductConfig" class="btn btn-primary btn-lg btn-block"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['continue'];?>
</button>
            </div>
          </div>

        </div>

      </div>

    </form>
  </div>
</div>

<?php echo '<script'; ?>
>recalctotals();<?php echo '</script'; ?>
>
<?php }
}
