<?php
/* Smarty version 3.1.29, created on 2018-08-01 21:05:33
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b61e85d6f25c7_85915846',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533143133,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b61e85d6f25c7_85915846 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>
Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,
</p>
<p>
Your order for <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>
 has now been activated. Please keep this message for your records.
</p>
<p>
Product/Service: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>
<br />
Payment Method: <?php echo $_smarty_tpl->tpl_vars['service_payment_method']->value;?>
<br />
Amount: <?php echo $_smarty_tpl->tpl_vars['service_recurring_amount']->value;?>
<br />
Billing Cycle: <?php echo $_smarty_tpl->tpl_vars['service_billing_cycle']->value;?>
<br />
Next Due Date: <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>

</p>
<p>
Thank you for choosing us.
</p>
<p>
<?php echo $_smarty_tpl->tpl_vars['signature']->value;?>

</p>
<?php }
}
