<?php
/* Smarty version 3.1.29, created on 2018-02-21 18:04:47
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewticket.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a8d7c7fd134b7_25389778',
  'file_dependency' => 
  array (
    'f33dc204ec464dd12376b8757001b24097e90ca5' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewticket.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a8d7c7fd134b7_25389778 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_regex_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.regex_replace.php';
if (!is_callable('smarty_modifier_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$template = $_smarty_tpl;
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "default"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("default", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
if (strstr($_smarty_tpl->tpl_vars['status']->value,"779500")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "success"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("success", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
} elseif (strstr($_smarty_tpl->tpl_vars['status']->value,"000000")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "primary"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("primary", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
} elseif (strstr($_smarty_tpl->tpl_vars['status']->value,"ff6600")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "warning"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("warning", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
} elseif (strstr($_smarty_tpl->tpl_vars['status']->value,"224488")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "info"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("info", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
} elseif (strstr($_smarty_tpl->tpl_vars['status']->value,"cc0000")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "danger"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("danger", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);
}?>

<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketinvalid'];?>
</p>
<?php } else { ?>

<h3 class="page-header"><?php echo (($_smarty_tpl->tpl_vars['LANG']->value['supportticketsviewticket']).(' #')).($_smarty_tpl->tpl_vars['tid']->value);?>
</h3>
<?php echo '<script'; ?>
>

$(document)
.on('change', '.btn-file :file', function() {
  var input = $(this),
  numFiles = input.get(0).files ? input.get(0).files.length : 1,
  label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});
$(document).ready( function() {
  $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
    var input = $(this).parents('.input-group').find(':text'),
    log = numFiles > 1 ? numFiles + ' files selected' : label;
    if( input.length ) {
      input.val(log);
    } else {
      if( log ) alert(log);
    }
  });
});

<?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
  <ul>
    <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

  </ul>
</div>
<?php }?>

<div class="alert alert-<?php echo $_smarty_tpl->tpl_vars['label']->value;?>
 alert-block">
  <div class="alert-watermark m-1">
    <i class="fa fa-ticket fa-5x <?php if ($_smarty_tpl->tpl_vars['label']->value != 'default') {?>fa-inverse<?php }?>"></i>
  </div>
  <h4><?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
</h4>
    <span class="mr-2"><span aria-hidden="true" class="icon icon-event"></span> <?php echo $_smarty_tpl->tpl_vars['date']->value;?>
</span>
    <span class="mr-2"><span aria-hidden="true" class="icon icon-people"></span>  <?php echo $_smarty_tpl->tpl_vars['department']->value;?>
 </span>
    <span class="mr-2"><span aria-hidden="true" class="icon icon-fire"></span>  <?php echo smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['urgency']->value,"/(<span>|<span [^>]*>|<\\/span>)/",'');?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketspriority'];?>
</span>
    <span class="mr-0"><span aria-hidden="true" class="icon icon-tag"></span> <?php echo smarty_modifier_replace(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['status']->value),' ','');?>
 <?php if ($_smarty_tpl->tpl_vars['showclosebutton']->value) {?> (<a style="text-transform: lowercase;" href='<?php echo $_SERVER['PHP_SELF'];?>
?tid=<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
&amp;closeticket=true'><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsclose'];?>
</a>) <?php }?></span>

  </div>

  <?php if ($_smarty_tpl->tpl_vars['customfields']->value) {?>
  <table class="table">
    <?php
$_from = $_smarty_tpl->tpl_vars['customfields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_customfield_0_saved_item = isset($_smarty_tpl->tpl_vars['customfield']) ? $_smarty_tpl->tpl_vars['customfield'] : false;
$_smarty_tpl->tpl_vars['customfield'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['customfield']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['customfield']->value) {
$_smarty_tpl->tpl_vars['customfield']->_loop = true;
$__foreach_customfield_0_saved_local_item = $_smarty_tpl->tpl_vars['customfield'];
?>
    <tr><td><?php echo $_smarty_tpl->tpl_vars['customfield']->value['name'];?>
:</td><td><?php echo $_smarty_tpl->tpl_vars['customfield']->value['value'];?>
</td></tr>
    <?php
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_0_saved_local_item;
}
if ($__foreach_customfield_0_saved_item) {
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_0_saved_item;
}
?>
  </table>
  <?php }?>
<div class="btn-toolbar px-1 pb-2" role="toolbar"><button type="button" onclick="jQuery('#replycont').slideToggle()" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsreply'];?>
</button></div>
  <div id="replycont" class="pb-2 clearfix <?php if (!$_GET['postreply']) {?> subhide<?php }?>">
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?tid=<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
&amp;postreply=true" enctype="multipart/form-data" class="form-stacked">
            <div class="row <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>subhidden<?php }?>">
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?><input class="form-control disabled input-sm" type="text" id="name" value="<?php echo $_smarty_tpl->tpl_vars['clientname']->value;?>
" disabled="disabled" /><?php } else { ?><input class="input-sm form-control" type="text" name="replyname" id="name" value="<?php echo $_smarty_tpl->tpl_vars['replyname']->value;?>
" /><?php }?>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?><input class="form-control disabled input-sm" type="text" id="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" disabled="disabled" /><?php } else { ?><input class="input-sm form-control" type="text" name="replyemail" id="email" value="<?php echo $_smarty_tpl->tpl_vars['replyemail']->value;?>
" /><?php }?></div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <textarea name="replymessage" id="message" class="form-control markdown-editor" data-auto-save-name="client_ticket_reply_<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['contactmessage'];?>
" rows="6"><?php echo $_smarty_tpl->tpl_vars['replymessage']->value;?>
</textarea>
                </div>
              </div>
              <div id="fileuploads" class="px-1 py-1">
                <div class="input-group input-group-sm">
                  <span class="input-group-btn">
                    <span class="btn btn-default btn-sm btn-file">
                      <span class="glyphicon glyphicon-folder-open"></span> <input type="file" name="attachments[]" multiple="">
                    </span>
                  </span>
                  <input type="text" class="form-control" readonly="">
                </div>
              </div>
              <span class="help-block px-1"><a href="#" class="btn btn-xs btn-default" onclick="extraTicketAttachment();return false"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['addmore'];?>
</a> <small> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsallowedextensions'];?>
: <?php echo $_smarty_tpl->tpl_vars['allowedfiletypes']->value;?>
</small></span>
            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketsubmit'];?>
" class="btn btn-primary pull-right flip mx-1" />
      </form>
   </div>
    <div class="panel panel-default panel-tickets">
      <?php
$_from = $_smarty_tpl->tpl_vars['descreplies']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_reply_1_saved_item = isset($_smarty_tpl->tpl_vars['reply']) ? $_smarty_tpl->tpl_vars['reply'] : false;
$__foreach_reply_1_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['reply'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['reply']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['reply']->value) {
$_smarty_tpl->tpl_vars['reply']->_loop = true;
$__foreach_reply_1_saved_local_item = $_smarty_tpl->tpl_vars['reply'];
?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['reply']->value['admin']) {?>admin<?php } else { ?>client<?php }?>header panel-heading markdown-content">
        <div class="pull-right flip"><h6 style="margin-top:10px;"><i class="fa fa-clock-o"></i> <?php echo $_smarty_tpl->tpl_vars['reply']->value['date'];?>
</h6></div>
        <h4 class="panel-title"><?php if ($_smarty_tpl->tpl_vars['reply']->value['admin']) {?>
          <img src="https://www.gravatar.com/avatar/<?php if (isset($_smarty_tpl->tpl_vars['descreplies_reply_admin_email']->value[$_smarty_tpl->tpl_vars['reply']->value['id']])) {
echo md5(strtolower(trim($_smarty_tpl->tpl_vars['descreplies_reply_admin_email']->value[$_smarty_tpl->tpl_vars['reply']->value['id']])));
} else {
echo md5(strtolower(trim($_smarty_tpl->tpl_vars['reply']->value['email'])));
}?>?s=30&d=mm" class="mr-1" alt=""><?php echo $_smarty_tpl->tpl_vars['reply']->value['name'];?>
 <span class="badge badge-warning"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsstaff'];?>
</span>
          <?php } elseif ($_smarty_tpl->tpl_vars['reply']->value['contactid']) {?>
          <?php echo $_smarty_tpl->tpl_vars['reply']->value['name'];?>
 <span class="badge"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketscontact'];?>
</span>
          <?php } elseif ($_smarty_tpl->tpl_vars['reply']->value['userid']) {?>
          <img src="https://www.gravatar.com/avatar/<?php echo md5(strtolower(trim($_smarty_tpl->tpl_vars['reply']->value['email'])));?>
?s=30&d=mm" class="mr-1" alt=""><?php echo $_smarty_tpl->tpl_vars['reply']->value['name'];?>

          <?php } else { ?>
          <?php echo $_smarty_tpl->tpl_vars['reply']->value['name'];?>
 <span class="badge"><?php echo $_smarty_tpl->tpl_vars['reply']->value['email'];?>
</span>
          <?php }?></h4>
        </div>
        <div class="<?php if ($_smarty_tpl->tpl_vars['reply']->value['admin']) {?>admin<?php } else { ?>client<?php }?>msg panel-body">
          <p><?php echo $_smarty_tpl->tpl_vars['reply']->value['message'];?>
</p>
          <?php if ($_smarty_tpl->tpl_vars['reply']->value['attachments']) {?>
          <div class="well well-sm">
            <h4><span class="glyphicon glyphicon-floppy-disk" style="color:#aaa;"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketattachments'];?>
:</h4>
            <?php
$_from = $_smarty_tpl->tpl_vars['reply']->value['attachments'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_attachment_2_saved_item = isset($_smarty_tpl->tpl_vars['attachment']) ? $_smarty_tpl->tpl_vars['attachment'] : false;
$__foreach_attachment_2_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['attachment'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['attachment']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['attachment']->value) {
$_smarty_tpl->tpl_vars['attachment']->_loop = true;
$__foreach_attachment_2_saved_local_item = $_smarty_tpl->tpl_vars['attachment'];
?>
            <a href="dl.php?type=<?php if ($_smarty_tpl->tpl_vars['reply']->value['id']) {?>ar&id=<?php echo $_smarty_tpl->tpl_vars['reply']->value['id'];
} else { ?>a&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;
}?>&i=<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['attachment']->value;?>
</a>
            <?php
$_smarty_tpl->tpl_vars['attachment'] = $__foreach_attachment_2_saved_local_item;
}
if ($__foreach_attachment_2_saved_item) {
$_smarty_tpl->tpl_vars['attachment'] = $__foreach_attachment_2_saved_item;
}
if ($__foreach_attachment_2_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_attachment_2_saved_key;
}
?>
          </div>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['reply']->value['id'] && $_smarty_tpl->tpl_vars['reply']->value['admin'] && $_smarty_tpl->tpl_vars['ratingenabled']->value) {?>
          <?php if ($_smarty_tpl->tpl_vars['reply']->value['rating']) {?>
          <table class="ticketrating" align="right">
            <tr>
              <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ticketreatinggiven'];?>
&nbsp;</td>
              <?php
$_from = $_smarty_tpl->tpl_vars['ratings']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rating_3_saved_item = isset($_smarty_tpl->tpl_vars['rating']) ? $_smarty_tpl->tpl_vars['rating'] : false;
$_smarty_tpl->tpl_vars['rating'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['rating']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['rating']->value) {
$_smarty_tpl->tpl_vars['rating']->_loop = true;
$__foreach_rating_3_saved_local_item = $_smarty_tpl->tpl_vars['rating'];
?>
              <td><span class="glyphicon glyphicon-star<?php if ($_smarty_tpl->tpl_vars['reply']->value['rating'] >= $_smarty_tpl->tpl_vars['rating']->value) {
} else { ?>-empty<?php }?>"></span></td>
              <?php
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_3_saved_local_item;
}
if ($__foreach_rating_3_saved_item) {
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_3_saved_item;
}
?>
            </tr>
          </table>
          <?php } else { ?>
          <table class="ticketrating" align="right">
            <tr onmouseout="rating_leave('rate<?php echo $_smarty_tpl->tpl_vars['reply']->value['id'];?>
')">
              <td><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ticketratingquestion'];?>
&nbsp;</td>
              <?php
$_from = $_smarty_tpl->tpl_vars['ratings']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rating_4_saved_item = isset($_smarty_tpl->tpl_vars['rating']) ? $_smarty_tpl->tpl_vars['rating'] : false;
$_smarty_tpl->tpl_vars['rating'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['rating']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['rating']->value) {
$_smarty_tpl->tpl_vars['rating']->_loop = true;
$__foreach_rating_4_saved_local_item = $_smarty_tpl->tpl_vars['rating'];
?>
              <td class="" id="rate<?php echo $_smarty_tpl->tpl_vars['reply']->value['id'];?>
_<?php echo $_smarty_tpl->tpl_vars['rating']->value;?>
" onmouseover="rating_hover(this.id)" onclick="rating_select('<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
',this.id)"><span class="glyphicon glyphicon-star-empty"></span></td>
              <?php
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_4_saved_local_item;
}
if ($__foreach_rating_4_saved_item) {
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_4_saved_item;
}
?>
            </tr>
          </table>
          <?php }?>
          <?php }?>
        </div>
        <?php
$_smarty_tpl->tpl_vars['reply'] = $__foreach_reply_1_saved_local_item;
}
if ($__foreach_reply_1_saved_item) {
$_smarty_tpl->tpl_vars['reply'] = $__foreach_reply_1_saved_item;
}
if ($__foreach_reply_1_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_reply_1_saved_key;
}
?>
      </div>
    <?php }
}
}
