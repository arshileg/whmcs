<?php
/* Smarty version 3.1.29, created on 2018-05-26 18:42:31
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b097257b7b784_27812780',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1527345751,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b097257b7b784_27812780 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ახლახანს, მომხმარებლის არიდან გამოიგზავნა პაროლის აღდგენის მოთხოვნა. თუკი ეს თქვენ არ მოგითხოვიათ, დააიგნორეთ წერილი.  ახალი პაროლი 2 საათში გამოუსადეგარი გახდება.


პაროლის აღსადგენად ეწვიეთ ქვემოთ მოცემულ ბმულს: 
<?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>



ზემოთ მოცემულ ბმულზე გადასვლის შემდეგ, თქვენ გექნებათ შესაძლებლობა აირჩიოთ ახალი პაროლი. 


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
