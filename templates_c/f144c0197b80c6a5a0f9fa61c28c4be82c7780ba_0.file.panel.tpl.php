<?php
/* Smarty version 3.1.29, created on 2018-03-07 10:50:15
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/includes/panel.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9f8ba75cb2f0_57098311',
  'file_dependency' => 
  array (
    'f144c0197b80c6a5a0f9fa61c28c4be82c7780ba' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/includes/panel.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9f8ba75cb2f0_57098311 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><div class="panel panel-<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
">
    <?php if (isset($_smarty_tpl->tpl_vars['headerTitle']->value)) {?>
        <div class="panel-heading">
            <h3 class="panel-title"><strong><?php echo $_smarty_tpl->tpl_vars['headerTitle']->value;?>
</strong></h3>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bodyContent']->value)) {?>
        <div class="panel-body<?php if (isset($_smarty_tpl->tpl_vars['bodyTextCenter']->value)) {?> text-center<?php }?>">
            <?php echo $_smarty_tpl->tpl_vars['bodyContent']->value;?>

        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['footerContent']->value)) {?>
        <div class="panel-footer<?php if (isset($_smarty_tpl->tpl_vars['footerTextCenter']->value)) {?> text-center<?php }?>">
            <?php echo $_smarty_tpl->tpl_vars['footerContent']->value;?>

        </div>
    <?php }?>
</div>
<?php }
}
