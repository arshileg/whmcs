<?php
/* Smarty version 3.1.29, created on 2018-08-03 06:08:30
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaproducts.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63f15ee5e611_16406118',
  'file_dependency' => 
  array (
    '7f6fc50ef4618b9ac49679269a64035dd5cd1e44' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaproducts.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63f15ee5e611_16406118 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"ServicesList",'filterColumn'=>"0",'noSortColumns'=>"4"), 0, true);
?>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableServicesList').removeClass('hidden').DataTable();
        <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'product') {?>
            table.order([1, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
'], [3, 'asc']);
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'amount' || $_smarty_tpl->tpl_vars['orderby']->value == 'billingcycle') {?>
            table.order(2, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'nextduedate') {?>
            table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'domainstatus') {?>
            table.order(3, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php }?>
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareaproducts'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareaproductsintro'],'icon'=>'layers'), 0, true);
?>


<div class="panel panel-default panel-datatable">

  <div class="panel-heading clearfix"> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('sidebar'=>$_smarty_tpl->tpl_vars['primarySidebar']->value), 0, true);
?>
</div>

    <table id="tableServicesList" class="table table-list hidden">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareastatus'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderproduct'];?>
</th>
                <th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaaddonpricing'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>
</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['services']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_service_0_saved_item = isset($_smarty_tpl->tpl_vars['service']) ? $_smarty_tpl->tpl_vars['service'] : false;
$__foreach_service_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['service'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['service']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['service']->value) {
$_smarty_tpl->tpl_vars['service']->_loop = true;
$__foreach_service_0_saved_local_item = $_smarty_tpl->tpl_vars['service'];
?>
                <tr>
                    <td><span class="label label-<?php echo strtolower($_smarty_tpl->tpl_vars['service']->value['status']);?>
"><?php echo $_smarty_tpl->tpl_vars['service']->value['statustext'];?>
</span></td>
                    <td><a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['service']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['service']->value['product'];?>
</a><?php if ($_smarty_tpl->tpl_vars['service']->value['domain']) {?><br><a href="http://<?php echo $_smarty_tpl->tpl_vars['service']->value['domain'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['service']->value['domain'];?>
</a><?php }?>
                    <ul class="cell-inner-list visible-sm visible-xs">
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderbillingcycle'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['service']->value['billingcycle'];?>
</li>
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>
</span> : <?php echo $_smarty_tpl->tpl_vars['service']->value['nextduedate'];?>
</li>
                    <li class="hidden-sm"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderprice'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['service']->value['amount'];?>
</li>
                    </ul>
                    </td>
                    <td data-order="<?php echo $_smarty_tpl->tpl_vars['service']->value['amountnum'];?>
" class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['service']->value['amount'];?>
<br><?php echo $_smarty_tpl->tpl_vars['service']->value['billingcycle'];?>
</td>
                    <td class="hidden-sm hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['service']->value['normalisedNextDueDate'];?>
</span><?php echo $_smarty_tpl->tpl_vars['service']->value['nextduedate'];?>
</td>
                    <td class="text-right"><a class="btn btn-link" href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['service']->value['id'];?>
"><i class="fa fa-chevron-right fa-lg pull-right flip"></i></a>
                    </td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_0_saved_local_item;
}
if ($__foreach_service_0_saved_item) {
$_smarty_tpl->tpl_vars['service'] = $__foreach_service_0_saved_item;
}
if ($__foreach_service_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_service_0_saved_key;
}
?>
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
    </div>
    <div class="clearfix"></div>
</div>
<?php }
}
