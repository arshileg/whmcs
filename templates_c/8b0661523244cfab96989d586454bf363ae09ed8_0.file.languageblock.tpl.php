<?php
/* Smarty version 3.1.29, created on 2018-08-03 04:41:52
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/languageblock.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63dd106f7ab6_89430615',
  'file_dependency' => 
  array (
    '8b0661523244cfab96989d586454bf363ae09ed8' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/languageblock.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63dd106f7ab6_89430615 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['languagechangeenabled']->value && count($_smarty_tpl->tpl_vars['locales']->value) > 1) {?>
<div class="visible-xs-block">
  <div class="col-md-4 col-md-offset-4">
    <div class="btn-group">
      <button type="button" class="btn btn-primary btn-sm btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'];?>
  <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <?php
$_from = $_smarty_tpl->tpl_vars['locales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_locale_0_saved_item = isset($_smarty_tpl->tpl_vars['locale']) ? $_smarty_tpl->tpl_vars['locale'] : false;
$_smarty_tpl->tpl_vars['locale'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['locale']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->_loop = true;
$__foreach_locale_0_saved_local_item = $_smarty_tpl->tpl_vars['locale'];
?>
        <?php if ($_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'] == $_smarty_tpl->tpl_vars['locale']->value['localisedName']) {?>
        <li><a class="active" href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
        <?php } else { ?>
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
" ><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
        <?php }?>
        <?php
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_local_item;
}
if ($__foreach_locale_0_saved_item) {
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_item;
}
?>
      </ul>
    </div>
  </div>
</div>
<?php }
}
}
