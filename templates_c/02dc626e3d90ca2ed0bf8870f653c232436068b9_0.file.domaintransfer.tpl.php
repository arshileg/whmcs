<?php
/* Smarty version 3.1.29, created on 2018-08-03 05:41:22
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/domaintransfer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63eb0247d557_62854717',
  'file_dependency' => 
  array (
    '02dc626e3d90ca2ed0bf8870f653c232436068b9' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/domaintransfer.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/cart-header.tpl' => 1,
  ),
),false)) {
function content_5b63eb0247d557_62854717 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/cart-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['transferdomain'],'desc'=>$_smarty_tpl->tpl_vars['groupname']->value,'step'=>2), 0, true);
?>

<div class="row flowcart-row">
  <div class="col-md-12">
    <h4>
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['transferdomain'];?>

    </h4>
    <p><?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.transferExtend'),$_smarty_tpl);?>
. <?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.extendExclusions'),$_smarty_tpl);?>
.</p>
    <form method="post" action="cart.php" id="frmDomainTransfer">
      <input type="hidden" name="a" value="addDomainTransfer">
      <div class="row">
        <div class="col-md-8">
            <div class="form-group">
              <label for="inputTransferDomain"><?php echo WHMCS\Smarty::langFunction(array('key'=>'domainname'),$_smarty_tpl);?>
</label>
              <input type="text" class="form-control" name="domain" id="inputTransferDomain" value="<?php echo $_smarty_tpl->tpl_vars['lookupTerm']->value;?>
" placeholder="<?php echo WHMCS\Smarty::langFunction(array('key'=>'yourdomainplaceholder'),$_smarty_tpl);?>
.<?php echo WHMCS\Smarty::langFunction(array('key'=>'yourtldplaceholder'),$_smarty_tpl);?>
" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="<?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.enterDomain'),$_smarty_tpl);?>
" />
            </div>
                    </div>
            <div class="col-md-4">
            <div class="form-group">
              <label for="inputAuthCode" style="width:100%;">
                <?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.authCode'),$_smarty_tpl);?>

              </label>
              <input type="text" class="form-control" name="epp" id="inputAuthCode" placeholder="<?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.authCodePlaceholder'),$_smarty_tpl);?>
" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="<?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.required'),$_smarty_tpl);?>
" />
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div id="transferUnavailable" class="alert alert-warning slim-alert text-center hidden"></div>
            <?php if ($_smarty_tpl->tpl_vars['captcha']->value) {?>
            <div class="captcha-container" id="captchaContainer">
              <?php if ($_smarty_tpl->tpl_vars['captcha']->value == "recaptcha") {?>
              <?php echo '<script'; ?>
 src="https://www.google.com/recaptcha/api.js" async defer><?php echo '</script'; ?>
>
              <div id="google-recaptcha" class="g-recaptcha recaptcha-transfer center-block" data-sitekey="<?php echo $_smarty_tpl->tpl_vars['reCaptchaPublicKey']->value;?>
" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="<?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.required'),$_smarty_tpl);?>
" ></div>
              <?php } else { ?>
              <div class="default-captcha">
                <p><?php echo WHMCS\Smarty::langFunction(array('key'=>"cartSimpleCaptcha"),$_smarty_tpl);?>
</p>
                <div>
                  <img id="inputCaptchaImage" src="includes/verifyimage.php" />
                  <input id="inputCaptcha" type="text" name="code" maxlength="5" class="form-control input-sm" data-toggle="tooltip" data-placement="right" data-trigger="manual" title="<?php echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.required'),$_smarty_tpl);?>
" />
                </div>
              </div>
              <?php }?>
            </div>
            <?php }?>
          <button type="submit" id="btnTransferDomain" class="btn btn-lg btn-primary btn-transfer">
            <span class="loader hidden" id="addTransferLoader">
              <i class="fa fa-fw fa-spinner fa-spin"></i>
            </span>
            <span id="addToCart"><?php echo WHMCS\Smarty::langFunction(array('key'=>"orderForm.addToCart"),$_smarty_tpl);?>
</span>
          </button>
        </div>
      </div>
    </form>
    <p class="t small"></p>
  </div>
</div>
<?php }
}
