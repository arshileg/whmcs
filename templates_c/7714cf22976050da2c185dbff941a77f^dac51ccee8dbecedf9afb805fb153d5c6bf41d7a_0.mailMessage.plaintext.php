<?php
/* Smarty version 3.1.29, created on 2018-05-25 20:27:06
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b08395acd6d77_84855640',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1527265626,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b08395acd6d77_84855640 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ინფორმაცია შეკვეთაზე


შეკვეთის ID: <?php echo $_smarty_tpl->tpl_vars['order_id']->value;?>

 შეკვეთის ნომერი: <?php echo $_smarty_tpl->tpl_vars['order_number']->value;?>

 თარიღი/დრო: <?php echo $_smarty_tpl->tpl_vars['order_date']->value;?>

 ინვოისის ნომერი: <?php echo $_smarty_tpl->tpl_vars['invoice_id']->value;?>

 Payment Method: <?php echo $_smarty_tpl->tpl_vars['order_payment_method']->value;?>



ინფორმაცია კლიენტზე


კლიენტის ID: <?php echo $_smarty_tpl->tpl_vars['client_id']->value;?>

 სახელი/გვარი: <?php echo $_smarty_tpl->tpl_vars['client_first_name']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['client_last_name']->value;?>

 ელ.ფოსტა: <?php echo $_smarty_tpl->tpl_vars['client_email']->value;?>

 კომპანია: <?php echo $_smarty_tpl->tpl_vars['client_company_name']->value;?>

 მისამართი 1: <?php echo $_smarty_tpl->tpl_vars['client_address1']->value;?>

მისამართი 2: <?php echo $_smarty_tpl->tpl_vars['client_address2']->value;?>

 ქალაქი: <?php echo $_smarty_tpl->tpl_vars['client_city']->value;?>

 რეგიონი: <?php echo $_smarty_tpl->tpl_vars['client_state']->value;?>

 საფოსტო კოდი: <?php echo $_smarty_tpl->tpl_vars['client_postcode']->value;?>

 ქვეყანა: <?php echo $_smarty_tpl->tpl_vars['client_country']->value;?>

 ტელეფონის ნომერი: <?php echo $_smarty_tpl->tpl_vars['client_phonenumber']->value;?>



შეკვეთილი პროდუქტები


<?php echo $_smarty_tpl->tpl_vars['order_items']->value;?>



<?php if ($_smarty_tpl->tpl_vars['order_notes']->value) {?>
შეკვეთის ჩანაწერი


<?php echo $_smarty_tpl->tpl_vars['order_notes']->value;?>



<?php }?>
ISP ინფორმაცია


IP: <?php echo $_smarty_tpl->tpl_vars['client_ip']->value;?>

 ჰოსტი: <?php echo $_smarty_tpl->tpl_vars['client_hostname']->value;?>



<?php echo $_smarty_tpl->tpl_vars['whmcs_admin_url']->value;?>
orders.php?action=view&id=<?php echo $_smarty_tpl->tpl_vars['order_id']->value;
}
}
