<?php
/* Smarty version 3.1.29, created on 2018-08-03 06:22:17
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareainvoices.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63f499030e92_25282977',
  'file_dependency' => 
  array (
    'ea79c6bfe26144b0711573a8dc56d549c9d49d3f' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareainvoices.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63f499030e92_25282977 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['invoices'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['invoicesintro'],'icon'=>'drawer'), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"InvoicesList",'filterColumn'=>"4",'noSortColumns'=>"5"), 0, true);
?>

<div class="alert alert-info mb-0">
<?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesoutstandingbalance'];?>
: <?php echo $_smarty_tpl->tpl_vars['totalbalance']->value;?>

<?php if ($_smarty_tpl->tpl_vars['masspay']->value) {?>
<div class="pull-right flip"><a href="clientarea.php?action=masspay&all=true" class="btn btn-link"><span class="glyphicon glyphicon-ok-circle"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['masspayall'];?>
</a></div>
<?php }?>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableInvoicesList').removeClass('hidden').DataTable();
        <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'default') {?>
            table.order([4, 'desc'], [2, 'asc']);
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'invoicenum') {?>
            table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'date') {?>
            table.order(1, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'duedate') {?>
            table.order(2, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'total') {?>
            table.order(3, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'status') {?>
            table.order(4, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php }?>
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
<?php echo '</script'; ?>
>

<div class="table-container clearfix">
    <div class="panel panel-default panel-datatable">
    <div class="panel-heading clearfix"> <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('sidebar'=>$_smarty_tpl->tpl_vars['primarySidebar']->value), 0, true);
?>
</div>
    <table id="tableInvoicesList" class="table table-list hidden">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestitle'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatecreated'];?>
</th>
                <th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatedue'];?>
</th>
                <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestotal'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesstatus'];?>
</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['invoices']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_invoice_0_saved_item = isset($_smarty_tpl->tpl_vars['invoice']) ? $_smarty_tpl->tpl_vars['invoice'] : false;
$__foreach_invoice_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['invoice'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['invoice']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['invoice']->value) {
$_smarty_tpl->tpl_vars['invoice']->_loop = true;
$__foreach_invoice_0_saved_local_item = $_smarty_tpl->tpl_vars['invoice'];
?>
                <tr>
                    <td><a href="viewinvoice.php?id=<?php echo $_smarty_tpl->tpl_vars['invoice']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['invoicenum'];?>
</a>

                    <ul class="cell-inner-list visible-sm visible-xs small">
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestotal'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['total'];?>
</li>
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatecreated'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datecreated'];?>
</li>
                    <li class="hidden-sm"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatedue'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datedue'];?>
</li>
                    </ul>
                    </td>

                    <td class="hidden-sm hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['normalisedDateCreated'];?>
</span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datecreated'];?>
</td>
                    <td class="hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['normalisedDateDue'];?>
</span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datedue'];?>
</td>
                    <td data-order="<?php echo $_smarty_tpl->tpl_vars['invoice']->value['totalnum'];?>
" class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['total'];?>
</td>
                    <td><span class="label label-<?php echo $_smarty_tpl->tpl_vars['invoice']->value['statusClass'];?>
"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['status'];?>
</span></td>
                    <td class="text-right"><a href="viewinvoice.php?id=<?php echo $_smarty_tpl->tpl_vars['invoice']->value['id'];?>
" class="btn btn-link">
                     <i class="fa fa-chevron-right fa-lg pull-right flip"></i>
                        </a>
                    </td>
                </tr>
            <?php
$_smarty_tpl->tpl_vars['invoice'] = $__foreach_invoice_0_saved_local_item;
}
if ($__foreach_invoice_0_saved_item) {
$_smarty_tpl->tpl_vars['invoice'] = $__foreach_invoice_0_saved_item;
}
if ($__foreach_invoice_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_invoice_0_saved_key;
}
?>
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
    </div>
    </div>
</div>
<?php }
}
