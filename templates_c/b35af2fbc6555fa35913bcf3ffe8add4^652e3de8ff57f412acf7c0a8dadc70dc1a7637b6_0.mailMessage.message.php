<?php
/* Smarty version 3.1.29, created on 2018-08-06 09:40:13
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b68177dd8f8a8_78239086',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533548413,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b68177dd8f8a8_78239086 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო<?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
, <br /><br />ეს გახლავთ გადახდის ჩეკი შემდეგი ინვოისისთვის ნომრით # <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
, რომელიც დაგენერირდა შემდეგი თარიღით <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
<br /><br /> <?php echo $_smarty_tpl->tpl_vars['invoice_html_contents']->value;?>
 <br /><br />თანხის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_last_payment_amount']->value;?>
 <br />ტრანზაქციის #: <?php echo $_smarty_tpl->tpl_vars['invoice_last_payment_transid']->value;?>
 <br />სულ გადახდილია: <?php echo $_smarty_tpl->tpl_vars['invoice_amount_paid']->value;?>
 <br />დარჩენილი ბალანსი: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>
 <br />სტატუსი: <?php echo $_smarty_tpl->tpl_vars['invoice_status']->value;?>
 <br /><br />შეგიძლიათ ნებისმიერ დროს მიმოიხილოთ თქვენი ინვოისები მომხმარებლის არეში შესვლით. <br /><br />შენიშვნა: ეს გახლავთ გადახდის დამადასტურებელი ოფიციალური წერილ <?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
