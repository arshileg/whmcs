<?php
/* Smarty version 3.1.29, created on 2018-07-31 17:45:30
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/contactaccessdenied.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6067fa9ce153_75329168',
  'file_dependency' => 
  array (
    '7f697918cadbd0333b009cd044d423576e0b887f' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/contactaccessdenied.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6067fa9ce153_75329168 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><div class="row px-1">
<div class="col-md-12">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['accessdenied']), 0, true);
?>

<div class="alert alert-danger">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['subaccountpermissiondenied'];?>
</p>
</div>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['subaccountallowedperms'];?>
</p>
<ul>
<?php
$_from = $_smarty_tpl->tpl_vars['allowedpermissions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_permission_0_saved_item = isset($_smarty_tpl->tpl_vars['permission']) ? $_smarty_tpl->tpl_vars['permission'] : false;
$_smarty_tpl->tpl_vars['permission'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['permission']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['permission']->value) {
$_smarty_tpl->tpl_vars['permission']->_loop = true;
$__foreach_permission_0_saved_local_item = $_smarty_tpl->tpl_vars['permission'];
?>
<li><?php echo $_smarty_tpl->tpl_vars['permission']->value;?>
</li>
<?php
$_smarty_tpl->tpl_vars['permission'] = $__foreach_permission_0_saved_local_item;
}
if ($__foreach_permission_0_saved_item) {
$_smarty_tpl->tpl_vars['permission'] = $__foreach_permission_0_saved_item;
}
?>
</ul>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['subaccountcontactmaster'];?>
</p>
<p class="textcenter"><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" onclick="history.go(-1)" class="btn btn-default btn-sm" /></p>
</div>
</div>
<?php }
}
