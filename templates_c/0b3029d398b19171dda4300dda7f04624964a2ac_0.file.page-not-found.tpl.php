<?php
/* Smarty version 3.1.29, created on 2018-08-03 23:27:15
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/error/page-not-found.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b64e4d39845b8_06090913',
  'file_dependency' => 
  array (
    '0b3029d398b19171dda4300dda7f04624964a2ac' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/error/page-not-found.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b64e4d39845b8_06090913 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>    <div class="alert alert-danger">
      <h4><i class="fa fa-warning"></i> <?php echo WHMCS\Smarty::langFunction(array('key'=>"errorPage.404.title"),$_smarty_tpl);?>
 <?php echo WHMCS\Smarty::langFunction(array('key'=>"errorPage.404.subtitle"),$_smarty_tpl);?>
</h4>
        <p><?php echo WHMCS\Smarty::langFunction(array('key'=>"errorPage.404.description"),$_smarty_tpl);?>
</p>
        <div class="buttons">
            <a href="<?php echo $_smarty_tpl->tpl_vars['systemurl']->value;?>
" class="btn btn-default">
                <?php echo WHMCS\Smarty::langFunction(array('key'=>"errorPage.404.home"),$_smarty_tpl);?>

            </a>
            <a href="<?php echo $_smarty_tpl->tpl_vars['systemurl']->value;?>
submitticket.php" class="btn btn-default">
                <?php echo WHMCS\Smarty::langFunction(array('key'=>"errorPage.404.submitTicket"),$_smarty_tpl);?>

            </a>
        </div>
    </div>
<?php }
}
