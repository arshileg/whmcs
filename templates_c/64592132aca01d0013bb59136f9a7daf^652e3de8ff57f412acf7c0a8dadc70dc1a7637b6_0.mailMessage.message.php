<?php
/* Smarty version 3.1.29, created on 2018-07-24 17:06:06
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b57243ee819f3_06794750',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1532437566,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b57243ee819f3_06794750 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>
Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,
</p>
<p>
This message is to confirm that your domain purchase has been successful. The details of the domain purchase are below:
</p>
<p>
Registration Date: <?php echo $_smarty_tpl->tpl_vars['domain_reg_date']->value;?>
<br />
Domain: <?php echo $_smarty_tpl->tpl_vars['domain_name']->value;?>
<br />
Registration Period: <?php echo $_smarty_tpl->tpl_vars['domain_reg_period']->value;?>
<br />
Amount: <?php echo $_smarty_tpl->tpl_vars['domain_first_payment_amount']->value;?>
<br />
Next Due Date: <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>

</p>
<p>
You may login to your client area at <a href="<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
</a> to manage your new domain.
</p>
<p>
<?php echo $_smarty_tpl->tpl_vars['signature']->value;?>

</p><?php }
}
