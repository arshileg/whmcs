<?php
/* Smarty version 3.1.29, created on 2018-06-26 21:55:36
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/markdown-guide.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b327e18238f03_35044560',
  'file_dependency' => 
  array (
    '761bfff90195b5510ad616a11346049f37a3af16' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/markdown-guide.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b327e18238f03_35044560 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.emphasis'),$_smarty_tpl);?>
</h4>
        <pre>
**<strong><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.bold'),$_smarty_tpl);?>
</strong>**
*<em><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.italics'),$_smarty_tpl);?>
</em>*
~~<strike><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.strikeThrough'),$_smarty_tpl);?>
</strike>~~</pre>
        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.headers'),$_smarty_tpl);?>
</h4>
        <pre>
# <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.bigHeader'),$_smarty_tpl);?>

## <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.mediumHeader'),$_smarty_tpl);?>

### <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.smallHeader'),$_smarty_tpl);?>

#### <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.tinyHeader'),$_smarty_tpl);?>
</pre>

        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.lists'),$_smarty_tpl);?>
</h4>
        <pre>
* <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.genericListItem'),$_smarty_tpl);?>

* <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.genericListItem'),$_smarty_tpl);?>

* <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.genericListItem'),$_smarty_tpl);?>


1. <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.numberedListItem'),$_smarty_tpl);?>

2. <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.numberedListItem'),$_smarty_tpl);?>

3. <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.numberedListItem'),$_smarty_tpl);?>
</pre>

        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.links'),$_smarty_tpl);?>
</h4>
        <pre>[<?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.textToDisplay'),$_smarty_tpl);?>
](<?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.exampleLink'),$_smarty_tpl);?>
)</pre>

        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.quotes'),$_smarty_tpl);?>
</h4>
        <pre>
> <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.thisIsAQuote'),$_smarty_tpl);?>

> <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.quoteMultipleLines'),$_smarty_tpl);?>
</pre>

        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.tables'),$_smarty_tpl);?>
</h4>
        <pre>
| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnOne'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnTwo'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnThree'),$_smarty_tpl);?>
 |
| -------- | -------- | -------- |
| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.john'),$_smarty_tpl);?>
     | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.doe'),$_smarty_tpl);?>
      | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.male'),$_smarty_tpl);?>
     |
| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.mary'),$_smarty_tpl);?>
     | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.smith'),$_smarty_tpl);?>
    | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.female'),$_smarty_tpl);?>
   |

<em><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.withoutAligning'),$_smarty_tpl);?>
</em>

| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnOne'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnTwo'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.columnThree'),$_smarty_tpl);?>
 |
| -------- | -------- | -------- |
| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.john'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.doe'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.male'),$_smarty_tpl);?>
 |
| <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.mary'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.smith'),$_smarty_tpl);?>
 | <?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.female'),$_smarty_tpl);?>
 |</pre>

        <h4><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.displayingCode'),$_smarty_tpl);?>
</h4>
        <pre>
`var example = "hello!";`

<em><?php echo WHMCS\Smarty::langFunction(array('key'=>'markdown.spanningMultipleLines'),$_smarty_tpl);?>
</em>

```
var example = "hello!";
alert(example);
```</pre>
    </div>
</div>
<?php }
}
