<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:28:22
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/includes/verifyemail.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63afb67369d2_88728891',
  'file_dependency' => 
  array (
    'b326fc4eb2136b6a38c23171c77e722facf506f0' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/includes/verifyemail.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63afb67369d2_88728891 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['emailVerificationIdValid']->value) {?>
        <div class="alert alert-success alert-verify">
          <div class="container">
            <i class="fa fa-check"></i>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['emailAddressVerified'];?>

        </div>
        </div>
<?php } elseif ($_smarty_tpl->tpl_vars['emailVerificationIdValid']->value === false) {?>
            <div class="alert alert-danger alert-verify">
            <div class="container">
            <i class="fa fa-times-circle"></i>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['emailKeyExpired'];?>

            <div class="pull-right">
                <button id="btnResendVerificationEmail" class="btn btn-danger btn-xs">
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['resendEmail'];?>

                </button>
            </div>
    </div>
    </div>
<?php } elseif ($_smarty_tpl->tpl_vars['emailVerificationPending']->value && !$_smarty_tpl->tpl_vars['showingLoginPage']->value) {?>
            <div class="alert alert-warning alert-verify">
            <div class="container">
            <i class="fa fa-warning"></i>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['verifyEmailAddress'];?>

            <div class="pull-right">
                <button id="btnResendVerificationEmail" class="btn btn-warning btn-xs">
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['resendEmail'];?>

                </button>
            </div>
        </div>
        </div>
<?php }
}
}
