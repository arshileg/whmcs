<?php
/* Smarty version 3.1.29, created on 2018-02-21 02:34:14
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/pwstrength.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a8ca2662d7153_98248709',
  'file_dependency' => 
  array (
    '7167ea973423fdb9e13a0ed4c3e43794a59bec21' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/pwstrength.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a8ca2662d7153_98248709 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo '<script'; ?>
>
jQuery(document).ready(function(){
    jQuery("#password").keyup(function () {
        var pw = jQuery("#password").val();
        var pwlength=(pw.length);
        if(pwlength>5)pwlength=5;
        var numnumeric=pw.replace(/[0-9]/g,"");
        var numeric=(pw.length-numnumeric.length);
        if(numeric>3)numeric=3;
        var symbols=pw.replace(/\W/g,"");
        var numsymbols=(pw.length-symbols.length);
        if(numsymbols>3)numsymbols=3;
        var numupper=pw.replace(/[A-Z]/g,"");
        var upper=(pw.length-numupper.length);
        if(upper>3)upper=3;
        var pwstrength=((pwlength*10)-20)+(numeric*10)+(numsymbols*15)+(upper*10);
        if(pwstrength<0){pwstrength=0}
        if(pwstrength>100){pwstrength=100}
        jQuery("#pwstrengthbox").removeClass("weak moderate strong");
        jQuery("#pwstrengthbox").html("<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthstrong'];?>
");
        jQuery("#pwstrengthbox").addClass("strong");
        if (pwstrength<75) {
            jQuery("#pwstrengthbox").html("<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthmoderate'];?>
");
            jQuery("#pwstrengthbox").addClass("moderate");
        }
        if (pwstrength<30) {
            jQuery("#pwstrengthbox").html("<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthweak'];?>
");
            jQuery("#pwstrengthbox").addClass("weak");
        }
    });
});
<?php echo '</script'; ?>
>

<div id="pwstrengthbox"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrength'];?>
</div><?php }
}
