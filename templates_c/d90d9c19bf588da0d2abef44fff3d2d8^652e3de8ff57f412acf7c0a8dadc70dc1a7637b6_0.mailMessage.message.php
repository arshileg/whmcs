<?php
/* Smarty version 3.1.29, created on 2018-08-13 08:25:12
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b714068e6a948_51893929',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1534148712,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b714068e6a948_51893929 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ამ წერილით გატყობინებთ, რომ თქვენი მომსახურეობა შეწყვეტილია. იხილეთ დეტალები შეწყვეტის შესახებ:</p>
<p>პროდუქტი/სერვისი: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>
<br /><?php if ($_smarty_tpl->tpl_vars['service_domain']->value) {?>დომეინი: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
<br /><?php }?>ოდენობა: <?php echo $_smarty_tpl->tpl_vars['service_recurring_amount']->value;?>
<br />გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>
<br />მომსახურეობის შეწყვეტის მიზეზი: <strong><?php echo $_smarty_tpl->tpl_vars['service_suspension_reason']->value;?>
</strong></p>
<p>დაგვიკავშირდით, რაც შეიძლება მალე, რათა სერვისი კვლავ აღდგეს. </p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
