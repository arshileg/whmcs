<?php
/* Smarty version 3.1.29, created on 2018-08-06 09:45:51
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6818cff38a60_49406615',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533548751,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6818cff38a60_49406615 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>Your email to our ticket system could not be accepted because the ticket being responded to has already been closed.</p>
<p><?php if ($_smarty_tpl->tpl_vars['client_id']->value) {?>If you wish to reopen this ticket, you can do so from our client area:
<?php echo $_smarty_tpl->tpl_vars['ticket_link']->value;
} else { ?>To open a new ticket, please visit:</p>
<p><a href="<?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/submitticket.php"><?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>
/submitticket.php</a><?php }?></p>
<p>This is an automated response from our support system.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
