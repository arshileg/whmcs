<?php
/* Smarty version 3.1.29, created on 2018-02-21 16:14:32
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-customFields.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a8d62a841d0a7_61874531',
  'file_dependency' => 
  array (
    '443a3dee2819a22de31366be7c21c9565b76a5b8' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-customFields.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a8d62a841d0a7_61874531 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo '<script'; ?>
>
	
	$(function () { $("[data-toggle='tooltip']").tooltip(); });
	
<?php echo '</script'; ?>
>
<?php
$_from = $_smarty_tpl->tpl_vars['customfields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_customfield_0_saved_item = isset($_smarty_tpl->tpl_vars['customfield']) ? $_smarty_tpl->tpl_vars['customfield'] : false;
$__foreach_customfield_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['customfield'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['customfield']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['customfield']->value) {
$_smarty_tpl->tpl_vars['customfield']->_loop = true;
$__foreach_customfield_0_saved_local_item = $_smarty_tpl->tpl_vars['customfield'];
?>
<div class="form-group">
<label for="customfield<?php echo $_smarty_tpl->tpl_vars['customfield']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['customfield']->value['name'];?>
 <a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['customfield']->value['description'];?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['customfield']->value['description'];?>
"><span class="glyphicon glyphicon-question-sign"></span></a></label> 
<?php echo $_smarty_tpl->tpl_vars['customfield']->value['input'];?>
						
</div>
<?php
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_0_saved_local_item;
}
if ($__foreach_customfield_0_saved_item) {
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_0_saved_item;
}
if ($__foreach_customfield_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_customfield_0_saved_key;
}
}
}
