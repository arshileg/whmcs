<?php
/* Smarty version 3.1.29, created on 2018-08-10 05:48:46
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d273e3f8638_75324481',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533880126,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d273e3f8638_75324481 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>ეს შეხსენებითი შეტყობინება ეხება შემდეგ თარიღით დაგენერირებულ ინვოისს:  <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
.</p>
<p>თქვენი არჩეული გადახდის მეთოდია: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>
</p>
<p>ინვოისი #<?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
<br /> გადასახადის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['invoice_total']->value;?>
<br /> გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>
</p>
<p><strong>ინვოისის პროდუქტები</strong></p>
<p><?php echo $_smarty_tpl->tpl_vars['invoice_html_contents']->value;?>
 <br /> ------------------------------------------------------</p>
<p>თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
