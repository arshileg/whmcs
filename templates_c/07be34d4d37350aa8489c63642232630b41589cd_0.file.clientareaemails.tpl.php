<?php
/* Smarty version 3.1.29, created on 2018-03-13 20:48:04
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareaemails.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5aa800c40bba57_53549647',
  'file_dependency' => 
  array (
    '07be34d4d37350aa8489c63642232630b41589cd' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareaemails.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5aa800c40bba57_53549647 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareaemails'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['emailstagline'],'icon'=>'envelope'), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"EmailsList",'noSortColumns'=>"-1"), 0, true);
?>

<?php echo '<script'; ?>
 type="text/javascript">
    jQuery(document).ready( function ()
    {
        var table = jQuery('#tableEmailsList').removeClass('hidden').DataTable();
        <?php if ($_smarty_tpl->tpl_vars['orderby']->value == 'date') {?>
            table.order(0, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php } elseif ($_smarty_tpl->tpl_vars['orderby']->value == 'subject') {?>
            table.order(1, '<?php echo $_smarty_tpl->tpl_vars['sort']->value;?>
');
        <?php }?>
        table.draw();
        jQuery('#tableLoading').addClass('hidden');
    });
<?php echo '</script'; ?>
>
<div class="table-container clearfix">
    <table id="tableEmailsList" class="table table-list hidden">
        <thead>
            <tr>
                <th class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaemailsdate'];?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaemailssubject'];?>
</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
$_from = $_smarty_tpl->tpl_vars['emails']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_email_0_saved_item = isset($_smarty_tpl->tpl_vars['email']) ? $_smarty_tpl->tpl_vars['email'] : false;
$_smarty_tpl->tpl_vars['email'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['email']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['email']->value) {
$_smarty_tpl->tpl_vars['email']->_loop = true;
$__foreach_email_0_saved_local_item = $_smarty_tpl->tpl_vars['email'];
?>
            <tr>
                <td class="hidden-xs"><span class="hidden"><?php echo $_smarty_tpl->tpl_vars['email']->value['normalisedDate'];?>
</span><?php echo $_smarty_tpl->tpl_vars['email']->value['date'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['email']->value['subject'];?>

                    <ul class="cell-inner-list visible-xs small">
                    <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaemailsdate'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['email']->value['date'];?>
</li>
                    </ul>

                </td>
                <td class="text-right"><a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['emailviewmessage'];?>
" href="#" onclick="popupWindow('viewemail.php?id=<?php echo $_smarty_tpl->tpl_vars['email']->value['id'];?>
','emlmsg',800,400);return false;"><i class="fa fa-external-link"> </i></a></td>
            </tr>
            <?php
$_smarty_tpl->tpl_vars['email'] = $__foreach_email_0_saved_local_item;
}
if ($__foreach_email_0_saved_item) {
$_smarty_tpl->tpl_vars['email'] = $__foreach_email_0_saved_item;
}
?>
        </tbody>
    </table>
    <div class="text-center" id="tableLoading">
        <p><i class="fa fa-spinner fa-spin"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['loading'];?>
</p>
    </div>
</div>
<?php }
}
