<?php
/* Smarty version 3.1.29, created on 2018-07-03 13:33:26
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b3b42e6a56020_92971735',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1530610406,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3b42e6a56020_92971735 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p><p>This email is to confirm that we have received your cancellation request for the service listed below.</p><p>Product/Service: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>
<br />Domain: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
</p><p><?php if ($_smarty_tpl->tpl_vars['service_cancellation_type']->value == "Immediate") {?>The service will be terminated within the next 24 hours.<?php } else { ?>The service will be cancelled at the end of your current billing period on <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>
.<?php }?></p><p>Thank you for using <?php echo $_smarty_tpl->tpl_vars['company_name']->value;?>
 and we hope to see you again in the future.</p><p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
