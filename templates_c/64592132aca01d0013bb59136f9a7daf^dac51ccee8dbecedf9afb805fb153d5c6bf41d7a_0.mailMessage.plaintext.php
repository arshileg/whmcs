<?php
/* Smarty version 3.1.29, created on 2018-07-24 17:06:06
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b57243ee8b1c7_53783138',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1532437566,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b57243ee8b1c7_53783138 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,

This message is to confirm that your domain purchase has been successful. The details of the domain purchase are below:

Registration Date: <?php echo $_smarty_tpl->tpl_vars['domain_reg_date']->value;?>

Domain: <?php echo $_smarty_tpl->tpl_vars['domain_name']->value;?>

Registration Period: <?php echo $_smarty_tpl->tpl_vars['domain_reg_period']->value;?>

Amount: <?php echo $_smarty_tpl->tpl_vars['domain_first_payment_amount']->value;?>

Next Due Date: <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>


You may login to your client area at <?php echo $_smarty_tpl->tpl_vars['whmcs_url']->value;?>


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
