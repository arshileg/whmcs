<?php
/* Smarty version 3.1.29, created on 2018-08-12 00:00:19
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6f789377ca09_11802509',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1534032019,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6f789377ca09_11802509 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>This is a notification to let you know that we are changing the status of your ticket #<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>
 to Closed as we have not received a response from you in over <?php echo $_smarty_tpl->tpl_vars['ticket_auto_close_time']->value;?>
 hours.</p>
<p>Subject: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>
<br />Department: <?php echo $_smarty_tpl->tpl_vars['ticket_department']->value;?>
<br />Priority: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>
<br />Status: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>
</p>
<p>If you have any further questions then please just reply to re-open the ticket.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
