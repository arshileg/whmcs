<?php
/* Smarty version 3.1.29, created on 2018-08-01 10:41:10
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6156061df2a1_87232889',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533105670,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6156061df2a1_87232889 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><span style="font-size: medium;">მოგესალმებით,<br /><br />გერმანიიდან კავკასუსის დათაცენტრში არსებულ ჩვენსავე სერვერზე პორტირება დასრულებულია.<br />ამ ცვლილებასთან ერთად საჭიროა რამდენიმე დეტალის გათვალისწინება:</span></p>
<p> </p>
<p><span style="font-size: medium;"><strong>- თუკი იყენებთ Cloudflare სისტემებს:<br /></strong><em>85.117.32.246 - მოცემული იპ მისამართით შეგიძლიათ ჩაასწოროთ ძველი.<br /><br /><strong>- თუკი იყენებთ გადახდის სისტემებს:<br /></strong>85.117.32.246 - მოცემული იპის გადაგზავნაა საჭირო ბანკის ტექნიკური ჯგუფისთვის, იპის "თეთრ სიაში" შესატანად და გადახდების დასაშვებად.<strong><br /><br /></strong></em></span></p>
<p><span style="color: #ff0000;"><strong><span style="font-size: medium;"><em>- თუკი საიტზე არ გიშვებთ ან 404 ერორს</em></span></strong><span style="font-size: medium;"><em> გიჩვენებთ:</em></span><span style="font-size: medium;"><em><br /></em></span><span style="font-size: medium;"><em>საჭიროა .htaccess ჩასწორება. თუკი თავად ვერ ახერხებთ, დაგვიკავშირდით და დაგეხმარებით.</em></span></span></p>
<p><br /><strong><em><span style="font-size: medium;">- მიგრაციის შემდგომ php ვერსია ყველა ანგარიშზე სტანდარტულ 5.6-ზე</span></em></strong><span style="font-size: medium;"><strong> დადგა:</strong><br /></span><em><span style="font-size: medium;">cPanel-ში MultiPHP Manager-ის საშუალებით შეძლებთ php ვერსიის შეცვლას.<br /><br /></span></em></p>
<p><em><span style="font-size: medium;">სხვა დამატებითი კითხვების მოგვმართეთ.<br /><br /></span></em></p>
<p><em><span style="font-size: medium;">პატივისცემით,<br />ჰოსტნოუდსის ტექნიკური ჯგუფი</span></em></p>
<p><span style="color: #ff0000;"><span style="font-size: medium;"><em> </em></span></span></p>
<p><span style="color: #ff0000;"><strong><span style="font-size: medium;"><em> </em></span></strong></span></p><?php }
}
