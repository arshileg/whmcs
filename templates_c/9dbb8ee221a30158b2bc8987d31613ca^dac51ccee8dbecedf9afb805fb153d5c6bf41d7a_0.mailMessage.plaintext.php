<?php
/* Smarty version 3.1.29, created on 2018-08-08 18:32:27
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b373bee2f04_82512985',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533753147,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b373bee2f04_82512985 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


Thank you for creating a <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
 account.


Please visit the link below and sign into your account to verify your email address and complete your registration.


<?php echo $_smarty_tpl->tpl_vars['client_email_verification_link']->value;?>



You are receiving this email because you recently created an account or changed your email address. If you did not do this, please contact us.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
