<?php
/* Smarty version 3.1.29, created on 2018-02-18 23:24:46
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewannouncement.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a89d2feefeaa2_54161634',
  'file_dependency' => 
  array (
    '7ad7adafd9e1d6f3f6f281a5d705ffc2c808d8d8' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewannouncement.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a89d2feefeaa2_54161634 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.date_format.php';
$template = $_smarty_tpl;
?><div class="well" style="px-1 py-2">
<h1 class="h3"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
<p><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['timestamp']->value,"%A, %B %e, %Y");?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['text']->value;?>
</p>
<a href="<?php echo routePath('announcement-index');?>
" class="btn btn-outline"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
</a>
</div>

<div class="row px-1">
  <div class="col-md-12">
  <div class="pull-right">
<?php if ($_smarty_tpl->tpl_vars['twittertweet']->value) {?>
<div style="float:left;margin-right:7px;">
        <a href="https://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="<?php echo $_smarty_tpl->tpl_vars['twitterusername']->value;?>
">Tweet</a><?php echo '<script'; ?>
 type="text/javascript" src="//platform.twitter.com/widgets.js"><?php echo '</script'; ?>
>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['googleplus1']->value) {?>

<?php echo '<script'; ?>
 src="https://apis.google.com/js/platform.js" async defer><?php echo '</script'; ?>
>
<div style="float:left;margin-right:7px;">
<div class="g-plusone" data-size="medium" data-annotation="none"></div>
</div>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['facebookrecommend']->value) {?>

<div style="float:left;">
<div id="fb-root"></div>
    <?php echo '<script'; ?>
>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
    
    <div class="fb-like" data-href="<?php echo fqdnRoutePath('announcement-view',$_smarty_tpl->tpl_vars['id']->value,$_smarty_tpl->tpl_vars['urlfriendlytitle']->value);?>
" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
</div>
<?php }?>
</div>
</div>
</div>


<?php if ($_smarty_tpl->tpl_vars['facebookcomments']->value) {?>
    
    <div id="fb-root">
    </div>
    <?php echo '<script'; ?>
>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
    
    <fb:comments href="<?php echo fqdnRoutePath('announcement-view',$_smarty_tpl->tpl_vars['id']->value,$_smarty_tpl->tpl_vars['urlfriendlytitle']->value);?>
" num_posts="5" data-width="100%"></fb:comments>
<?php }
}
}
