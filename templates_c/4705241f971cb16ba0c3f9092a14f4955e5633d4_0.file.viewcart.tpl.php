<?php
/* Smarty version 3.1.29, created on 2018-02-18 00:44:19
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/viewcart.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a889423a695b5_17560352',
  'file_dependency' => 
  array (
    '4705241f971cb16ba0c3f9092a14f4955e5633d4' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/viewcart.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/cart-header.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/checkout.tpl' => 1,
  ),
),false)) {
function content_5a889423a695b5_17560352 ($_smarty_tpl) {
$template = $_smarty_tpl;
echo '<script'; ?>
>
  if (window.location.href.indexOf('cart.php?a=view')!=-1){
     window.location = 'cart.php?a=checkout';
  }
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
// Define state tab index value
var statesTab = 10;
var stateNotRequired = true;
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/cart-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['rs_step4'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['rs_step4_desc'],'step'=>4,'pid'=>$_smarty_tpl->tpl_vars['products']->value[0]['pid']), 0, true);
?>


<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/StatesDropdown.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/PasswordStrength.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
window.langPasswordStrength = "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrength'];?>
";
window.langPasswordWeak = "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthweak'];?>
";
window.langPasswordModerate = "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthmoderate'];?>
";
window.langPasswordStrong = "<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthstrong'];?>
";
<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['promoerrormessage']->value) {?>
<div class="alert alert-warning text-center" role="alert">
<?php echo $_smarty_tpl->tpl_vars['promoerrormessage']->value;?>

</div>
<?php } elseif ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger" role="alert">
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['correctErrors'];?>
:</p>
<ul>
  <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

</ul>
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['promotioncode']->value && $_smarty_tpl->tpl_vars['rawdiscount']->value == "0.00") {?>
<div class="alert alert-info text-center" role="alert">
<?php echo $_smarty_tpl->tpl_vars['LANG']->value['promoappliedbutnodiscount'];?>

</div>
<?php } elseif ($_smarty_tpl->tpl_vars['promoaddedsuccess']->value) {?>
<div class="alert alert-success text-center" role="alert">
<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['promotionAccepted'];?>

</div>
<?php }
if ($_smarty_tpl->tpl_vars['bundlewarnings']->value) {?>
<div class="alert alert-warning" role="alert">
<strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bundlereqsnotmet'];?>
</strong><br />
<ul>
  <?php
$_from = $_smarty_tpl->tpl_vars['bundlewarnings']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_warning_0_saved_item = isset($_smarty_tpl->tpl_vars['warning']) ? $_smarty_tpl->tpl_vars['warning'] : false;
$_smarty_tpl->tpl_vars['warning'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['warning']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['warning']->value) {
$_smarty_tpl->tpl_vars['warning']->_loop = true;
$__foreach_warning_0_saved_local_item = $_smarty_tpl->tpl_vars['warning'];
?>
  <li><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</li>
  <?php
$_smarty_tpl->tpl_vars['warning'] = $__foreach_warning_0_saved_local_item;
}
if ($__foreach_warning_0_saved_item) {
$_smarty_tpl->tpl_vars['warning'] = $__foreach_warning_0_saved_item;
}
?>
</ul>
</div>
<?php }?>

<?php
$_from = $_smarty_tpl->tpl_vars['hookOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_1_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_1_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
    <div>
        <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

    </div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_local_item;
}
if ($__foreach_output_1_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_item;
}
?>

<div id="order-standard_cart">
<div class="row flowcart-row">
<div class="col-md-12">
  <div class="row">
    <div class="col-md-8">
      <?php
$_from = $_smarty_tpl->tpl_vars['gatewaysoutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gatewayoutput_2_saved_item = isset($_smarty_tpl->tpl_vars['gatewayoutput']) ? $_smarty_tpl->tpl_vars['gatewayoutput'] : false;
$_smarty_tpl->tpl_vars['gatewayoutput'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gatewayoutput']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['gatewayoutput']->value) {
$_smarty_tpl->tpl_vars['gatewayoutput']->_loop = true;
$__foreach_gatewayoutput_2_saved_local_item = $_smarty_tpl->tpl_vars['gatewayoutput'];
?>
      <div class="view-cart-gateway-checkout">
        <?php echo $_smarty_tpl->tpl_vars['gatewayoutput']->value;?>

      </div>
      <?php
$_smarty_tpl->tpl_vars['gatewayoutput'] = $__foreach_gatewayoutput_2_saved_local_item;
}
if ($__foreach_gatewayoutput_2_saved_item) {
$_smarty_tpl->tpl_vars['gatewayoutput'] = $__foreach_gatewayoutput_2_saved_item;
}
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/checkout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    </div>
    <div class="col-md-4" id="scrollingPanelContainer">
      <div class="order-summary" id="orderSummary">
      <!--ordersummary-->
        <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersummary'];?>
 <span class="loader" id="orderSummaryLoader"><i class="fa fa-fw fa-refresh fa-spin"></i></span> </h4>
        <div class="well well well-primary">
          <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?a=view">
            <div class="view-cart-items">
              <?php
$_from = $_smarty_tpl->tpl_vars['products']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_product_3_saved_item = isset($_smarty_tpl->tpl_vars['product']) ? $_smarty_tpl->tpl_vars['product'] : false;
$__foreach_product_3_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['product'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['product']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
$__foreach_product_3_saved_local_item = $_smarty_tpl->tpl_vars['product'];
?>
                  <div class="row my-2">
                    <div class="col-sm-9">
                        <h6><?php echo $_smarty_tpl->tpl_vars['product']->value['productinfo']['name'];?>
 <?php if ($_smarty_tpl->tpl_vars['product']->value['domain']) {?> - <?php echo $_smarty_tpl->tpl_vars['product']->value['domain'];
}?></h6>
                      <?php if ($_smarty_tpl->tpl_vars['product']->value['configoptions']) {?>
                      <small>
                        <?php
$_from = $_smarty_tpl->tpl_vars['product']->value['configoptions'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_configoption_4_saved_item = isset($_smarty_tpl->tpl_vars['configoption']) ? $_smarty_tpl->tpl_vars['configoption'] : false;
$__foreach_configoption_4_saved_key = isset($_smarty_tpl->tpl_vars['confnum']) ? $_smarty_tpl->tpl_vars['confnum'] : false;
$_smarty_tpl->tpl_vars['configoption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['confnum'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['configoption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['confnum']->value => $_smarty_tpl->tpl_vars['configoption']->value) {
$_smarty_tpl->tpl_vars['configoption']->_loop = true;
$__foreach_configoption_4_saved_local_item = $_smarty_tpl->tpl_vars['configoption'];
?>
                        &nbsp;&raquo; <?php echo $_smarty_tpl->tpl_vars['configoption']->value['name'];?>
: <?php if ($_smarty_tpl->tpl_vars['configoption']->value['type'] == 1 || $_smarty_tpl->tpl_vars['configoption']->value['type'] == 2) {
echo $_smarty_tpl->tpl_vars['configoption']->value['option'];
} elseif ($_smarty_tpl->tpl_vars['configoption']->value['type'] == 3) {
if ($_smarty_tpl->tpl_vars['configoption']->value['qty']) {
echo $_smarty_tpl->tpl_vars['configoption']->value['option'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['no'];
}
} elseif ($_smarty_tpl->tpl_vars['configoption']->value['type'] == 4) {
echo $_smarty_tpl->tpl_vars['configoption']->value['qty'];?>
 x <?php echo $_smarty_tpl->tpl_vars['configoption']->value['option'];
}?><br />
                        <?php
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_4_saved_local_item;
}
if ($__foreach_configoption_4_saved_item) {
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_4_saved_item;
}
if ($__foreach_configoption_4_saved_key) {
$_smarty_tpl->tpl_vars['confnum'] = $__foreach_configoption_4_saved_key;
}
?>
                      </small>
                      <?php }?>
                      <div class="item-price">
                        <span><?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['totalTodayExcludingTaxSetup'];?>
</span>
                        <span class="cycle"><?php echo $_smarty_tpl->tpl_vars['product']->value['billingcyclefriendly'];?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value['pricing']['productonlysetup']) {?>
                        <?php echo $_smarty_tpl->tpl_vars['product']->value['pricing']['productonlysetup']->toPrefixed();?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];?>

                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value['proratadate']) {?><br />(<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderprorata'];?>
 <?php echo $_smarty_tpl->tpl_vars['product']->value['proratadate'];?>
)<?php }?>
                      </div>
                    </div>
                    <div class="col-sm-3 col-xs-2 text-right">
                      <a href="<?php echo $_SERVER['PHP_SELF'];?>
?a=confproduct&i=<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" class="btn btn-xs btn-link">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                      </a>
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('p','<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
')">
                        <i class="fa fa-times text-danger"></i>
                      </button>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['showqtyoptions']->value) {?>
                    <div class="col-md-12 item-qty">
                      <?php if ($_smarty_tpl->tpl_vars['product']->value['allowqty']) {?>
                      <div class="input-group">
                      <input type="number" name="qty[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['product']->value['qty'];?>
" class="form-control" />
                           <span class="input-group-btn">
                             <button class="btn btn-default"type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                           </span>
                       </div>
                      <?php }?>
                    </div>
                    <?php }?>
                  </div>
              <?php
$_from = $_smarty_tpl->tpl_vars['product']->value['addons'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_5_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$__foreach_addon_5_saved_key = isset($_smarty_tpl->tpl_vars['addonnum']) ? $_smarty_tpl->tpl_vars['addonnum'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addonnum'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addonnum']->value => $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_5_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="small">
                      <span class="item-title">
                      <i class="fa fa-fw text-muted fa-puzzle-piece" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>

                      </span>
                      <?php if ($_smarty_tpl->tpl_vars['addon']->value['setup']) {?><span class="item-setup"><?php echo $_smarty_tpl->tpl_vars['addon']->value['setup'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];?>
</span><?php }?>
                      <span><?php echo $_smarty_tpl->tpl_vars['addon']->value['totaltoday'];?>
</span>
                      <span class="cycle"><?php echo $_smarty_tpl->tpl_vars['addon']->value['billingcyclefriendly'];?>
</span>
                    </p>
                    </div>
                  </div>
              <?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_local_item;
}
if ($__foreach_addon_5_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_item;
}
if ($__foreach_addon_5_saved_key) {
$_smarty_tpl->tpl_vars['addonnum'] = $__foreach_addon_5_saved_key;
}
?>
              <?php
$_smarty_tpl->tpl_vars['product'] = $__foreach_product_3_saved_local_item;
}
if ($__foreach_product_3_saved_item) {
$_smarty_tpl->tpl_vars['product'] = $__foreach_product_3_saved_item;
}
if ($__foreach_product_3_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_product_3_saved_key;
}
?>
              <?php
$_from = $_smarty_tpl->tpl_vars['addons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_6_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$__foreach_addon_6_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_6_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
                  <div class="row my-2">
                    <div class="col-sm-10">
                      <h6><?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>
</h6>
                      <p><?php echo $_smarty_tpl->tpl_vars['addon']->value['productname'];
if ($_smarty_tpl->tpl_vars['addon']->value['domainname']) {?> <strong><?php echo $_smarty_tpl->tpl_vars['addon']->value['domainname'];?>
</strong><?php }?></p>
                      <?php if ($_smarty_tpl->tpl_vars['addon']->value['setup']) {?>
                      <p><?php echo $_smarty_tpl->tpl_vars['addon']->value['setup'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];?>
</p>
                      <?php }?>
                      <p><?php echo $_smarty_tpl->tpl_vars['addon']->value['pricingtext'];?>
 <?php echo $_smarty_tpl->tpl_vars['addon']->value['billingcyclefriendly'];?>
</p>
                    </div>
                    <div class="col-sm-2 text-right hidden-xs">
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('a','<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
')">
                        <i class="fa fa-times text-danger"></i>
                      </button>
                    </div>
                  </div>
              <?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_6_saved_local_item;
}
if ($__foreach_addon_6_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_6_saved_item;
}
if ($__foreach_addon_6_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_addon_6_saved_key;
}
?>
              <?php
$_from = $_smarty_tpl->tpl_vars['domains']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domain_7_saved_item = isset($_smarty_tpl->tpl_vars['domain']) ? $_smarty_tpl->tpl_vars['domain'] : false;
$__foreach_domain_7_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domain']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
$__foreach_domain_7_saved_local_item = $_smarty_tpl->tpl_vars['domain'];
?>
                  <div class="row my-2">
                    <div class="col-md-9">
                        <h6><?php if ($_smarty_tpl->tpl_vars['domain']->value['domain']) {?> <?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];
}?> <?php if ($_smarty_tpl->tpl_vars['domain']->value['type'] == "register") {
echo $_smarty_tpl->tpl_vars['LANG']->value['orderdomainregistration'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['orderdomaintransfer'];
}?></h6>
                      <?php if ($_smarty_tpl->tpl_vars['domain']->value['dnsmanagement']) {?><p class="small"><i class="fa fa-plus fa-fw text-muted" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsmanagement'];?>
</p><?php }?>
                      <?php if ($_smarty_tpl->tpl_vars['domain']->value['emailforwarding']) {?><p class="small"><i class="fa fa-plus fa-fw text-muted" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainemailforwarding'];?>
</p><?php }?>
                      <?php if ($_smarty_tpl->tpl_vars['domain']->value['idprotection']) {?><p class="small"><i class="fa fa-plus fa-fw text-muted" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainidprotection'];?>
</p><?php }?>
                      <div class="item-price">
                        <?php if (count($_smarty_tpl->tpl_vars['domain']->value['pricing']) == 1 || $_smarty_tpl->tpl_vars['domain']->value['type'] == 'transfer') {?>
                        <span name="<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
Price"><?php echo $_smarty_tpl->tpl_vars['domain']->value['price'];?>
</span>
                        <p class="cycle"><?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['domain']->value['yearsLanguage'];?>
</p>
                        <span class="renewal cycle small">
                          <?php echo WHMCS\Smarty::langFunction(array('key'=>'domainrenewalprice'),$_smarty_tpl);?>
 <span class="renewal-price cycle"><?php echo $_smarty_tpl->tpl_vars['domain']->value['renewprice']->toPrefixed();
echo $_smarty_tpl->tpl_vars['domain']->value['shortYearsLanguage'];?>
</span>
                        </span>
                        <?php } else { ?>
                        <p><span name="<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
Price"><?php echo $_smarty_tpl->tpl_vars['domain']->value['price'];?>
</span></p>
                        <div class="form-group">
                          <div class="dropdown">
                            <button class="btn btn-default btn-xs  dropdown-toggle" type="button" id="<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
Pricing" name="<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
Pricing" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              <?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['domain']->value['yearsLanguage'];?>

                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
Pricing">
                              <?php
$_from = $_smarty_tpl->tpl_vars['domain']->value['pricing'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_price_8_saved_item = isset($_smarty_tpl->tpl_vars['price']) ? $_smarty_tpl->tpl_vars['price'] : false;
$__foreach_price_8_saved_key = isset($_smarty_tpl->tpl_vars['years']) ? $_smarty_tpl->tpl_vars['years'] : false;
$_smarty_tpl->tpl_vars['price'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['years'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['price']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['years']->value => $_smarty_tpl->tpl_vars['price']->value) {
$_smarty_tpl->tpl_vars['price']->_loop = true;
$__foreach_price_8_saved_local_item = $_smarty_tpl->tpl_vars['price'];
?>
                              <li>
                                <a href="#" onclick="selectDomainPeriodInCart('<?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
', '<?php echo $_smarty_tpl->tpl_vars['price']->value['register'];?>
', <?php echo $_smarty_tpl->tpl_vars['years']->value;?>
, '<?php if ($_smarty_tpl->tpl_vars['years']->value == 1) {
echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.year'),$_smarty_tpl);
} else {
echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.years'),$_smarty_tpl);
}?>');return false;">
                                  <?php echo $_smarty_tpl->tpl_vars['years']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['years']->value == 1) {
echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.year'),$_smarty_tpl);
} else {
echo WHMCS\Smarty::langFunction(array('key'=>'orderForm.years'),$_smarty_tpl);
}?> @ <?php echo $_smarty_tpl->tpl_vars['price']->value['register'];?>

                                </a>
                              </li>
                              <?php
$_smarty_tpl->tpl_vars['price'] = $__foreach_price_8_saved_local_item;
}
if ($__foreach_price_8_saved_item) {
$_smarty_tpl->tpl_vars['price'] = $__foreach_price_8_saved_item;
}
if ($__foreach_price_8_saved_key) {
$_smarty_tpl->tpl_vars['years'] = $__foreach_price_8_saved_key;
}
?>
                            </ul>
                          </div>
                        </div>
                        <p class="renewal cycle small">
                          <?php echo WHMCS\Smarty::langFunction(array('key'=>'domainrenewalprice'),$_smarty_tpl);?>
 <span class="renewal-price cycle"><?php echo $_smarty_tpl->tpl_vars['domain']->value['renewprice']->toPrefixed();
echo $_smarty_tpl->tpl_vars['domain']->value['shortYearsLanguage'];?>
</span>
                        </p>
                        <?php }?>
                      </div>
                    </div>
                    <div class="col-sm-3 col-xs-2 text-right">
                      <a href="<?php echo $_SERVER['PHP_SELF'];?>
?a=confdomains" class="btn btn-link btn-xs">
                        <i class="fa fa-pencil"></i>
                      </a>
                      <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('d','<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
')">
                        <i class="fa fa-times text-danger"></i>
                      </button>
                    </div>
                  </div>
              <?php
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_7_saved_local_item;
}
if ($__foreach_domain_7_saved_item) {
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_7_saved_item;
}
if ($__foreach_domain_7_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_domain_7_saved_key;
}
?>
              <?php
$_from = $_smarty_tpl->tpl_vars['renewals']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domain_9_saved_item = isset($_smarty_tpl->tpl_vars['domain']) ? $_smarty_tpl->tpl_vars['domain'] : false;
$__foreach_domain_9_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domain']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
$__foreach_domain_9_saved_local_item = $_smarty_tpl->tpl_vars['domain'];
?>
              <div class="item">
                <div class="row">
                  <div class="col-sm-7">
                    <span class="item-title">
                      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewal'];?>

                    </span>
                    <span class="item-domain">
                      <?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>

                    </span>
                    <?php if ($_smarty_tpl->tpl_vars['domain']->value['dnsmanagement']) {?>&nbsp;&raquo; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsmanagement'];?>
<br /><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['domain']->value['emailforwarding']) {?>&nbsp;&raquo; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainemailforwarding'];?>
<br /><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['domain']->value['idprotection']) {?>&nbsp;&raquo; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainidprotection'];?>
<br /><?php }?>
                  </div>
                  <div class="col-sm-4 item-price">
                    <span><?php echo $_smarty_tpl->tpl_vars['domain']->value['price'];?>
</span>
                    <span class="cycle"><?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
</span>
                  </div>
                  <div class="col-sm-1">
                    <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('r','<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
')">
                      <i class="fa fa-times text-danger"></i>
                      <span class="visible-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['remove'];?>
</span>
                    </button>
                  </div>
                </div>
              </div>
              <?php
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_9_saved_local_item;
}
if ($__foreach_domain_9_saved_item) {
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_9_saved_item;
}
if ($__foreach_domain_9_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_domain_9_saved_key;
}
?>
              <?php
$_from = $_smarty_tpl->tpl_vars['upgrades']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_upgrade_10_saved_item = isset($_smarty_tpl->tpl_vars['upgrade']) ? $_smarty_tpl->tpl_vars['upgrade'] : false;
$__foreach_upgrade_10_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['upgrade'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['upgrade']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['upgrade']->value) {
$_smarty_tpl->tpl_vars['upgrade']->_loop = true;
$__foreach_upgrade_10_saved_local_item = $_smarty_tpl->tpl_vars['upgrade'];
?>
                  <div class="item">
                      <div class="row">
                          <div class="col-sm-7">
                              <span class="item-title">
                                  <?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgrade'];?>

                              </span>
                              <span class="item-group">
                                  <?php if ($_smarty_tpl->tpl_vars['upgrade']->value->type == 'service') {?>
                                      <?php echo $_smarty_tpl->tpl_vars['upgrade']->value->originalProduct->productGroup->name;?>
<br><?php echo $_smarty_tpl->tpl_vars['upgrade']->value->originalProduct->name;?>
 => <?php echo $_smarty_tpl->tpl_vars['upgrade']->value->newProduct->name;?>

                                  <?php } elseif ($_smarty_tpl->tpl_vars['upgrade']->value->type == 'addon') {?>
                                      <?php echo $_smarty_tpl->tpl_vars['upgrade']->value->originalAddon->name;?>
 => <?php echo $_smarty_tpl->tpl_vars['upgrade']->value->newAddon->name;?>

                                  <?php }?>
                              </span>
                              <span class="item-domain">
                                  <?php if ($_smarty_tpl->tpl_vars['upgrade']->value->type == 'service') {?>
                                      <?php echo $_smarty_tpl->tpl_vars['upgrade']->value->service->domain;?>

                                  <?php }?>
                              </span>
                          </div>
                          <div class="col-sm-4 item-price">
                              <span><?php echo $_smarty_tpl->tpl_vars['upgrade']->value->newRecurringAmount;?>
</span>
                              <span class="cycle"><?php echo $_smarty_tpl->tpl_vars['upgrade']->value->newCycle;?>
</span>
                          </div>
                          <div class="col-sm-1">
                              <button type="button" class="btn btn-link btn-xs btn-remove-from-cart" onclick="removeItem('u','<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
')">
                                  <i class="fa fa-times"></i>
                                  <span class="visible-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['remove'];?>
</span>
                              </button>
                          </div>
                      </div>
                      <div class="row row-upgrade-credit">
                          <div class="col-sm-7">
                              <span class="item-group">
                                  <?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradeCredit'];?>

                              </span>
                              <div class="upgrade-calc-msg">
                                  <?php echo WHMCS\Smarty::langFunction(array('key'=>"upgradeCreditDescription",'daysRemaining'=>$_smarty_tpl->tpl_vars['upgrade']->value->daysRemaining,'totalDays'=>$_smarty_tpl->tpl_vars['upgrade']->value->totalDaysInCycle),$_smarty_tpl);?>

                              </div>
                          </div>
                          <div class="col-sm-4 item-price">
                              <span>-<?php echo $_smarty_tpl->tpl_vars['upgrade']->value->creditAmount;?>
</span>
                          </div>
                      </div>
                  </div>
              <?php
$_smarty_tpl->tpl_vars['upgrade'] = $__foreach_upgrade_10_saved_local_item;
}
if ($__foreach_upgrade_10_saved_item) {
$_smarty_tpl->tpl_vars['upgrade'] = $__foreach_upgrade_10_saved_item;
}
if ($__foreach_upgrade_10_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_upgrade_10_saved_key;
}
?>
              <?php if ($_smarty_tpl->tpl_vars['cartitems']->value == 0) {?>
                  <div class="view-cart-empty">
                    <h6><i class="fa fa-fw fa-shopping-cart" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartempty'];?>
</h6>
                  </div>
              <?php }?>
            </div>
          </form>
          <div class="summary-container">
            <div class="subtotal clearfix">
              <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersubtotal'];?>
</span>
              <span id="subtotal" class="pull-right"><?php echo $_smarty_tpl->tpl_vars['subtotal']->value;?>
</span>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['promotioncode']->value || $_smarty_tpl->tpl_vars['taxrate']->value || $_smarty_tpl->tpl_vars['taxrate2']->value) {?>
            <div class="bordered-totals">
              <?php if ($_smarty_tpl->tpl_vars['promotioncode']->value) {?>
              <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['promotiondescription']->value;?>
</span>
                <span id="discount" class="pull-right"><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</span>
              </div>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['taxrate']->value) {?>
              <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['taxname']->value;?>
 @ <?php echo $_smarty_tpl->tpl_vars['taxrate']->value;?>
%</span>
                <span id="taxTotal1" class="pull-right"><?php echo $_smarty_tpl->tpl_vars['taxtotal']->value;?>
</span>
              </div>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['taxrate2']->value) {?>
              <div class="clearfix">
              <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['taxname2']->value;?>
 @ <?php echo $_smarty_tpl->tpl_vars['taxrate2']->value;?>
%</span>
              <span id="taxTotal2" class="pull-right"><?php echo $_smarty_tpl->tpl_vars['taxtotal2']->value;?>
</span>
              </div>
              <?php }?>
            </div>
            <?php }?>
            <div class="row my-2">
            <div class="col-md-12">
            <div class="recurring-totals">
              <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['totals'];?>
</span>
              <span id="recurring" class="pull-right text-right recurring-charges">
                <span id="recurringMonthly" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringmonthly']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringmonthly']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermmonthly'];?>
<br />
                </span>
                <span id="recurringQuarterly" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringquarterly']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringquarterly']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermquarterly'];?>
<br />
                </span>
                <span id="recurringSemiAnnually" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringsemiannually']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringsemiannually']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermsemiannually'];?>
<br />
                </span>
                <span id="recurringAnnually" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringannually']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringannually']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermannually'];?>
<br />
                </span>
                <span id="recurringBiennially" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringbiennially']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringbiennially']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermbiennially'];?>
<br />
                </span>
                <span id="recurringTriennially" <?php if (!$_smarty_tpl->tpl_vars['totalrecurringtriennially']->value) {?>style="display:none;"<?php }?>>
                  <span class="cost"><?php echo $_smarty_tpl->tpl_vars['totalrecurringtriennially']->value;?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermtriennially'];?>
<br />
                </span>
              </span>
            </div>
          </div>
        </div>
              <div class="row order-total my-1 text-uppercase">
                <div class="col-md-12">
              <span id="totalDueToday" class="amt pull-right flip"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</span>
              <span class="pull-left flip"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertotalduetoday'];?>
</span>
            </div>
          </div>
          </div>
<?php if (!$_smarty_tpl->tpl_vars['flowcart_hide_promo']->value) {?>
<div class="row mt-2">
<div class="col-md-12">
          <div class="promo" id="applyPromo">
            <?php if ($_smarty_tpl->tpl_vars['promotioncode']->value) {?>
            <div class="row">
              <div class="col-md-12">
            <div class="pull-left view-cart-promotion-code">
              <?php echo $_smarty_tpl->tpl_vars['promotioncode']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['promotiondescription']->value;?>

            </div>
            <div class="pull-right">
            <a id="removePromotionCode" href="<?php echo $_SERVER['PHP_SELF'];?>
?a=removepromo" class="btn btn-link btn-xs"><i class="fa fa-lg fa-times text-danger"></i></a>
          </div>
        </div>
      </div>
            <?php } else { ?>
            <form id="formPromotionCode" method="post" action="cart.php?a=checkout">
              <div class="form-group <?php if ($_smarty_tpl->tpl_vars['promoerrormessage']->value) {?> has-error has-feedback <?php }?> ">
                <?php if ($_smarty_tpl->tpl_vars['promoerrormessage']->value) {?><label class="control-label" for="inputPromotionCode"><?php echo $_smarty_tpl->tpl_vars['promoerrormessage']->value;?>
</label><?php }?>
                <div class="input-group">
                  <input type="text" name="promocode" id="inputPromotionCode" class="form-control" placeholder="<?php echo WHMCS\Smarty::langFunction(array('key'=>"orderPromoCodePlaceholder"),$_smarty_tpl);?>
" required="required" aria-describedby="inputPromotionCodeStatus">

                  <span class="input-group-btn">
                    <button type="submit" name="validatepromo" class="btn btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpromovalidatebutton'];?>
"><i class="fa fa-chevron-right"></i></button>
                  </span>
                  <?php if ($_smarty_tpl->tpl_vars['promoerrormessage']->value) {?>
                    <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                    <span id="inputPromotionCodeStatus" class="sr-only">(error)</span>
                  <?php }?>                   
                </div>
              </div>
            </form>
            <?php }?>
          </div>
        </div>
      </div>
  <?php }?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['cartitems']->value > 0 && $_smarty_tpl->tpl_vars['flowcart_display_empty_cart_button']->value) {?>
        <span class="empty-cart">
          <button type="button" class="btn btn-block btn-danger btn-lg" id="btnEmptyCart">
            <i class="fa fa-trash"></i></button>
        </span>
        <?php }?>
        <!--endordersummary-->
      </div>

      <?php if ($_smarty_tpl->tpl_vars['taxenabled']->value && !$_smarty_tpl->tpl_vars['loggedin']->value) {?>
<div class="view-cart-tabs">
      <div id="calcTaxes">
        <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['estimateTaxes'];?>
</h4>
        <form method="post" action="cart.php?a=setstateandcountry">
            <div class="form-group">
              <label for="inputState" class="control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['state'];?>
</label>
                <input type="text" name="state" id="inputState" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['state'];?>
" class="form-control"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> disabled="disabled"<?php }?> />
              </div>
            <div class="form-group">
              <label for="inputCountry" class="control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['country'];?>
</label>
                <select name="country" id="inputCountry" class="form-control">
                  <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_countrylabel_11_saved_item = isset($_smarty_tpl->tpl_vars['countrylabel']) ? $_smarty_tpl->tpl_vars['countrylabel'] : false;
$__foreach_countrylabel_11_saved_key = isset($_smarty_tpl->tpl_vars['countrycode']) ? $_smarty_tpl->tpl_vars['countrycode'] : false;
$_smarty_tpl->tpl_vars['countrylabel'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrycode'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrylabel']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['countrycode']->value => $_smarty_tpl->tpl_vars['countrylabel']->value) {
$_smarty_tpl->tpl_vars['countrylabel']->_loop = true;
$__foreach_countrylabel_11_saved_local_item = $_smarty_tpl->tpl_vars['countrylabel'];
?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['countrycode']->value;?>
"<?php if ((!$_smarty_tpl->tpl_vars['country']->value && $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['defaultcountry']->value) || $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['country']->value) {?> selected<?php }?>>
                    <?php echo $_smarty_tpl->tpl_vars['countrylabel']->value;?>

                  </option>
                  <?php
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_11_saved_local_item;
}
if ($__foreach_countrylabel_11_saved_item) {
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_11_saved_item;
}
if ($__foreach_countrylabel_11_saved_key) {
$_smarty_tpl->tpl_vars['countrycode'] = $__foreach_countrylabel_11_saved_key;
}
?>
                </select>
              </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default btn-block">
                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['updateTotals'];?>

              </button>
            </div>
        </form>
      </div>
  </div>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['flowcart_final_order_button_tosidebar']->value) {?>
         <?php if ($_smarty_tpl->tpl_vars['accepttos']->value) {?><div class="well">
            <div class="form-group">
              <label class="checkbox-inline accepttossidebar">
                <input type="checkbox" name="accepttos" id="accepttossidebar" />
                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertosagreement'];?>

                <a href="<?php echo $_smarty_tpl->tpl_vars['tosurl']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertos'];?>
</a>
              </label>
            </div>
        <?php }?>
      <button id="btnCompleteOrdersidebar" class="btn btn-warning btn-lg btn-block" <?php if ($_smarty_tpl->tpl_vars['cartitems']->value == 0) {?> disabled="disabled"<?php }?> onclick="this.value='<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pleasewait'];?>
'" ><?php echo $_smarty_tpl->tpl_vars['LANG']->value['completeorder'];?>
</button>
      <?php if ($_smarty_tpl->tpl_vars['accepttos']->value) {?></div><?php }?>
      <?php }?>
    </div>
  </div>
</div>
</div>
</div>
<form method="post" action="cart.php">
<input type="hidden" name="a" value="remove" />
<input type="hidden" name="r" value="" id="inputRemoveItemType" />
<input type="hidden" name="i" value="" id="inputRemoveItemRef" />
<div class="modal fade modal-remove-item" id="modalRemoveItem" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['close'];?>
">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">
          <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['removeItem'];?>
</span>
        </h4>
      </div>
      <div class="modal-body">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartremoveitemconfirm'];?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['no'];?>
</button>
        <button type="submit" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['yes'];?>
</button>
      </div>
    </div>
  </div>
</div>
</form>
<form method="post" action="cart.php">
<input type="hidden" name="a" value="empty" />
<div class="modal fade modal-remove-item" id="modalEmptyCart" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['close'];?>
">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">
          <i class="fa fa-trash"></i>
          <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['emptycart'];?>
</span>
        </h4>
      </div>
      <div class="modal-body">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartemptyconfirm'];?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['no'];?>
</button>
        <button type="submit" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['yes'];?>
</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php }
}
