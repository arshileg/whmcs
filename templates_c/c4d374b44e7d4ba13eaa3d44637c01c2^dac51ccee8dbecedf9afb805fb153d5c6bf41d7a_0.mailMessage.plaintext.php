<?php
/* Smarty version 3.1.29, created on 2018-08-01 00:00:52
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b60bff4d1dc85_93236632',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533067252,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b60bff4d1dc85_93236632 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


This is your monthly affiliate referrals report. You can view your referral statistics at any time by logging in to the client area.


Total Visitors Referred: <?php echo $_smarty_tpl->tpl_vars['affiliate_total_visits']->value;?>

 Current Earnings: <?php echo $_smarty_tpl->tpl_vars['affiliate_balance']->value;?>

 Amount Withdrawn: <?php echo $_smarty_tpl->tpl_vars['affiliate_withdrawn']->value;?>



Your New Signups this Month


<?php echo $_smarty_tpl->tpl_vars['affiliate_referrals_table']->value;?>



Remember, you can refer new customers using your unique affiliate link: <?php echo $_smarty_tpl->tpl_vars['affiliate_referral_url']->value;?>



<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
