<?php
/* Smarty version 3.1.29, created on 2018-08-08 18:32:27
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b373bedc734_81972582',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533753147,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b373bedc734_81972582 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>Thank you for creating a <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
 account.</p>
<p>Please visit the link below and sign into your account to verify your email address and complete your registration.</p>
<p><?php echo $_smarty_tpl->tpl_vars['client_email_verification_link']->value;?>
</p>
<p>You are receiving this email because you recently created an account or changed your email address. If you did not do this, please contact us.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
