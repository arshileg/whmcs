<?php
/* Smarty version 3.1.29, created on 2018-05-26 13:54:27
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/boxes/error.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b092ed3e69df7_32865140',
  'file_dependency' => 
  array (
    '7c6130261530efd863c1c018e7c731b9b305f06e' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/boxes/error.tpl',
      1 => 1515132966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b092ed3e69df7_32865140 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><link rel="stylesheet" type="text/css" href="templates/orderforms/<?php echo $_smarty_tpl->tpl_vars['carttpl']->value;?>
/style.css" />

<div id="order-boxes">

    <div class="header-lined">
        <h1><?php echo $_smarty_tpl->tpl_vars['errortitle']->value;?>
</h1>
    </div>

    <p><?php echo $_smarty_tpl->tpl_vars['errormsg']->value;?>
</p>

    <p class="text-center">
        <a href="javascript:history.go(-1)" class="btn btn-default">&laquo; <?php echo $_smarty_tpl->tpl_vars['LANG']->value['problemgoback'];?>
</a>
    </p>

</div>
<?php }
}
