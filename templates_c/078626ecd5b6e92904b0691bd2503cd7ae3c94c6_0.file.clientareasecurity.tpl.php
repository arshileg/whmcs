<?php
/* Smarty version 3.1.29, created on 2018-08-06 19:32:44
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareasecurity.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b68a25c3fe020_37548400',
  'file_dependency' => 
  array (
    '078626ecd5b6e92904b0691bd2503cd7ae3c94c6' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareasecurity.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b68a25c3fe020_37548400 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/dataTables.bootstrap.css">
<?php echo '<script'; ?>
 type="text/javascript" charset="utf8" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" charset="utf8" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareanavsecurity'],'icon'=>'lock'), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/clientareadetailslinks.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['successful']->value) {?>
<div class="alert alert-success">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['changessavedsuccessfully'];?>
</p>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
  <ul>
    <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

  </ul>
</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['linkableProviders']->value) {?>
<div class="p-2">
    <h2>
        <?php echo WHMCS\Smarty::langFunction(array('key'=>'remoteAuthn.titleLinkedAccounts'),$_smarty_tpl);?>

    </h2>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/linkedaccounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('linkContext'=>"clientsecurity"), 0, true);
?>

    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/linkedaccounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('linkContext'=>"linktable"), 0, true);
?>

</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['twofaavailable']->value) {
if ($_smarty_tpl->tpl_vars['twofaactivation']->value) {?>
<div id="twofaactivation">
  <?php echo $_smarty_tpl->tpl_vars['twofaactivation']->value;?>

</div>
<?php echo '<script'; ?>
 type="text/javascript">
jQuery("#twofaactivation input:text:visible:first,#twofaactivation input:password:visible:first").focus();
<?php echo '</script'; ?>
>
<?php } else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['twofactorauth']), 0, true);
?>

    <div class="alert alert-info"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['twofaactivationintro'];?>

    <form method="post" action="clientarea.php?action=security">
      <input type="hidden" name="2fasetup" value="1" />
      <div class="form-group">
        <?php if ($_smarty_tpl->tpl_vars['twofastatus']->value) {?>
        <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['twofadisableclickhere'];?>
" class="btn btn-danger" />
        <?php } else { ?>
        <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['twofaenableclickhere'];?>
" class="btn btn-success" />
        <?php }?>
      </div>
    </form>
    </div>
<?php }
}
if ($_smarty_tpl->tpl_vars['securityquestionsenabled']->value && !$_smarty_tpl->tpl_vars['twofaactivation']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareanavsecurityquestions']), 0, true);
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=changesq" class="form-horizontal p-2">
  <?php if (!$_smarty_tpl->tpl_vars['nocurrent']->value) {?>
  <div class="form-group">
    <label for="currentans" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['currentquestion']->value;?>
</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="currentsecurityqans" id="currentans" autocomplete="off" />
    </div>
  </div>
  <?php }?>
  <div class="form-group">
    <label for="securityqid" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasecurityquestion'];?>
</label>
    <div class="col-sm-6">
      <select name="securityqid" class="form-control" id="securityqid">
        <?php
$_from = $_smarty_tpl->tpl_vars['securityquestions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_question_0_saved_item = isset($_smarty_tpl->tpl_vars['question']) ? $_smarty_tpl->tpl_vars['question'] : false;
$__foreach_question_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['question'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['question']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['question']->value) {
$_smarty_tpl->tpl_vars['question']->_loop = true;
$__foreach_question_0_saved_local_item = $_smarty_tpl->tpl_vars['question'];
?>
        <option value=<?php echo $_smarty_tpl->tpl_vars['question']->value['id'];?>
><?php echo $_smarty_tpl->tpl_vars['question']->value['question'];?>
</option>
        <?php
$_smarty_tpl->tpl_vars['question'] = $__foreach_question_0_saved_local_item;
}
if ($__foreach_question_0_saved_item) {
$_smarty_tpl->tpl_vars['question'] = $__foreach_question_0_saved_item;
}
if ($__foreach_question_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_question_0_saved_key;
}
?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="securityqans" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasecurityanswer'];?>
</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="securityqans" id="securityqans" autocomplete="off" />
    </div>
  </div>
  <div class="form-group">
    <label for="securityqans2" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasecurityconfanswer'];?>
</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" name="securityqans2" id="securityqans2" autocomplete="off" />
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" />
    </div>
  </div>
</form>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['showSsoSetting']->value && !$_smarty_tpl->tpl_vars['twofaactivation']->value) {?>

<h2><?php echo $_smarty_tpl->tpl_vars['LANG']->value['sso']['title'];?>
</h2>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['sso']['summary']), 0, true);
?>


<form id="frmSingleSignOn">
  <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" />
  <input type="hidden" name="action" value="security" />
  <input type="hidden" name="toggle_sso" value="1" />
  <div class="margin-10">
    <input type="checkbox" name="allow_sso" class="toggle-switch-success" id="inputAllowSso"<?php if ($_smarty_tpl->tpl_vars['isSsoEnabled']->value) {?> checked<?php }?>>
    &nbsp;
    <span id="ssoStatusTextEnabled"<?php if (!$_smarty_tpl->tpl_vars['isSsoEnabled']->value) {?> class="hidden"<?php }?>>
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['sso']['enabled'];?>

    </span>
    <span id="ssoStatusTextDisabled"<?php if ($_smarty_tpl->tpl_vars['isSsoEnabled']->value) {?> class="hidden"<?php }?>>
      <?php echo $_smarty_tpl->tpl_vars['LANG']->value['sso']['disabled'];?>

    </span>
  </div>
</form>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['sso']['disablenotice'];?>
</p>
<link href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/bootstrap-switch.min.css" rel="stylesheet">
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/bootstrap-switch.min.js"><?php echo '</script'; ?>
>
<?php }
}
}
