<?php
/* Smarty version 3.1.29, created on 2018-02-18 13:45:11
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-steptwo.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a894b27ea5a72_49261639',
  'file_dependency' => 
  array (
    '4b4d2c10c2d00585067fecb8833c1ddcbb210598' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-steptwo.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a894b27ea5a72_49261639 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['navopenticket'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['supportticketsintro'],'icon'=>'support'), 0, true);
?>

<?php echo '<script'; ?>
>
	var currentcheckcontent,lastcheckcontent;
	<?php if ($_smarty_tpl->tpl_vars['kbsuggestions']->value) {?>
	
	function getticketsuggestions() {
		currentcheckcontent = jQuery("#message").val();
		if (currentcheckcontent!=lastcheckcontent && currentcheckcontent!="") {
			jQuery.post("submitticket.php", { action: "getkbarticles", text: currentcheckcontent },
				function(data){
					if (data) {
						jQuery("#searchresults").html(data);
						jQuery("#searchresults").slideDown();
					}
				});
			lastcheckcontent = currentcheckcontent;
		}
		setTimeout('getticketsuggestions();', 3000);
	}
	getticketsuggestions();
	
	<?php }?>

$( document ).ready(function() {
    getCustomFields();
});

function getCustomFields() {
    /**
     * Load the custom fields for the specific department
     */
    jQuery("#department").prop('disabled', true);
    jQuery("#customFields").load(
        "submitticket.php",
        { action: "getcustomfields", deptid: jQuery("#department").val() }
    );
    jQuery("#customFields").slideDown();
    jQuery("#department").prop('disabled', false);
}

<?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
>
	
		$(document)
			.on('change', '.btn-file :file', function() {
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
		});

		$(document).ready( function() {
			$('.btn-file :file').on('fileselect', function(event, numFiles, label) {

				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;

				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}

			});
		});
		
	<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
	<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
	<ul>
		<?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

	</ul>
</div>
<?php }?>
<form name="submitticket" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?step=3" enctype="multipart/form-data">
	<div class="row py-2">
	<div class="col-md-6 col-md-offset-3">
		<div class="row">
			<div class="col-md-6">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span> </span>
					<select name="deptid" class="form-control" id="department" disabled onchange="getCustomFields()"><?php
$_from = $_smarty_tpl->tpl_vars['departments']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_department_0_saved_item = isset($_smarty_tpl->tpl_vars['department']) ? $_smarty_tpl->tpl_vars['department'] : false;
$_smarty_tpl->tpl_vars['department'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['department']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['department']->value) {
$_smarty_tpl->tpl_vars['department']->_loop = true;
$__foreach_department_0_saved_local_item = $_smarty_tpl->tpl_vars['department'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['department']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['department']->value['id'] == $_smarty_tpl->tpl_vars['deptid']->value) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['department']->value['name'];?>
</option><?php
$_smarty_tpl->tpl_vars['department'] = $__foreach_department_0_saved_local_item;
}
if ($__foreach_department_0_saved_item) {
$_smarty_tpl->tpl_vars['department'] = $__foreach_department_0_saved_item;
}
?></select>
				</div>
				</div>
			</div>
			<div class="col-md-6">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-fire"></span> </span>
					<select name="urgency" class="form-control" id="priority"><option value="High"<?php if ($_smarty_tpl->tpl_vars['urgency']->value == "High") {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgencyhigh'];?>
</option><option value="Medium"<?php if ($_smarty_tpl->tpl_vars['urgency']->value == "Medium" || !$_smarty_tpl->tpl_vars['urgency']->value) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgencymedium'];?>
</option><option value="Low"<?php if ($_smarty_tpl->tpl_vars['urgency']->value == "Low") {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgencylow'];?>
</option></select>
				</div>
				</div>
				</div>
				</div>
					<div class="row <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>subhidden<?php }?>">
						<div class="col-md-6">
							<label for="name"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsclientname'];?>
</label>
							<div class="form-group">
								<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?><input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->tpl_vars['clientname']->value;?>
" disabled class="form-control"><?php } else { ?><input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" class="form-control"><?php }?>
							</div>
						</div>
						<div class="col-md-6">
							<label for="email"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsclientemail'];?>
</label>
							<div class="form-group">
								<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" disabled class="form-control"><?php } else { ?><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" class="form-control"><?php }?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label for="subject"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketsubject'];?>
</label>
							<div class="form-group">
								<input class="form-control" type="text" name="subject" id="subject" value="<?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
"/>
							</div>
						</div>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['relatedservices']->value) {?>
					<div class="row">
						<div class="col-lg-12">
							<label for="relatedservice"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['relatedservice'];?>
</label>
							<div class="form-group">
								<select name="relatedservice" class="form-control" id="relatedservice">
									<option value=""><?php echo $_smarty_tpl->tpl_vars['LANG']->value['none'];?>
</option>
									<?php
$_from = $_smarty_tpl->tpl_vars['relatedservices']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_relatedservice_1_saved_item = isset($_smarty_tpl->tpl_vars['relatedservice']) ? $_smarty_tpl->tpl_vars['relatedservice'] : false;
$_smarty_tpl->tpl_vars['relatedservice'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['relatedservice']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['relatedservice']->value) {
$_smarty_tpl->tpl_vars['relatedservice']->_loop = true;
$__foreach_relatedservice_1_saved_local_item = $_smarty_tpl->tpl_vars['relatedservice'];
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['relatedservice']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['relatedservice']->value['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['relatedservice']->value['status'];?>
)</option>
									<?php
$_smarty_tpl->tpl_vars['relatedservice'] = $__foreach_relatedservice_1_saved_local_item;
}
if ($__foreach_relatedservice_1_saved_item) {
$_smarty_tpl->tpl_vars['relatedservice'] = $__foreach_relatedservice_1_saved_item;
}
?>
								</select>
							</div>
						</div>
					</div>
					<?php }?>
					<div class="row">
						<div class="col-lg-12">
							<label for="message"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['contactmessage'];?>
</label>
							<div class="form-group">
								<textarea name="message" id="message" rows="10" class="form-control markdown-editor" data-auto-save-name="client_ticket_open"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</textarea>
							</div>
						</div>
					</div>
					<div id="searchresults" class="contentbox well well-sm" style="display:none;"></div>
					<div id="customFields" class="contentbox" style="display:none;"></div>
					<div id="fileuploads">
					<div class="form-group">
					<label><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketattachments'];?>
</label> <a href="javascript:void(0)" data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsallowedextensions'];?>
: <?php echo $_smarty_tpl->tpl_vars['allowedfiletypes']->value;?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsallowedextensions'];?>
: <?php echo $_smarty_tpl->tpl_vars['allowedfiletypes']->value;?>
"><span class="glyphicon glyphicon-question-sign"></span></a>
							<div class="input-group input-group-sm">
								<span class="input-group-btn">
									<span class="btn btn-default btn-file">
										<span class="glyphicon glyphicon-folder-open"></span> <input type="file" name="attachments[]" multiple="">
									</span>
								</span>
								<input type="text" class="form-control" readonly="">
								<span class="input-group-btn">
									<a href="#" class="btn btn-default btn-file" onclick="extraTicketAttachment();return false"><span class="glyphicon glyphicon-plus-sign"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['addmore'];?>
</a>
								</span>
							</div>
						</div>
						</div>
						<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/captcha.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

						<div class="row">
							<div class="col-md-12">
								<input class="btn btn-primary btn-lg btn-block" type="submit" name="save" id="openTicketSubmit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketsubmit'];?>
" />
							</div>
						</div>
					</div>
				</div>
			</form>
<?php }
}
