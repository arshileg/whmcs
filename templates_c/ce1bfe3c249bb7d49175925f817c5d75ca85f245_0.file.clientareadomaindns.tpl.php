<?php
/* Smarty version 3.1.29, created on 2018-04-18 18:38:04
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareadomaindns.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ad7584c357382_65384869',
  'file_dependency' => 
  array (
    'ce1bfe3c249bb7d49175925f817c5d75ca85f245' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareadomaindns.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ad7584c357382_65384869 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['domaindnsmanagement'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['domaindnsmanagementdesc']), 0, true);
?>

<div class="alert alert-info">
<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainname'];?>
: <?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
</h4>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindetails">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
" />
<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" class="btn btn-default" />
</form>
</div>

<?php if ($_smarty_tpl->tpl_vars['error']->value) {?>
<div class="alert alert-danger">
    <?php echo $_smarty_tpl->tpl_vars['error']->value;?>

</div>
<?php }
if ($_smarty_tpl->tpl_vars['external']->value) {?>
<p><?php echo $_smarty_tpl->tpl_vars['code']->value;?>
</p>
<?php } else { ?>
<form class="form-horizontal" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindns">
<input type="hidden" name="sub" value="save" />
<input type="hidden" name="domainid" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
" />
<div class="row">
<div class="col-md-12">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/tablelist.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('tableName'=>"DomainsList",'startOrderCol'=>"1",'filterColumn'=>"0"), 0, true);
?>

<table class="table table-condensed" id="tableDomainsList">
    <thead>
        <tr>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnshostname'];?>
</th>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsrecordtype'];?>
</th>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsaddress'];?>
</th>
            <th class="textcenter"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnspriority'];?>
 *</th>
        </tr>
    </thead>
    <tbody>
<?php
$_from = $_smarty_tpl->tpl_vars['dnsrecords']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_dnsrecord_0_saved_item = isset($_smarty_tpl->tpl_vars['dnsrecord']) ? $_smarty_tpl->tpl_vars['dnsrecord'] : false;
$_smarty_tpl->tpl_vars['dnsrecord'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['dnsrecord']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['dnsrecord']->value) {
$_smarty_tpl->tpl_vars['dnsrecord']->_loop = true;
$__foreach_dnsrecord_0_saved_local_item = $_smarty_tpl->tpl_vars['dnsrecord'];
?>
        <tr>
            <td><input type="hidden" name="dnsrecid[]" value="<?php echo $_smarty_tpl->tpl_vars['dnsrecord']->value['recid'];?>
" /><input type="text" name="dnsrecordhost[]" value="<?php echo $_smarty_tpl->tpl_vars['dnsrecord']->value['hostname'];?>
" class="form-control input-sm" /></td>
            <td><select class="form-control input-sm" name="dnsrecordtype[]">
<option value="A"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "A") {?> selected="selected"<?php }?>>A (Address)</option>
<option value="AAAA"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "AAAA") {?> selected="selected"<?php }?>>AAAA (Address)</option>
<option value="MXE"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "MXE") {?> selected="selected"<?php }?>>MXE (Mail Easy)</option>
<option value="MX"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "MX") {?> selected="selected"<?php }?>>MX (Mail)</option>
<option value="CNAME"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "CNAME") {?> selected="selected"<?php }?>>CNAME (Alias)</option>
<option value="TXT"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "TXT") {?> selected="selected"<?php }?>>SPF (txt)</option>
<option value="URL"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "URL") {?> selected="selected"<?php }?>>URL Redirect</option>
<option value="FRAME"<?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "FRAME") {?> selected="selected"<?php }?>>URL Frame</option>
</select></td>
            <td><input type="text" name="dnsrecordaddress[]" value="<?php echo $_smarty_tpl->tpl_vars['dnsrecord']->value['address'];?>
" class="form-control input-sm" /></td>
            <td><?php if ($_smarty_tpl->tpl_vars['dnsrecord']->value['type'] == "MX") {?><input type="text" name="dnsrecordpriority[]" value="<?php echo $_smarty_tpl->tpl_vars['dnsrecord']->value['priority'];?>
" class="form-control input-sm" /><?php } else { ?><input type="hidden" name="dnsrecordpriority[]" value="N/A" /><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregnotavailable'];
}?></td>
        </tr>
<?php
$_smarty_tpl->tpl_vars['dnsrecord'] = $__foreach_dnsrecord_0_saved_local_item;
}
if ($__foreach_dnsrecord_0_saved_item) {
$_smarty_tpl->tpl_vars['dnsrecord'] = $__foreach_dnsrecord_0_saved_item;
}
?>
        <tr>
            <td><input type="text" name="dnsrecordhost[]" class="form-control input-sm" /></td>
            <td><select name="dnsrecordtype[]" class="form-control input-sm">
<option value="A">A (Address)</option>
<option value="AAAA">AAAA (Address)</option>
<option value="MXE">MXE (Mail Easy)</option>
<option value="MX">MX (Mail)</option>
<option value="CNAME">CNAME (Alias)</option>
<option value="TXT">SPF (txt)</option>
<option value="URL">URL Redirect</option>
<option value="FRAME">URL Frame</option>
</select></td>
            <td><input type="text" name="dnsrecordaddress[]" class="form-control input-sm" /></td>
            <td><input type="text" name="dnsrecordpriority[]" class="form-control input-sm" /></td>
        </tr>
    </tbody>
</table>
</div>
</div>
<p class="px-1"><small>*<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsmxonly'];?>
</small></p>
<div class="p-1">
<input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" class="btn btn-primary" />
</div>
</form>
<?php }
}
}
