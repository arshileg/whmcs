<?php
/* Smarty version 3.1.29, created on 2018-08-01 00:00:52
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b60bff4d16c70_85942214',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533067252,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b60bff4d16c70_85942214 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>This is your monthly affiliate referrals report. You can view your referral statistics at any time by logging in to the client area.</p>
<p>Total Visitors Referred: <?php echo $_smarty_tpl->tpl_vars['affiliate_total_visits']->value;?>
<br /> Current Earnings: <?php echo $_smarty_tpl->tpl_vars['affiliate_balance']->value;?>
<br /> Amount Withdrawn: <?php echo $_smarty_tpl->tpl_vars['affiliate_withdrawn']->value;?>
</p>
<p><strong>Your New Signups this Month</strong></p>
<p><?php echo $_smarty_tpl->tpl_vars['affiliate_referrals_table']->value;?>
</p>
<p>Remember, you can refer new customers using your unique affiliate link: <?php echo $_smarty_tpl->tpl_vars['affiliate_referral_url']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
