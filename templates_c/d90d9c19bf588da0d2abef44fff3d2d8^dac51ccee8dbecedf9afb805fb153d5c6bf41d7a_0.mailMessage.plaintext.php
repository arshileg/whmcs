<?php
/* Smarty version 3.1.29, created on 2018-08-13 08:25:12
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b714068e92208_72607493',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1534148712,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b714068e92208_72607493 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


ამ წერილით გატყობინებთ, რომ თქვენი მომსახურეობა შეწყვეტილია. იხილეთ დეტალები შეწყვეტის შესახებ:


პროდუქტი/სერვისი: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>

<?php if ($_smarty_tpl->tpl_vars['service_domain']->value) {?>დომეინი: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>

<?php }?>ოდენობა: <?php echo $_smarty_tpl->tpl_vars['service_recurring_amount']->value;?>

გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>

მომსახურეობის შეწყვეტის მიზეზი: <?php echo $_smarty_tpl->tpl_vars['service_suspension_reason']->value;?>



დაგვიკავშირდით, რაც შეიძლება მალე, რათა სერვისი კვლავ აღდგეს. 


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
