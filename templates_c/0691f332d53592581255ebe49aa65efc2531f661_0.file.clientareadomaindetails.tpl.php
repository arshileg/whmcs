<?php
/* Smarty version 3.1.29, created on 2018-02-20 12:59:23
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareadomaindetails.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a8be36b5c1328_29500411',
  'file_dependency' => 
  array (
    '0691f332d53592581255ebe49aa65efc2531f661' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareadomaindetails.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a8be36b5c1328_29500411 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>(($_smarty_tpl->tpl_vars['LANG']->value['managing']).(' ')).($_smarty_tpl->tpl_vars['domain']->value)), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['registrarcustombuttonresult']->value == "success") {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['moduleactionsuccess'],'textcenter'=>true), 0, true);
?>

<?php } elseif ($_smarty_tpl->tpl_vars['registrarcustombuttonresult']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['moduleactionfailed'],'textcenter'=>true), 0, true);
?>

<?php }?>
<ul class="nav nav-material nav-material-horizontal px-lg-30 pt-lg-30">
    <li class="active"><a href="#tabOverview" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['information'];?>
</a></li>
    <li><a href="#tabAutorenew" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenew'];?>
</a></li>
    <?php if ($_smarty_tpl->tpl_vars['rawstatus']->value == "active" && $_smarty_tpl->tpl_vars['managens']->value) {?><li><a href="#tabNameservers" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameservers'];?>
</a></li><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['lockstatus']->value) {
if ($_smarty_tpl->tpl_vars['tld']->value != "co.uk" && $_smarty_tpl->tpl_vars['tld']->value != "org.uk" && $_smarty_tpl->tpl_vars['tld']->value != "ltd.uk" && $_smarty_tpl->tpl_vars['tld']->value != "plc.uk" && $_smarty_tpl->tpl_vars['tld']->value != "me.uk") {?><li><a href="#tabReglock" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregistrarlock'];?>
</a></li><?php }
}?>
    <?php if ($_smarty_tpl->tpl_vars['releasedomain']->value) {?><li><a href="#tabRelease" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrelease'];?>
</a></li><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['addonscount']->value) {?><li><a href="#tabAddons" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingaddons'];?>
</a></li><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['managecontacts']->value || $_smarty_tpl->tpl_vars['registerns']->value || $_smarty_tpl->tpl_vars['dnsmanagement']->value || $_smarty_tpl->tpl_vars['emailforwarding']->value || $_smarty_tpl->tpl_vars['getepp']->value) {?><li class="dropdown"><a data-toggle="dropdown" href="#" class="dropdown-toggle"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainmanagementtools'];?>
&nbsp;<b class="caret"></b></a>
    <ul class="dropdown-menu">
        <?php if ($_smarty_tpl->tpl_vars['managecontacts']->value) {?><li><a href="clientarea.php?action=domaincontacts&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaincontactinfo'];?>
</a></li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['registerns']->value) {?><li><a href="clientarea.php?action=domainregisterns&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregisterns'];?>
</a></li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['dnsmanagement']->value) {?><li><a href="clientarea.php?action=domaindns&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareadomainmanagedns'];?>
</a></li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['emailforwarding']->value) {?><li><a href="clientarea.php?action=domainemailforwarding&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareadomainmanageemailfwds'];?>
</a></li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['getepp']->value) {?><li class="divider"></li>
        <li><a href="clientarea.php?action=domaingetepp&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaingeteppcode'];?>
</a></li><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['registrarcustombuttons']->value) {?><li class="divider"></li>
        <?php
$_from = $_smarty_tpl->tpl_vars['registrarcustombuttons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_command_0_saved_item = isset($_smarty_tpl->tpl_vars['command']) ? $_smarty_tpl->tpl_vars['command'] : false;
$__foreach_command_0_saved_key = isset($_smarty_tpl->tpl_vars['label']) ? $_smarty_tpl->tpl_vars['label'] : false;
$_smarty_tpl->tpl_vars['command'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['label'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['command']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['label']->value => $_smarty_tpl->tpl_vars['command']->value) {
$_smarty_tpl->tpl_vars['command']->_loop = true;
$__foreach_command_0_saved_local_item = $_smarty_tpl->tpl_vars['command'];
?>
        <li><a href="clientarea.php?action=domaindetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
&amp;modop=custom&amp;a=<?php echo $_smarty_tpl->tpl_vars['command']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['label']->value;?>
</a></li>
        <?php
$_smarty_tpl->tpl_vars['command'] = $__foreach_command_0_saved_local_item;
}
if ($__foreach_command_0_saved_item) {
$_smarty_tpl->tpl_vars['command'] = $__foreach_command_0_saved_item;
}
if ($__foreach_command_0_saved_key) {
$_smarty_tpl->tpl_vars['label'] = $__foreach_command_0_saved_key;
}
}?>
    </ul>
</li><?php }?>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="tabOverview">
        <?php if ($_smarty_tpl->tpl_vars['systemStatus']->value != 'Active') {?>
        <div class="alert alert-warning text-center" role="alert">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainCannotBeManagedUnlessActive'];?>

        </div>
        <?php }?>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['overview']), 0, true);
?>

        <?php if ($_smarty_tpl->tpl_vars['lockstatus']->value == "unlocked") {?>
        <?php $_smarty_tpl->_cache['capture_stack'][] = array("domainUnlockedMsg", null, null); ob_start(); ?><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaincurrentlyunlocked'];?>
</strong><br /><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaincurrentlyunlockedexp'];
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>(isset($_smarty_tpl->_cache['__smarty_capture']['domainUnlockedMsg']) ? $_smarty_tpl->_cache['__smarty_capture']['domainUnlockedMsg'] : null)), 0, true);
?>

        <?php }?>
        <div class="well mx-1">
        <div class="row">
            <div class="col-sm-6">
                <dl class="dl-horizontal">
                  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingdomain'];?>
</dt>
                  <dd><a href="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
</a></dd>
                  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingregdate'];?>
</dt>
                  <dd><?php echo $_smarty_tpl->tpl_vars['registrationdate']->value;?>
</dd>
                  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>
</dt>
                  <dd><?php echo $_smarty_tpl->tpl_vars['nextduedate']->value;?>
</dd>
                  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareastatus'];?>
</dt>
                  <dd><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</dd>
              </dl>
          </div>
          <div class="col-sm-6">
            <dl class="dl-horizontal">
              <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['firstpaymentamount'];?>
</dt>
              <dd><?php echo $_smarty_tpl->tpl_vars['firstpaymentamount']->value;?>
</dd>
              <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['recurringamount'];?>
</dt>
              <dd><?php echo $_smarty_tpl->tpl_vars['recurringamount']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['every'];?>
 <?php echo $_smarty_tpl->tpl_vars['registrationperiod']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
</dd>
              <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
</dt>
              <dd><?php echo $_smarty_tpl->tpl_vars['paymentmethod']->value;?>
</dd>
          </dl>
      </div>
  </div>
</div>
  <?php if ($_smarty_tpl->tpl_vars['registrarclientarea']->value) {?>
  <div class="moduleoutput">
    <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['registrarclientarea']->value,'modulebutton','btn');?>

</div>
<?php }
$_from = $_smarty_tpl->tpl_vars['hookOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_1_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_1_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
<div>
    <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

</div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_local_item;
}
if ($__foreach_output_1_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_item;
}
?>
        <?php if ($_smarty_tpl->tpl_vars['systemStatus']->value == 'Active' && ($_smarty_tpl->tpl_vars['managementoptions']->value['nameservers'] || $_smarty_tpl->tpl_vars['managementoptions']->value['contacts'] || $_smarty_tpl->tpl_vars['managementoptions']->value['locking'] || $_smarty_tpl->tpl_vars['renew']->value)) {?>
                
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['doToday']), 0, true);
?>

<div class="well mx-1">
  <ul class="list-unstyled">
                <?php if ($_smarty_tpl->tpl_vars['managementoptions']->value['nameservers']) {?>
                    <li>
                        <a class="tabControlLink" data-toggle="tab" href="#tabNameservers">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['changeDomainNS'];?>

                        </a>
                    </li>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['managementoptions']->value['contacts']) {?>
                    <li>
                        <a href="clientarea.php?action=domaincontacts&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['updateWhoisContact'];?>

                        </a>
                    </li>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['managementoptions']->value['locking']) {?>
                    <li>
                        <a class="tabControlLink" data-toggle="tab" href="#tabReglock">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['changeRegLock'];?>

                        </a>
                    </li>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['renew']->value) {?>
                    <li>
                        <a href="cart.php?gid=renewals">
                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['renewYourDomain'];?>

                        </a>
                    </li>
                <?php }?>
            </ul>
</div>
        <?php }?>
</div>
<div class="tab-pane fade" id="tabAutorenew">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['domainsautorenew']), 0, true);
?>


    <?php if ($_smarty_tpl->tpl_vars['changeAutoRenewStatusSuccessful']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['changessavedsuccessfully'],'textcenter'=>true), 0, true);
?>

    <?php }?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['domainrenewexp']), 0, true);
?>

    <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="well">
  <h4 class="py-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainautorenewstatus'];?>
 <span class="pull-right flip"><span class="label label-<?php if ($_smarty_tpl->tpl_vars['autorenew']->value) {?>success<?php } else { ?>danger<?php }?>"><?php if ($_smarty_tpl->tpl_vars['autorenew']->value) {
echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewenabled'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewdisabled'];
}?></span></span></h4>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindetails#tabAutorenew">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
">
        <input type="hidden" name="sub" value="autorenew" />
        <?php if ($_smarty_tpl->tpl_vars['autorenew']->value) {?>
        <input type="hidden" name="autorenew" value="disable">
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-danger btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewdisable'];?>
" />
        </div>
        <?php } else { ?>
        <input type="hidden" name="autorenew" value="enable">
        <div class="form-group">
            <input type="submit" class="btn btn-lg btn-success btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewenable'];?>
" />
        </div>
        <?php }?>
    </form>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="tabNameservers">
    <h4 class="px-1 pt-1">Nameservers</h4>
    <?php if ($_smarty_tpl->tpl_vars['nameservererror']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['nameservererror']->value,'textcenter'=>true), 0, true);
?>

    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subaction']->value == "savens") {?>
    <?php if ($_smarty_tpl->tpl_vars['updatesuccess']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['changessavedsuccessfully'],'textcenter'=>true), 0, true);
?>

    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['error']->value,'textcenter'=>true), 0, true);
?>

    <?php }?>
    <?php }?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['domainnsexp']), 0, true);
?>

    <form class="form-horizontal p-2" role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindetails#tabNameservers">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
" />
        <input type="hidden" name="sub" value="savens" />
        <div class="radio">
            <label>
                <input type="radio" name="nschoice" value="default" onclick="disableFields('domnsinputs',true)"<?php if ($_smarty_tpl->tpl_vars['defaultns']->value) {?> checked<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['nschoicedefault'];?>

            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="nschoice" value="custom" onclick="disableFields('domnsinputs',false)"<?php if (!$_smarty_tpl->tpl_vars['defaultns']->value) {?> checked<?php }?> /> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['nschoicecustom'];?>

            </label>
        </div>
        <br />
        <?php
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['num']->step = 1;$_smarty_tpl->tpl_vars['num']->total = (int) ceil(($_smarty_tpl->tpl_vars['num']->step > 0 ? 5+1 - (1) : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['num']->step));
if ($_smarty_tpl->tpl_vars['num']->total > 0) {
for ($_smarty_tpl->tpl_vars['num']->value = 1, $_smarty_tpl->tpl_vars['num']->iteration = 1;$_smarty_tpl->tpl_vars['num']->iteration <= $_smarty_tpl->tpl_vars['num']->total;$_smarty_tpl->tpl_vars['num']->value += $_smarty_tpl->tpl_vars['num']->step, $_smarty_tpl->tpl_vars['num']->iteration++) {
$_smarty_tpl->tpl_vars['num']->first = $_smarty_tpl->tpl_vars['num']->iteration == 1;$_smarty_tpl->tpl_vars['num']->last = $_smarty_tpl->tpl_vars['num']->iteration == $_smarty_tpl->tpl_vars['num']->total;?>
        <div class="form-group">
            <label for="inputNs<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" class="col-sm-4 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanameserver'];?>
 <?php echo $_smarty_tpl->tpl_vars['num']->value;?>
</label>
            <div class="col-sm-7">
                <input type="text" name="ns<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" class="form-control domnsinputs" id="inputNs<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['nameservers']->value[$_smarty_tpl->tpl_vars['num']->value]['value'];?>
" />
            </div>
        </div>
        <?php }
}
?>

        <p class="text-center">
            <input type="submit" class="btn btn-primary" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['changenameservers'];?>
" />
        </p>
    </form>
</div>
<div class="tab-pane fade" id="tabReglock">
    <h4 class="px-1 pt-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregistrarlock'];?>
</h4>
    <?php if ($_smarty_tpl->tpl_vars['subaction']->value == "savereglock") {?>
    <?php if ($_smarty_tpl->tpl_vars['updatesuccess']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['changessavedsuccessfully'],'textcenter'=>true), 0, true);
?>

    <?php } elseif ($_smarty_tpl->tpl_vars['error']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['error']->value,'textcenter'=>true), 0, true);
?>

    <?php }?>
    <?php }?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['domainlockingexp']), 0, true);
?>

    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="well">
    <h4 class="py-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainreglockstatus'];?>
  <span class="pull-right flip"><span class="label label-<?php if ($_smarty_tpl->tpl_vars['lockstatus']->value == "locked") {?>success<?php } else { ?>danger<?php }?>"><?php if ($_smarty_tpl->tpl_vars['lockstatus']->value == "locked") {
echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewenabled'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['domainsautorenewdisabled'];
}?></span></span></h4>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindetails#tabReglock">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
">
        <input type="hidden" name="sub" value="savereglock" />
        <?php if ($_smarty_tpl->tpl_vars['lockstatus']->value == "locked") {?>
            <input type="submit" class="btn btn-lg btn-danger btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainreglockdisable'];?>
" />
        <?php } else { ?>
        <input type="hidden" name="autorenew" value="enable">
            <input type="submit" class="btn btn-lg btn-success btn-block" name="reglock" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainreglockenable'];?>
" />
        <?php }?>
    </form>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="tabRelease">
    <h4 class="px-1 pt-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrelease'];?>
</h4>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['domainreleasedescription']), 0, true);
?>

    <form class="form-horizontal p-2" role="form" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=domaindetails">
        <input type="hidden" name="sub" value="releasedomain">
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
">
        <div class="form-group">
            <label for="inputReleaseTag" class="col-xs-4 control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainreleasetag'];?>
</label>
            <div class="col-xs-6 col-sm-5">
                <input type="text" class="form-control" id="inputReleaseTag" name="transtag" />
            </div>
        </div>
        <p class="text-center">
            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrelease'];?>
" class="btn btn-primary" />
        </p>
    </form>
</div>
<div class="tab-pane fade" id="tabAddons">
    <h4 class="px-2 pt-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddons'];?>
</h4>
    <p class="px-2">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsinfo'];?>

    </p>
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['idprotection']) {?>
            <div class="row mx-1">
                <div class="col-md-12">
                <div class="well">
                <h3>
                <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-shield fa-stack-1x fa-inverse"></i></span>
                <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainidprotection'];?>

                </h3>
                    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsidprotectioninfo'];?>
</p>
                    <form action="clientarea.php?action=domainaddons" method="post">
                        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"/>
                        <?php if ($_smarty_tpl->tpl_vars['addonstatus']->value['idprotection']) {?>
                            <input type="hidden" name="disable" value="idprotect"/>
                            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['disable'];?>
" class="btn btn-link"/>
                        <?php } else { ?>
                            <input type="hidden" name="buy" value="idprotect"/>
                            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsbuynow'];?>
 <?php echo $_smarty_tpl->tpl_vars['addonspricing']->value['idprotection'];?>
" class="btn btn-link"/>
                        <?php }?>
                    </form>
                </div>
                </div>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['dnsmanagement']) {?>
            <div class="row mb-1 mx-1">
                <div class="col-md-12">
                <div class="well">
                    <h3><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-cloud fa-stack-1x fa-inverse"></i></span>
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsdnsmanagement'];?>

                    </h3>
                    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsdnsmanagementinfo'];?>
</p>
                    <form action="clientarea.php?action=domainaddons" method="post">
                        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"/>
                        <?php if ($_smarty_tpl->tpl_vars['addonstatus']->value['dnsmanagement']) {?>
                            <input type="hidden" name="disable" value="dnsmanagement"/>
                            <a class="btn btn-link" href="clientarea.php?action=domaindns&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['manage'];?>
</a> | <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['disable'];?>
" class="btn btn-link"/>
                        <?php } else { ?>
                            <input type="hidden" name="buy" value="dnsmanagement"/>
                            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsbuynow'];?>
 <?php echo $_smarty_tpl->tpl_vars['addonspricing']->value['dnsmanagement'];?>
" class="btn btn-link"/>
                        <?php }?>
                    </form>
                </div>
                </div>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['emailforwarding']) {?>
            <div class="row mb-1 mx-1">
                <div class="col-md-12">
                <div class="well">
                    <h3><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-mail-forward fa-stack-1x fa-inverse"></i></span>
                    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainemailforwarding'];?>

                    </h3>
                    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsemailforwardinginfo'];?>
</p>
                    <form action="clientarea.php?action=domainaddons" method="post">
                        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"/>
                        <?php if ($_smarty_tpl->tpl_vars['addonstatus']->value['emailforwarding']) {?>
                            <input type="hidden" name="disable" value="emailfwd"/>
                            <a class="btn btn-link" href="clientarea.php?action=domainemailforwarding&domainid=<?php echo $_smarty_tpl->tpl_vars['domainid']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['manage'];?>
</a> | <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['disable'];?>
" class="btn btn-link"/>
                        <?php } else { ?>
                            <input type="hidden" name="buy" value="emailfwd"/>
                            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsbuynow'];?>
 <?php echo $_smarty_tpl->tpl_vars['addonspricing']->value['emailforwarding'];?>
" class="btn btn-link"/>
                        <?php }?>
                    </form>
                </div>
            </div>
            </div>
        <?php }?>
</div>
</div>
</div>
<?php }
}
