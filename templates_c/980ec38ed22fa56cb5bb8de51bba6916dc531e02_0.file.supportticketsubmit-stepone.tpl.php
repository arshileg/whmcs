<?php
/* Smarty version 3.1.29, created on 2018-02-18 19:17:53
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-stepone.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a89992128b879_47221336',
  'file_dependency' => 
  array (
    '980ec38ed22fa56cb5bb8de51bba6916dc531e02' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/supportticketsubmit-stepone.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a89992128b879_47221336 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['navopenticket'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['supportticketsheader'],'icon'=>'support'), 0, true);
?>

<div class="row px-1 py-2">
    <?php
$_from = $_smarty_tpl->tpl_vars['departments']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_department_0_saved_item = isset($_smarty_tpl->tpl_vars['department']) ? $_smarty_tpl->tpl_vars['department'] : false;
$_smarty_tpl->tpl_vars['department'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['department']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['department']->value) {
$_smarty_tpl->tpl_vars['department']->_loop = true;
$__foreach_department_0_saved_local_item = $_smarty_tpl->tpl_vars['department'];
?>
        <div class="col-md-6">
        <div class="well">
        <div class="alert-watermark m-2">
        <i class="fa fa-envelope fa-4x"></i>
        </div>
         <h5><a href="<?php echo $_SERVER['PHP_SELF'];?>
?step=2&amp;deptid=<?php echo $_smarty_tpl->tpl_vars['department']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['department']->value['name'];?>
</a></h5>
    			<?php if ($_smarty_tpl->tpl_vars['department']->value['description']) {?><p><?php echo $_smarty_tpl->tpl_vars['department']->value['description'];?>
</p><?php }?>
        </div>
        </div>
    <?php
$_smarty_tpl->tpl_vars['department'] = $__foreach_department_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['department']->_loop) {
?>
    <div class="alert alert-info">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['nosupportdepartments'];?>

    </div>
    <?php
}
if ($__foreach_department_0_saved_item) {
$_smarty_tpl->tpl_vars['department'] = $__foreach_department_0_saved_item;
}
?>
</div>
<?php }
}
