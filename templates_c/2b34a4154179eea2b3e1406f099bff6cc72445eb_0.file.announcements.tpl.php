<?php
/* Smarty version 3.1.29, created on 2018-02-18 11:24:44
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/announcements.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a892a3c656196_07436201',
  'file_dependency' => 
  array (
    '2b34a4154179eea2b3e1406f099bff6cc72445eb' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/announcements.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a892a3c656196_07436201 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['announcementsFbRecommend']->value) {?>
    <?php echo '<script'; ?>
>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/<?php echo $_smarty_tpl->tpl_vars['LANG']->value['locale'];?>
/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    <?php echo '</script'; ?>
>
<?php }
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['announcementstitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['announcementsdescription'],'icon'=>'feed'), 0, true);
?>

<div class="announce-items-wrap announce-standard">
    <div class="timeline"></div>
    <ul class="announce-items list-unstyled row standard-items clearfix">
<?php
$_from = $_smarty_tpl->tpl_vars['announcements']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_announcement_0_saved_item = isset($_smarty_tpl->tpl_vars['announcement']) ? $_smarty_tpl->tpl_vars['announcement'] : false;
$_smarty_tpl->tpl_vars['announcement'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['announcement']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['announcement']->value) {
$_smarty_tpl->tpl_vars['announcement']->_loop = true;
$__foreach_announcement_0_saved_local_item = $_smarty_tpl->tpl_vars['announcement'];
?>
            <li class="announce-item col-sm-12">
              <span class="standard-post-date"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?php echo date("M jS, Y",$_smarty_tpl->tpl_vars['announcement']->value['timestamp']);?>
</span>
                <div class="standard-post-content no-thumb clearfix">
                    <h1 class="h3"><a href="<?php echo routePath('announcement-view',$_smarty_tpl->tpl_vars['announcement']->value['id'],$_smarty_tpl->tpl_vars['announcement']->value['urlfriendlytitle']);?>
"><?php echo $_smarty_tpl->tpl_vars['announcement']->value['title'];?>
</a></h1>
                    <div class="excerpt">
                        <p><?php if (strlen(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcement']->value['text'])) < 360) {
echo $_smarty_tpl->tpl_vars['announcement']->value['text'];
} else {
echo $_smarty_tpl->tpl_vars['announcement']->value['summary'];
}?></p>
                    </div>
                   <a href="<?php echo routePath('announcement-view',$_smarty_tpl->tpl_vars['announcement']->value['id'],$_smarty_tpl->tpl_vars['announcement']->value['urlfriendlytitle']);?>
" class="btn btn-outline btn-lg text-uppercase"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['readmore'];?>
</a>
                    <div class="comments-likes">
                      <?php if ($_smarty_tpl->tpl_vars['announcementsFbRecommend']->value) {?>
                          <div class="fb-like" data-layout="button_count" data-href="<?php echo fqdnRoutePath('announcement-view',$_smarty_tpl->tpl_vars['announcement']->value['id'],$_smarty_tpl->tpl_vars['announcement']->value['urlfriendlytitle']);?>
" data-send="true"></div>
                      <?php }?>
                    </div>
                </div>
            </li>
<?php
$_smarty_tpl->tpl_vars['announcement'] = $__foreach_announcement_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['announcement']->_loop) {
?>
<li class="announce-item col-sm-12"><div class="well"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['noannouncements'];?>
</div></li>
<?php
}
if ($__foreach_announcement_0_saved_item) {
$_smarty_tpl->tpl_vars['announcement'] = $__foreach_announcement_0_saved_item;
}
?>
</ul>

    </div>
<?php if ($_smarty_tpl->tpl_vars['prevpage']->value || $_smarty_tpl->tpl_vars['nextpage']->value) {?>

<nav class="text-center" aria-label="Page Navigation">
  <ul class="pagination pagination-sm">
 <?php if ($_smarty_tpl->tpl_vars['prevpage']->value) {?>
    <li>
      <a href="<?php echo routePath('announcement-index-paged',$_smarty_tpl->tpl_vars['prevpage']->value,$_smarty_tpl->tpl_vars['view']->value);?>
" aria-label="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['previouspage'];?>
">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
<?php }?>
    <li class="disabled"><a href="#"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['page'];?>
 <?php echo $_smarty_tpl->tpl_vars['pagenumber']->value;?>
</a></li>

    <?php if ($_smarty_tpl->tpl_vars['nextpage']->value) {?>
    <li>
      <a href="<?php echo routePath('announcement-index-paged',$_smarty_tpl->tpl_vars['nextpage']->value,$_smarty_tpl->tpl_vars['view']->value);?>
" aria-label="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['nextpage'];?>
">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
   <?php }?>
  </ul>
</nav>
<?php }
}
}
