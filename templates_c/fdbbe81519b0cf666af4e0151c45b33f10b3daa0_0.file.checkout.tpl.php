<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:34:20
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/checkout.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63b11cee7654_10902341',
  'file_dependency' => 
  array (
    'fdbbe81519b0cf666af4e0151c45b33f10b3daa0' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/checkout.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/linkedaccounts.tpl' => 2,
  ),
  'tpl_function' => 
  array (
    'getFontAwesomeCCIcon' => 
    array (
      'called_functions' => 
      array (
      ),
      'compiled_filepath' => '/home/hostnodesnet/public_html/templates_c/fdbbe81519b0cf666af4e0151c45b33f10b3daa0_0.file.checkout.tpl.php',
      'uid' => 'fdbbe81519b0cf666af4e0151c45b33f10b3daa0',
      'call_name' => 'smarty_template_function_getFontAwesomeCCIcon_18096844255b63b11cb33388_79312840',
    ),
  ),
),false)) {
function content_5b63b11cee7654_10902341 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>


<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-material nav-material-horizontal mb-1">
         <li class="pull-right flip"><a href="#" id="btnAlreadyRegistered" class="<?php if ($_smarty_tpl->tpl_vars['loggedin']->value || !$_smarty_tpl->tpl_vars['loggedin']->value && $_smarty_tpl->tpl_vars['custtype']->value == "existing") {?> hidden<?php }?>"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['alreadyRegistered'];?>
</a></li>
        <li class="pull-right flip"><a href="#" id="btnNewUserSignup" class="<?php if ($_smarty_tpl->tpl_vars['loggedin']->value || $_smarty_tpl->tpl_vars['custtype']->value != "existing") {?> hidden<?php }?>"><i class="fa fa-user-plus" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['createAccount'];?>
</a></li>
    </ul>

    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?a=checkout" name="orderfrm" id="frmCheckout">
      <input type="hidden" name="submit" value="true" />
      <input type="hidden" name="custtype" id="inputCustType" value="<?php echo $_smarty_tpl->tpl_vars['custtype']->value;?>
" />

      <div id="containerExistingUserSignin"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value || $_smarty_tpl->tpl_vars['custtype']->value != "existing") {?> class="hidden"<?php }?>>

        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['existingCustomerLogin'];?>
</h6>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="inputLoginEmail"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['emailAddress'];?>
</label>
              <input type="text" name="loginemail" id="inputLoginEmail" class="form-control input-lg">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="inputLoginPassword"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareapassword'];?>
</label>
              <input type="password" name="loginpassword" id="inputLoginPassword" class="form-control input-lg">
            </div>
          </div>
        </div>
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/linkedaccounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('linkContext'=>"checkout-existing"), 0, true);
?>

      </div>

      <div id="containerNewUserSignup"<?php if (!$_smarty_tpl->tpl_vars['loggedin']->value && $_smarty_tpl->tpl_vars['custtype']->value == "existing") {?> class="hidden"<?php }?>>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/linkedaccounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('linkContext'=>"checkout-new"), 0, true);
?>

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="inputFirstName"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['firstName'];?>
</label>
              <input type="text" name="firstname" id="inputFirstName" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['firstname'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?> autofocus>
            </div>
          </div>
          <div class="col-sm-4">
            <label for="inputLastName"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['lastName'];?>
</label>
            <input type="text" name="lastname" id="inputLastName" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['lastname'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="inputCompanyName"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['companyName'];?>
</label>
              <input type="text" name="companyname" id="inputCompanyName" class="form-control input-lg" placeholder="(<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['optional'];?>
)" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['companyname'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputEmail"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['emailAddress'];?>
</label>
            <input type="email" name="email" id="inputEmail" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['email'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputPhone"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['phoneNumber'];?>
</label>
            <input type="tel" name="phonenumber" id="inputPhone" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['phonenumber'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
      </div>
<hr>
<?php if (!$_smarty_tpl->tpl_vars['loggedin']->value) {?>

    <div id="containerNewUserSecurity"<?php if ((!$_smarty_tpl->tpl_vars['loggedin']->value && $_smarty_tpl->tpl_vars['custtype']->value == "existing") || ($_smarty_tpl->tpl_vars['remote_auth_prelinked']->value && !$_smarty_tpl->tpl_vars['securityquestions']->value)) {?> class="hidden"<?php }?>>
        <div id="containerPassword" class="row<?php if ($_smarty_tpl->tpl_vars['remote_auth_prelinked']->value && $_smarty_tpl->tpl_vars['securityquestions']->value) {?> hidden<?php }?>">
            <div id="passwdFeedback" style="display: none;" class="alert alert-info text-center col-sm-12"></div>
            <div class="col-sm-6">
                    <input type="password" name="password" id="inputNewPassword1" data-error-threshold="<?php echo $_smarty_tpl->tpl_vars['pwStrengthErrorThreshold']->value;?>
" data-warning-threshold="<?php echo $_smarty_tpl->tpl_vars['pwStrengthWarningThreshold']->value;?>
" class="field form-control input-lg" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareapassword'];?>
"<?php if ($_smarty_tpl->tpl_vars['remote_auth_prelinked']->value) {?> value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
"<?php }?>>
            </div>
            <div class="col-sm-6">
                    <input type="password" name="password2" id="inputNewPassword2" class="field form-control input-lg" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaconfirmpassword'];?>
"<?php if ($_smarty_tpl->tpl_vars['remote_auth_prelinked']->value) {?> value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
"<?php }?>>
            </div>
            <div class="col-sm-6 mt-1">
                    <div class="progress-bar progress-bar-success progress-bar-striped" style="height:3px;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" id="passwordStrengthMeterBar">
                </div>
            </div>
            <div class="col-sm-6" style="margin-top:7px;">
                <p class="small text-muted" id="passwordStrengthTextLabel"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrength'];?>
: <?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwstrengthenter'];?>
</p>
            </div>
        </div>

<?php if ($_smarty_tpl->tpl_vars['securityquestions']->value) {?>
<div class="row">
  <div class="col-sm-6">
    <select name="securityqid" id="inputSecurityQId" class="form-control input-lg">
      <option value=""><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasecurityquestion'];?>
</option>
      <?php
$_from = $_smarty_tpl->tpl_vars['securityquestions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_question_0_saved_item = isset($_smarty_tpl->tpl_vars['question']) ? $_smarty_tpl->tpl_vars['question'] : false;
$_smarty_tpl->tpl_vars['question'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['question']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['question']->value) {
$_smarty_tpl->tpl_vars['question']->_loop = true;
$__foreach_question_0_saved_local_item = $_smarty_tpl->tpl_vars['question'];
?>
      <option value="<?php echo $_smarty_tpl->tpl_vars['question']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['question']->value['id'] == $_smarty_tpl->tpl_vars['securityqid']->value) {?> selected<?php }?>>
        <?php echo $_smarty_tpl->tpl_vars['question']->value['question'];?>

      </option>
      <?php
$_smarty_tpl->tpl_vars['question'] = $__foreach_question_0_saved_local_item;
}
if ($__foreach_question_0_saved_item) {
$_smarty_tpl->tpl_vars['question'] = $__foreach_question_0_saved_item;
}
?>
    </select>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <input type="password" name="securityqans" id="inputSecurityQAns" class="form-control input-lg" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasecurityanswer'];?>
">
    </div>
  </div>
</div>
<?php }?>
</div>
<hr>
<?php }?>
      <div class="row">
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputAddress1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['streetAddress'];?>
</label>
            <input type="text" name="address1" id="inputAddress1" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['address1'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="inputAddress2"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['streetAddress2'];?>
</label>
            <input type="text" name="address2" id="inputAddress2" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['address2'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
      </div>
        <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="inputCity"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['city'];?>
</label>
            <input type="text" name="city" id="inputCity" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['city'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <label for="inputState"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['state'];?>
</label>
            <input type="text" name="state" id="inputState" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['state'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <label for="inputPostcode"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['postcode'];?>
</label>
            <input type="text" name="postcode" id="inputPostcode" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['postcode'];?>
"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> readonly="readonly"<?php }?>>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputCountry"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['country'];?>
</label>
            <select name="country" id="inputCountry" class="form-control input-lg"<?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?> disabled="disabled"<?php }?>>
              <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_countrylabel_1_saved_item = isset($_smarty_tpl->tpl_vars['countrylabel']) ? $_smarty_tpl->tpl_vars['countrylabel'] : false;
$__foreach_countrylabel_1_saved_key = isset($_smarty_tpl->tpl_vars['countrycode']) ? $_smarty_tpl->tpl_vars['countrycode'] : false;
$_smarty_tpl->tpl_vars['countrylabel'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrycode'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrylabel']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['countrycode']->value => $_smarty_tpl->tpl_vars['countrylabel']->value) {
$_smarty_tpl->tpl_vars['countrylabel']->_loop = true;
$__foreach_countrylabel_1_saved_local_item = $_smarty_tpl->tpl_vars['countrylabel'];
?>
              <option value="<?php echo $_smarty_tpl->tpl_vars['countrycode']->value;?>
"<?php if ((!$_smarty_tpl->tpl_vars['country']->value && $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['defaultcountry']->value) || $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['country']->value) {?> selected<?php }?>>
                <?php echo $_smarty_tpl->tpl_vars['countrylabel']->value;?>

              </option>
              <?php
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_1_saved_local_item;
}
if ($__foreach_countrylabel_1_saved_item) {
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_1_saved_item;
}
if ($__foreach_countrylabel_1_saved_key) {
$_smarty_tpl->tpl_vars['countrycode'] = $__foreach_countrylabel_1_saved_key;
}
?>
            </select>
          </div>
        </div>
      </div>
<hr>
      <?php if ($_smarty_tpl->tpl_vars['customfields']->value) {?>
        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderadditionalrequiredinfo'];?>
</h6>
        <div class="row">
          <?php
$_from = $_smarty_tpl->tpl_vars['customfields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_customfield_2_saved_item = isset($_smarty_tpl->tpl_vars['customfield']) ? $_smarty_tpl->tpl_vars['customfield'] : false;
$_smarty_tpl->tpl_vars['customfield'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['customfield']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['customfield']->value) {
$_smarty_tpl->tpl_vars['customfield']->_loop = true;
$__foreach_customfield_2_saved_local_item = $_smarty_tpl->tpl_vars['customfield'];
?>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="customfield<?php echo $_smarty_tpl->tpl_vars['customfield']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['customfield']->value['name'];?>
</label>
              <?php echo $_smarty_tpl->tpl_vars['customfield']->value['input'];?>

              <?php if ($_smarty_tpl->tpl_vars['customfield']->value['description']) {?>
             <p class="help-block">
                <?php echo $_smarty_tpl->tpl_vars['customfield']->value['description'];?>

            </p>
              <?php }?>
            </div>
          </div>
          <?php
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_2_saved_local_item;
}
if ($__foreach_customfield_2_saved_item) {
$_smarty_tpl->tpl_vars['customfield'] = $__foreach_customfield_2_saved_item;
}
?>
        </div>
      <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['domainsinorder']->value) {?>
    <div class="row">
      <div class="col-sm-12">
        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregistrantinfo'];?>
</h6>
      <p class="small text-muted"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['domainAlternativeContact'];?>
</p>
      <div class="form-group">
        <select name="contact" id="inputDomainContact" class="form-control input-lg">
          <option value=""><?php echo $_smarty_tpl->tpl_vars['LANG']->value['usedefaultcontact'];?>
</option>
          <?php
$_from = $_smarty_tpl->tpl_vars['domaincontacts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domcontact_3_saved_item = isset($_smarty_tpl->tpl_vars['domcontact']) ? $_smarty_tpl->tpl_vars['domcontact'] : false;
$_smarty_tpl->tpl_vars['domcontact'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domcontact']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['domcontact']->value) {
$_smarty_tpl->tpl_vars['domcontact']->_loop = true;
$__foreach_domcontact_3_saved_local_item = $_smarty_tpl->tpl_vars['domcontact'];
?>
          <option value="<?php echo $_smarty_tpl->tpl_vars['domcontact']->value['id'];?>
"<?php if ($_smarty_tpl->tpl_vars['contact']->value == $_smarty_tpl->tpl_vars['domcontact']->value['id']) {?> selected<?php }?>>
            <?php echo $_smarty_tpl->tpl_vars['domcontact']->value['name'];?>

          </option>
          <?php
$_smarty_tpl->tpl_vars['domcontact'] = $__foreach_domcontact_3_saved_local_item;
}
if ($__foreach_domcontact_3_saved_item) {
$_smarty_tpl->tpl_vars['domcontact'] = $__foreach_domcontact_3_saved_item;
}
?>
          <option value="addingnew"<?php if ($_smarty_tpl->tpl_vars['contact']->value == "addingnew") {?> selected<?php }?>>
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavaddcontact'];?>
...
          </option>
        </select>
      </div>
    </div>
    </div>

    <div class="<?php if ($_smarty_tpl->tpl_vars['contact']->value != "addingnew") {?> hidden<?php }?>" id="domainRegistrantInputFields">
      <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCFirstName">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['firstName'];?>

          </label>
          <input type="text" name="domaincontactfirstname" id="inputDCFirstName" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['firstname'];?>
">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCLastName">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['lastName'];?>

          </label>
          <input type="text" name="domaincontactlastname" id="inputDCLastName" class="form-control input-lg"  value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['lastname'];?>
">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCCompanyName">
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['companyName'];?>
 (<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['optional'];?>
)
          </label>
          <input type="text" name="domaincontactcompanyname" id="inputDCCompanyName" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['companyname'];?>
">
        </div>
      </div>
      </div>
      <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label for="inputDCEmail">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['emailAddress'];?>

          </label>
          <input type="email" name="domaincontactemail" id="inputDCEmail" class="form-control input-lg"  value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['email'];?>
">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label for="inputDCPhone">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['phoneNumber'];?>

          </label>
          <input type="tel" name="domaincontactphonenumber" id="inputDCPhone" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['phonenumber'];?>
">
        </div>
      </div>
    </div>
      <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label for="inputDCAddress1">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['streetAddress'];?>

          </label>
          <input type="text" name="domaincontactaddress1" id="inputDCAddress1" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['address1'];?>
">
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCAddress2">
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['streetAddress2'];?>

          </label>
          <input type="text" name="domaincontactaddress2" id="inputDCAddress2" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['address2'];?>
">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          <label for="inputDCCity">
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['city'];?>

          </label>
          <input type="text" name="domaincontactcity" id="inputDCCity" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['city'];?>
">
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label for="inputDCState">
         <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['state'];?>

          </label>
          <input type="text" name="domaincontactstate" id="inputDCState" class="form-control input-lg" value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['state'];?>
">
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label for="inputDCPostcode">
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['postcode'];?>

          </label>
          <input type="text" name="domaincontactpostcode" id="inputDCPostcode" class="form-control input-lg"  value="<?php echo $_smarty_tpl->tpl_vars['domaincontact']->value['postcode'];?>
">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-9">
        <div class="form-group">
          <label for="domaincontactcountry"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['country'];?>
</label>
          <select name="domaincontactcountry" id="inputDCCountry" class="form-control input-lg">
            <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_countrylabel_4_saved_item = isset($_smarty_tpl->tpl_vars['countrylabel']) ? $_smarty_tpl->tpl_vars['countrylabel'] : false;
$__foreach_countrylabel_4_saved_key = isset($_smarty_tpl->tpl_vars['countrycode']) ? $_smarty_tpl->tpl_vars['countrycode'] : false;
$_smarty_tpl->tpl_vars['countrylabel'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrycode'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['countrylabel']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['countrycode']->value => $_smarty_tpl->tpl_vars['countrylabel']->value) {
$_smarty_tpl->tpl_vars['countrylabel']->_loop = true;
$__foreach_countrylabel_4_saved_local_item = $_smarty_tpl->tpl_vars['countrylabel'];
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['countrycode']->value;?>
"<?php if ((!$_smarty_tpl->tpl_vars['domaincontact']->value['country'] && $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['defaultcountry']->value) || $_smarty_tpl->tpl_vars['countrycode']->value == $_smarty_tpl->tpl_vars['domaincontact']->value['country']) {?> selected<?php }?>>
              <?php echo $_smarty_tpl->tpl_vars['countrylabel']->value;?>

            </option>
            <?php
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_4_saved_local_item;
}
if ($__foreach_countrylabel_4_saved_item) {
$_smarty_tpl->tpl_vars['countrylabel'] = $__foreach_countrylabel_4_saved_item;
}
if ($__foreach_countrylabel_4_saved_key) {
$_smarty_tpl->tpl_vars['countrycode'] = $__foreach_countrylabel_4_saved_key;
}
?>
          </select>
        </div>
      </div>
    </div>
</div>
<?php }?>

<?php
$_from = $_smarty_tpl->tpl_vars['hookOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_5_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_5_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
    <div>
        <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

    </div>
<?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_5_saved_local_item;
}
if ($__foreach_output_5_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_5_saved_item;
}
?>

<?php if ($_smarty_tpl->tpl_vars['canUseCreditOnCheckout']->value) {?>
    <div id="applyCreditContainer" class="apply-credit-container" data-apply-credit="<?php echo $_smarty_tpl->tpl_vars['applyCredit']->value;?>
">
        <p><?php echo WHMCS\Smarty::langFunction(array('key'=>'cart.availableCreditBalance','amount'=>$_smarty_tpl->tpl_vars['creditBalance']->value),$_smarty_tpl);?>
</p>

        <?php if ($_smarty_tpl->tpl_vars['creditBalance']->value->toNumeric() >= $_smarty_tpl->tpl_vars['total']->value->toNumeric()) {?>
            <label class="radio">
                <input id="useFullCreditOnCheckout" type="radio" name="applycredit" value="1"<?php if ($_smarty_tpl->tpl_vars['applyCredit']->value) {?> checked<?php }?>>
                <?php echo WHMCS\Smarty::langFunction(array('key'=>'cart.applyCreditAmountNoFurtherPayment','amount'=>$_smarty_tpl->tpl_vars['total']->value),$_smarty_tpl);?>

            </label>
        <?php } else { ?>
            <label class="radio">
                <input id="useCreditOnCheckout" type="radio" name="applycredit" value="1"<?php if ($_smarty_tpl->tpl_vars['applyCredit']->value) {?> checked<?php }?>>
                <?php echo WHMCS\Smarty::langFunction(array('key'=>'cart.applyCreditAmount','amount'=>$_smarty_tpl->tpl_vars['creditBalance']->value),$_smarty_tpl);?>

            </label>
        <?php }?>

        <label class="radio">
            <input id="skipCreditOnCheckout" type="radio" name="applycredit" value="0"<?php if (!$_smarty_tpl->tpl_vars['applyCredit']->value) {?> checked<?php }?>>
            <?php echo WHMCS\Smarty::langFunction(array('key'=>'cart.applyCreditSkip','amount'=>$_smarty_tpl->tpl_vars['creditBalance']->value),$_smarty_tpl);?>

        </label>
    </div>
<?php }?>
<div id="paymentGatewaysContainer" class="form-group">
        <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
</h6>
        <div class="text-left">
            <?php
$_from = $_smarty_tpl->tpl_vars['gateways']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gateway_6_saved_item = isset($_smarty_tpl->tpl_vars['gateway']) ? $_smarty_tpl->tpl_vars['gateway'] : false;
$__foreach_gateway_6_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['gateway'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gateway']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['gateway']->value) {
$_smarty_tpl->tpl_vars['gateway']->_loop = true;
$__foreach_gateway_6_saved_local_item = $_smarty_tpl->tpl_vars['gateway'];
?>
                <label class="radio-inline">
                    <input type="radio" name="paymentmethod" value="<?php echo $_smarty_tpl->tpl_vars['gateway']->value['sysname'];?>
" class="payment-methods<?php if ($_smarty_tpl->tpl_vars['gateway']->value['type'] == "CC") {?> is-credit-card<?php }?>"<?php if ($_smarty_tpl->tpl_vars['selectedgateway']->value == $_smarty_tpl->tpl_vars['gateway']->value['sysname']) {?> checked<?php }?> />
                    <?php echo $_smarty_tpl->tpl_vars['gateway']->value['name'];?>

                </label>
            <?php
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_6_saved_local_item;
}
if ($__foreach_gateway_6_saved_item) {
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_6_saved_item;
}
if ($__foreach_gateway_6_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_gateway_6_saved_key;
}
?>
    </div>
</div>
    <div class="alert alert-danger text-center gateway-errors hidden"></div>
    <div id="creditCardInputFields"<?php if ($_smarty_tpl->tpl_vars['selectedgatewaytype']->value != "CC") {?> class="hidden"<?php }?>>
      <?php if ($_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour']) {?>
      <div class="row margin-bottom">
        <div class="col-sm-12">
          <div class="text-left pb-2">
            <label class="radio-inline">
              <input type="radio" name="ccinfo" value="useexisting" id="useexisting" <?php if ($_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour']) {?> checked<?php } else { ?> disabled<?php }?> />
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcarduseexisting'];?>

              <?php if ($_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour']) {?>
              (<?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour'];?>
)
              <?php }?>
            </label>
            <label class="radio-inline">
              <input type="radio" name="ccinfo" value="new" id="new" <?php if (!$_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour'] || $_smarty_tpl->tpl_vars['ccinfo']->value == "new") {?> checked<?php }?> />
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardenternewcard'];?>

            </label>
          </div>
        </div>
      </div>
      <?php } else { ?>
      <input type="hidden" name="ccinfo" value="new" />
      <?php }?>
      <div id="newCardInfo" class="row<?php if ($_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour'] && $_smarty_tpl->tpl_vars['ccinfo']->value != "new") {?> hidden<?php }?>">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="cctype">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardcardtype'];?>

            </label>
            <input type="hidden" id="cctype" name="cctype" value="<?php echo $_smarty_tpl->tpl_vars['acceptedcctypes']->value[0];?>
" />
            <div class="dropdown" id="cardType">
              <button class="btn btn-default btn-lg btn-block dropdown-toggle field" type="button" id="creditCardType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <span id="selectedCardType">
                  <i class="fa <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'getFontAwesomeCCIcon', array('ccType'=>$_smarty_tpl->tpl_vars['acceptedcctypes']->value[0]), true);?>
 fa-fw"></i>
                  <?php echo $_smarty_tpl->tpl_vars['acceptedcctypes']->value[0];?>

                </span>
                <span class="fa fa-caret-down fa-fw"></span>
              </button>
              <ul class="dropdown-menu" id="creditCardTypeDropDown" aria-labelledby="creditCardType">
                <?php
$_from = $_smarty_tpl->tpl_vars['acceptedcctypes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cardType_7_saved_item = isset($_smarty_tpl->tpl_vars['cardType']) ? $_smarty_tpl->tpl_vars['cardType'] : false;
$_smarty_tpl->tpl_vars['cardType'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cardType']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cardType']->value) {
$_smarty_tpl->tpl_vars['cardType']->_loop = true;
$__foreach_cardType_7_saved_local_item = $_smarty_tpl->tpl_vars['cardType'];
?>
                <li>
                  <a href="#">
                    <i class="fa <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'getFontAwesomeCCIcon', array('ccType'=>$_smarty_tpl->tpl_vars['cardType']->value), true);?>
 fa-fw"></i>
                    <span class="type">
                      <?php echo $_smarty_tpl->tpl_vars['cardType']->value;?>

                    </span>
                  </a>
                </li>
                <?php
$_smarty_tpl->tpl_vars['cardType'] = $__foreach_cardType_7_saved_local_item;
}
if ($__foreach_cardType_7_saved_item) {
$_smarty_tpl->tpl_vars['cardType'] = $__foreach_cardType_7_saved_item;
}
?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardNumber">
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['cardNumber'];?>

            </label>
            <input type="tel" name="ccnumber" id="inputCardNumber" class="form-control input-lg" placeholder="•••• •••• •••• ••••" autocomplete="cc-number">
          </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['showccissuestart']->value) {?>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardStart">
              <i class="fa fa-calendar-check-o"></i>
            </label>
            <input type="tel" name="ccstartdate" id="inputCardStart" class="form-control input-lg" placeholder="MM / YY (<?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardcardstart'];?>
)" autocomplete="cc-exp">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardIssue">
              <i class="fa fa-asterisk"></i>
            </label>
            <input type="tel" name="ccissuenum" id="inputCardIssue" class="form-control input-lg" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardcardissuenum'];?>
">
          </div>
        </div>
        <?php }?>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardExpiry">
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardcardexpires'];?>
 MM / YY
            </label>
            <input type="tel" name="ccexpirydate" id="inputCardExpiry" class="form-control input-lg" placeholder="MM / YY<?php if ($_smarty_tpl->tpl_vars['showccissuestart']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['LANG']->value['creditcardcardexpires'];?>
)<?php }?>" autocomplete="cc-exp">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputCardCVV">
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['cvv'];?>

            </label>
            <input type="tel" name="cccvv" id="inputCardCVV" class="form-control input-lg"  placeholder="CVV" autocomplete="cc-cvc">
          </div>
        </div>
      </div>
      <div id="existingCardInfo" class="row<?php if (!$_smarty_tpl->tpl_vars['clientsdetails']->value['cclastfour'] || $_smarty_tpl->tpl_vars['ccinfo']->value == "new") {?> hidden<?php }?>">
        <div class="col-sm-12">
          <div class="form-group">
            <label for="inputCardCvvExisting">
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['cvv'];?>

            </label>
            <input type="tel" name="cccvvexisting" id="inputCardCvvExisting" class="form-control input-lg" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['cvv'];?>
" autocomplete="cc-cvc">
          </div>
        </div>
      </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['shownotesfield']->value) {?>
    <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['additionalNotes'];?>
</h6>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <textarea name="notes" class="form-control" rows="4" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernotesdescription'];?>
"><?php echo $_smarty_tpl->tpl_vars['orderNotes']->value;?>
</textarea>
        </div>
      </div>
    </div>

    <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['accepttos']->value) {?>
          <p class="<?php if ($_smarty_tpl->tpl_vars['flowcart_final_order_button_tosidebar']->value) {?>hidden<?php }?>">
            <label class="checkbox-inline">
              <input type="checkbox" name="accepttos" id="accepttos" />
              &nbsp;
              <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertosagreement'];?>

              <a href="<?php echo $_smarty_tpl->tpl_vars['tosurl']->value;?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertos'];?>
</a>
            </label>
          </p>
          <?php }?>
          <div class="text-right">
          <button type="submit" id="btnCompleteOrder" class="<?php if ($_smarty_tpl->tpl_vars['flowcart_final_order_button_tosidebar']->value) {?>hidden<?php }?> btn btn-primary btn-lg"<?php if ($_smarty_tpl->tpl_vars['cartitems']->value == 0) {?> disabled="disabled"<?php }?> onclick="this.value='<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pleasewait'];?>
'">
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['completeorder'];?>

          </button>
        </div>
  </form>
</div>
</div>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.payment.js"><?php echo '</script'; ?>
>
<?php }
/* smarty_template_function_getFontAwesomeCCIcon_18096844255b63b11cb33388_79312840 */
if (!function_exists('smarty_template_function_getFontAwesomeCCIcon_18096844255b63b11cb33388_79312840')) {
function smarty_template_function_getFontAwesomeCCIcon_18096844255b63b11cb33388_79312840($_smarty_tpl,$params) {
$saved_tpl_vars = $_smarty_tpl->tpl_vars;
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value);
}
if ($_smarty_tpl->tpl_vars['ccType']->value == "Visa") {?>
fa-cc-visa
<?php } elseif ($_smarty_tpl->tpl_vars['ccType']->value == "MasterCard") {?>
fa-cc-mastercard
<?php } elseif ($_smarty_tpl->tpl_vars['ccType']->value == "Discover") {?>
fa-cc-discover
<?php } elseif ($_smarty_tpl->tpl_vars['ccType']->value == "American Express") {?>
fa-cc-amex
<?php } elseif ($_smarty_tpl->tpl_vars['ccType']->value == "JCB") {?>
fa-cc-jcb
<?php } elseif ($_smarty_tpl->tpl_vars['ccType']->value == "Diners Club" || $_smarty_tpl->tpl_vars['ccType']->value == "EnRoute") {?>
fa-cc-diners-club
<?php } else { ?>
fa-credit-card
<?php }
foreach (Smarty::$global_tpl_vars as $key => $value){
if (!isset($_smarty_tpl->tpl_vars[$key]) || $_smarty_tpl->tpl_vars[$key] === $value) $saved_tpl_vars[$key] = $value;
}
$_smarty_tpl->tpl_vars = $saved_tpl_vars;
}
}
/*/ smarty_template_function_getFontAwesomeCCIcon_18096844255b63b11cb33388_79312840 */
}
