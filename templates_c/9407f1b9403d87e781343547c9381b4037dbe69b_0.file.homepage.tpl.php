<?php
/* Smarty version 3.1.29, created on 2018-08-03 01:28:22
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/homepage.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63afb689c133_65257771',
  'file_dependency' => 
  array (
    '9407f1b9403d87e781343547c9381b4037dbe69b' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/homepage.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63afb689c133_65257771 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/home/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['doToday'],'icon'=>'calendar'), 0, true);
?>

   <div class="row px-1 pt-1">
   <?php if ($_smarty_tpl->tpl_vars['registerdomainenabled']->value || $_smarty_tpl->tpl_vars['transferdomainenabled']->value) {?>
     <div class="col-md-4">
      <a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['navservices'];?>
" href="domainchecker.php">
       <div class="info-box  bg-info  text-white" id="initial-tour">
        <div class="info-icon bg-info-dark">
         <span aria-hidden="true" class="icon icon-globe"></span>
       </div>
       <div class="info-details">
         <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['buyadomain'];?>
</h4>
         <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
</p>
       </div>
     </div>
   </a>
 </div>
 <?php }?>
 <div class="col-md-4">
  <a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductdomain'];?>
" href="cart.php">
   <div class="info-box  bg-info  text-white">
    <div class="info-icon bg-info-dark">
     <span aria-hidden="true" class="icon icon-layers"></span>
   </div>
   <div class="info-details">
     <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderhosting'];?>
</h4>
     <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
</p>
   </div>
 </div>
</a>
</div>
<div class="col-md-4">
<a title="<?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numdueinvoices'];?>
" href="submitticket.php">
  <div class="info-box  bg-info  text-white">
   <div class="info-icon bg-warn-dark">
    <span aria-hidden="true" class="icon icon-support"></span>
  </div>
  <div class="info-details">
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['getsupport'];?>
</h4>
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketssubmitticket'];?>
</p>
  </div>
</div>
</a>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['announcements']->value) {?>
<div class="panel panel-default panel-news">
  <div class="panel-heading text-uppercase"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ourlatestnews'];?>

    <div class="pull-right flip">
    <a class="prev pull-left"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="next"><span class="glyphicon glyphicon-chevron-right"></span></a></div>
  </div>
  <div class="panel-body">
   <div id="owl-news" class="owl-carousel pb-1">
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i> <a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[0]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[0]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[0]['text']),500,'...');?>
</div>
    <?php if ($_smarty_tpl->tpl_vars['announcements']->value[1]['text']) {?>
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i><a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[1]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[1]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[1]['text']),500,'...');?>
</div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['announcements']->value[2]['text']) {?>
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i><a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[2]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[2]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[2]['text']),500,'...');?>
</div>
    <?php }?>
  </div>
</div>
</div>
<?php echo '<script'; ?>
>$(document).ready(function() {
  var owl = $("#owl-news");owl.owlCarousel({
      autoHeight : true, loop:true, items:1
      <?php if ($_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'ar_AR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'fa_IR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'he_IL') {?> ,rtl:true <?php }?>
    });
$('.next').click(function() {
    owl.trigger('next.owl.carousel');
})
$('.prev').click(function() {
    owl.trigger('prev.owl.carousel', [300]);
})

});<?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['ccexpiringsoon']->value) {?>
<div class="alert alert-danger">
 <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ccexpiringsoon'];?>
:</strong> <?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['ccexpiringsoondesc'],'
  <a href="clientarea.php?action=creditcard" class="btn btn-danger btn-xs pull-right flip">','</a>');?>
</p>
</div>
<?php }?>


<section id="home-banner">
        <?php if ($_smarty_tpl->tpl_vars['registerdomainenabled']->value || $_smarty_tpl->tpl_vars['transferdomainenabled']->value) {?>
            <form method="post" action="domainchecker.php">
                <div class="row py-2 px-1">
                    <div class="col-md-12 pb-1">
                      <h4 class="text-center"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['homebegin'];?>
</h4>
                        <div class="input-group input-group-lg">
                            <input type="text" class="form-control" name="domain" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['exampledomain'];?>
" autocapitalize="none" />
                            <span class="input-group-btn">
                                <?php if ($_smarty_tpl->tpl_vars['registerdomainenabled']->value) {?>
                                    <input type="submit" class="btn btn-outline" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['search'];?>
" />
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['transferdomainenabled']->value) {?>
                                    <input type="submit" name="transfer" class="btn btn-outline" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainstransfer'];?>
" />
                                <?php }?>
                            </span>
                        </div>
                    </div>
                </div>
                <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/captcha.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

            </form>
        <?php }?>
    
</section>
<?php }
}
