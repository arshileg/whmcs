<?php
/* Smarty version 3.1.29, created on 2018-08-03 08:03:32
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/knowledgebasearticle.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b640c54dc2037_29765204',
  'file_dependency' => 
  array (
    'cf7586391407c5589f31be4b051c51552aba98d2' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/knowledgebasearticle.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b640c54dc2037_29765204 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasetitle'],'icon'=>'notebook'), 0, true);
?>

<p class="px-1"><small><?php echo $_smarty_tpl->tpl_vars['breadcrumbnav']->value;?>
</small></p>
<div class="row px-1">
<div class="col-md-12">
<h3><?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];?>
</h3>
<?php if ($_smarty_tpl->tpl_vars['kbarticle']->value['tags']) {?>
<p><i class="fa fa-tag"></i> <?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['tags'];?>
</p>
<?php }?>
<p><?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['text'];?>
</p>
</div>
</div>
<hr>
<div class="row px-1">
<div class="col-md-12">
<div class="hidden-print form-group">
<?php if ($_smarty_tpl->tpl_vars['kbarticle']->value['voted']) {
echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebaserating'];
}
if ($_smarty_tpl->tpl_vars['kbarticle']->value['voted']) {
echo $_smarty_tpl->tpl_vars['kbarticle']->value['useful'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebaseratingtext'];?>
 (<?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['votes'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebasevotes'];?>
)
			<?php } else { ?>
                <form action="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['id'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['urlfriendlytitle'];
$_tmp2=ob_get_clean();
echo routePath('knowledgebase-article-view',$_tmp1,$_tmp2);?>
" method="post">
                    <input type="hidden" name="useful" value="vote">
                    <div class="btn-group pull-left" role="group">
                    <button type="submit" name="vote" value="yes" class="btn btn-success" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebasehelpful'];?>
 - <?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebaseyes'];?>
">&nbsp;&nbsp;<i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;</button>
                    <button type="submit" name="vote" value="no" class="btn btn-danger" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebasehelpful'];?>
 - <?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebaseno'];?>
"><i class="fa fa-thumbs-o-down"></i></button>
                    </div>
                </form>
            <?php }?>
    <span class="pull-right"><a href="#" class="btn btn-link" onclick="window.print();return false"><i class="fa fa-print">&nbsp;</i><?php echo $_smarty_tpl->tpl_vars['LANG']->value['knowledgebaseprint'];?>
</a></span>
</div>
</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['kbarticles']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['knowledgebasealsoread'],'icon'=>'paper'-'clip'), 0, true);
?>


<div class="list-group">
	<?php
$_from = $_smarty_tpl->tpl_vars['kbarticles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_kbarticle_0_saved_item = isset($_smarty_tpl->tpl_vars['kbarticle']) ? $_smarty_tpl->tpl_vars['kbarticle'] : false;
$__foreach_kbarticle_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['kbarticle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['kbarticle']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['kbarticle']->value) {
$_smarty_tpl->tpl_vars['kbarticle']->_loop = true;
$__foreach_kbarticle_0_saved_local_item = $_smarty_tpl->tpl_vars['kbarticle'];
?>
<a class="list-group-item" href="<?php ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['id'];
$_tmp3=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['kbarticle']->value['urlfriendlytitle'];
$_tmp4=ob_get_clean();
echo routePath('knowledgebase-article-view',$_tmp3,$_tmp4);?>
"><?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];?>
 <span class="badge"><?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['views'];?>
</span></a>
	<?php
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticle_0_saved_local_item;
}
if ($__foreach_kbarticle_0_saved_item) {
$_smarty_tpl->tpl_vars['kbarticle'] = $__foreach_kbarticle_0_saved_item;
}
if ($__foreach_kbarticle_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_kbarticle_0_saved_key;
}
?>
</div>
<?php }
}
}
