<?php
/* Smarty version 3.1.29, created on 2018-08-01 11:32:45
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b61621e000f01_91252712',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533108765,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b61621e000f01_91252712 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><span style="font-size: medium;">მოგესალმებით,<br /><br />გერმანიიდან კავკასუსის დათაცენტრში არსებულ ჩვენსავე სერვერზე პორტირება დასრულებულია.<br />ამ ცვლილებასთან ერთად საჭიროა რამდენიმე დეტალის გათვალისწინება:</span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;"><strong>- თუკი იყენებთ Cloudflare სისტემას:</strong><br /><em>85.117.32.246 - მოცემული იპ მისამართით შეგიძლიათ ჩაასწოროთ ძველი.<br /><br /><strong>- თუკი იყენებთ გადახდის სისტემებს:</strong><br />85.117.32.246 - მოცემული იპის გადაგზავნაა საჭირო ბანკის ტექნიკური ჯგუფისთვის, იპის "თეთრ სიაში" შესატანად და გადახდების დასაშვებად.<br /><br /></em></span></p>
<p><span style="font-size: medium;"><span style="color: #ff0000;"><strong><em>- თუკი საიტზე არ გიშვებთ ან 404 ერორს</em><em> გიჩვენებთ (შიდა გვერდები გადაამოწმეთ):</em></strong></span><em><br /></em><em>-საჭიროა .htaccess ჩასწორება. თუკი თავად ვერ ახერხებთ, დაგვიკავშირდით და დაგეხმარებით.<br />-Wordpress-ზე საჭიროა შემდეგი: Settings &gt; permanlinks &gt; save settings (სულ ეს არის :))</em></span></p>
<p><br /><span style="font-size: medium;"><strong><span style="color: #ff0000;"><em>- მიგრაციის შემდგომ php ვერსია ყველა ანგარიშზე სტანდარტულ 5.6-ზე</em> დადგა:</span></strong><br /><em>cPanel-ში MultiPHP Manager-ის საშუალებით შეძლებთ php ვერსიის შეცვლას.<br /><br /></em></span></p>
<p><span style="font-size: medium;"><em>სხვა დამატებითი კითხვების მოგვმართეთ.<br /><br /></em></span></p>
<p><span style="font-size: medium;"><em>პატივისცემით,<br />ჰოსტნოუდსის ტექნიკური ჯგუფი</em></span></p><?php }
}
