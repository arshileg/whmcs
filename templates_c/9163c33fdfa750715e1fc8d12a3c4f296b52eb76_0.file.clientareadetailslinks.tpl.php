<?php
/* Smarty version 3.1.29, created on 2018-08-03 13:15:21
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareadetailslinks.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6455698651a0_95389596',
  'file_dependency' => 
  array (
    '9163c33fdfa750715e1fc8d12a3c4f296b52eb76' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareadetailslinks.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6455698651a0_95389596 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>    <ul class="nav nav-material nav-material-horizontal px-lg-30 pt-lg-30">
        <li <?php if ($_smarty_tpl->tpl_vars['clientareaaction']->value == "details") {?>class="active"<?php }?>><a href="clientarea.php?action=details"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavdetails'];?>
</a></li>
        <?php if ($_smarty_tpl->tpl_vars['condlinks']->value['updatecc']) {?><li <?php if ($_smarty_tpl->tpl_vars['clientareaaction']->value == "creditcard") {?>class="active"<?php }?>><a href="<?php echo $_SERVER['PHP_SELF'];?>
?action=creditcard"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavccdetails'];?>
</a></li><?php }?>
        <li <?php if ($_smarty_tpl->tpl_vars['clientareaaction']->value == "contacts" || $_smarty_tpl->tpl_vars['clientareaaction']->value == "addcontact") {?>class="active"<?php }?>><a href="<?php echo $_SERVER['PHP_SELF'];?>
?action=contacts"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavcontacts'];?>
</a></li>
        <li <?php if ($_smarty_tpl->tpl_vars['clientareaaction']->value == "changepw") {?>class="active"<?php }?>><a href="<?php echo $_SERVER['PHP_SELF'];?>
?action=changepw"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavchangepw'];?>
</a></li>
        <?php if ($_smarty_tpl->tpl_vars['condlinks']->value['security']) {?><li <?php if ($_smarty_tpl->tpl_vars['clientareaaction']->value == "security") {?>class="active"<?php }?>><a href="<?php echo $_SERVER['PHP_SELF'];?>
?action=security"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavsecurity'];?>
</a></li><?php }?>
    </ul>
<?php }
}
