<?php
/* Smarty version 3.1.29, created on 2018-07-25 00:00:02
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b5785427eb946_53384733',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1532462402,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b5785427eb946_53384733 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


Re: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;
if ($_smarty_tpl->tpl_vars['service_domain']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
)<?php }?>



    Recently you placed an upgrade order with us.
    Today your automated renewal invoice has been generated for the product/service listed above which has invalidated the upgrade quote and invoice.
    As a result, your upgrade order has now been cancelled.



Should you wish to continue with the upgrade, we ask that you please first pay the renewal invoice, after which you will be able to order the upgrade again and simply pay the difference.


We thank you for your business.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
