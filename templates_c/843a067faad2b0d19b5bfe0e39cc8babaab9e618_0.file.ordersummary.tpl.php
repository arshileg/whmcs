<?php
/* Smarty version 3.1.29, created on 2018-08-06 14:26:06
  from "/home/hostnodesnet/public_html/templates/orderforms/flowcart7/ordersummary.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b685a7e655548_59235679',
  'file_dependency' => 
  array (
    '843a067faad2b0d19b5bfe0e39cc8babaab9e618' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/orderforms/flowcart7/ordersummary.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b685a7e655548_59235679 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><h6><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['productinfo']['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['producttotals']->value['productinfo']['groupname'];?>
</h6>

<div class="order-row clearfix">
    <span class="pull-left flip"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['productinfo']['name'];?>
</span>
    <span class="pull-right flip"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['baseprice'];?>
</span>
</div>

<?php
$_from = $_smarty_tpl->tpl_vars['producttotals']->value['configoptions'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_configoption_0_saved_item = isset($_smarty_tpl->tpl_vars['configoption']) ? $_smarty_tpl->tpl_vars['configoption'] : false;
$_smarty_tpl->tpl_vars['configoption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['configoption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['configoption']->value) {
$_smarty_tpl->tpl_vars['configoption']->_loop = true;
$__foreach_configoption_0_saved_local_item = $_smarty_tpl->tpl_vars['configoption'];
?>
    <?php if ($_smarty_tpl->tpl_vars['configoption']->value) {?>
        <div class="clearfix">
            <span class="pull-left">&nbsp;&raquo; <?php echo $_smarty_tpl->tpl_vars['configoption']->value['name'];?>
: <?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</span>
            <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['configoption']->value['recurring'];
if ($_smarty_tpl->tpl_vars['configoption']->value['setup']) {?> + <?php echo $_smarty_tpl->tpl_vars['configoption']->value['setup'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersetupfee'];
}?></span>
        </div>
    <?php }
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_0_saved_local_item;
}
if ($__foreach_configoption_0_saved_item) {
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_0_saved_item;
}
?>

<?php
$_from = $_smarty_tpl->tpl_vars['producttotals']->value['addons'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_1_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_1_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
    <div class="row">
      <div class="col-md-12">
        <span class="pull-left"><i class="fa fa-fw fa-plus text-muted" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>
</span>
        <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['addon']->value['recurring'];?>
</span>
    </div>
  </div>
<?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_1_saved_local_item;
}
if ($__foreach_addon_1_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_1_saved_item;
}
?>

<?php if ($_smarty_tpl->tpl_vars['producttotals']->value['pricing']['setup'] || $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['recurring'] || $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['addons']) {?>
    <div class="summary-totals mt-1">
        <?php if ($_smarty_tpl->tpl_vars['producttotals']->value['pricing']['setup']) {?>
            <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartsetupfees'];?>
:</span>
                <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['setup'];?>
</span>
            </div>
        <?php }?>
        <?php
$_from = $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['recurringexcltax'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_recurring_2_saved_item = isset($_smarty_tpl->tpl_vars['recurring']) ? $_smarty_tpl->tpl_vars['recurring'] : false;
$__foreach_recurring_2_saved_key = isset($_smarty_tpl->tpl_vars['cycle']) ? $_smarty_tpl->tpl_vars['cycle'] : false;
$_smarty_tpl->tpl_vars['recurring'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cycle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['recurring']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cycle']->value => $_smarty_tpl->tpl_vars['recurring']->value) {
$_smarty_tpl->tpl_vars['recurring']->_loop = true;
$__foreach_recurring_2_saved_local_item = $_smarty_tpl->tpl_vars['recurring'];
?>
            <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['cycle']->value;?>
:</span>
                <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['recurring']->value;?>
</span>
            </div>
        <?php
$_smarty_tpl->tpl_vars['recurring'] = $__foreach_recurring_2_saved_local_item;
}
if ($__foreach_recurring_2_saved_item) {
$_smarty_tpl->tpl_vars['recurring'] = $__foreach_recurring_2_saved_item;
}
if ($__foreach_recurring_2_saved_key) {
$_smarty_tpl->tpl_vars['cycle'] = $__foreach_recurring_2_saved_key;
}
?>
        <?php if ($_smarty_tpl->tpl_vars['producttotals']->value['pricing']['tax1']) {?>
            <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['carttotals']->value['taxname'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['carttotals']->value['taxrate'];?>
%:</span>
                <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['tax1'];?>
</span>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['producttotals']->value['pricing']['tax2']) {?>
            <div class="clearfix">
                <span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['carttotals']->value['taxname2'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['carttotals']->value['taxrate2'];?>
%:</span>
                <span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['tax2'];?>
</span>
            </div>
        <?php }?>
    </div>
<?php }?>

<div class="order-row order-total text-uppercase">
  <span><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertotalduetoday'];?>
</span><span class="totaldue pull-right"><?php echo $_smarty_tpl->tpl_vars['producttotals']->value['pricing']['totaltoday'];?>
</span>
 </div>
<?php }
}
