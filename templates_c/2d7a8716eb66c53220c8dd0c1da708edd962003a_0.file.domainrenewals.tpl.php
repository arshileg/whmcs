<?php
/* Smarty version 3.1.29, created on 2018-04-21 15:43:45
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/domainrenewals.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5adb23f130c914_90194290',
  'file_dependency' => 
  array (
    '2d7a8716eb66c53220c8dd0c1da708edd962003a' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/domainrenewals.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/cart-header.tpl' => 1,
  ),
),false)) {
function content_5adb23f130c914_90194290 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/cart-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['registerdomain'],'desc'=>$_smarty_tpl->tpl_vars['groupname']->value,'step'=>2), 0, true);
?>

<div class="row flowcart-row">
        <div class="col-md-12">
                <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewals'];?>
</h4>
                <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewdesc'];?>
</p>
        </div>
  </div>
            <form method="post" action="cart.php?a=add&renewals=true">
                <table class="table table-hover table-striped renewals">
                    <thead>
                        <tr>
                            <th width="20"></th>
                            <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderdomain'];?>
</th>
                            <th class="text-center"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainstatus'];?>
</th>
                            <th class="text-center"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindaysuntilexpiry'];?>
</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
$_from = $_smarty_tpl->tpl_vars['renewals']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_renewal_0_saved_item = isset($_smarty_tpl->tpl_vars['renewal']) ? $_smarty_tpl->tpl_vars['renewal'] : false;
$_smarty_tpl->tpl_vars['renewal'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['renewal']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['renewal']->value) {
$_smarty_tpl->tpl_vars['renewal']->_loop = true;
$__foreach_renewal_0_saved_local_item = $_smarty_tpl->tpl_vars['renewal'];
?>
                            <tr>
                                <td>
                                    <?php if (!$_smarty_tpl->tpl_vars['renewal']->value['pastgraceperiod'] && !$_smarty_tpl->tpl_vars['renewal']->value['beforerenewlimit']) {?>
                                        <input type="checkbox" name="renewalids[]" value="<?php echo $_smarty_tpl->tpl_vars['renewal']->value['id'];?>
" />
                                    <?php }?>
                                </td>
                                <td>
                                    <?php echo $_smarty_tpl->tpl_vars['renewal']->value['domain'];?>

                                </td>
                                <td class="text-center">
                                    <?php echo $_smarty_tpl->tpl_vars['renewal']->value['status'];?>

                                </td>
                                <td class="text-center">
                                    <?php if ($_smarty_tpl->tpl_vars['renewal']->value['daysuntilexpiry'] > 30) {?>
                                        <span class="text-success">
                                            <?php echo $_smarty_tpl->tpl_vars['renewal']->value['daysuntilexpiry'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsdays'];?>

                                        </span>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['renewal']->value['daysuntilexpiry'] > 0) {?>
                                        <span class="text-danger">
                                            <?php echo $_smarty_tpl->tpl_vars['renewal']->value['daysuntilexpiry'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsdays'];?>

                                        </span>
                                    <?php } else { ?>
                                        <span>
                                            <?php echo $_smarty_tpl->tpl_vars['renewal']->value['daysuntilexpiry']*-1;?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsdaysago'];?>

                                        </span>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['renewal']->value['ingraceperiod']) {?>
                                        <br />
                                        <span class="text-danger">
                                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsingraceperiod'];?>

                                        </span>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php if ($_smarty_tpl->tpl_vars['renewal']->value['beforerenewlimit']) {?>
                                        <span class="text-danger">
                                            <?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsbeforerenewlimit'],$_smarty_tpl->tpl_vars['renewal']->value['beforerenewlimitdays']);?>

                                        </span>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['renewal']->value['pastgraceperiod']) {?>
                                        <span class="text-danger">
                                            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalspastgraceperiod'];?>

                                        </span>
                                    <?php } else { ?>
                                        <select class="form-control" name="renewalperiod[<?php echo $_smarty_tpl->tpl_vars['renewal']->value['id'];?>
]">
                                            <?php
$_from = $_smarty_tpl->tpl_vars['renewal']->value['renewaloptions'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_renewaloption_1_saved_item = isset($_smarty_tpl->tpl_vars['renewaloption']) ? $_smarty_tpl->tpl_vars['renewaloption'] : false;
$_smarty_tpl->tpl_vars['renewaloption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['renewaloption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['renewaloption']->value) {
$_smarty_tpl->tpl_vars['renewaloption']->_loop = true;
$__foreach_renewaloption_1_saved_local_item = $_smarty_tpl->tpl_vars['renewaloption'];
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['renewaloption']->value['period'];?>
">
                                                    <?php echo $_smarty_tpl->tpl_vars['renewaloption']->value['period'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
 @ <?php echo $_smarty_tpl->tpl_vars['renewaloption']->value['price'];?>

                                                </option>
                                            <?php
$_smarty_tpl->tpl_vars['renewaloption'] = $__foreach_renewaloption_1_saved_local_item;
}
if ($__foreach_renewaloption_1_saved_item) {
$_smarty_tpl->tpl_vars['renewaloption'] = $__foreach_renewaloption_1_saved_item;
}
?>
                                        </select>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php
$_smarty_tpl->tpl_vars['renewal'] = $__foreach_renewal_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['renewal']->_loop) {
?>
                            <tr class="carttablerow">
                                <td colspan="5"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainrenewalsnoneavailable'];?>
</td>
                            </tr>
                        <?php
}
if ($__foreach_renewal_0_saved_item) {
$_smarty_tpl->tpl_vars['renewal'] = $__foreach_renewal_0_saved_item;
}
?>
                    </tbody>
                </table>
                <div class="form-group text-right mx-1">
                    <button type="submit" class="btn btn-lg btn-success">
                        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>

                    </button>
                </div>
            </form>
<?php }
}
