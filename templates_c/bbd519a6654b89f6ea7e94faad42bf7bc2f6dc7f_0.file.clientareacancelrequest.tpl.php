<?php
/* Smarty version 3.1.29, created on 2018-04-21 04:12:47
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareacancelrequest.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ada81ffb89ac4_90699630',
  'file_dependency' => 
  array (
    'bbd519a6654b89f6ea7e94faad42bf7bc2f6dc7f' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareacancelrequest.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ada81ffb89ac4_90699630 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareacancelrequest'],'icon'=>'power'), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['invalid']->value) {?>
<div class="alert alert-warning">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelinvalid'];?>
</p>
</div>
<div class="textcenter">
    <input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" class="btn btn-default btn-sm pull-right flip" onclick="window.location='clientarea.php?action=productdetails&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
'" />
</div>
<?php } elseif ($_smarty_tpl->tpl_vars['requested']->value) {?>
<div class="alert alert-success">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelconfirmation'];?>
</p>
</div>
<div class="textcenter">
    <input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareabacklink'];?>
" class="btn btn-default btn-sm" onclick="window.location='clientarea.php?action=productdetails&id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
'" />
</div>
<?php } else {
if ($_smarty_tpl->tpl_vars['error']->value) {?>
<div class="alert alert-danger">
    <p class="bold"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
    <ul>
        <li><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelreasonrequired'];?>
</li>
    </ul>
</div>
<?php }?>
<div class="alert alert-block alert-warning">
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelproduct'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['groupname']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['productname']->value;?>
</strong><?php if ($_smarty_tpl->tpl_vars['domain']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
)<?php }?></p>
</div>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=cancel&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="form-stacked">
<div class="row">
      <div class="col-md-6 col-md-offset-3">
    <input type="hidden" name="sub" value="submit" />
    <fieldset class="control-group">
        <div class="form-group">
            <label class="control-label" for="cancellationreason"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelreason'];?>
</label>
            <textarea name="cancellationreason" id="cancellationreason" rows="6" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label class="control-label" for="type"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancellationtype'];?>
</label>
            <select name="type" class="form-control" id="type">
                <option value="Immediate"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancellationimmediate'];?>
</option>
                <option value="End of Billing Period"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancellationendofbillingperiod'];?>
</option>
            </select>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['domainid']->value) {?>
        <div class="alert alert-block alert-warning">
            <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cancelrequestdomain'];?>
</h4>
            <p><?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['cancelrequestdomaindesc'],$_smarty_tpl->tpl_vars['domainnextduedate']->value,$_smarty_tpl->tpl_vars['domainprice']->value,$_smarty_tpl->tpl_vars['domainregperiod']->value);?>
</p>
        </div>
        <div class="checkbox"><label><input type="checkbox" name="canceldomain" id="canceldomain" /><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cancelrequestdomainconfirm'];?>
</label></div>
        <?php }?>

            <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelrequestbutton'];?>
" class="btn btn-danger btn-lg btn-block" />
    </fieldset>
</div>
</div>
</form>
<?php }
}
}
