<?php
/* Smarty version 3.1.29, created on 2018-08-08 11:29:39
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6ad423a24216_36803523',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533727779,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6ad423a24216_36803523 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>გმადლობთ, რომ ჩვენს ტექნიკურ დახმრების ჯგუფს დაუკავშირდით. თქვენი მოთხოვნისამებრ ბილეთი გახსნილია, რომელზედაც პასუხის შემთხვევაში გაცნობებთ ელ.ფოსტის მეშვეობით. თქვენი ბილეთის დეტალები კი ქვემოთ არის მოცემული: </p>
<p>თემა: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>
<br /> პრიორიტეტი: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>
<br /> სტატუსი: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>
</p>
<p>ბილეთის ნახვა ნებისმიერ დროს შეგიძლიათ შემდეგ ბმულზე <?php echo $_smarty_tpl->tpl_vars['ticket_link']->value;?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
