<?php
/* Smarty version 3.1.29, created on 2018-02-18 08:41:41
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/pwreset.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a89040510bc68_57691875',
  'file_dependency' => 
  array (
    'c3ac9af7ce6fe639e90b8d2101d465f562f61321' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/pwreset.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a89040510bc68_57691875 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 0) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['pwreset']), 0, true);
?>

<?php }
if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>
    <div class="row">
      <div class="col-md-4 col-md-offset-4 box">
        <div class="content-wrap">
          <div class="alert alert-danger">
            <p  class="px-1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['noPasswordResetWhenLoggedIn'];?>
</p>
          </div>
          <?php } else { ?>
          <div class="row">
            <div class="col-md-4 col-md-offset-4 box">
              <p class="text-danger bg-danger text-alert <?php if (!$_smarty_tpl->tpl_vars['incorrect']->value) {?> displaynone<?php }?>" id="login-error"><strong><span aria-hidden="true" class="icon icon-ban"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['warning'];?>
</strong><br/><?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginincorrect'];?>
</p>
              <div class="content-wrap">
              <?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?><h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwreset'];?>
</h6><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['success']->value) {?>
                <p class="text-success bg-success text-alert"><span aria-hidden="true" class="icon icon-paper-plane"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetvalidationsent'];?>
</p><p><small><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetvalidationcheckemail'];?>
</small></p>
                <?php } else { ?>
                <?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
                <p class="text-danger bg-danger text-alert"><strong><span aria-hidden="true" class="icon icon-ban"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['warning'];?>
</strong><br/><?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>
</p>
                <?php } elseif (!$_smarty_tpl->tpl_vars['securityquestion']->value) {?>
                <p  class="px-1"><small><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetdesc'];?>
</small></p>
                <?php }?>
                <form method="post" action="pwreset.php"  name="frmpwreset">
                  <input type="hidden" name="action" value="reset" />
                  <?php if ($_smarty_tpl->tpl_vars['securityquestion']->value) {?>
                  <input type="hidden" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" />
                  <p><small><?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetsecurityquestionrequired'];?>
</small></p>
                  <div class="form-group">
                    <div class="input-group input-group-lg">
                      <span class="input-group-addon"><span aria-hidden="true" class="icon icon-question"></span></span>
                      <input class="form-control" name="answer" id="answer" type="text" value="<?php echo $_smarty_tpl->tpl_vars['answer']->value;?>
" placeholder="<?php echo $_smarty_tpl->tpl_vars['securityquestion']->value;?>
" />
                    </div>
                  </div>
                  <?php } else { ?>
                  <div class="form-group">
                    <div class="input-group input-group-lg">
                      <span class="input-group-addon"><span aria-hidden="true" class="icon icon-envelope"></span></span>
                      <input class="form-control" name="email" id="email" type="email" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['loginemail'];?>
" />
                    </div>
                  </div>
                  <?php }?>
                  <div class="row">
                   <div class="col-md-12">
                    <p><input type="submit" class="btn btn-primary btn-lg btn-block" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['pwresetsubmit'];?>
" /></p>
                  </div>
                </div>
              </form>
            </div>
            <?php }?>
          </div>
        </div>

<?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?>
        <div class="languageblock">
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/languageblock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        </div>
<?php }
}
}
}
