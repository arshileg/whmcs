<?php
/* Smarty version 3.1.29, created on 2018-08-11 00:00:22
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6e271624bb28_94813633',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533945622,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6e271624bb28_94813633 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,</p>
<p>გვინდა გაცნობოთ რომ ბილეთი #<?php echo $_smarty_tpl->tpl_vars['ticket_id']->value;?>
 ავტომატურად დაიხურა, რადგან თქვენგან პასუხი ბოლო <?php echo $_smarty_tpl->tpl_vars['ticket_auto_close_time']->value;?>
 საათის მანძილზე არ მიგვიღია..</p>
<p>თემა: <?php echo $_smarty_tpl->tpl_vars['ticket_subject']->value;?>
<br />განყოფილება: <?php echo $_smarty_tpl->tpl_vars['ticket_department']->value;?>
<br />პრიორიტეტი: <?php echo $_smarty_tpl->tpl_vars['ticket_priority']->value;?>
<br />სტატუსი: <?php echo $_smarty_tpl->tpl_vars['ticket_status']->value;?>
</p>
<p>დამატებით კითხვებისთვის შეგიძლიათ ბილეთი ხელახლა გახსნათ.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
