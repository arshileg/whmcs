<?php
/* Smarty version 3.1.29, created on 2018-08-12 00:00:19
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6f789354d807_44613917',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1534032019,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6f789354d807_44613917 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>This support request has been marked as completed.</p>
<p>We would really appreciate it if you would just take a moment to let us know about the quality of your experience.</p>
<p><a href="<?php echo $_smarty_tpl->tpl_vars['ticket_url']->value;?>
&amp;feedback=1"><?php echo $_smarty_tpl->tpl_vars['ticket_url']->value;?>
&amp;feedback=1</a></p>
<p>Your feedback is very important to us.</p>
<p>Thank you for your business.</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
