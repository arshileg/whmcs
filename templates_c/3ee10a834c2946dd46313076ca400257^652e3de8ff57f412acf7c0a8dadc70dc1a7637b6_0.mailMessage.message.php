<?php
/* Smarty version 3.1.29, created on 2018-08-10 06:04:29
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6d2aed6f2dd3_60580341',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1533881069,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6d2aed6f2dd3_60580341 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,<br /><br />თქვენი მოთხოვნისამებრ პაროლი აღდგენილია. მომხმარებლის არეში შესასვლელი მონაცემები მითითებულია ქვევით:</p>
<p><?php echo $_smarty_tpl->tpl_vars['whmcs_link']->value;?>
<br />ელ.ფოსტა: <?php echo $_smarty_tpl->tpl_vars['client_email']->value;?>
<br />პაროლი: <?php echo $_smarty_tpl->tpl_vars['client_password']->value;?>
</p>
<p>იმისათვის, რომ დაგენერირებული პაროლი შეცვალოთ დასამახსოვრებელი ვარიანტით, მომხმარებლის არეში შესვლის შემდეგ - გადადით გვერდზე "ანგარიშის მონაცემების ჩასწორება" &gt; "პაროლის შეცვლა".</p>
<p><?php echo $_smarty_tpl->tpl_vars['signature']->value;?>
</p><?php }
}
