<?php
/* Smarty version 3.1.29, created on 2018-08-12 00:00:19
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6f7893559aa6_57782821',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1534032019,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6f7893559aa6_57782821 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>This support request has been marked as completed.


We would really appreciate it if you would just take a moment to let us know about the quality of your experience.


<?php echo $_smarty_tpl->tpl_vars['ticket_url']->value;?>
&feedback=1


Your feedback is very important to us.


Thank you for your business.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
