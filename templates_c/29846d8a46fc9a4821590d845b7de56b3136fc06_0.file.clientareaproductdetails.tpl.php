<?php
/* Smarty version 3.1.29, created on 2018-08-03 06:08:35
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaproductdetails.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63f1638805c2_90919349',
  'file_dependency' => 
  array (
    '29846d8a46fc9a4821590d845b7de56b3136fc06' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/clientareaproductdetails.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63f1638805c2_90919349 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/home/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>

<?php if ($_smarty_tpl->tpl_vars['modulecustombuttonresult']->value) {
if ($_smarty_tpl->tpl_vars['modulecustombuttonresult']->value == "success") {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['moduleactionsuccess'],'textcenter'=>true,'idname'=>"alertModuleCustomButtonSuccess"), 0, true);
?>

<?php } else {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>($_smarty_tpl->tpl_vars['LANG']->value['moduleactionfailed']).(' ').($_smarty_tpl->tpl_vars['modulecustombuttonresult']->value),'textcenter'=>true,'idname'=>"alertModuleCustomButtonFailed"), 0, true);
?>

<?php }
}
if ($_smarty_tpl->tpl_vars['pendingcancellation']->value) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['LANG']->value['cancellationrequestedexplanation'],'textcenter'=>true,'idname'=>"alertPendingCancellation"), 0, true);
?>

<?php }?>
<ul class="nav nav-material nav-material-horizontal px-lg-30 pt-lg-30" id="tabs">
  <li class="active"><a href="#tabOverview" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['information'];?>
</a></li>
  <?php if ($_smarty_tpl->tpl_vars['modulechangepassword']->value) {?><li><a href="#tabChangepw" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverchangepassword'];?>
</a></li><?php }?>
  <?php if ($_smarty_tpl->tpl_vars['downloads']->value) {?><li><a href="#tabDownloads" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['downloadstitle'];?>
</a></li><?php }?>
  <?php if ($_smarty_tpl->tpl_vars['addonsavailable']->value) {?><li><a href="#tabAddons" data-toggle="tab"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingaddons'];?>
</a></li><?php }?>
  <?php if ($_smarty_tpl->tpl_vars['packagesupgrade']->value || $_smarty_tpl->tpl_vars['configoptionsupgrade']->value || $_smarty_tpl->tpl_vars['showcancelbutton']->value || $_smarty_tpl->tpl_vars['modulecustombuttons']->value) {?><li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['productmanagementactions'];?>
</a>
    <ul class="dropdown-menu">
      <?php
$_from = $_smarty_tpl->tpl_vars['modulecustombuttons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_command_0_saved_item = isset($_smarty_tpl->tpl_vars['command']) ? $_smarty_tpl->tpl_vars['command'] : false;
$__foreach_command_0_saved_key = isset($_smarty_tpl->tpl_vars['label']) ? $_smarty_tpl->tpl_vars['label'] : false;
$_smarty_tpl->tpl_vars['command'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['label'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['command']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['label']->value => $_smarty_tpl->tpl_vars['command']->value) {
$_smarty_tpl->tpl_vars['command']->_loop = true;
$__foreach_command_0_saved_local_item = $_smarty_tpl->tpl_vars['command'];
?>
      <li><a href="clientarea.php?action=productdetails&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
&amp;modop=custom&amp;a=<?php echo $_smarty_tpl->tpl_vars['command']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['label']->value;?>
</a></li>
      <?php
$_smarty_tpl->tpl_vars['command'] = $__foreach_command_0_saved_local_item;
}
if ($__foreach_command_0_saved_item) {
$_smarty_tpl->tpl_vars['command'] = $__foreach_command_0_saved_item;
}
if ($__foreach_command_0_saved_key) {
$_smarty_tpl->tpl_vars['label'] = $__foreach_command_0_saved_key;
}
?>
      <?php if ($_smarty_tpl->tpl_vars['packagesupgrade']->value) {?><li><a href="upgrade.php?type=package&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradedowngradepackage'];?>
</a></li><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['configoptionsupgrade']->value) {?><li><a href="upgrade.php?type=configoptions&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradedowngradeconfigoptions'];?>
</a></li><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['showcancelbutton']->value) {?><li><a href="clientarea.php?action=cancel&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareacancelrequestbutton'];?>
</a></li><?php }?>
    </ul>
  </li>
  <?php }?>
</ul>
<div class="tab-content">
  <div class="tab-pane clearfix active" id="tabOverview">
    <?php if ($_smarty_tpl->tpl_vars['tplOverviewTabOutput']->value) {?>
    <?php echo $_smarty_tpl->tpl_vars['tplOverviewTabOutput']->value;?>

    <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['type'] == 'cpanel') {?>
    <?php if ($_smarty_tpl->tpl_vars['moduleclientarea']->value) {?><div class="moduleoutput text-right"><?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['moduleclientarea']->value,'modulebutton','btn');?>
</div><?php }?>
    <?php }?>
  <?php } else { ?>
<div class="alert alert-<?php echo strtolower($_smarty_tpl->tpl_vars['rawstatus']->value);?>
 alert-product-details alert-block mt-2">
<div class="alert-watermark m-1">
    <i class="fa fa-<?php if ($_smarty_tpl->tpl_vars['type']->value == "hostingaccount" || $_smarty_tpl->tpl_vars['type']->value == "reselleraccount") {?>hdd-o<?php } elseif ($_smarty_tpl->tpl_vars['type']->value == "server") {?>database<?php } else { ?>archive<?php }?> fa-5x fa-inverse"></i>
  </div>
  <h4><?php echo $_smarty_tpl->tpl_vars['product']->value;?>
</h4>
  <span class="mr-2"><span aria-hidden="true" class="icon icon-layers"></span> <?php echo $_smarty_tpl->tpl_vars['groupname']->value;?>
</span>
  <span class="mr-2"><span aria-hidden="true" class="icon icon-info"></span> <?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
  <?php if ($_smarty_tpl->tpl_vars['packagesupgrade']->value) {?>
  <span class="mr-2"><span aria-hidden="true" class="icon icon-arrow-up-circle"></span> <a href="upgrade.php?type=package&amp;id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgrade'];?>
</a></span>
    <?php }?>
</div>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['information']), 0, true);
?>

  <div class="well mx-1 mt-1">
    <div class="row">
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingregdate'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['regdate']->value;?>
</dd>
          <?php if ($_smarty_tpl->tpl_vars['firstpaymentamount']->value != $_smarty_tpl->tpl_vars['recurringamount']->value) {?>
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['firstpaymentamount'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['firstpaymentamount']->value;?>
</dd>
          <?php }?>
        </dl>
      </div>
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <?php if ($_smarty_tpl->tpl_vars['billingcycle']->value != $_smarty_tpl->tpl_vars['LANG']->value['orderpaymenttermonetime'] && $_smarty_tpl->tpl_vars['billingcycle']->value != $_smarty_tpl->tpl_vars['LANG']->value['orderfree']) {?>
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['recurringamount'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['recurringamount']->value;?>
</dd>
          <?php }?>
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderbillingcycle'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['billingcycle']->value;?>
</dd>
        </dl>
      </div>
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['paymentmethod']->value;?>
</dd>
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['nextduedate']->value;?>
</dd>
          <?php if ($_smarty_tpl->tpl_vars['suspendreason']->value) {?>
          <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['suspendreason'];?>
</dt>
          <dd><?php echo $_smarty_tpl->tpl_vars['suspendreason']->value;?>
</dd>
          <?php }?>
        </dl>
      </div>
    </div>
  </div>
  <?php
$_from = $_smarty_tpl->tpl_vars['hookOutput']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_output_1_saved_item = isset($_smarty_tpl->tpl_vars['output']) ? $_smarty_tpl->tpl_vars['output'] : false;
$_smarty_tpl->tpl_vars['output'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['output']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['output']->value) {
$_smarty_tpl->tpl_vars['output']->_loop = true;
$__foreach_output_1_saved_local_item = $_smarty_tpl->tpl_vars['output'];
?>
  <div>
  <?php echo $_smarty_tpl->tpl_vars['output']->value;?>

  </div>
  <?php
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_local_item;
}
if ($__foreach_output_1_saved_item) {
$_smarty_tpl->tpl_vars['output'] = $__foreach_output_1_saved_item;
}
?>
  <?php if ($_smarty_tpl->tpl_vars['domain']->value || $_smarty_tpl->tpl_vars['moduleclientarea']->value || $_smarty_tpl->tpl_vars['configurableoptions']->value || $_smarty_tpl->tpl_vars['customfields']->value || $_smarty_tpl->tpl_vars['lastupdate']->value) {?>
  <?php if ($_smarty_tpl->tpl_vars['domain']->value) {?>
  <div class="row px-1 pt-1">
  <div class="col-md-12">
  <h4><?php if ($_smarty_tpl->tpl_vars['type']->value == "server") {
echo $_smarty_tpl->tpl_vars['LANG']->value['sslserverinfo'];
} elseif (($_smarty_tpl->tpl_vars['type']->value == "hostingaccount" || $_smarty_tpl->tpl_vars['type']->value == "reselleraccount") && $_smarty_tpl->tpl_vars['serverdata']->value) {
echo $_smarty_tpl->tpl_vars['LANG']->value['hostingInfo'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingdomain'];
}?></h4>
</div>
</div>
  <div class="well mx-1" id="domain">
    <?php if ($_smarty_tpl->tpl_vars['type']->value == "server") {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverhostname'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['domain']->value;?>

      </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['dedicatedip']->value) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['primaryIP'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['dedicatedip']->value;?>

      </div>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['assignedips']->value) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['assignedIPs'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo nl2br($_smarty_tpl->tpl_vars['assignedips']->value);?>

      </div>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['ns1']->value || $_smarty_tpl->tpl_vars['ns2']->value) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameservers'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['ns1']->value;?>
<br /><?php echo $_smarty_tpl->tpl_vars['ns2']->value;?>

      </div>
    </div>
    <?php }?>
    <?php } elseif (($_smarty_tpl->tpl_vars['type']->value == "hostingaccount" || $_smarty_tpl->tpl_vars['type']->value == "reselleraccount") && $_smarty_tpl->tpl_vars['serverdata']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['domain']->value) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderdomain'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
&nbsp;<a href="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
" target="_blank" class="btn btn-default btn-xs" ><?php echo $_smarty_tpl->tpl_vars['LANG']->value['visitwebsite'];?>
</a>
      </div>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['username']->value) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverusername'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['username']->value;?>

      </div>
    </div>
    <?php }?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['servername'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['serverdata']->value['hostname'];?>

      </div>
    </div>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainregisternsip'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['serverdata']->value['ipaddress'];?>

      </div>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver1'] || $_smarty_tpl->tpl_vars['serverdata']->value['nameserver2'] || $_smarty_tpl->tpl_vars['serverdata']->value['nameserver3'] || $_smarty_tpl->tpl_vars['serverdata']->value['nameserver4'] || $_smarty_tpl->tpl_vars['serverdata']->value['nameserver5']) {?>
    <div class="row">
      <div class="col-sm-5 text-right">
        <strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameservers'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver1']) {
echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver1'];?>
 (<?php echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver1ip'];?>
)<br /><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver2']) {
echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver2'];?>
 (<?php echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver2ip'];?>
)<br /><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver3']) {
echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver3'];?>
 (<?php echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver3ip'];?>
)<br /><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver4']) {
echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver4'];?>
 (<?php echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver4ip'];?>
)<br /><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['serverdata']->value['nameserver5']) {
echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver5'];?>
 (<?php echo $_smarty_tpl->tpl_vars['serverdata']->value['nameserver5ip'];?>
)<br /><?php }?>
      </div>
    </div>
    <?php }?>
    <?php } else { ?>
    <p>
      <?php echo $_smarty_tpl->tpl_vars['domain']->value;?>

    </p>
    <p>
      <a href="http://<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
" class="btn btn-default" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['visitwebsite'];?>
</a>
      <?php if ($_smarty_tpl->tpl_vars['domainId']->value) {?>
      <a href="clientarea.php?action=domaindetails&id=<?php echo $_smarty_tpl->tpl_vars['domainId']->value;?>
" class="btn btn-default" target="_blank"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['managedomain'];?>
</a>
      <?php }?>
      <input type="button" onclick="popupWindow('whois.php?domain=<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
','whois',650,420);return false;" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['whoisinfo'];?>
" class="btn btn-default" />
    </p>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['moduleclientarea']->value) {?>
    <div class="text-center module-client-area">
      <?php echo $_smarty_tpl->tpl_vars['moduleclientarea']->value;?>

    </div>
    <?php }?>
  </div>
  <?php } elseif ($_smarty_tpl->tpl_vars['moduleclientarea']->value) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['manage']), 0, true);
?>

  <div class="well mx-1">
    <?php if ($_smarty_tpl->tpl_vars['moduleclientarea']->value) {?>
    <div class="text-center module-client-area">
      <?php echo $_smarty_tpl->tpl_vars['moduleclientarea']->value;?>

    </div>
    <?php }?>
  </div>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['configurableoptions']->value) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['orderconfigpackage']), 0, true);
?>

  <div class="well mx-1">
    <?php
$_from = $_smarty_tpl->tpl_vars['configurableoptions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_configoption_2_saved_item = isset($_smarty_tpl->tpl_vars['configoption']) ? $_smarty_tpl->tpl_vars['configoption'] : false;
$_smarty_tpl->tpl_vars['configoption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['configoption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['configoption']->value) {
$_smarty_tpl->tpl_vars['configoption']->_loop = true;
$__foreach_configoption_2_saved_local_item = $_smarty_tpl->tpl_vars['configoption'];
?>
    <div class="row">
      <div class="col-sm-5">
        <strong><?php echo $_smarty_tpl->tpl_vars['configoption']->value['optionname'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php if ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 3) {
if ($_smarty_tpl->tpl_vars['configoption']->value['selectedqty']) {
echo $_smarty_tpl->tpl_vars['LANG']->value['yes'];
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['no'];
}
} elseif ($_smarty_tpl->tpl_vars['configoption']->value['optiontype'] == 4) {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedqty'];?>
 x <?php echo $_smarty_tpl->tpl_vars['configoption']->value['selectedoption'];
} else {
echo $_smarty_tpl->tpl_vars['configoption']->value['selectedoption'];
}?>
      </div>
    </div>
    <?php
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_2_saved_local_item;
}
if ($__foreach_configoption_2_saved_item) {
$_smarty_tpl->tpl_vars['configoption'] = $__foreach_configoption_2_saved_item;
}
?>
  </div>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['customfields']->value) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['additionalInfo']), 0, true);
?>

  <div class="well mx-1">
    <?php
$_from = $_smarty_tpl->tpl_vars['customfields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_3_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_3_saved_local_item = $_smarty_tpl->tpl_vars['field'];
?>
    <div class="row">
      <div class="col-sm-5">
        <strong><?php echo $_smarty_tpl->tpl_vars['field']->value['name'];?>
</strong>
      </div>
      <div class="col-sm-7 text-left">
        <?php echo $_smarty_tpl->tpl_vars['field']->value['value'];?>

      </div>
    </div>
    <?php
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_3_saved_local_item;
}
if ($__foreach_field_3_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_3_saved_item;
}
?>
  </div>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['lastupdate']->value) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['resourceUsage']), 0, true);
?>

  <div class="well mx-1">
    <div class="col-sm-10 col-sm-offset-1">
      <div class="col-sm-6">
        <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['diskSpace'];?>
</h4>
        <input type="text" value="<?php echo substr($_smarty_tpl->tpl_vars['diskpercent']->value,0,-1);?>
" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
        <p><?php echo $_smarty_tpl->tpl_vars['diskusage']->value;?>
MB / <?php echo $_smarty_tpl->tpl_vars['disklimit']->value;?>
MB</p>
      </div>
      <div class="col-sm-6">
        <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['bandwidth'];?>
</h4>
        <input type="text" value="<?php echo substr($_smarty_tpl->tpl_vars['bwpercent']->value,0,-1);?>
" class="dial-usage" data-width="100" data-height="100" data-min="0" data-readOnly="true" />
        <p><?php echo $_smarty_tpl->tpl_vars['bwusage']->value;?>
MB / <?php echo $_smarty_tpl->tpl_vars['bwlimit']->value;?>
MB</p>
      </div>
    </div>
    <div class="clearfix">
    </div>
    <p><small><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientarealastupdated'];?>
: <?php echo $_smarty_tpl->tpl_vars['lastupdate']->value;?>
</small></p>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.knob.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
    jQuery(function() {
    jQuery(".dial-usage").knob({'format':function (v) { alert(v); }});
    });
    <?php echo '</script'; ?>
>
  </div>
  <?php }?>

<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/bootstrap-tabdrop.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
jQuery('.nav-tabs-overflow').tabdrop();
<?php echo '</script'; ?>
>
<?php }?>
</div>
<div class="tab-pane" id="tabDownloads">
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['downloadstitle']), 0, true);
?>

  <?php ob_start();
echo WHMCS\Smarty::langFunction(array('key'=>"clientAreaProductDownloadsAvailable"),$_smarty_tpl);
$_tmp1=ob_get_clean();
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_tmp1), 0, true);
?>

  <div class="row  px-1 pt-1">
    <?php
$_from = $_smarty_tpl->tpl_vars['downloads']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_download_4_saved_item = isset($_smarty_tpl->tpl_vars['download']) ? $_smarty_tpl->tpl_vars['download'] : false;
$_smarty_tpl->tpl_vars['download'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['download']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['download']->value) {
$_smarty_tpl->tpl_vars['download']->_loop = true;
$__foreach_download_4_saved_local_item = $_smarty_tpl->tpl_vars['download'];
?>
    <div class="col-md-6">
    <div class="well">
      <h4><?php echo $_smarty_tpl->tpl_vars['download']->value['title'];?>
</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['download']->value['description'];?>
</p>
      <div class="form-group">
        <a href="<?php echo $_smarty_tpl->tpl_vars['download']->value['link'];?>
" class="btn btn-default"><i class="fa fa-download"></i> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['downloadname'];?>
</a>
      </div>
    </div>
    </div>
    <?php
$_smarty_tpl->tpl_vars['download'] = $__foreach_download_4_saved_local_item;
}
if ($__foreach_download_4_saved_item) {
$_smarty_tpl->tpl_vars['download'] = $__foreach_download_4_saved_item;
}
?>
  </div>
</div>
<div class="tab-pane" id="tabAddons">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['clientareahostingaddons']), 0, true);
?>

  <?php if ($_smarty_tpl->tpl_vars['addonsavailable']->value) {?>
  <?php ob_start();
echo WHMCS\Smarty::langFunction(array('key'=>"clientAreaProductAddonsAvailable"),$_smarty_tpl);
$_tmp2=ob_get_clean();
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>$_tmp2), 0, true);
?>

  <?php }?>
  <div class="row  px-1 pt-1">
    <?php
$_from = $_smarty_tpl->tpl_vars['addons']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_5_saved_item = isset($_smarty_tpl->tpl_vars['addon']) ? $_smarty_tpl->tpl_vars['addon'] : false;
$_smarty_tpl->tpl_vars['addon'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addon']->value) {
$_smarty_tpl->tpl_vars['addon']->_loop = true;
$__foreach_addon_5_saved_local_item = $_smarty_tpl->tpl_vars['addon'];
?>
    <div class="col-md-12">
    <div class="well">
      <h4><?php echo $_smarty_tpl->tpl_vars['addon']->value['name'];?>
 <small><span class="label status-<?php echo strtolower($_smarty_tpl->tpl_vars['addon']->value['rawstatus']);?>
"><?php echo $_smarty_tpl->tpl_vars['addon']->value['status'];?>
</span></small></h4>
      <span class="mr-2"><?php echo $_smarty_tpl->tpl_vars['addon']->value['pricing'];?>
</span><span class="mr-2"><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['registered'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['addon']->value['regdate'];?>
</span><span class="mr-2"><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareahostingnextduedate'];?>
:</strong> <?php echo $_smarty_tpl->tpl_vars['addon']->value['nextduedate'];?>
</span>
      <div class="form-group"><?php echo $_smarty_tpl->tpl_vars['addon']->value['managementActions'];?>
</div>      
    </div>
    </div>
    <?php
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_local_item;
}
if ($__foreach_addon_5_saved_item) {
$_smarty_tpl->tpl_vars['addon'] = $__foreach_addon_5_saved_item;
}
?>
  </div>
</div>
<div class="tab-pane" id="tabChangepw">
  <?php if ($_smarty_tpl->tpl_vars['modulechangepwresult']->value) {?>
  <?php if ($_smarty_tpl->tpl_vars['modulechangepwresult']->value == "success") {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"success",'msg'=>$_smarty_tpl->tpl_vars['modulechangepasswordmessage']->value,'textcenter'=>true), 0, true);
?>

  <?php } elseif ($_smarty_tpl->tpl_vars['modulechangepwresult']->value == "error") {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['modulechangepasswordmessage']->value),'textcenter'=>true), 0, true);
?>

  <?php }?>
  <?php }?>
<div class="row">
<div class="col-md-6 col-md-offset-3">
<h3><?php echo $_smarty_tpl->tpl_vars['LANG']->value['serverchangepassword'];?>
</h3>
  <div class="well">
    <form class="using-password-strength" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?action=productdetails#tabChangepw" role="form">
      <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" />
      <input type="hidden" name="modulechangepassword" value="true" />
      <div id="newPassword1" class="form-group has-feedback">
        <label for="inputNewPassword1" class="control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['newpassword'];?>
</label>
          <input type="password" class="form-control" id="inputNewPassword1" name="newpw"  autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/pwstrength-notips.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

      </div>
      <div id="newPassword2" class="form-group has-feedback">
        <label for="inputNewPassword2" class="control-label"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['confirmnewpassword'];?>
</label>
          <input type="password" class="form-control" id="inputNewPassword2" name="confirmpw"  autocomplete="off" />
          <span class="form-control-feedback glyphicon"></span>
          <div id="inputNewPassword2Msg">
        </div>
      </div>
      <div class="form-group">
          <input class="btn btn-primary btn-lg btn-block" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
" />
        </div>
    </form>
  </div>
  </div>
  </div>
  </div>
</div>
<?php }
}
