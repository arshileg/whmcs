<?php
/* Smarty version 3.1.29, created on 2018-05-31 21:08:06
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/upgradesummary.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b102bf6302de0_07970880',
  'file_dependency' => 
  array (
    '0f2d01e84f39d053175dd6942103df887bc6888c' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/upgradesummary.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b102bf6302de0_07970880 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['upgradedowngradepackage']), 0, true);
?>


<?php if ($_smarty_tpl->tpl_vars['promoerror']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"error",'msg'=>$_smarty_tpl->tpl_vars['promoerror']->value,'textcenter'=>true), 0, true);
?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['promorecurring']->value) {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"info",'msg'=>WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['recurringpromodesc'],$_smarty_tpl->tpl_vars['promorecurring']->value),'textcenter'=>true), 0, true);
?>

<?php }?>

<div class="alert alert-block alert-info">
    <?php echo $_smarty_tpl->tpl_vars['LANG']->value['upgradecurrentconfig'];?>
: <strong><?php echo $_smarty_tpl->tpl_vars['groupname']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['productname']->value;?>
</strong><?php if ($_smarty_tpl->tpl_vars['domain']->value) {?> (<?php echo $_smarty_tpl->tpl_vars['domain']->value;?>
)<?php }?>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderdesc'];?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderprice'];?>
</th>
        </tr>
    </thead>
    <tbody>
        <?php
$_from = $_smarty_tpl->tpl_vars['upgrades']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_upgrade_0_saved_item = isset($_smarty_tpl->tpl_vars['upgrade']) ? $_smarty_tpl->tpl_vars['upgrade'] : false;
$__foreach_upgrade_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['upgrade'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['upgrade']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['upgrade']->value) {
$_smarty_tpl->tpl_vars['upgrade']->_loop = true;
$__foreach_upgrade_0_saved_local_item = $_smarty_tpl->tpl_vars['upgrade'];
?>
            <?php if ($_smarty_tpl->tpl_vars['type']->value == "package") {?>
                <tr>
                    <td><input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['upgrade']->value['newproductid'];?>
" /><input type="hidden" name="billingcycle" value="<?php echo $_smarty_tpl->tpl_vars['upgrade']->value['newproductbillingcycle'];?>
" /><?php echo $_smarty_tpl->tpl_vars['upgrade']->value['oldproductname'];?>
 => <?php echo $_smarty_tpl->tpl_vars['upgrade']->value['newproductname'];?>
</td>
                    <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['upgrade']->value['price'];?>
</td>
                </tr>
            <?php } elseif ($_smarty_tpl->tpl_vars['type']->value == "configoptions") {?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['upgrade']->value['configname'];?>
: <?php echo $_smarty_tpl->tpl_vars['upgrade']->value['originalvalue'];?>
 => <?php echo $_smarty_tpl->tpl_vars['upgrade']->value['newvalue'];?>
</td>
                    <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['upgrade']->value['price'];?>
</td>
                </tr>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['upgrade'] = $__foreach_upgrade_0_saved_local_item;
}
if ($__foreach_upgrade_0_saved_item) {
$_smarty_tpl->tpl_vars['upgrade'] = $__foreach_upgrade_0_saved_item;
}
if ($__foreach_upgrade_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_upgrade_0_saved_key;
}
?>
        <tr class="masspay-total">
            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordersubtotal'];?>
:</td>
            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['subtotal']->value;?>
</td>
        </tr>
        <?php if ($_smarty_tpl->tpl_vars['promodesc']->value) {?>
            <tr class="masspay-total">
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['promodesc']->value;?>
:</td>
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
</td>
            </tr>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['taxrate']->value) {?>
            <tr class="masspay-total">
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['taxname']->value;?>
 @ <?php echo $_smarty_tpl->tpl_vars['taxrate']->value;?>
%:</td>
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['tax']->value;?>
</td>
            </tr>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['taxrate2']->value) {?>
            <tr class="masspay-total">
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['taxname2']->value;?>
 @ <?php echo $_smarty_tpl->tpl_vars['taxrate2']->value;?>
%:</td>
                <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['tax2']->value;?>
</td>
            </tr>
        <?php }?>
        <tr class="masspay-total">
            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordertotalduetoday'];?>
:</td>
            <td class="text-right"><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td>
        </tr>
    </tbody>
</table>

<?php if ($_smarty_tpl->tpl_vars['type']->value == "package") {?>
    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/alert.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('type'=>"warning",'msg'=>((((($_smarty_tpl->tpl_vars['LANG']->value['upgradeproductlogic']).(' (')).($_smarty_tpl->tpl_vars['upgrade']->value['daysuntilrenewal'])).(' ')).($_smarty_tpl->tpl_vars['LANG']->value['days'])).(')')), 0, true);
?>

<?php }?>

<div class="row px-1">
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
" role="form">
            <input type="hidden" name="step" value="2" />
            <input type="hidden" name="type" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" />
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" />
            <?php if ($_smarty_tpl->tpl_vars['type']->value == "package") {?>
                <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['upgrades']->value[0]['newproductid'];?>
" />
                <input type="hidden" name="billingcycle" value="<?php echo $_smarty_tpl->tpl_vars['upgrades']->value[0]['newproductbillingcycle'];?>
" />
            <?php }?>
            <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpromotioncode'];?>
</h5>
            <?php
$_from = $_smarty_tpl->tpl_vars['configoptions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_1_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_1_saved_key = isset($_smarty_tpl->tpl_vars['cid']) ? $_smarty_tpl->tpl_vars['cid'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cid'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cid']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$__foreach_value_1_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                <input type="hidden" name="configoption[<?php echo $_smarty_tpl->tpl_vars['cid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" />
            <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_local_item;
}
if ($__foreach_value_1_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_1_saved_item;
}
if ($__foreach_value_1_saved_key) {
$_smarty_tpl->tpl_vars['cid'] = $__foreach_value_1_saved_key;
}
?>
            <div class="input-group">
                <input class="form-control" type="text" name="promocode" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpromotioncode'];?>
" width="40"
                        <?php if ($_smarty_tpl->tpl_vars['promocode']->value) {?>value="<?php echo $_smarty_tpl->tpl_vars['promocode']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['promodesc']->value;?>
" disabled="disabled"<?php }?>>
                <?php if ($_smarty_tpl->tpl_vars['promocode']->value) {?>
                    <span class="input-group-btn">
                        <input type="submit" name="removepromo" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderdontusepromo'];?>
"
                               class="btn btn-danger" />
                    </span>
                <?php } else { ?>
                    <span class="input-group-btn">
                        <input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpromovalidatebutton'];?>
" class="btn btn-default" />
                    </span>
                <?php }?>
            </div>
        </form>

        <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
">
            <input type="hidden" name="step" value="3" />
            <input type="hidden" name="type" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" />
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" />
            <?php if ($_smarty_tpl->tpl_vars['type']->value == "package") {?>
                <input type="hidden" name="pid" value="<?php echo $_smarty_tpl->tpl_vars['upgrades']->value[0]['newproductid'];?>
" />
                <input type="hidden" name="billingcycle" value="<?php echo $_smarty_tpl->tpl_vars['upgrades']->value[0]['newproductbillingcycle'];?>
" />
            <?php }?>
            <?php
$_from = $_smarty_tpl->tpl_vars['configoptions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_value_2_saved_item = isset($_smarty_tpl->tpl_vars['value']) ? $_smarty_tpl->tpl_vars['value'] : false;
$__foreach_value_2_saved_key = isset($_smarty_tpl->tpl_vars['cid']) ? $_smarty_tpl->tpl_vars['cid'] : false;
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cid'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cid']->value => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$__foreach_value_2_saved_local_item = $_smarty_tpl->tpl_vars['value'];
?>
                <input type="hidden" name="configoption[<?php echo $_smarty_tpl->tpl_vars['cid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" />
            <?php
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_local_item;
}
if ($__foreach_value_2_saved_item) {
$_smarty_tpl->tpl_vars['value'] = $__foreach_value_2_saved_item;
}
if ($__foreach_value_2_saved_key) {
$_smarty_tpl->tpl_vars['cid'] = $__foreach_value_2_saved_key;
}
?>
            <?php if ($_smarty_tpl->tpl_vars['promocode']->value) {?><input type="hidden" name="promocode" value="<?php echo $_smarty_tpl->tpl_vars['promocode']->value;?>
"><?php }?>

            <h5><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderpaymentmethod'];?>
</h5>
            <div class="form-group">
                <select name="paymentmethod" id="inputPaymentMethod" class="form-control">
                    <?php if ($_smarty_tpl->tpl_vars['allowgatewayselection']->value) {?>
                        <option value="none"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['paymentmethoddefault'];?>
</option>
                    <?php }?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['gateways']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gateway_3_saved_item = isset($_smarty_tpl->tpl_vars['gateway']) ? $_smarty_tpl->tpl_vars['gateway'] : false;
$__foreach_gateway_3_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['gateway'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gateway']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['gateway']->value) {
$_smarty_tpl->tpl_vars['gateway']->_loop = true;
$__foreach_gateway_3_saved_local_item = $_smarty_tpl->tpl_vars['gateway'];
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['gateway']->value['sysname'];?>
"<?php if ($_smarty_tpl->tpl_vars['gateway']->value['sysname'] == $_smarty_tpl->tpl_vars['selectedgateway']->value) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['gateway']->value['name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_3_saved_local_item;
}
if ($__foreach_gateway_3_saved_item) {
$_smarty_tpl->tpl_vars['gateway'] = $__foreach_gateway_3_saved_item;
}
if ($__foreach_gateway_3_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_gateway_3_saved_key;
}
?>
                </select>
            </div>
            <div class="form-group"><input type="submit" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordercontinuebutton'];?>
" class="btn btn-primary btn-lg btn-block" /></div>
          </form>
    </div>
</div>
<?php }
}
