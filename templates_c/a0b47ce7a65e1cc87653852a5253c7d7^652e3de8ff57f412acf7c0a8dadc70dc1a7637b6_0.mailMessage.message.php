<?php
/* Smarty version 3.1.29, created on 2018-06-22 19:29:48
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b2d15ecbe92c2_26796575',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1529681388,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b2d15ecbe92c2_26796575 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><span style="font-size: medium;">მოგესალმებით,</span></p>
<p><span style="font-size: medium;">გვინდა განგიმარტოთ ის მიზეზები, რომელიც 22 ივნისს უძღოდა რამდენიმე საათიან გათიშვას:</span></p>
<p> </p>
<ul>
<li><span style="font-size: small;">სერვერზე დილის საათებიდან არსებობდა პრობლემა, რომელიც არ იყო სისტემური.</span></li>
<li><span style="font-size: small;">ამიტომაც, დავუკავშირდით ჩვენს გერმანულ დათაცენტრს, საიდანაც ტექნიკოსები ჩაერთნენ და გაეცნენ საქმის ვითარებას.</span></li>
<li><span style="font-size: small;">დიაგნოსტიკის შედეგად აღმოჩნდა რომ სერვერის კვების ბლოკი გადაიწვა.</span></li>
<li><span style="font-size: small;">შემდგომ დაფიქსირდა სხვა სერვერული ნაწილების მწყობრიდან გამოსვლაც. </span></li>
<li><span style="font-size: small;">ამიტომაც, საჭირო გახდა სერვერის ცვლილება. ამ ყველაფრის შემდგომ კი სერვერი ჩაირთო.</span></li>
</ul>
<p><span style="font-size: small;"> </span></p>
<p><span style="font-size: medium;">აქვე ვაანონსებთ "კავკასუს ონლაინთან" არსებულ შეთანხმებას, რომლის მიხედვით ვიცვლით ლოკაციას და ივლისის თვეში უკვე ქართული დათაცენტრიდან მოგემსახურებით.</span></p>
<p><span style="font-size: medium;">მოცემული სიახლე საიტების სისწრაფეზეც აისახება, ამავდროულად კი მოგვცემს საშუალებას საჭიროების შემთხვევაში ოპერატიულად აღმოფხვრათ ხარვეზები.</span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;">პატივისცემით,<br />ჰოსტნოუდსის ჯგუფი</span></p>
<p><span style="font-size: medium;"> </span></p><?php }
}
