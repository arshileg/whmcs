<?php
/* Smarty version 3.1.29, created on 2018-08-08 10:16:30
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6ac2fe496bf5_64532578',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533723390,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6ac2fe496bf5_64532578 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


Recently a request was submitted to reset your password for our client area. If you did not request this, please ignore this email. It will expire and become useless in 2 hours time.


To reset your password, please visit the url below:
<?php echo $_smarty_tpl->tpl_vars['pw_reset_url']->value;?>



When you visit the link above, you will have the opportunity to choose a new password.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
