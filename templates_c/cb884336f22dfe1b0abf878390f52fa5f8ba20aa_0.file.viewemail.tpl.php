<?php
/* Smarty version 3.1.29, created on 2018-02-27 13:08:42
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewemail.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a95201a54aad1_71027188',
  'file_dependency' => 
  array (
    'cb884336f22dfe1b0abf878390f52fa5f8ba20aa' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/viewemail.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a95201a54aad1_71027188 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaemails'];?>
 - <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/bootstrap-hexa.min.css" rel="stylesheet">
	<link href="templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/style.css" rel="stylesheet">
	<?php if ($_smarty_tpl->tpl_vars['hexa_extras_style']->value) {?><link rel='stylesheet' href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/skins/<?php echo $_smarty_tpl->tpl_vars['hexa_extras_style']->value;?>
" />
	<?php } else { ?>
	<link rel='stylesheet' href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['tehexa_extras_stylemplate']->value;?>
/assets/css/skins/city.min.css" />
	<?php }?>
</head>
<body style="background:#fff;">
	<div class="container">
		<div class="row py-1">
			<div class="col-md-12">
				<h3 class="text-center"><?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
</h3>
				<div class="well well-lg">
					<?php echo $_smarty_tpl->tpl_vars['message']->value;?>

				</div>
				<div class="form-group text-center">
					<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['closewindow'];?>
" class="btn btn-primary" onclick="window.close()" />
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php }
}
