<?php
/* Smarty version 3.1.29, created on 2018-08-11 00:00:12
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6e270c49b068_90552783',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533945612,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6e270c49b068_90552783 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


<?php if ($_smarty_tpl->tpl_vars['days_until_expiry']->value) {?>The domain(s) listed below are due to expire within the next <?php echo $_smarty_tpl->tpl_vars['days_until_expiry']->value;?>
 days.<?php } else { ?>The domain(s) listed below are going to expire in <?php echo $_smarty_tpl->tpl_vars['domain_days_until_expiry']->value;?>
 days. Renew now before it's too late...<?php }?>


<?php if ($_smarty_tpl->tpl_vars['expiring_domains']->value) {
$_from = $_smarty_tpl->tpl_vars['expiring_domains']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domain_0_saved_item = isset($_smarty_tpl->tpl_vars['domain']) ? $_smarty_tpl->tpl_vars['domain'] : false;
$_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domain']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
$__foreach_domain_0_saved_local_item = $_smarty_tpl->tpl_vars['domain'];
echo $_smarty_tpl->tpl_vars['domain']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['domain']->value['nextduedate'];?>
 (<?php echo $_smarty_tpl->tpl_vars['domain']->value['days'];?>
 Days)
<?php
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_0_saved_local_item;
}
if ($__foreach_domain_0_saved_item) {
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_0_saved_item;
}
} elseif ($_smarty_tpl->tpl_vars['domains']->value) {
$_from = $_smarty_tpl->tpl_vars['domains']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domain_1_saved_item = isset($_smarty_tpl->tpl_vars['domain']) ? $_smarty_tpl->tpl_vars['domain'] : false;
$_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domain']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
$__foreach_domain_1_saved_local_item = $_smarty_tpl->tpl_vars['domain'];
echo $_smarty_tpl->tpl_vars['domain']->value['name'];?>
 - <?php echo $_smarty_tpl->tpl_vars['domain']->value['nextduedate'];?>

<?php
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_1_saved_local_item;
}
if ($__foreach_domain_1_saved_item) {
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_1_saved_item;
}
} else {
echo $_smarty_tpl->tpl_vars['domain_name']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['domain_next_due_date']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['domain_days_until_nextdue']->value;?>
 Days)<?php }?>


To ensure the domain does not expire, you should renew it now. You can do this from the domains management section of our client area here: <?php echo $_smarty_tpl->tpl_vars['whmcs_link']->value;?>



Should you allow the domain to expire, you will be able to renew it for up to 30 days after the renewal date. During this time, the domain will not be accessible so any web site or email services associated with it will stop working.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
