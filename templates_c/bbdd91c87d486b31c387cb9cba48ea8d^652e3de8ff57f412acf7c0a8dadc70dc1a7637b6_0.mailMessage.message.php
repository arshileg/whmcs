<?php
/* Smarty version 3.1.29, created on 2018-07-03 22:00:48
  from "mailMessage:message" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b3bb9d0744592_56270914',
  'file_dependency' => 
  array (
    '652e3de8ff57f412acf7c0a8dadc70dc1a7637b6' => 
    array (
      0 => 'mailMessage:message',
      1 => 1530640848,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3bb9d0744592_56270914 ($_smarty_tpl) {
$template = $_smarty_tpl;
?><p><span style="font-size: medium;">მოგესალმებით,</span><br /><br /><span style="font-size: medium;">გვინდა გაცნობოთ, რომ <strong>გადმოვდივართ გერმანული დათაცენტრიდან კავკასუსის ქართულ დათაცენტრში.<br /></strong><span style="text-decoration: underline;"><strong>გადმოტანის პროცესი დაიწყება 4 ივლისიდან</strong>.</span><br /><br />ვებ-გვერდების შეუფერხებელი მუშაობისთვის <strong>საიტები 00:00-დან 08:00-მდე მონაკვეთში გადმოვა ქართულ დათაცენტრში</strong>.</span></p>
<p><br /><span style="font-size: medium;">ყველა ვებ-გვერდის გადმოტანა რამდენიმე ეტაპად მოხდება. <br /><br /><em><span style="text-decoration: underline;">თქვენი ანგარიშის გადმოსვლის შესახებ ინფორმაციას მიიღებთ ელ.ფოსტის სახი</span></em>თ.<br /><br /></span><span style="font-size: medium;">ამგვარად,<strong><span style="text-decoration: underline;"> გეკისრებით წერილის მიღების შემდგომ საიტის შემოწმების ვალდებულება</span></strong>.</span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;">პ.ს ყველა სხვა დამატებით კითხვებზე პასუხს მიიღებთ წერილის სახით. ამიტომაც, გახსენით ბილეთი ან მოგვწერეთ ფეისბუქზე, სადაც დროულად დაგიბრუნებთ პასუხს.<br /><br />პატივისცემით,<br />ჰოსტნოუდსის გუნდი</span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;"> </span></p>
<p><span style="font-size: medium;"> </span></p>
<p> </p><?php }
}
