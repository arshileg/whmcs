<?php
/* Smarty version 3.1.29, created on 2018-08-07 06:49:23
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6940f3583361_59199104',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533624563,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6940f3583361_59199104 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>ძვირფასო <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


სრულად გაეცანით წერილს და შეინახეთ მისი ასლი


მოხარულები ვართ, რადგან ჩვენ აგვირჩიეთ! თქვენი ჰოსტინგ ანგარიში მზადაა, ეს წერილი კი შეიცავს ყველა საჭირო ინფორმაცია, რომელიც მისი მართვისთვის გამოგადგებათ.


თუკი თქვენ რეგისტრაციისას მოითხოვეთ დომეინი, მხედველობაში მიიღეთ ის, რომ დომეინი ინტერნეტში უცბადვე არ გავრცელდება.  "დომეინის გავრცელების პროცესს" სჭირდება 1-48 საათამდე. სანამ თქვენი დომეინი არ გავრცელდება, მანამ თქვენი საიტი და ელ.ფოსტა არ იფუნქციონირებს. ჩვენ კი მოგაწვდით დროებით მისამართებს, რათა შეძლოთ თქვენი საიტი წინასწარი ხილვა და საჭირო ფაილების ატვირთვა. 


ახალი ანგარიშის დეტალები


ჰოსტინგ პაკეტი: <?php echo $_smarty_tpl->tpl_vars['service_product_name']->value;?>

დომეინი: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>

პირველადი გადახდის ოდენობა: <?php echo $_smarty_tpl->tpl_vars['service_first_payment_amount']->value;?>

პერიოდულად გადასახდელი თანხა: <?php echo $_smarty_tpl->tpl_vars['service_recurring_amount']->value;?>

გადახდის ციკლი: <?php echo $_smarty_tpl->tpl_vars['service_billing_cycle']->value;?>

შემდგომი გადახდის თარიღი: <?php echo $_smarty_tpl->tpl_vars['service_next_due_date']->value;?>



დეტალები შესვლისთვის


Username: <?php echo $_smarty_tpl->tpl_vars['service_username']->value;?>

Password: <?php echo $_smarty_tpl->tpl_vars['service_password']->value;?>



საკონტროლო პანელის მისამართი: http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
:2082/
დომეინის მიბმის შემდგომ, შეგიძლიათ გამოიყენოთ შემდეგი მისამართი: http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>
:2082/


ინფორმაცია სერვერზე


სერვერის დასახელები: <?php echo $_smarty_tpl->tpl_vars['service_server_name']->value;?>

სერვერის IP: <?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>



თუკი თქვენ იყენებთ არსებულ დომეინს თქვენი ახალი ანგარიშისთვის, მოგიწევთ ამ დომეინისთვის nameserver-ების შეცვლა. 


Nameserver 1: <?php echo $_smarty_tpl->tpl_vars['service_ns1']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns1_ip']->value;?>
)
Nameserver 2: <?php echo $_smarty_tpl->tpl_vars['service_ns2']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns2_ip']->value;?>
)<?php if ($_smarty_tpl->tpl_vars['service_ns3']->value) {?>
Nameserver 3: <?php echo $_smarty_tpl->tpl_vars['service_ns3']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns3_ip']->value;?>
)<?php }
if ($_smarty_tpl->tpl_vars['service_ns4']->value) {?>
Nameserver 4: <?php echo $_smarty_tpl->tpl_vars['service_ns4']->value;?>
 (<?php echo $_smarty_tpl->tpl_vars['service_ns4_ip']->value;?>
)<?php }?>


საიტზე ატვირთვა


დროებით, თქვენ შეგიძლიათ გამოიყენოთ ქვემოთ მოცემული მისამართები საიტის სამართავად: 


დროებითი FTP Hostname: <?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>

დროებითი მისამართი საიტზე წვდომისთვის: http://<?php echo $_smarty_tpl->tpl_vars['service_server_ip']->value;?>
/~<?php echo $_smarty_tpl->tpl_vars['service_username']->value;?>
/


დომეინის მიბმის შემდეგ კი შეგიძლიათ გამოიყენოთ ქვემოთ მოცემული მისამართები:


FTP Hostname: <?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>

Webpage URL: http://www.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>



ელ.ფოსტის პარამეტრები


ელ.ფოსტის ანგარიშების მოსაწყობა, თქვენს საფოსტო პროგრამაში უნდა გამოიყენოთ შემდეგი დასაკავშირებელი მონაცემები: 


POP3 Host Address: mail.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>

SMTP Host Address: mail.<?php echo $_smarty_tpl->tpl_vars['service_domain']->value;?>

Username: ელ.ფოსტა, რომლის შემოწმებაც გსურთ
Password: პაროლი, რომელსაც საკონტროლო პანელში იყენებთ


გმადლობთ, რომ აგვირჩიეთ.


<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
