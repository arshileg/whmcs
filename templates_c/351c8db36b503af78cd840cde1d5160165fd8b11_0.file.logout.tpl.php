<?php
/* Smarty version 3.1.29, created on 2018-08-03 08:43:54
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/logout.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6415ca204e08_59694833',
  'file_dependency' => 
  array (
    '351c8db36b503af78cd840cde1d5160165fd8b11' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/logout.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6415ca204e08_59694833 ($_smarty_tpl) {
$template = $_smarty_tpl;
?> <div class="row">
  <div class="col-md-4 col-md-offset-4 box">
    <div class="content-wrap">
      <h6><?php echo $_smarty_tpl->tpl_vars['LANG']->value['logouttitle'];?>
</h6>
        <p class="text-success bg-success text-alert"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['logoutsuccessful'];?>
</p>
       <div class="row">
         <div class="col-md-12">
          <p><a class="btn btn-primary btn-lg btn-block" href="index.php" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['logoutcontinuetext'];?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['logoutcontinuetext'];?>
</a></p>
    </div>
    </div>
    </div>
  </div>
  </div>
<?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?>
<div class="languageblock">
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/languageblock.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

</div>
<?php }
}
}
