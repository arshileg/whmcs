<?php
/* Smarty version 3.1.29, created on 2018-08-09 00:00:09
  from "mailMessage:plaintext" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b6b840962d0c7_98763106',
  'file_dependency' => 
  array (
    'dac51ccee8dbecedf9afb805fb153d5c6bf41d7a' => 
    array (
      0 => 'mailMessage:plaintext',
      1 => 1533772809,
      2 => 'mailMessage',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6b840962d0c7_98763106 ($_smarty_tpl) {
$template = $_smarty_tpl;
?>Dear <?php echo $_smarty_tpl->tpl_vars['client_name']->value;?>
,


This is a billing notice that your invoice no. <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>
 which was generated on <?php echo $_smarty_tpl->tpl_vars['invoice_date_created']->value;?>
 is now overdue.


Your payment method is: <?php echo $_smarty_tpl->tpl_vars['invoice_payment_method']->value;?>



Invoice: <?php echo $_smarty_tpl->tpl_vars['invoice_num']->value;?>

 Balance Due: <?php echo $_smarty_tpl->tpl_vars['invoice_balance']->value;?>

 Due Date: <?php echo $_smarty_tpl->tpl_vars['invoice_date_due']->value;?>



თქვენ შეგიძლია შეხვიდეთ მომხმარებლის არეში, რათა ნახოთ და გადაიხდაოთ ინვოისი. ინვოისის ლინკი: <?php echo $_smarty_tpl->tpl_vars['invoice_link']->value;?>



<?php echo $_smarty_tpl->tpl_vars['signature']->value;
}
}
