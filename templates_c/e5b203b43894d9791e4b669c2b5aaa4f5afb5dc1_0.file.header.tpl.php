<?php
/* Smarty version 3.1.29, created on 2018-02-18 00:02:23
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a888a4fca4c29_83762480',
  'file_dependency' => 
  array (
    'e5b203b43894d9791e4b669c2b5aaa4f5afb5dc1' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/header.tpl',
      1 => 1512111033,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a888a4fca4c29_83762480 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
$template = $_smarty_tpl;
if (file_exists($_smarty_tpl->tpl_vars['header_override']->value)) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/overrides/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php } else { ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9"> <![endif]-->
<head>
  <meta charset="UTF-8">
  <title><?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['pagetitle']->value;
if ($_smarty_tpl->tpl_vars['kbarticle']->value['title']) {?> - <?php echo $_smarty_tpl->tpl_vars['kbarticle']->value['title'];
}?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/bootstrap-hexa.min.css" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/icons.min.css" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/perfect-scrollbar.min.css" rel="stylesheet">
   <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/fonts/bpg-arial.min.css">
  <?php if (!empty($_smarty_tpl->tpl_vars['loadMarkdownEditor']->value)) {?><link href="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_CSS']->value;?>
/bootstrap-markdown.min.css" rel="stylesheet" /><?php }?>
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/style.css" rel="stylesheet">
  <?php if ($_smarty_tpl->tpl_vars['hexa_extras_style']->value) {?><link rel='stylesheet' href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/skins/<?php echo $_smarty_tpl->tpl_vars['hexa_extras_style']->value;?>
" />
  <?php } else { ?>
  <link rel='stylesheet' href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/skins/original.min.css" />
  <?php }?>
  <?php echo '<script'; ?>
 type="text/javascript">
  var csrfToken = '<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
',
  markdownGuide = '<?php echo WHMCS\Smarty::langFunction(array('key'=>"markdown.title"),$_smarty_tpl);?>
',
  locale = '<?php if (!empty($_smarty_tpl->tpl_vars['mdeLocale']->value)) {
echo $_smarty_tpl->tpl_vars['mdeLocale']->value;
} else { ?>en<?php }?>',
  saved = '<?php echo WHMCS\Smarty::langFunction(array('key'=>"markdown.saved"),$_smarty_tpl);?>
',
  saving = '<?php echo WHMCS\Smarty::langFunction(array('key'=>"markdown.saving"),$_smarty_tpl);?>
';
  <?php echo '</script'; ?>
>
  <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['BASE_PATH_JS']->value;?>
/jquery.min.js"><?php echo '</script'; ?>
>
  <?php if ($_smarty_tpl->tpl_vars['templatefile']->value == "viewticket" && !$_smarty_tpl->tpl_vars['loggedin']->value) {?>
  <meta name="robots" content="noindex" />
  <?php }?>
  <?php echo $_smarty_tpl->tpl_vars['headoutput']->value;?>

  <?php if (file_exists($_smarty_tpl->tpl_vars['style_override']->value)) {?>
  <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/overrides/style.css">
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'ar_AR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'fa_IR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'he_IL') {?>
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/bootstrap-rtl.css" rel="stylesheet">
  <link href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/templates/<?php echo $_smarty_tpl->tpl_vars['template']->value;?>
/assets/css/style-rtl.css" rel="stylesheet">
  <?php }?>
  <?php if (isset($_smarty_tpl->tpl_vars['hexa_scss']->value)) {?>
  <style type="text/css">
  <?php echo $_smarty_tpl->tpl_vars['hexa_scss']->value;?>

  </style>
  <?php }?>

</head>
<body  data-phone-cc-input="<?php echo $_smarty_tpl->tpl_vars['phoneNumberInputStyle']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'ar_AR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'fa_IR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'he_IL') {?>cmsbased-rtl<?php } else { ?>cmsbased-ltr<?php }
if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?> minimal<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['hexa_background_image']->value)) {?> style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['hexa_background_image']->value;?>
)"<?php }?>>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/includes/verifyemail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <?php if ($_smarty_tpl->tpl_vars['hexa_minimal']->value == 1) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template_head_minimal']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <?php } elseif ($_smarty_tpl->tpl_vars['minimal_cart']->value == 'on' && $_smarty_tpl->tpl_vars['filename']->value == 'cart') {?>
  <div class="hexa-container hidden-xs"><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/contact.php" class="btn btn-xs btn-default hexa-btn" data-original-title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['contactus'];?>
"><i class="fa fa-send-o"></i></a></div>
  <div class="login-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4 logo-page">
          <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/index.php" title="<?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
">
            <?php if (!isset($_smarty_tpl->tpl_vars['hexa_logo']->value)) {?>
            <span class="logo">
              <span aria-hidden="true" class="icon icon-map"></span> <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
</span>
              <?php } else { ?>
              <span class="logo">
                <img <?php if (isset($_smarty_tpl->tpl_vars['hexa_logo_width']->value)) {?> style="width:250px" <?php }?> src="<?php echo $_smarty_tpl->tpl_vars['hexa_logo']->value;?>
">
              </span>
              <?php }?>
            </a>
          </div>
        </div>
        <?php } else { ?>
        <?php echo $_smarty_tpl->tpl_vars['headeroutput']->value;?>

        <div class="hexa-container hidden-xs"><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/contact.php" class="btn btn-xs btn-default hexa-btn" data-original-title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['contactus'];?>
"><i class="fa fa-send-o"></i></a></div>

          <?php if ($_smarty_tpl->tpl_vars['languagechangeenabled']->value && count($_smarty_tpl->tpl_vars['locales']->value) > 1 && !$_smarty_tpl->tpl_vars['loggedin']->value) {?>
          <div class="hexa-container hexa-container-lang hidden-xs">
          <div class="btn-group">
          <button type="button" class="btn btn-default dropdown-toggle hexa-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa icon-globe"></i>
          </button>
          <ul class="dropdown-menu">
                 <?php
$_from = $_smarty_tpl->tpl_vars['locales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_locale_0_saved_item = isset($_smarty_tpl->tpl_vars['locale']) ? $_smarty_tpl->tpl_vars['locale'] : false;
$_smarty_tpl->tpl_vars['locale'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['locale']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->_loop = true;
$__foreach_locale_0_saved_local_item = $_smarty_tpl->tpl_vars['locale'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'] == $_smarty_tpl->tpl_vars['locale']->value['localisedName']) {?>
                                          <li><a class="active" href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                                          <?php } else { ?>
                                          <li><a href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
" ><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                            <?php }?>
                 <?php
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_local_item;
}
if ($__foreach_locale_0_saved_item) {
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_item;
}
?>
          </ul>
        </div>
          </div>
        <?php }?>

        <nav class="navbar hx-top-header" role="navigation">
          <div class="container">
            <?php if (!isset($_smarty_tpl->tpl_vars['hexa_logo']->value)) {?>
            <a class="navbar-brand" href="index.php"><span aria-hidden="true" class="icon icon-map"></span> <?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
</a>
            <?php } else { ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/index.php" class="navbar-brand" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['hexa_logo']->value;?>
); <?php if (isset($_smarty_tpl->tpl_vars['hexa_logo_width']->value)) {?> width:250px<?php }?>;     height: 55px;"></a>
            <?php }?>
            <ul class="nav navbar-nav hx-top-nav">
              <li class="nav-item visible-xs"><span class="nav-link"><span aria-hidden="true" class="icon icon-menu btn-nav-toggle-responsive"></span></span></li>
            </ul>

            <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>

            <ul class="nav navbar-nav hx-user-nav hidden-xs">
              <li class="dropdown nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle nav-link"> <img src="https://www.gravatar.com/avatar/<?php echo $_smarty_tpl->tpl_vars['usermailhash']->value;?>
?s=50&d=mm" class="avatar"><span class="user-name"><?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['clientsdetails']->value['lastname'];?>
</span><span class="angle-down icon icon-arrow-down"></span></a>
                <div role="menu" class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=details"><span aria-hidden="true" class="icon icon-user"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['editaccountdetails'];?>
</a>
                  <a class="dropdown-item" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=changepw"><span aria-hidden="true" class="icon icon-lock"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareanavchangepw'];?>
</a>
                  <?php if ($_smarty_tpl->tpl_vars['condlinks']->value['addfunds']) {?><a class="dropdown-item" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=addfunds"><span aria-hidden="true" class="icon icon-plus"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfunds'];?>
</a><?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['condlinks']->value['updatecc']) {?><a class="dropdown-item" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=creditcard"><span aria-hidden="true" class="icon icon-credit-card"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['navmanagecc'];?>
</a><?php }?>
                  <a class="dropdown-item" href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/logout.php"><span aria-hidden="true" class="icon icon-logout"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['logouttitle'];?>
</a>
                </div>
              </li>
            </ul>

            <ul class="navbar-nav hx-icons-nav hidden-xs">
              <li class="dropdown nav-item hx-messages"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span class="icon icon-envelope"></span></a>
                <ul class="dropdown-menu">
                  <li>
                    <div class="title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['navemailssent'];?>
</div>
                    <div class="hx-scroller ps-messages">
                      <div class="content">
                        <ul>
                          <?php
$_from = $_smarty_tpl->tpl_vars['he_emaillist']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_he_email_1_saved_item = isset($_smarty_tpl->tpl_vars['he_email']) ? $_smarty_tpl->tpl_vars['he_email'] : false;
$__foreach_he_email_1_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['he_email'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['he_email']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['he_email']->value) {
$_smarty_tpl->tpl_vars['he_email']->_loop = true;
$__foreach_he_email_1_saved_local_item = $_smarty_tpl->tpl_vars['he_email'];
?>
                          <li><a href="#" onclick="popupWindow('<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/viewemail.php?id=<?php echo $_smarty_tpl->tpl_vars['he_email']->value['id'];?>
','emlmsg',960,480);return false;">
                            <div class="icon"><span aria-hidden="true" class="icon icon-envelope"></span></div>
                            <div class="content"><span class="date"><span class="glyphicon glyphicon-time"></span> <?php echo $_smarty_tpl->tpl_vars['he_email']->value['time'];?>
</span>
                              <span class="name"><?php echo $_smarty_tpl->tpl_vars['companyname']->value;?>
</span><span class="desc"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['he_email']->value['subject'],150,'...');?>
</span></div></a>
                            </li>
                              <?php
$_smarty_tpl->tpl_vars['he_email'] = $__foreach_he_email_1_saved_local_item;
}
if ($__foreach_he_email_1_saved_item) {
$_smarty_tpl->tpl_vars['he_email'] = $__foreach_he_email_1_saved_item;
}
if ($__foreach_he_email_1_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_he_email_1_saved_key;
}
?>
                            </ul>
                          </div>
                        </div>
                        <div class="footer"> <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=emails"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['viewAll'];?>
</a></div>
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown nav-item hx-notifications"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span class="icon icon-bell"></span><?php if ($_smarty_tpl->tpl_vars['clientsstats']->value['numoverdueinvoices'] > 0 || $_smarty_tpl->tpl_vars['openissuescount']->value || $_smarty_tpl->tpl_vars['ccexpiringsoon']->value || $_smarty_tpl->tpl_vars['ccexpiringsoon']->value) {?><span class="indicator"></span><?php }?></a>
                    <ul class="dropdown-menu">
                      <li>
                        <div class="title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['notifications'];?>
</div>
                        <div class="hx-scroller">
                          <div class="content">
                            <ul>
                              <?php if ($_smarty_tpl->tpl_vars['clientsstats']->value['numoverdueinvoices'] > 0 && in_array('invoices',$_smarty_tpl->tpl_vars['contactpermissions']->value)) {?>
                              <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=masspay&amp;all=true">
                                  <div class="icon"><span aria-hidden="true" class="icon icon-drawer"></span></div>
                                  <div class="content"><span class="desc"><?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['youhaveoverdueinvoices'],$_smarty_tpl->tpl_vars['clientsstats']->value['numoverdueinvoices']);?>
</span></div>
                                </a>
                              </li>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['clientsstats']->value['incredit'] && in_array('invoices',$_smarty_tpl->tpl_vars['contactpermissions']->value)) {?>
                              <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=addfunds">
                                  <div class="icon"><span aria-hidden="true" class="icon icon-wallet"></span></div>
                                  <div class="content"><span class="desc"><?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['availcreditbaldesc'],$_smarty_tpl->tpl_vars['clientsstats']->value['creditbalance']);?>
</span></div>
                                </a>
                              </li>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['openissuescount']->value) {?>
                              <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/serverstatus.php?view=open">
                                  <div class="icon"><span aria-hidden="true" class="icon icon-feed"></span></div>
                                  <div class="content"><span class="desc"><?php echo $_smarty_tpl->tpl_vars['openissuescount']->value;?>
 Open Service Issues</span></div>
                                </a>
                              </li>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['scheduledissuescount']->value) {?>
                              <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/serverstatus.php?view=scheduled">
                                  <div class="icon"><span aria-hidden="true" class="icon icon-feed"></span></div>
                                  <div class="content"><span class="desc"><?php echo $_smarty_tpl->tpl_vars['scheduledissuescount']->value;?>
 Scheduled Service</span></div>
                                </a>
                              </li>
                              <?php }?>
                              <?php if ($_smarty_tpl->tpl_vars['ccexpiringsoon']->value && in_array('invoices',$_smarty_tpl->tpl_vars['contactpermissions']->value)) {?>
                              <li>
                                <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php?action=creditcard">
                                  <div class="icon"><span aria-hidden="true" class="icon icon-credit-card"></span></div>
                                  <div class="content"><span class="desc"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ccexpiringsoon'];?>
</span></div>
                                </a>
                              </li>
                              <?php }?>
                            </ul>
                          </div></div>
                          <div class="footer"> <a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/serverstatus.php"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['networkstatustitle'];?>
</a></div>
                        </li>
                      </ul>
                    </li>
                    <li class="nav-item hx-settings"><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/cart.php?a=view" class="nav-link"><span aria-hidden="true" class="icon icon-basket"></span><span class="cart-items"><?php echo $_smarty_tpl->tpl_vars['cartitems']->value;?>
</span></a></li>
                    <?php if ($_smarty_tpl->tpl_vars['adminMasqueradingAsClient']->value) {?>
                    <li class="nav-item hx-settings"><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/logout.php?returntoadmin=1" class="nav-link" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['logoutandreturntoadminarea'];?>
"><span aria-hidden="true" class="icon icon-magic-wand"></span></a></li>
                    <?php } elseif ($_smarty_tpl->tpl_vars['adminLoggedIn']->value) {?>
                    <li class="nav-item hx-settings"><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/logout.php?returntoadmin=1" class="nav-link" title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['returntoadminarea'];?>
"><span aria-hidden="true" class="icon icon-magic-wand"></span></a></li>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['languagechangeenabled']->value && count($_smarty_tpl->tpl_vars['locales']->value) > 1) {?>
                    <li class="dropdown nav-item hx-settings"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span class="icon icon-globe-alt"></span></a>
                      <ul class="dropdown-menu">
                        <li>
                          <div class="title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'];?>
</div>
                          <div class="hx-scroller ps-languages">
                            <div class="content">
                              <ul>
                                <?php
$_from = $_smarty_tpl->tpl_vars['locales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_locale_2_saved_item = isset($_smarty_tpl->tpl_vars['locale']) ? $_smarty_tpl->tpl_vars['locale'] : false;
$_smarty_tpl->tpl_vars['locale'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['locale']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->_loop = true;
$__foreach_locale_2_saved_local_item = $_smarty_tpl->tpl_vars['locale'];
?>
                                <?php if ($_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'] == $_smarty_tpl->tpl_vars['locale']->value['localisedName']) {?>
                                <li class="dropdown-item"><a class="active" href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                                <?php } else { ?>
                                <li class="dropdown-item"><a href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
" ><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
                                <?php }?>
                                <?php
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_2_saved_local_item;
}
if ($__foreach_locale_2_saved_item) {
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_2_saved_item;
}
?>
                              </ul>
                            </div>
                          </div>

                        </li>
                      </ul>
                    </li>
                    <?php }?>
                  </ul>
                  <?php } else { ?>
                  <ul class="nav navbar-nav hx-logout-nav hidden-xs">
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['WEB_ROOT']->value;?>
/clientarea.php"><span aria-hidden="true" class="icon icon-login"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['login'];?>
</a></li>
                  </ul>

                  <?php }?>
                </div>
              </nav>
              <div class="site-holder container <?php echo $_smarty_tpl->tpl_vars['mini']->value;?>
">
                <div class="box-holder">
                  <div class="left-sidebar">
                    <div class="sidebar-holder" data-original-title="" title="">
                      <?php if ($_smarty_tpl->tpl_vars['loggedin']->value) {?>
                      <ul class="nav nav-list">
                        <li class="nav-toggle"><button class="btn  btn-nav-toggle"><i class="fa
                          <?php if ($_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'ar_AR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'fa_IR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'he_IL') {?>
                          toggle-right fa-angle-double-right
                          <?php } else { ?>
                          toggle-left fa-angle-double-left
                          <?php }?>
                          " style="color:#eee;"></i> </button></li>

                          <?php
$_from = $_smarty_tpl->tpl_vars['hexa_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_menuitem_3_saved_item = isset($_smarty_tpl->tpl_vars['menuitem']) ? $_smarty_tpl->tpl_vars['menuitem'] : false;
$__foreach_menuitem_3_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['menuitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['menuitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['menuitem']->value) {
$_smarty_tpl->tpl_vars['menuitem']->_loop = true;
$__foreach_menuitem_3_saved_local_item = $_smarty_tpl->tpl_vars['menuitem'];
?>

                          <li class="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['class'];?>
 <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> submenu <?php }?> <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['active'] && !isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> active <?php }?>  ">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['url'];?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['name'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> class="dropdown <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['active']) {?> active-parent <?php }?> "<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['submenuitem']->value['new_window']) && $_smarty_tpl->tpl_vars['submenuitem']->value['new_window'] == 1) {?> target="_blank" <?php }?>  >
                            <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['icon'] != '') {?><<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon_holder_tag'];?>
 aria-hidden="true" class=" <?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon'];?>
 "></<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon_holder_tag'];?>
><?php }?>
                            <span class="hidden-minibar <?php echo $_smarty_tpl->tpl_vars['hide_labels']->value;?>
"> <?php echo $_smarty_tpl->tpl_vars['menuitem']->value['name'];?>
</span> <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> <span class="fa arrow rotate"> </span><?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['badge']) {?>
                            <span class="badge badge-default pull-right flip"><?php echo $_smarty_tpl->tpl_vars['menuitem']->value['badge'];?>
</span>
                            <?php }?>
                          </a>
                          <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?>
                          <ul <?php echo $_smarty_tpl->tpl_vars['display']->value;?>
>
                            <?php
$_from = $_smarty_tpl->tpl_vars['menuitem']->value['child'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_submenuitem_4_saved_item = isset($_smarty_tpl->tpl_vars['submenuitem']) ? $_smarty_tpl->tpl_vars['submenuitem'] : false;
$__foreach_submenuitem_4_saved_key = isset($_smarty_tpl->tpl_vars['subnum']) ? $_smarty_tpl->tpl_vars['subnum'] : false;
$_smarty_tpl->tpl_vars['submenuitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['subnum'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['submenuitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['subnum']->value => $_smarty_tpl->tpl_vars['submenuitem']->value) {
$_smarty_tpl->tpl_vars['submenuitem']->_loop = true;
$__foreach_submenuitem_4_saved_local_item = $_smarty_tpl->tpl_vars['submenuitem'];
?>
                            <li class="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['class'];?>
 <?php if ($_smarty_tpl->tpl_vars['submenuitem']->value['active']) {?> active <?php }?>">
                              <a href="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['url'];?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['name'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['submenuitem']->value['new_window']) && $_smarty_tpl->tpl_vars['submenuitem']->value['new_window'] == 1) {?> target="_blank" <?php }?> >
                                <?php if ($_smarty_tpl->tpl_vars['submenuitem']->value['icon'] != '') {?><<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon_holder_tag'];?>
 aria-hidden="true" class=" <?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon'];?>
 "></<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon_holder_tag'];?>
><?php }?>
                                <span class="hidden-minibar  <?php echo $_smarty_tpl->tpl_vars['hide_labels']->value;?>
"> <?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['name'];?>
</span>
                                <?php if ($_smarty_tpl->tpl_vars['submenuitem']->value['badge']) {?>
                                <span class="badge badge-default pull-right flip"><?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['badge'];?>
</span>
                                <?php }?>
                              </a>
                            </li>
                            <?php
$_smarty_tpl->tpl_vars['submenuitem'] = $__foreach_submenuitem_4_saved_local_item;
}
if ($__foreach_submenuitem_4_saved_item) {
$_smarty_tpl->tpl_vars['submenuitem'] = $__foreach_submenuitem_4_saved_item;
}
if ($__foreach_submenuitem_4_saved_key) {
$_smarty_tpl->tpl_vars['subnum'] = $__foreach_submenuitem_4_saved_key;
}
?>
                          </ul>
                          <?php }?>
                        </li>
                        <?php
$_smarty_tpl->tpl_vars['menuitem'] = $__foreach_menuitem_3_saved_local_item;
}
if ($__foreach_menuitem_3_saved_item) {
$_smarty_tpl->tpl_vars['menuitem'] = $__foreach_menuitem_3_saved_item;
}
if ($__foreach_menuitem_3_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_menuitem_3_saved_key;
}
?>
                      </ul>
                      <?php } else { ?>
                      <ul class="nav nav-list">
                        <li class="nav-toggle"><button class="btn  btn-nav-toggle"><i class="fa toggle-left fa-angle-double-left" style="color:#eee;"></i> </button></li>

                        <?php
$_from = $_smarty_tpl->tpl_vars['hexa_menu']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_menuitem_5_saved_item = isset($_smarty_tpl->tpl_vars['menuitem']) ? $_smarty_tpl->tpl_vars['menuitem'] : false;
$__foreach_menuitem_5_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['menuitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['menuitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['menuitem']->value) {
$_smarty_tpl->tpl_vars['menuitem']->_loop = true;
$__foreach_menuitem_5_saved_local_item = $_smarty_tpl->tpl_vars['menuitem'];
?>

                        <li class="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['class'];?>
 <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> submenu <?php }?> <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['active'] && !isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> active <?php }?>  ">
                          <a href="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['url'];?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['name'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> class="dropdown <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['active']) {?> active-parent <?php }?> "<?php }?> <?php if (isset($_smarty_tpl->tpl_vars['submenuitem']->value['new_window']) && $_smarty_tpl->tpl_vars['submenuitem']->value['new_window'] == 1) {?> target="_blank" <?php }?>  >
                            <?php if ($_smarty_tpl->tpl_vars['menuitem']->value['icon'] != '') {?><<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon_holder_tag'];?>
 aria-hidden="true" class=" <?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon'];?>
 "></<?php echo $_smarty_tpl->tpl_vars['menuitem']->value['icon_holder_tag'];?>
><?php }?>
                            <span class="hidden-minibar <?php echo $_smarty_tpl->tpl_vars['hide_labels']->value;?>
"> <?php echo $_smarty_tpl->tpl_vars['menuitem']->value['name'];?>
</span> <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?> <span class="fa arrow rotate"> </span><?php }?>
                          </a>
                          <?php if (isset($_smarty_tpl->tpl_vars['menuitem']->value['child'])) {?>
                          <ul <?php echo $_smarty_tpl->tpl_vars['display']->value;?>
>
                            <?php
$_from = $_smarty_tpl->tpl_vars['menuitem']->value['child'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_submenuitem_6_saved_item = isset($_smarty_tpl->tpl_vars['submenuitem']) ? $_smarty_tpl->tpl_vars['submenuitem'] : false;
$__foreach_submenuitem_6_saved_key = isset($_smarty_tpl->tpl_vars['subnum']) ? $_smarty_tpl->tpl_vars['subnum'] : false;
$_smarty_tpl->tpl_vars['submenuitem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['subnum'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['submenuitem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['subnum']->value => $_smarty_tpl->tpl_vars['submenuitem']->value) {
$_smarty_tpl->tpl_vars['submenuitem']->_loop = true;
$__foreach_submenuitem_6_saved_local_item = $_smarty_tpl->tpl_vars['submenuitem'];
?>
                            <li class="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['class'];?>
 <?php if ($_smarty_tpl->tpl_vars['submenuitem']->value['active']) {?> active <?php }?>">
                              <a href="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['url'];?>
" data-original-title="<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['name'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['submenuitem']->value['new_window']) && $_smarty_tpl->tpl_vars['submenuitem']->value['new_window'] == 1) {?> target="_blank" <?php }?> >
                                <?php if ($_smarty_tpl->tpl_vars['submenuitem']->value['icon'] != '') {?><<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon_holder_tag'];?>
 aria-hidden="true" class=" <?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon'];?>
 "></<?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['icon_holder_tag'];?>
><?php }?>
                                <span class="hidden-minibar  <?php echo $_smarty_tpl->tpl_vars['hide_labels']->value;?>
"> <?php echo $_smarty_tpl->tpl_vars['submenuitem']->value['name'];?>
</span>
                              </a>
                            </li>
                            <?php
$_smarty_tpl->tpl_vars['submenuitem'] = $__foreach_submenuitem_6_saved_local_item;
}
if ($__foreach_submenuitem_6_saved_item) {
$_smarty_tpl->tpl_vars['submenuitem'] = $__foreach_submenuitem_6_saved_item;
}
if ($__foreach_submenuitem_6_saved_key) {
$_smarty_tpl->tpl_vars['subnum'] = $__foreach_submenuitem_6_saved_key;
}
?>
                          </ul>
                          <?php }?>
                        </li>
                        <?php
$_smarty_tpl->tpl_vars['menuitem'] = $__foreach_menuitem_5_saved_local_item;
}
if ($__foreach_menuitem_5_saved_item) {
$_smarty_tpl->tpl_vars['menuitem'] = $__foreach_menuitem_5_saved_item;
}
if ($__foreach_menuitem_5_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_menuitem_5_saved_key;
}
?>
                      </ul>
                      <?php }?>
                    </div>
                  </div>
                  <div class="content main-content onload">
                    <div class="row">
                      <div class="col-md-12">
                        <?php }?>
                        <?php }?>
                        <div id="cmsbased_alerts_holder"></div>
<?php }
}
