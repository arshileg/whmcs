<?php
/* Smarty version 3.1.29, created on 2018-03-30 12:28:33
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/ticketfeedback.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5abdf53118b596_66014907',
  'file_dependency' => 
  array (
    'd4be2d2e2ec6cd2496f2098aa03c9f92a5fea5e0' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/ticketfeedback.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5abdf53118b596_66014907 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['error']->value) {?>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketinvalid'];?>
</p>
<?php } elseif ($_smarty_tpl->tpl_vars['stillopen']->value) {?>
<h3 class="page-header"><span aria-hidden="true" class="icon icon-speech"></span> <?php echo (($_smarty_tpl->tpl_vars['LANG']->value['ticketfeedbacktitle']).(' #')).($_smarty_tpl->tpl_vars['tid']->value);?>
 <i class="fa fa-info-circle animated bounce show-info"></i></h3>
           <blockquote class="page-information hidden">
                <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackdesc'];?>
</p>
            </blockquote>
<div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackclosed'];?>
</div>
<p><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['returnclient'];?>
" onclick="window.location='clientarea.php'" class="btn btn-default" /></p>
<?php } elseif ($_smarty_tpl->tpl_vars['feedbackdone']->value) {?>
<h3 class="page-header"><span aria-hidden="true" class="icon icon-speech"></span> <?php echo (($_smarty_tpl->tpl_vars['LANG']->value['ticketfeedbacktitle']).(' #')).($_smarty_tpl->tpl_vars['tid']->value);?>
 <i class="fa fa-info-circle animated bounce show-info"></i></h3>
           <blockquote class="page-information hidden">
                <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackdesc'];?>
</p>
            </blockquote>
<div class="alert alert-success textcenter">
<p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackprovided'];?>
</strong></p>
</div>
<p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackthankyou'];?>
</p>
<input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['returnclient'];?>
" onclick="window.location='clientarea.php'" class="btn btn-default" />
<?php } elseif ($_smarty_tpl->tpl_vars['success']->value) {?>
<h3 class="page-header"><span aria-hidden="true" class="icon icon-speech"></span> <?php echo (($_smarty_tpl->tpl_vars['LANG']->value['ticketfeedbacktitle']).(' #')).($_smarty_tpl->tpl_vars['tid']->value);?>
 <i class="fa fa-info-circle animated bounce show-info"></i></h3>
           <blockquote class="page-information hidden">
                <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackdesc'];?>
</p>
            </blockquote>
<div class="alert alert-success">
    <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackreceived'];?>
</strong></p>
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackthankyou'];?>
</p>
</div>
<p class="textcenter"><input type="button" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['returnclient'];?>
" onclick="window.location='clientarea.php'" class="btn btn-default" /></p>
<?php } else { ?>
<h3 class="page-header"><span aria-hidden="true" class="icon icon-speech"></span> <?php echo (($_smarty_tpl->tpl_vars['LANG']->value['ticketfeedbacktitle']).(' #')).($_smarty_tpl->tpl_vars['tid']->value);?>
 <i class="fa fa-info-circle animated bounce show-info"></i></h3>
           <blockquote class="page-information hidden">
                <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackdesc'];?>
</p>
            </blockquote>
<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger">
    <p class="bold"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaerrors'];?>
</p>
    <ul>
        <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

    </ul>
</div>
<?php }?>
<div class="panel panel-default">
  <div class="panel-body">
<dl class="dl-horizontal">
  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackopenedat'];?>
</dt>
  <dd><?php echo $_smarty_tpl->tpl_vars['opened']->value;?>
</dd>
  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbacklastreplied'];?>
</dt>
  <dd><?php echo $_smarty_tpl->tpl_vars['lastreply']->value;?>
</dd>
  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackstaffinvolved'];?>
</dt>
  <dd><?php if ($_smarty_tpl->tpl_vars['staffinvolvedtext']->value) {
echo $_smarty_tpl->tpl_vars['staffinvolvedtext']->value;
} else {
echo $_smarty_tpl->tpl_vars['LANG']->value['none'];
}?></dd>
  <dt><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbacktotalduration'];?>
</dt>
  <dd><?php echo $_smarty_tpl->tpl_vars['duration']->value;?>
</dd>
</dl>
<a class="btn btn-default btn-sm" href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
&c=<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackclickreview'];?>
</a>
</div>
</div>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?tid=<?php echo $_smarty_tpl->tpl_vars['tid']->value;?>
&c=<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
&feedback=1">
    <input type="hidden" name="validate" value="true" />
    <?php
$_from = $_smarty_tpl->tpl_vars['staffinvolved']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_staff_0_saved_item = isset($_smarty_tpl->tpl_vars['staff']) ? $_smarty_tpl->tpl_vars['staff'] : false;
$__foreach_staff_0_saved_key = isset($_smarty_tpl->tpl_vars['staffid']) ? $_smarty_tpl->tpl_vars['staffid'] : false;
$_smarty_tpl->tpl_vars['staff'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['staffid'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['staff']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['staffid']->value => $_smarty_tpl->tpl_vars['staff']->value) {
$_smarty_tpl->tpl_vars['staff']->_loop = true;
$__foreach_staff_0_saved_local_item = $_smarty_tpl->tpl_vars['staff'];
?>
        <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackpleaserate1'];?>
 <strong><?php echo $_smarty_tpl->tpl_vars['staff']->value;?>
</strong> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackhandled'];?>
:</p>
        <div class="well well-sm">
        <div class="row">        
            <div class="col-sm-1"><h4><span aria-hidden="true" class="icon icon-dislike"></span></h4></div>
            <?php
$_from = $_smarty_tpl->tpl_vars['ratings']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rating_1_saved_item = isset($_smarty_tpl->tpl_vars['rating']) ? $_smarty_tpl->tpl_vars['rating'] : false;
$_smarty_tpl->tpl_vars['rating'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['rating']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['rating']->value) {
$_smarty_tpl->tpl_vars['rating']->_loop = true;
$__foreach_rating_1_saved_local_item = $_smarty_tpl->tpl_vars['rating'];
?>
            <div class="col-sm-1"><div class="radio"><label><input type="radio" name="rate[<?php echo $_smarty_tpl->tpl_vars['staffid']->value;?>
]" value="<?php echo $_smarty_tpl->tpl_vars['rating']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['rate']->value[$_smarty_tpl->tpl_vars['staffid']->value] == $_smarty_tpl->tpl_vars['rating']->value) {?> checked<?php }?> /><?php echo $_smarty_tpl->tpl_vars['rating']->value;?>
</label></div></div>
            <?php
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_1_saved_local_item;
}
if ($__foreach_rating_1_saved_item) {
$_smarty_tpl->tpl_vars['rating'] = $__foreach_rating_1_saved_item;
}
?>
            <div class="col-sm-1"><h4><span aria-hidden="true" class="icon icon-like"></span></h4></div>
                </div>
                </div>
        <div class="form-group">
        <label for="feedbackcomment[<?php echo $_smarty_tpl->tpl_vars['staffid']->value;?>
]"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackpleasecomment1'];?>
 <strong><?php echo $_smarty_tpl->tpl_vars['staff']->value;?>
</strong> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackhandled'];?>
</label>
        <textarea class="form-control" id="feedbackcomment[<?php echo $_smarty_tpl->tpl_vars['staffid']->value;?>
]" name="comments[<?php echo $_smarty_tpl->tpl_vars['staffid']->value;?>
]" rows="3"><?php echo $_smarty_tpl->tpl_vars['comments']->value[$_smarty_tpl->tpl_vars['staffid']->value];?>
</textarea>
        </div>
    <?php
$_smarty_tpl->tpl_vars['staff'] = $__foreach_staff_0_saved_local_item;
}
if ($__foreach_staff_0_saved_item) {
$_smarty_tpl->tpl_vars['staff'] = $__foreach_staff_0_saved_item;
}
if ($__foreach_staff_0_saved_key) {
$_smarty_tpl->tpl_vars['staffid'] = $__foreach_staff_0_saved_key;
}
?>
    <hr>
    <div class="form-group">
    <label for="feedbackimprove"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['feedbackimprove'];?>
</label>
    <textarea class="form-control" id="feedbackimprove" name="comments[generic]" rows="3"><?php echo $_smarty_tpl->tpl_vars['comments']->value['generic'];?>
</textarea>
    </div>
    <div class="form-group pull-right flip">
        <input class="btn btn-link" type="reset" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cancel'];?>
">
        <input class="btn btn-primary" type="submit" name="save" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareasavechanges'];?>
">        
    </div>
</form>
<?php }
}
}
