<?php
/* Smarty version 3.1.29, created on 2018-02-18 00:44:09
  from "/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/configuredomains.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a889419658f17_68789561',
  'file_dependency' => 
  array (
    '9756749cc1fb83b4543b4a342022dd099b823ab4' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/orderforms/flowcart7/configuredomains.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/common.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/custom-styles.tpl' => 1,
    'file:orderforms/".((string)$_smarty_tpl->tpl_vars[\'carttpl\']->value)."/cart-header.tpl' => 1,
  ),
),false)) {
function content_5a889419658f17_68789561 ($_smarty_tpl) {
if (!is_callable('smarty_function_math')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/function.math.php';
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/common.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/custom-styles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:orderforms/".((string)$_smarty_tpl->tpl_vars['carttpl']->value)."/cart-header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['step2'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['step2desc'],'step'=>2), 0, true);
?>

<?php echo '<script'; ?>
>
var _localLang = {
  'addToCart': '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['LANG']->value['orderForm']['addToCart'], ENT_QUOTES, 'UTF-8', true);?>
',
  'addedToCartRemove': '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['LANG']->value['orderForm']['addedToCartRemove'], ENT_QUOTES, 'UTF-8', true);?>
'
}
<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['errormessage']->value) {?>
<div class="alert alert-danger" role="alert">
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['correctErrors'];?>
:</p>
  <ul>
    <?php echo $_smarty_tpl->tpl_vars['errormessage']->value;?>

  </ul>
</div>
<?php }?>
<div class="row flowcart-row">
  <div class="col-md-12 ">
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>
?a=confdomains" id="frmConfigureDomains">
      <input type="hidden" name="update" value="true" />
      <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderForm']['reviewDomainAndAddons'];?>
</p>
      <?php
$_from = $_smarty_tpl->tpl_vars['domains']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domain_0_saved_item = isset($_smarty_tpl->tpl_vars['domain']) ? $_smarty_tpl->tpl_vars['domain'] : false;
$__foreach_domain_0_saved_key = isset($_smarty_tpl->tpl_vars['num']) ? $_smarty_tpl->tpl_vars['num'] : false;
$_smarty_tpl->tpl_vars['domain'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['num'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domain']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['num']->value => $_smarty_tpl->tpl_vars['domain']->value) {
$_smarty_tpl->tpl_vars['domain']->_loop = true;
$__foreach_domain_0_saved_local_item = $_smarty_tpl->tpl_vars['domain'];
?>
      <hr>
        <h4><?php echo $_smarty_tpl->tpl_vars['domain']->value['domain'];?>
</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderregperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['hosting'];?>

        <?php if ($_smarty_tpl->tpl_vars['domain']->value['hosting']) {?><span class="text-sucess">[<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartdomainshashosting'];?>
]</span><?php } else { ?><a href="cart.php" class="text-danger">[<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartdomainsnohosting'];?>
]</a><?php }?></p>
      <?php if ($_smarty_tpl->tpl_vars['domain']->value['eppenabled']) {?>
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input type="text" name="epp[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]" id="inputEppcode<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['domain']->value['eppvalue'];?>
" class="form-control" placeholder="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaineppcode'];?>
" />
        </div>
<p class="help-block"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaineppcodedesc'];?>
</p>
        </div>
      <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['domain']->value['dnsmanagement'] || $_smarty_tpl->tpl_vars['domain']->value['emailforwarding'] || $_smarty_tpl->tpl_vars['domain']->value['idprotection']) {?>
  <div class="row addon-products">
    <?php if ($_smarty_tpl->tpl_vars['domain']->value['dnsmanagement']) {?>
    <div class="col-sm-<?php echo smarty_function_math(array('equation'=>"12 / numAddons",'numAddons'=>$_smarty_tpl->tpl_vars['domain']->value['addonsCount']),$_smarty_tpl);?>
">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
         <div class="checkbox">
          <label>
            <input type="checkbox" name="dnsmanagement[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]"<?php if ($_smarty_tpl->tpl_vars['domain']->value['dnsmanagementselected']) {?> checked<?php }?> />
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domaindnsmanagement'];?>

          </label>
        </div>
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsdnsmanagementinfo'];?>

        </div>
        <div class="panel-price p-1">
          <h6><?php echo $_smarty_tpl->tpl_vars['domain']->value['dnsmanagementprice'];?>
 / <?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
</h6>
        </div>
      </div>
    </div>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['domain']->value['idprotection']) {?>
    <div class="col-sm-<?php echo smarty_function_math(array('equation'=>"12 / numAddons",'numAddons'=>$_smarty_tpl->tpl_vars['domain']->value['addonsCount']),$_smarty_tpl);?>
">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
          <div class="checkbox">
          <label>
            <input type="checkbox" name="idprotection[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]"<?php if ($_smarty_tpl->tpl_vars['domain']->value['idprotectionselected']) {?> checked<?php }?> /><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainidprotection'];?>

          </label>
        </div>
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsidprotectioninfo'];?>

        </div>
        <div class="panel-price p-1">
          <h6><?php echo $_smarty_tpl->tpl_vars['domain']->value['idprotectionprice'];?>
 / <?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
</h6>
        </div>
      </div>
    </div>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['domain']->value['emailforwarding']) {?>
    <div class="col-sm-<?php echo smarty_function_math(array('equation'=>"12 / numAddons",'numAddons'=>$_smarty_tpl->tpl_vars['domain']->value['addonsCount']),$_smarty_tpl);?>
">
      <div class="panel panel-default panel-addons">
        <div class="panel-body">
          <div class="checkbox">
          <label>
            <input type="checkbox" name="emailforwarding[<?php echo $_smarty_tpl->tpl_vars['num']->value;?>
]"<?php if ($_smarty_tpl->tpl_vars['domain']->value['emailforwardingselected']) {?> checked<?php }?> />
            <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainemailforwarding'];?>

          </label>
        </div>
          <?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainaddonsemailforwardinginfo'];?>

        </div>
        <div class="panel-price p-1">
        <h6><?php echo $_smarty_tpl->tpl_vars['domain']->value['emailforwardingprice'];?>
 / <?php echo $_smarty_tpl->tpl_vars['domain']->value['regperiod'];?>
 <?php echo $_smarty_tpl->tpl_vars['LANG']->value['orderyears'];?>
</h6>
        </div>

      </div>
    </div>
    <?php }?>

  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
  <?php
$_from = $_smarty_tpl->tpl_vars['domain']->value['fields'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_domainfield_1_saved_item = isset($_smarty_tpl->tpl_vars['domainfield']) ? $_smarty_tpl->tpl_vars['domainfield'] : false;
$__foreach_domainfield_1_saved_key = isset($_smarty_tpl->tpl_vars['domainfieldname']) ? $_smarty_tpl->tpl_vars['domainfieldname'] : false;
$_smarty_tpl->tpl_vars['domainfield'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domainfieldname'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['domainfield']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['domainfieldname']->value => $_smarty_tpl->tpl_vars['domainfield']->value) {
$_smarty_tpl->tpl_vars['domainfield']->_loop = true;
$__foreach_domainfield_1_saved_local_item = $_smarty_tpl->tpl_vars['domainfield'];
?>
<div class="form-group"
    <label><?php echo $_smarty_tpl->tpl_vars['domainfieldname']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['domainfield']->value;?>

  </label>
  </div>
  <?php
$_smarty_tpl->tpl_vars['domainfield'] = $__foreach_domainfield_1_saved_local_item;
}
if ($__foreach_domainfield_1_saved_item) {
$_smarty_tpl->tpl_vars['domainfield'] = $__foreach_domainfield_1_saved_item;
}
if ($__foreach_domainfield_1_saved_key) {
$_smarty_tpl->tpl_vars['domainfieldname'] = $__foreach_domainfield_1_saved_key;
}
?>
</div>
</div>
<?php
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_0_saved_local_item;
}
if ($__foreach_domain_0_saved_item) {
$_smarty_tpl->tpl_vars['domain'] = $__foreach_domain_0_saved_item;
}
if ($__foreach_domain_0_saved_key) {
$_smarty_tpl->tpl_vars['num'] = $__foreach_domain_0_saved_key;
}
?>

<?php if ($_smarty_tpl->tpl_vars['atleastonenohosting']->value) {?>
<h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameservers'];?>
</h4>
  <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartnameserversdesc'];?>
</p>

  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameserver1'];?>
</label>
        <input type="text" class="form-control" id="inputNs1" name="domainns1" value="<?php echo $_smarty_tpl->tpl_vars['domainns1']->value;?>
" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs2"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameserver2'];?>
</label>
        <input type="text" class="form-control" id="inputNs2" name="domainns2" value="<?php echo $_smarty_tpl->tpl_vars['domainns2']->value;?>
" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs3"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameserver3'];?>
</label>
        <input type="text" class="form-control" id="inputNs3" name="domainns3" value="<?php echo $_smarty_tpl->tpl_vars['domainns3']->value;?>
" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs1"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameserver4'];?>
</label>
        <input type="text" class="form-control" id="inputNs4" name="domainns4" value="<?php echo $_smarty_tpl->tpl_vars['domainns4']->value;?>
" />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label for="inputNs5"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['domainnameserver5'];?>
</label>
        <input type="text" class="form-control" id="inputNs5" name="domainns5" value="<?php echo $_smarty_tpl->tpl_vars['domainns5']->value;?>
" />
      </div>
    </div>
  </div>
  <?php }?>
  <div class="text-right">
    <button type="submit" class="btn btn-primary btn-lg"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['continue'];?>
</button>
  </div>
</form>
</div>
</div>
<?php }
}
