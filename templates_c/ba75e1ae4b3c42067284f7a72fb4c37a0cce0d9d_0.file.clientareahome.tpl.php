<?php
/* Smarty version 3.1.29, created on 2018-02-18 00:02:27
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareahome.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a888a536de309_26375107',
  'file_dependency' => 
  array (
    'ba75e1ae4b3c42067284f7a72fb4c37a0cce0d9d' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/clientareahome.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a888a536de309_26375107 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_replace')) require_once '/otherhome/hostnodesnet/public_html/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
$template = $_smarty_tpl;
?>    <div class="row">
    	<div class="col-md-12">
    		<h3 class="page-header"><span aria-hidden="true" class="icon icon-speedometer"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['dashboard'];?>
 <i class="fa fa-angle-down show-info" aria-hidden="true"></i>
          <?php if ($_smarty_tpl->tpl_vars['show_inclientarea']->value != 1 && $_smarty_tpl->tpl_vars['showqsl']->value) {?>
          <span class="pull-right flip qsl"><a href="#" data-original-title="Quick Server Logins"><span aria-hidden="true" class="icon icon-settings settings-toggle"></span></a></span>
          <?php }?>
        </h3>
        <blockquote class="page-information hidden">
         <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['dashboardintro'];?>
</p>
       </blockquote>
     </div>
   </div>
   <div class="row px-1 pt-1">
     <div class="col-md-4">
      <a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['navservices'];?>
" href="clientarea.php?action=products">
       <div class="info-box bg-info text-white" id="initial-tour">
        <div class="info-icon bg-info-dark">
         <span aria-hidden="true" class="icon icon-layers"></span>
       </div>
       <div class="info-details">
         <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['navservices'];?>
<span class="pull-right flip"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['productsnumtotal'];?>
</span></h4>
         <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaactive'];?>
<span class="badge pull-right flip bg-white text-success"> <?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['productsnumactive'];?>
</span> </p>
       </div>
     </div>
   </a>
 </div>
<?php if ($_smarty_tpl->tpl_vars['condlinks']->value['domainreg'] || $_smarty_tpl->tpl_vars['condlinks']->value['domaintrans']) {?>
 <div class="col-md-4">
  <a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['cartproductdomain'];?>
" href="clientarea.php?action=domains">
   <div class="info-box bg-info text-white">
    <div class="info-icon bg-info-dark">
     <span aria-hidden="true" class="icon icon-globe"></span>
   </div>
   <div class="info-details">
     <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['navdomains'];?>
<span class="pull-right flip"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numdomains'];?>
</span></h4>
     <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareaactive'];?>
<span class="badge pull-right flip bg-white text-success"> <?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numactivedomains'];?>
 </span> </p>
   </div>
 </div>
</a>
</div>
<?php }?>
<div class="col-md-4">
<a title="<?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numdueinvoices'];?>
" href="clientarea.php?action=invoices">
  <div class="info-box bg-info text-white">
   <div class="info-icon bg-info-dark">
    <span aria-hidden="true" class="icon icon-drawer"></span>
  </div>
  <div class="info-details">
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['navinvoices'];?>
<span class="pull-right flip"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numdueinvoices'];?>
</span></h4>
    <p><span class="badge"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['dueinvoicesbalance'];?>
</span></p>
  </div>
</div>
</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['condlinks']->value['addfunds']) {?>
<div class="col-md-4">
<a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfunds'];?>
" href="clientarea.php?action=addfunds">
  <div class="info-box  bg-warn text-white">
   <div class="info-icon bg-warn-dark">
    <span aria-hidden="true" class="icon icon-wallet"></span>
  </div>
  <div class="info-details">
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['statscreditbalance'];?>
<span class="pull-right flip"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['creditbalance'];?>
</span></h4>
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['addfunds'];?>
</p>
  </div>
</div>
</a>
</div>
<?php }?>
<div class="col-md-4">
 <a title="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
" href="cart.php">
  <div class="info-box  bg-inactive  text-white">
   <div class="info-icon bg-inactive-dark">
    <span aria-hidden="true" class="icon icon-plus"></span>
  </div>
  <div class="info-details">
    <h4><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ordernowbutton'];?>
</h4>
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['statsnumproducts'];?>
</p>
  </div>
</div>
</a>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['announcements']->value) {?>
<div class="panel panel-default panel-news">
  <div class="panel-heading text-uppercase"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ourlatestnews'];?>

    <div class="pull-right flip">
    <a class="prev pull-left"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="next"><span class="glyphicon glyphicon-chevron-right"></span></a></div>
  </div>
  <div class="panel-body">
   <div id="owl-news" class="owl-carousel pb-1">
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i> <a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[0]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[0]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[0]['text']),500,'...');?>
</div>
    <?php if ($_smarty_tpl->tpl_vars['announcements']->value[1]['text']) {?>
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i><a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[1]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[1]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[1]['text']),500,'...');?>
</div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['announcements']->value[2]['text']) {?>
    <div class="item px-1"><i class="fa fa-clock-o pull-left flip" aria-hidden="true"></i><a class="date pull-left flip pr-1" href="announcements.php?id=<?php echo $_smarty_tpl->tpl_vars['announcements']->value[2]['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['announcements']->value[2]['date'];?>
</a> <?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['announcements']->value[2]['text']),500,'...');?>
</div>
    <?php }?>
  </div>
</div>
</div>

<?php echo '<script'; ?>
>
  $(document).ready(function() {
  var owl = $("#owl-news");owl.owlCarousel({
      autoHeight : true, loop:true, items:1
      <?php if ($_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'ar_AR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'fa_IR' || $_smarty_tpl->tpl_vars['LANG']->value['locale'] == 'he_IL') {?> ,rtl:true <?php }?>
    });
$('.next').click(function() {
    owl.trigger('next.owl.carousel');
})
$('.prev').click(function() {
    owl.trigger('prev.owl.carousel', [300]);
})
});
<?php echo '</script'; ?>
>

<?php }
if ($_smarty_tpl->tpl_vars['ccexpiringsoon']->value) {?>
<div class="alert alert-danger">
 <p><strong><?php echo $_smarty_tpl->tpl_vars['LANG']->value['ccexpiringsoon'];?>
:</strong> <?php echo WHMCS\Smarty::sprintf2Modifier($_smarty_tpl->tpl_vars['LANG']->value['ccexpiringsoondesc'],'
  <a href="clientarea.php?action=creditcard" class="btn btn-danger btn-xs pull-right flip">','</a>');?>
</p>
</div>
<?php }
$_from = $_smarty_tpl->tpl_vars['addons_html']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_addon_html_0_saved_item = isset($_smarty_tpl->tpl_vars['addon_html']) ? $_smarty_tpl->tpl_vars['addon_html'] : false;
$_smarty_tpl->tpl_vars['addon_html'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['addon_html']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['addon_html']->value) {
$_smarty_tpl->tpl_vars['addon_html']->_loop = true;
$__foreach_addon_html_0_saved_local_item = $_smarty_tpl->tpl_vars['addon_html'];
?>
<div class="py-1"><?php echo $_smarty_tpl->tpl_vars['addon_html']->value;?>
</div>
<?php
$_smarty_tpl->tpl_vars['addon_html'] = $__foreach_addon_html_0_saved_local_item;
}
if ($__foreach_addon_html_0_saved_item) {
$_smarty_tpl->tpl_vars['addon_html'] = $__foreach_addon_html_0_saved_item;
}
?>

<?php if ($_smarty_tpl->tpl_vars['show_inclientarea']->value == 1 && $_smarty_tpl->tpl_vars['showqsl']->value) {?>
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['qsl_inclientarea_template']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php }?>


<div class="row">
  <div class="col-md-12">
    <div class="p-1">
    <?php if (in_array('tickets',$_smarty_tpl->tpl_vars['contactpermissions']->value)) {?>
    <ul class="nav nav-material nav-material-horizontal">
     <li class="active"><a href="#home1" data-toggle="tab"><span class="badge badge-circle badge-success"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numactivetickets'];?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsopentickets'];?>
</a></li>
     <li class="pull-right flip"><a href="submitticket.php"><span aria-hidden="true" class="icon icon-settings"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['opennewticket'];?>
</a></li>
   </ul>
   <table class="table table-data table-hover px-1">
    <thead>
      <tr>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketssubject'];?>
</th>
        <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsdepartment'];?>
</th>
        <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgency'];?>
</th>
        <th class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketlastupdated'];?>
</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php
$_from = $_smarty_tpl->tpl_vars['tickets']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ticket_1_saved_item = isset($_smarty_tpl->tpl_vars['ticket']) ? $_smarty_tpl->tpl_vars['ticket'] : false;
$_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['ticket']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
$__foreach_ticket_1_saved_local_item = $_smarty_tpl->tpl_vars['ticket'];
?>

      <?php if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "default"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("default", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php if (strstr($_smarty_tpl->tpl_vars['ticket']->value['status'],"779500")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "success"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("success", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php } elseif (strstr($_smarty_tpl->tpl_vars['ticket']->value['status'],"000000")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "primary"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("primary", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php } elseif (strstr($_smarty_tpl->tpl_vars['ticket']->value['status'],"ff6600")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "warning"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("warning", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php } elseif (strstr($_smarty_tpl->tpl_vars['ticket']->value['status'],"224488")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "info"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("info", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php } elseif (strstr($_smarty_tpl->tpl_vars['ticket']->value['status'],"cc0000")) {
if (isset($_smarty_tpl->tpl_vars["label"])) {$_smarty_tpl->tpl_vars["label"] = clone $_smarty_tpl->tpl_vars["label"];
$_smarty_tpl->tpl_vars["label"]->value = "danger"; $_smarty_tpl->tpl_vars["label"]->nocache = null;
} else $_smarty_tpl->tpl_vars["label"] = new Smarty_Variable("danger", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "label", 0);?>
      <?php }?>

      <tr>
        <td>
          <span class="label label-<?php echo $_smarty_tpl->tpl_vars['label']->value;?>
"><?php echo smarty_modifier_replace(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['ticket']->value['status']),' ','');?>
</span> <a href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['tid'];?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['c'];?>
"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['subject'];?>
</a>
          <ul class="cell-inner-list">
            <li class="visible-sm visible-xs"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketlastupdated'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['lastreply'];?>
</li>
            <li class="visible-sm visible-xs"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsdepartment'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['department'];?>
</li>
            <li class="visible-sm visible-xs"><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['supportticketsticketurgency'];?>
: </span><?php echo $_smarty_tpl->tpl_vars['ticket']->value['urgency'];?>
</li>
          </ul>
        </td>
        <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['department'];?>
</td>
        <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['urgency'];?>
</td>
        <td class="hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['ticket']->value['lastreply'];?>
</td>
        <td><a href="viewticket.php?tid=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['tid'];?>
&amp;c=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['c'];?>
"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </td>
      </tr>
      <?php
$_smarty_tpl->tpl_vars['ticket'] = $__foreach_ticket_1_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['ticket']->_loop) {
?>
      <tr>
        <td colspan="5" class="norecords"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['norecordsfound'];?>
</td>
      </tr><?php
}
if ($__foreach_ticket_1_saved_item) {
$_smarty_tpl->tpl_vars['ticket'] = $__foreach_ticket_1_saved_item;
}
?>
    </tbody>
  </table>
  <?php }?>
  <?php if (in_array('invoices',$_smarty_tpl->tpl_vars['contactpermissions']->value)) {?>
  <ul class="nav nav-material nav-material-horizontal">
    <li class="active"><a href="#home2" data-toggle="tab"><span class="badge badge-circle badge-important"><?php echo $_smarty_tpl->tpl_vars['clientsstats']->value['numdueinvoices'];?>
</span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdue'];?>
</a></li>
    <li class="pull-right flip"><a href="clientarea.php?action=masspay&amp;all=true"><span aria-hidden="true" class="icon icon-arrow-right"></span> <?php echo $_smarty_tpl->tpl_vars['LANG']->value['masspayall'];?>
</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="home2">

     <form method="post" action="clientarea.php?action=masspay">
      <table class="table table-data table-hover">
       <thead>
        <tr><?php if ($_smarty_tpl->tpl_vars['masspay']->value) {?>
         <th class="cell-checkbox">
          <input type="checkbox" onclick="toggleCheckboxes('invids')" />
        </th><?php }?>
        <th><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestitle'];?>
</th>
        <th class="text-center hidden-sm hidden-xs" style="white-space: nowrap;"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatecreated'];?>
</th>
        <th class="text-center hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatedue'];?>
</th>
        <th class="text-center hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesstatus'];?>
</th>
        <th class="text-right hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestotal'];?>
</th>
        <th class="cell-view"></th>
      </tr>
    </thead>
    <tbody><?php
$_from = $_smarty_tpl->tpl_vars['invoices']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_invoice_2_saved_item = isset($_smarty_tpl->tpl_vars['invoice']) ? $_smarty_tpl->tpl_vars['invoice'] : false;
$_smarty_tpl->tpl_vars['invoice'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['invoice']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['invoice']->value) {
$_smarty_tpl->tpl_vars['invoice']->_loop = true;
$__foreach_invoice_2_saved_local_item = $_smarty_tpl->tpl_vars['invoice'];
?>
      <tr><?php if ($_smarty_tpl->tpl_vars['masspay']->value) {?>
       <td class="cell-checkbox">
        <input type="checkbox" name="invoiceids[]" value="<?php echo $_smarty_tpl->tpl_vars['invoice']->value['id'];?>
" class="invids" />
      </td><?php }?>
      <td><a href="viewinvoice.php?id=<?php echo $_smarty_tpl->tpl_vars['invoice']->value['id'];?>
" target="_blank" class="item-title"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['invoicenum'];?>
</a>
        <ul class="cell-inner-list visible-sm visible-xs">
         <li><span class="label label-<?php echo $_smarty_tpl->tpl_vars['invoice']->value['rawstatus'];?>
 label-danger"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['statustext'];?>
</span></li>
         <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicestotal'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['total'];?>
</li>
         <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatecreated'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datecreated'];?>
</li>
         <li><span class="item-title"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['invoicesdatedue'];?>
 : </span><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datedue'];?>
</li>
       </ul>
     </td>
     <td class="text-center hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datecreated'];?>
</td>
     <td class="text-center hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['datedue'];?>
</td>
     <td class="text-center hidden-sm hidden-xs"><span class="label label-<?php echo $_smarty_tpl->tpl_vars['invoice']->value['rawstatus'];?>
 label-danger"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['statustext'];?>
</span>
     </td>
     <td class="text-right hidden-sm hidden-xs"><?php echo $_smarty_tpl->tpl_vars['invoice']->value['total'];?>
</td>
     <td class="cell-view"><a href="viewinvoice.php?id=<?php echo $_smarty_tpl->tpl_vars['invoice']->value['id'];?>
" target="_blank"><span class="glyphicon glyphicon-chevron-right pull-right flip"></span></a>
     </td>
   </tr><?php
$_smarty_tpl->tpl_vars['invoice'] = $__foreach_invoice_2_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['invoice']->_loop) {
?>
   <tr>
     <td colspan="<?php if ($_smarty_tpl->tpl_vars['masspay']->value) {?>7<?php } else { ?>6<?php }?>" class="norecords"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['norecordsfound'];?>
</td>
   </tr><?php
}
if ($__foreach_invoice_2_saved_item) {
$_smarty_tpl->tpl_vars['invoice'] = $__foreach_invoice_2_saved_item;
}
?></tbody><?php if ($_smarty_tpl->tpl_vars['masspay']->value) {?>
   <tfoot>
     <tr>
      <td class="cell-checkbox"><input type="checkbox" onclick="toggleCheckboxes('invids')" class="invids" /></td>
      <td colspan="5" class=""><input type="submit" name="masspayselected" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['masspayselected'];?>
" class="btn btn-default btn-xs" /></td><td class="hidden-sm"></td>
    </tr>
  </tfoot><?php }?>
</table>
</form>
</div>
</div>
<?php }?>
</div>
</div>
</div>
<?php if ($_smarty_tpl->tpl_vars['files']->value) {?>
<h3><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareafiles'];?>
</h3>
<div class="row">
  <div class="form-group"><?php
$_from = $_smarty_tpl->tpl_vars['files']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_file_3_saved_item = isset($_smarty_tpl->tpl_vars['file']) ? $_smarty_tpl->tpl_vars['file'] : false;
$_smarty_tpl->tpl_vars['file'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['file']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->_loop = true;
$__foreach_file_3_saved_local_item = $_smarty_tpl->tpl_vars['file'];
?>
   <div class="col-lg-6"><div class="well well-sm">
    <a href="dl.php?type=f&amp;id=<?php echo $_smarty_tpl->tpl_vars['file']->value['id'];?>
"><h4><span class="glyphicon glyphicon-floppy-disk"></span> <?php echo $_smarty_tpl->tpl_vars['file']->value['title'];?>
</h4></a>
    <p><?php echo $_smarty_tpl->tpl_vars['LANG']->value['clientareafilesdate'];?>
: <?php echo $_smarty_tpl->tpl_vars['file']->value['date'];?>
</p></div></div><?php
$_smarty_tpl->tpl_vars['file'] = $__foreach_file_3_saved_local_item;
}
if ($__foreach_file_3_saved_item) {
$_smarty_tpl->tpl_vars['file'] = $__foreach_file_3_saved_item;
}
?></div>
  </div>
  <?php }?>
  <div class="right-sidebar right-sidebar-hidden">
   <div class="right-sidebar-holder">
     <h4 class="page-header"><?php echo $_smarty_tpl->tpl_vars['LANG']->value['quickserverlogins'];?>
<a href="javascript:;"  class="theme-panel-close text-primary pull-right flip"><span aria-hidden="true" class="icon icon-close"></span></a></h4>
      <?php if ($_smarty_tpl->tpl_vars['show_inclientarea']->value != 1 && $_smarty_tpl->tpl_vars['showqsl']->value) {?>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['qsl_modalarea_template']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

      <?php }?>
    </div>
  </div>
<?php }
}
