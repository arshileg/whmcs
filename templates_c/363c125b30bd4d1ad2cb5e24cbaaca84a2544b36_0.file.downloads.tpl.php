<?php
/* Smarty version 3.1.29, created on 2018-08-03 03:58:57
  from "/home/hostnodesnet/public_html/templates/hostnodesv1/downloads.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5b63d301275bf6_85907650',
  'file_dependency' => 
  array (
    '363c125b30bd4d1ad2cb5e24cbaaca84a2544b36' => 
    array (
      0 => '/home/hostnodesnet/public_html/templates/hostnodesv1/downloads.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b63d301275bf6_85907650 ($_smarty_tpl) {
$template = $_smarty_tpl;
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/pageheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['downloadstitle'],'desc'=>$_smarty_tpl->tpl_vars['LANG']->value['downloadsintrotext'],'icon'=>'doc'), 0, true);
?>

<div class="row px-1 py-2">
  <form method="post" action="<?php echo routePath('download-search');?>
">
    <div class="col-md-12">
      <div class="form-group">
        <div class="input-group">
          <input type="text" name="search" value="<?php echo $_smarty_tpl->tpl_vars['LANG']->value['downloadssearch'];?>
" class="form-control" onfocus="if(this.value=='<?php echo $_smarty_tpl->tpl_vars['LANG']->value['downloadssearch'];?>
')this.value=''" />
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </div>
    </div>
  </form>
</div>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['downloadscategories']), 0, true);
?>

<div class="row px-1">
  <?php
$_from = $_smarty_tpl->tpl_vars['dlcats']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_dlcat_0_saved_item = isset($_smarty_tpl->tpl_vars['dlcat']) ? $_smarty_tpl->tpl_vars['dlcat'] : false;
$_smarty_tpl->tpl_vars['dlcat'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['dlcat']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['dlcat']->value) {
$_smarty_tpl->tpl_vars['dlcat']->_loop = true;
$__foreach_dlcat_0_saved_local_item = $_smarty_tpl->tpl_vars['dlcat'];
?>
  <div class="col-md-4">
   <h5><span class="glyphicon glyphicon-folder-close"></span> <a href="<?php echo routePath('download-by-cat',$_smarty_tpl->tpl_vars['dlcat']->value['id'],$_smarty_tpl->tpl_vars['dlcat']->value['urlfriendlyname']);?>
"><?php echo $_smarty_tpl->tpl_vars['dlcat']->value['name'];?>
</a>
     <span class="badge"><?php echo $_smarty_tpl->tpl_vars['dlcat']->value['numarticles'];?>
</span></h5>
     <p><?php echo $_smarty_tpl->tpl_vars['dlcat']->value['description'];?>
</p>
   </div>
   <?php
$_smarty_tpl->tpl_vars['dlcat'] = $__foreach_dlcat_0_saved_local_item;
}
if ($__foreach_dlcat_0_saved_item) {
$_smarty_tpl->tpl_vars['dlcat'] = $__foreach_dlcat_0_saved_item;
}
?>
 </div>
 <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['template']->value)."/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>$_smarty_tpl->tpl_vars['LANG']->value['downloadspopular']), 0, true);
?>

 <div class="row px-1">
  <div class="col-md-12">
    <div class="list-group">
      <?php
$_from = $_smarty_tpl->tpl_vars['mostdownloads']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_download_1_saved_item = isset($_smarty_tpl->tpl_vars['download']) ? $_smarty_tpl->tpl_vars['download'] : false;
$_smarty_tpl->tpl_vars['download'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['download']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['download']->value) {
$_smarty_tpl->tpl_vars['download']->_loop = true;
$__foreach_download_1_saved_local_item = $_smarty_tpl->tpl_vars['download'];
?>
      <a href="<?php echo $_smarty_tpl->tpl_vars['download']->value['link'];?>
" class="list-group-item"><h5><?php echo $_smarty_tpl->tpl_vars['download']->value['type'];?>
 <?php echo $_smarty_tpl->tpl_vars['download']->value['title'];
if ($_smarty_tpl->tpl_vars['download']->value['clientsonly']) {?> <span class="glyphicon glyphicon-lock"></span> <?php }?> <span class="label label-default"><?php echo $_smarty_tpl->tpl_vars['download']->value['filesize'];?>
</span></h5>
      <p class="list-group-item-text"><?php echo $_smarty_tpl->tpl_vars['download']->value['description'];?>
</p>
      </a>
      <?php
$_smarty_tpl->tpl_vars['download'] = $__foreach_download_1_saved_local_item;
}
if ($__foreach_download_1_saved_item) {
$_smarty_tpl->tpl_vars['download'] = $__foreach_download_1_saved_item;
}
?>
    </div>
  </div>
</div>
<?php }
}
