<?php
/* Smarty version 3.1.29, created on 2018-02-18 00:02:23
  from "/otherhome/hostnodesnet/public_html/templates/hostnodesv1/languageblock.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a888a4fd499c2_11367250',
  'file_dependency' => 
  array (
    '1f01523697691c7ed0db56f7a1cadc860d1fe914' => 
    array (
      0 => '/otherhome/hostnodesnet/public_html/templates/hostnodesv1/languageblock.tpl',
      1 => 1510854922,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a888a4fd499c2_11367250 ($_smarty_tpl) {
$template = $_smarty_tpl;
if ($_smarty_tpl->tpl_vars['languagechangeenabled']->value && count($_smarty_tpl->tpl_vars['locales']->value) > 1) {?>
<div class="visible-xs-block">
  <div class="col-md-4 col-md-offset-4">
    <div class="btn-group">
      <button type="button" class="btn btn-primary btn-sm btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo $_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'];?>
  <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
        <?php
$_from = $_smarty_tpl->tpl_vars['locales']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_locale_0_saved_item = isset($_smarty_tpl->tpl_vars['locale']) ? $_smarty_tpl->tpl_vars['locale'] : false;
$_smarty_tpl->tpl_vars['locale'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['locale']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['locale']->value) {
$_smarty_tpl->tpl_vars['locale']->_loop = true;
$__foreach_locale_0_saved_local_item = $_smarty_tpl->tpl_vars['locale'];
?>
        <?php if ($_smarty_tpl->tpl_vars['LANG']->value['chooselanguage'] == $_smarty_tpl->tpl_vars['locale']->value['localisedName']) {?>
        <li><a class="active" href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
        <?php } else { ?>
        <li><a href="<?php echo $_smarty_tpl->tpl_vars['currentpagelinkback']->value;?>
language=<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
"  data-lang="<?php echo $_smarty_tpl->tpl_vars['locale']->value['language'];?>
" ><?php echo $_smarty_tpl->tpl_vars['locale']->value['localisedName'];?>
</a></li>
        <?php }?>
        <?php
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_local_item;
}
if ($__foreach_locale_0_saved_item) {
$_smarty_tpl->tpl_vars['locale'] = $__foreach_locale_0_saved_item;
}
?>
      </ul>
    </div>
  </div>
</div>
<?php }
}
}
